/******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized.
* This software is owned by Renesas Electronics Corporation and is  protected
* under all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES
* REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY,
* INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR  A
* PARTICULAR PURPOSE AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE  EXPRESSLY
* DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE  LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES
* FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS
* AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this
* software and to discontinue the availability of this software.
* By using this software, you agree to the additional terms and
* conditions found by accessing the following link:
* http://www.renesas.com/disclaimer
*******************************************************************************
* Copyright (C) 2010(2011) Renesas Electronics Corpration
* and Renesas Solutions Corp. All rights reserved.
*******************************************************************************
* File Name    : r_usb_ATAPIdriver.c
* Version      : 1.00
* Device(s)    : Renesas SH-Series, RX-Series
* Tool-Chain   : Renesas SuperH RISC engine Standard Toolchain
*              : Renesas RX Standard Toolchain
* OS           : Common to None and uITRON 4.0 Spec
* H/W Platform : Independent
* Description  : USB Host and Peripheral Interrupt code
*******************************************************************************
* History : DD.MM.YYYY Version Description
*         : 17.03.2010 0.80    First Release
*         : 30.07.2010 0.90    Updated comments
*         : 02.08.2010 0.91    Updated comments
*         : 29.10.2010 1.00    Mass Production Release
*         : 17.06.2011 1.10    Version 1.10 Release
******************************************************************************/


/******************************************************************************
Includes   <System Includes> , "Project Includes"
******************************************************************************/
#include "r_usbc_cDefUsr.h"			/* System definition */
#include "r_usbc_cTypedef.h"		/* Type define */
#include "r_usbc_cKernelId.h"		/* Kernel ID definition */
#include "r_usb_cMSCdefine.h"		/* USB	Mass Storage Class Header */
#include "r_usb_pMSCdefine.h"		/* Peri Mass Storage Class Driver define */
#include "r_usb_cATAPIdefine.h"		/* Peripheral ATAPI Device extern define */
#include "r_usbc_cDefUSBIP.h"		/* Transfer Status from PCD */
#include "r_usbc_cMacSystemcall.h"	/* uITRON system call macro */
#include "r_usbc_cMacPrint.h"		/* Standard IO macro */
#include "r_usb_cExtern.h"			/* USB-FW grobal define */

#include "RX63nSAKURA_Define.h"

/******************************************************************************
Section    <Section Difinition> , "Project Sections"
******************************************************************************/
#pragma section _sdram


/******************************************************************************
Constant values
******************************************************************************/

// Inquiry data
#define	STR_PRODUCT_VERSION				"1.00"				// 4文字
#define	STR_VENDOR_ID					"Renesas "			// 8文字
#define	STR_PRODUCT_ID					"Mass Storage    "	// 16文字
										//1234567890123456

// Directory entry data for Volume label
#define	INIT_ROOT_DIR_ENTRY_VOLUME_LABEL											\
	'G' ,'R' ,'-' ,'S' ,'A' ,'K' ,'U' ,'R' ,'A' ,0x20,0x20,0x08,0x00,0x00,0x00,0x00,\
	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,\

// Directory entry data for "Gadget Renesas Project Home.html"
#define	INIT_ROOT_DIR_ENTRY_HTML_FILE												\
	0x43,0x65,0x00,0x2E,0x00,0x68,0x00,0x74,0x00,0x6D,0x00,0x0F,0x00,0x43,0x6C,0x00,\
	0x00,0x00,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0x00,0x00,0xFF,0xFF,0xFF,0xFF,\
	0x02,0x73,0x00,0x20,0x00,0x50,0x00,0x72,0x00,0x6F,0x00,0x0F,0x00,0x43,0x6A,0x00,\
	0x65,0x00,0x63,0x00,0x74,0x00,0x20,0x00,0x48,0x00,0x00,0x00,0x6F,0x00,0x6D,0x00,\
	0x01,0x47,0x00,0x61,0x00,0x64,0x00,0x67,0x00,0x65,0x00,0x0F,0x00,0x43,0x74,0x00,\
	0x20,0x00,0x52,0x00,0x65,0x00,0x6E,0x00,0x65,0x00,0x00,0x00,0x73,0x00,0x61,0x00,\
	0x47,0x41,0x44,0x47,0x45,0x54,0x7E,0x31,0x48,0x54,0x4D,0x21,0x00,0x2D,0x8D,0x6C,\
	0xAB,0x40,0xAB,0x40,0x00,0x00,0xC0,0x68,0xAB,0x40,0x02,0x00,0x6A,0x00,0x00,0x00,\

// Directory entry data
#define	INIT_ROOT_DIR_ENTRY				INIT_ROOT_DIR_ENTRY_VOLUME_LABEL			\
										INIT_ROOT_DIR_ENTRY_HTML_FILE				\

// HTML file contents
#define	HTML_FILE_DATA																\
	"<a href=\"http://tool-support.renesas.com/jpn/toolnews/gr/index.html\">"	"\r\n"\
	"Gadget Renesas Project Home"												"\r\n"\
	"</a>"																		"\r\n"

#define	HTML_FILE_DATA_CLUSTER_COUNT	1	// round_up(sizeof(HTML_FILE_DATA),FATFS_CLUSTER_SIZE)
#define	HTML_FILE_DATA_SECTOR_COUNT		(HTML_FILE_DATA_CLUSTER_COUNT*FATFS_SECTOR_PER_CLUSTER)


#if 	ENABLE_FAT16

#define	PARTITION_ID					0x04					// DOS(FAT16<=32MB)
#define	INIT_FAT_DATA_RESERVED			0xF8,0xFF,0xFF,0xFF		// for FAT16

 #if 	(HTML_FILE_DATA_CLUSTER_COUNT==1)
#define	INIT_FAT_DATA					INIT_FAT_DATA_RESERVED,				\
										0xFF,0xFF			// used 1 cluster
 #elif	(HTML_FILE_DATA_CLUSTER_COUNT==2)
#define	INIT_FAT_DATA					INIT_FAT_DATA_RESERVED,				\
										0x03,0x00,0xFF,0xFF	// used 2 clusters
 #else
	#error Update INIT_FAT_DATA by HTML_FILE_DATA_CLUSTER_COUNT.
 #endif
 
#else	//FAT12

#define	PARTITION_ID					0x01					// DOS(FAT12)
#define	INIT_FAT_DATA_RESERVED			0xF8,0xFF,0xFF			// for FAT12

 #if 	(HTML_FILE_DATA_CLUSTER_COUNT==1)
#define	INIT_FAT_DATA					INIT_FAT_DATA_RESERVED,				\
										0xFF,0x0F,0x00		// used 1 cluster
 #elif	(HTML_FILE_DATA_CLUSTER_COUNT==2)
#define	INIT_FAT_DATA					INIT_FAT_DATA_RESERVED,				\
										0x03,0xF0,0xFF		// used 2 clusters
 #else
	#error Update INIT_FAT_DATA by HTML_FILE_DATA_CLUSTER_COUNT.
 #endif
 
#endif	//ENABLE_FAT16


#define	LBA_BASE_VOLUME					(MBR_SECTOR_COUNT)
#define	LBA_BASE_FAT1					(LBA_BASE_VOLUME+FATFS_RESERVED_SECTOR_COUNT)
#define	LBA_BASE_FAT2					(LBA_BASE_FAT1+FATFS_FAT_SECTOR_COUNT)
#define	LBA_BASE_ROOT_DIR				(LBA_BASE_FAT2+FATFS_FAT_SECTOR_COUNT)
#define	LBA_BASE_CLUSTERS				(LBA_BASE_ROOT_DIR+FATFS_ROOT_DIR_SECTOR_COUNT)
#define	LBA_BASE_HTML_FILE_CLUSTER		(LBA_BASE_CLUSTERS)
#define	LBA_BASE_STORAGE_CLUSTERS		(LBA_BASE_HTML_FILE_CLUSTER + HTML_FILE_DATA_SECTOR_COUNT)

#define	USBC_ATAPI_TOTAL_SECTOR_COUNT	(LBA_BASE_STORAGE_CLUSTERS +	\
										 USBC_ATAPI_STORAGE_SECTOR_COUNT)

#define	MBR_PARTITION_ID				PARTITION_ID
#define	MBR_END_HEAD_NO					63
#define	MBR_END_SECTOR_NO				32
#define	MBR_END_CYLINDER_NO				((USBC_ATAPI_TOTAL_SECTOR_COUNT/((MBR_END_HEAD_NO+1)*MBR_END_SECTOR_NO))-1)

#define	FATFS_VOLUME_TOTAL_SECTOR_COUNT	(USBC_ATAPI_TOTAL_SECTOR_COUNT - \
										 MBR_SECTOR_COUNT)

#define	FATFS_SECTOR_SIZE_LO			((USBC_ATAPI_BLOCK_UNIT>>0)&0xFF)
#define	FATFS_SECTOR_SIZE_HI			((USBC_ATAPI_BLOCK_UNIT>>8)&0xFF)

#define	FATFS_HIDDEN_SECTOR_COUNT_LO	((MBR_SECTOR_COUNT>>0)&0xFF)
#define	FATFS_HIDDEN_SECTOR_COUNT_HI	((MBR_SECTOR_COUNT>>8)&0xFF)

#define	FATFS_RESERVED_SECTOR_COUNT_LO	((FATFS_RESERVED_SECTOR_COUNT>>0)&0xFF)
#define	FATFS_RESERVED_SECTOR_COUNT_HI	((FATFS_RESERVED_SECTOR_COUNT>>8)&0xFF)

#define	FATFS_ROOT_DIR_COUNT			(FATFS_ROOT_DIR_SECTOR_COUNT*USBC_ATAPI_BLOCK_UNIT/32)
#define	FATFS_ROOT_DIR_COUNT_LO			((FATFS_ROOT_DIR_COUNT>>0)&0xFF)
#define	FATFS_ROOT_DIR_COUNT_HI			((FATFS_ROOT_DIR_COUNT>>8)&0xFF)

#if	(USBC_ATAPI_TOTAL_SECTOR_COUNT < 0x10000ul)
#define	FATFS_TOTAL_SECTOR_COUNT_LO		((FATFS_VOLUME_TOTAL_SECTOR_COUNT>>0)&0xFF)
#define	FATFS_TOTAL_SECTOR_COUNT_HI		((FATFS_VOLUME_TOTAL_SECTOR_COUNT>>8)&0xFF)
#define	FATFS_TOTAL_SECTOR_COUNT_EX_0	0
#define	FATFS_TOTAL_SECTOR_COUNT_EX_1	0
#define	FATFS_TOTAL_SECTOR_COUNT_EX_2	0
#define	FATFS_TOTAL_SECTOR_COUNT_EX_3	0
#else
#define	FATFS_TOTAL_SECTOR_COUNT_LO		0
#define	FATFS_TOTAL_SECTOR_COUNT_HI		0
#define	FATFS_TOTAL_SECTOR_COUNT_EX_0	((FATFS_VOLUME_TOTAL_SECTOR_COUNT>> 0)&0xFF)
#define	FATFS_TOTAL_SECTOR_COUNT_EX_1	((FATFS_VOLUME_TOTAL_SECTOR_COUNT>> 8)&0xFF)
#define	FATFS_TOTAL_SECTOR_COUNT_EX_2	((FATFS_VOLUME_TOTAL_SECTOR_COUNT>>16)&0xFF)
#define	FATFS_TOTAL_SECTOR_COUNT_EX_3	((FATFS_VOLUME_TOTAL_SECTOR_COUNT>>24)&0xFF)
#endif

#define	FATFS_FAT_SECTOR_COUNT_LO		((FATFS_FAT_SECTOR_COUNT>>0)&0xFF)
#define	FATFS_FAT_SECTOR_COUNT_HI		((FATFS_FAT_SECTOR_COUNT>>8)&0xFF)


/******************************************************************************
Local variables and functions
******************************************************************************/
static	uint32_t	usb_gpmsc_Atapi_lbaCurrent;		// Current LBA for READ/WRITE
static	uint32_t	usb_gpmsc_Atapi_nCurXferCount;	// Current Transfer sector count for READ/WRITE
static	uint32_t	usb_gpmsc_Atapi_lbaLast;		// Last LBA for READ/WRITE
static	uint32_t	usb_gpmsc_Atapi_nLastXferCount;	// Last Transfer sector count for READ/WRITE

static uint8_t usb_gpmsc_Atapi_SenseInfoBuffer[18] =
{
	0x70,0x00,0x00,0x00,0x00,0x00,0x00,0x0A,
	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
	0x00,0x00
};

#if 	(ENABLE_KEEP_FILE_INFO==0)
static uint8_t usb_gpmsc_AtapiFatBuffer[FATFS_FAT_SECTOR_COUNT*USBC_ATAPI_BLOCK_UNIT] =
{
	INIT_FAT_DATA
};
static uint8_t usb_gpmsc_AtapiRootDirBuffer[FATFS_ROOT_DIR_SECTOR_COUNT*USBC_ATAPI_BLOCK_UNIT] =
{
	INIT_ROOT_DIR_ENTRY
};
#endif	//(ENABLE_KEEP_FILE_INFO==0)


static	void*		usb_pmsc_SmpAtapi_EmuInquiry( uint32_t *size );
static	void*		usb_pmsc_SmpAtapi_EmuRequestSense( uint32_t *size );
static	void*		usb_pmsc_SmpAtapi_EmuModeSense6( uint32_t *size );
static	void*		usb_pmsc_SmpAtapi_EmuModeSense10( uint32_t *size );
static	void*		usb_pmsc_SmpAtapi_EmuReadCapacity( uint32_t *size ); 

static	void*		usb_pmsc_SmpAtapi_EmuRead( uint32_t *size ); 
static	void*		usb_pmsc_SmpAtapi_EmuWrite( uint32_t *size );
static	BOOL		usb_pmsc_SmpAtapi_PostWrite( void );



/******************************************************************************
External variables and functions
******************************************************************************/
USBC_ER_t	usb_pmsc_SmpAtapiCloseMedia(void);
void		usb_pmsc_SmpAtapiAnalyzeCbwCb(uint8_t *cbwcb);
void		usb_pmsc_SmpAtapiTask(USBC_VP_INT_t);
void		usb_pmsc_SmpAtapiCommandExecute(uint8_t *cbw, uint16_t status
				, USBC_CB_t complete);

USBC_PMSC_CDB_t* usb_gpmsc_AtapiCbwcb;
USBC_UTR_t	usb_gpmsc_AtapiMess;	/* PMSC <--> ATAPI */

uint32_t	usb_gpmsc_Atapi_timeLastWrite = 0;

uint8_t		usb_gpmsc_AtapiDataBuffer[FATFS_CLUSTER_SIZE];


#define	LED_SET_READ()		usbc_cpu_LedSetBit( 1, 0 )	// LED:READ
#define	LED_SET_WRITE()		usbc_cpu_LedSetBit( 2, 0 )	// LED:WRITE
#define	LED_RESET()			usbc_cpu_LedSet( 0xFF )		// LED:RESET

#define	SET_SENSE_KEY_ASC_ASCQ(_key_,_asc_,_ascq_)	\
	usb_gpmsc_Atapi_SenseInfoBuffer[2]  = (_key_);	\
	usb_gpmsc_Atapi_SenseInfoBuffer[12] = (_asc_);	\
	usb_gpmsc_Atapi_SenseInfoBuffer[13] = (_ascq_);	\
	DBG_printf(("(%u) ASC=%02Xh\n",__LINE__,(_asc_)))


extern USBC_PMSC_CBM_t	usb_gpmsc_Massage;
extern uint32_t			usb_gpmsc_Dtl;


/*	Data SetUp Flag */
uint8_t		usb_gpmsc_AtapiSetupFlg;
/* Transfer Size */
uint32_t	usb_gpmsc_AtapiFlashSize;
/* Transfer Address */
void		*usb_gpmsc_AtapiFlashAdr;
/* The amount of real data transferred */
uint32_t	usb_gpmsc_AtapiRealData;


/******************************************************************************
Renesas Abstracted ATAPI Driver API functions
******************************************************************************/

/******************************************************************************
Function Name   : usb_pmsc_SmpAtapiAnalyzeCbwCb
Description     : Analyze ATAPI commands
Arguments       : uint8_t *cbwcb            : CBW
Return value    : none
******************************************************************************/
void usb_pmsc_SmpAtapiAnalyzeCbwCb(uint8_t *cbwcb)
{
	uint32_t	nDataSize;

	usb_gpmsc_AtapiSetupFlg 		= 0u;
	usb_gpmsc_Atapi_lbaLast			= (uint32_t)-1;
	usb_gpmsc_Atapi_nLastXferCount	= (uint32_t)-1;
	
	usb_gpmsc_Massage.ar_rst		= USBC_ATAPI_COMMAND_ANALYZE_ERROR;
	usb_gpmsc_Massage.ul_size		= 0;

	usb_gpmsc_AtapiCbwcb = (USBC_PMSC_CDB_t*)cbwcb;
	
	DBG_printf(( "CDB: %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X\n",
		usb_gpmsc_AtapiCbwcb->uc[0],
		usb_gpmsc_AtapiCbwcb->uc[1],
		usb_gpmsc_AtapiCbwcb->uc[2],
		usb_gpmsc_AtapiCbwcb->uc[3],
		usb_gpmsc_AtapiCbwcb->uc[4],
		usb_gpmsc_AtapiCbwcb->uc[5],
		usb_gpmsc_AtapiCbwcb->uc[6],
		usb_gpmsc_AtapiCbwcb->uc[7],
		usb_gpmsc_AtapiCbwcb->uc[8],
		usb_gpmsc_AtapiCbwcb->uc[9] ));

	switch( usb_gpmsc_AtapiCbwcb->s_usb_ptn0.uc_OpCode )
	{
	case USBC_ATAPI_READ10:
		/* LBAと転送セクタ数を取得 */
		usb_gpmsc_Atapi_lbaCurrent = 
			((uint32_t)usb_gpmsc_AtapiCbwcb->s_usb_ptn4569.ul_LogicalBlock0 << 24) |
			((uint32_t)usb_gpmsc_AtapiCbwcb->s_usb_ptn4569.ul_LogicalBlock1 << 16) |
			((uint32_t)usb_gpmsc_AtapiCbwcb->s_usb_ptn4569.ul_LogicalBlock2 <<  8) |
			((uint32_t)usb_gpmsc_AtapiCbwcb->s_usb_ptn4569.ul_LogicalBlock3 <<  0);
		usb_gpmsc_Atapi_nCurXferCount = 
			((uint32_t)usb_gpmsc_AtapiCbwcb->s_usb_ptn4569.us_Length_Hi << 8) |
			((uint32_t)usb_gpmsc_AtapiCbwcb->s_usb_ptn4569.us_Length_Lo << 0);

		if( (usb_gpmsc_Atapi_lbaCurrent + usb_gpmsc_Atapi_nCurXferCount) > USBC_ATAPI_TOTAL_SECTOR_COUNT )
		{
			usb_gpmsc_Massage.ar_rst = USBC_ATAPI_COMMAND_ANALYZE_ERROR;
			SET_SENSE_KEY_ASC_ASCQ( 0x05, 0x21, 0x00 );	// LOGICAL BLOCK ADDRESS OUT OF RANGE
			break;
		}
		if( usb_gpmsc_Atapi_nCurXferCount == 0 )
		{
			usb_gpmsc_Massage.ar_rst = USBC_ATAPI_NO_DATA;
			break;
		}
	
		/* Block Unit -> bytes */
		usb_gpmsc_Massage.ul_size = usb_gpmsc_Atapi_nCurXferCount * (uint32_t)USBC_ATAPI_BLOCK_UNIT;
		usb_gpmsc_Massage.ar_rst = USBC_ATAPI_SND_DATAS;
		break;
		
	case USBC_ATAPI_WRITE10:
	case USBC_ATAPI_WRITE_AND_VERIFY:
		/* LBAと転送セクタ数を取得 */
		usb_gpmsc_Atapi_lbaCurrent = 
			((uint32_t)usb_gpmsc_AtapiCbwcb->s_usb_ptn4569.ul_LogicalBlock0 << 24) |
			((uint32_t)usb_gpmsc_AtapiCbwcb->s_usb_ptn4569.ul_LogicalBlock1 << 16) |
			((uint32_t)usb_gpmsc_AtapiCbwcb->s_usb_ptn4569.ul_LogicalBlock2 <<  8) |
			((uint32_t)usb_gpmsc_AtapiCbwcb->s_usb_ptn4569.ul_LogicalBlock3 <<  0);
		usb_gpmsc_Atapi_nCurXferCount = 
			((uint32_t)usb_gpmsc_AtapiCbwcb->s_usb_ptn4569.us_Length_Hi << 8) |
			((uint32_t)usb_gpmsc_AtapiCbwcb->s_usb_ptn4569.us_Length_Lo << 0);

		if( (usb_gpmsc_Atapi_lbaCurrent + usb_gpmsc_Atapi_nCurXferCount) > USBC_ATAPI_TOTAL_SECTOR_COUNT )
		{
			usb_gpmsc_Massage.ar_rst = USBC_ATAPI_COMMAND_ANALYZE_ERROR;
			SET_SENSE_KEY_ASC_ASCQ( 0x05, 0x21, 0x00 );	// LOGICAL BLOCK ADDRESS OUT OF RANGE
			break;
		}
		if( usb_gpmsc_Atapi_nCurXferCount == 0 )
		{
			usb_gpmsc_Massage.ar_rst = USBC_ATAPI_NO_DATA;
			break;
		}

		/* Block Unit -> bytes */
		usb_gpmsc_Massage.ul_size = usb_gpmsc_Atapi_nCurXferCount * (uint32_t)USBC_ATAPI_BLOCK_UNIT;
		usb_gpmsc_Massage.ar_rst = USBC_ATAPI_RCV_DATAS;
		break;

	case USBC_ATAPI_TEST_UNIT_READY:
	case USBC_ATAPI_PREVENT_ALLOW:
	case USBC_ATAPI_START_STOP_UNIT:
	case USBC_ATAPI_SEEK:
	case USBC_ATAPI_VERIFY10:
		usb_gpmsc_Massage.ul_size = 0;
		usb_gpmsc_Massage.ar_rst = USBC_ATAPI_NO_DATA;
		break;

	case USBC_ATAPI_INQUIRY:
		usb_gpmsc_Massage.ul_size = usb_gpmsc_AtapiCbwcb->s_usb_ptn12.uc_Allocation;
		if( usb_gpmsc_Massage.ul_size == 0 )
		{
			usb_gpmsc_Massage.ar_rst = USBC_ATAPI_NO_DATA;
		}
		else if( usb_pmsc_SmpAtapi_EmuInquiry( &nDataSize ) == NULL )
		{
			usb_gpmsc_Massage.ar_rst = USBC_ATAPI_COMMAND_ANALYZE_ERROR;
		}
		else
		{
			usb_gpmsc_Massage.ul_size = nDataSize;
			usb_gpmsc_Massage.ar_rst = USBC_ATAPI_SND_DATAS;
		}
		break;
	
	case USBC_ATAPI_REQUEST_SENSE:
		usb_gpmsc_Massage.ul_size = usb_gpmsc_AtapiCbwcb->s_usb_ptn12.uc_Allocation;
		if( usb_gpmsc_Massage.ul_size == 0 )
		{
			usb_gpmsc_Massage.ar_rst = USBC_ATAPI_NO_DATA;
		}
		else
		{
			usb_pmsc_SmpAtapi_EmuRequestSense( &nDataSize );
			usb_gpmsc_Massage.ul_size = nDataSize;
			usb_gpmsc_Massage.ar_rst = USBC_ATAPI_SND_DATAS;
		}
		break;
	
	case USBC_ATAPI_MODE_SENSE6:
		usb_gpmsc_Massage.ul_size = usb_gpmsc_AtapiCbwcb->s_usb_ptn12.uc_Allocation;
		if( usb_gpmsc_Massage.ul_size == 0 )
		{
			usb_gpmsc_Massage.ar_rst = USBC_ATAPI_NO_DATA;
		}
		else if( usb_pmsc_SmpAtapi_EmuModeSense6( &nDataSize ) == NULL )
		{
			usb_gpmsc_Massage.ar_rst = USBC_ATAPI_COMMAND_ANALYZE_ERROR;
		}
		else
		{
			usb_gpmsc_Massage.ul_size = nDataSize;
			usb_gpmsc_Massage.ar_rst = USBC_ATAPI_SND_DATAS;
		}
		break;
	case USBC_ATAPI_MODE_SENSE10:
		usb_gpmsc_Massage.ul_size =
			((uint32_t)usb_gpmsc_AtapiCbwcb->s_usb_ptn4569.us_Length_Hi << 8) |
			((uint32_t)usb_gpmsc_AtapiCbwcb->s_usb_ptn4569.us_Length_Lo << 0);
		if( usb_gpmsc_Massage.ul_size == 0 )
		{
			usb_gpmsc_Massage.ar_rst = USBC_ATAPI_NO_DATA;
		}
		else if( usb_pmsc_SmpAtapi_EmuModeSense10( &nDataSize ) == NULL )
		{
			usb_gpmsc_Massage.ar_rst = USBC_ATAPI_COMMAND_ANALYZE_ERROR;
		}
		else
		{
			usb_gpmsc_Massage.ul_size = nDataSize;
			usb_gpmsc_Massage.ar_rst = USBC_ATAPI_SND_DATAS;
		}
		break;

	case USBC_ATAPI_READ_CAPACITY:
		usb_gpmsc_Massage.ar_rst = USBC_ATAPI_SND_DATAS;
		usb_gpmsc_Massage.ul_size = 8;
		break;

	case USBC_ATAPI_MODE_SELECT10:
		usb_gpmsc_Massage.ul_size =
			((uint32_t)usb_gpmsc_AtapiCbwcb->s_usb_ptn4569.us_Length_Hi << 8) |
			((uint32_t)usb_gpmsc_AtapiCbwcb->s_usb_ptn4569.us_Length_Lo << 0);
		if( usb_gpmsc_Massage.ul_size == 0 )
		{
			usb_gpmsc_Massage.ar_rst = USBC_ATAPI_NO_DATA;
		}
		else
		{
			usb_gpmsc_Massage.ar_rst = USBC_ATAPI_RCV_DATAS;
		}
		break;

	default:
		USBC_PRINTF0("### CBW Command Not Support\n");
		SET_SENSE_KEY_ASC_ASCQ( 0x05, 0x20, 0x00 );	// INVALID COMMAND OPERATION CODE
		break;
	}
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pmsc_SmpAtapiTask
Description     : Execute ATAPI commands
Arguments       : USBC_VP_INT_t stacd        : start code
Return value    : none
******************************************************************************/
void usb_pmsc_SmpAtapiTask(USBC_VP_INT_t stacd)
{
	USBC_UTR_t	*mess;
	
	uint32_t	residue_size;
	uint16_t	status, usb_result;
	USBC_ER_t	err = (USBC_ER_t)0u;

/* Condition compilation by the difference of the operating system */
#if USBC_FW_PP == USBC_FW_NONOS_PP
#else	/* USBC_FW_PP == USBC_FW_NONOS_PP */
	while( 1 )
	{
#endif	/* USBC_FW_PP == USCB_FW_NONOS_PP */

		err = USBC_RCV_MSG(USB_PFLSH_MBX, (USBC_MSG_t**)&mess);
		if( err != USBC_E_OK )
		{
			USBC_PRINTF1("pmsc_AtapiTask rcv_msg error (%ld)\n", err);
			return;
		}

		usb_gpmsc_AtapiCbwcb = (USBC_PMSC_CDB_t*)(mess->tranadr);
		usb_result = mess->status;

		#define	break_with_status(sts)	status = (uint16_t)(sts); break

		if( usb_gpmsc_AtapiSetupFlg	 == 0 )
		{	/* 1st Transfer Sequence */

			usb_gpmsc_AtapiRealData = 0ul;

			switch( usb_gpmsc_AtapiCbwcb->s_usb_ptn0.uc_OpCode )
			{
			case USBC_ATAPI_READ10:
				//
				//	READ(10)
				//
				if( usb_gpmsc_Massage.ul_size == 0ul )
				{
					break_with_status( USBC_PMSC_CMD_COMPLETE );
				}
				
				LED_SET_READ();
				usb_gpmsc_AtapiFlashAdr = usb_pmsc_SmpAtapi_EmuRead( &usb_gpmsc_AtapiFlashSize );
				if( usb_gpmsc_AtapiFlashAdr == NULL )
				{
					// READ ERROR
					break_with_status( USBC_PMSC_CMD_FAILED );
				}
				break_with_status( USBC_PMSC_CMD_CONTINUE );
				
					
			case USBC_ATAPI_WRITE10:
			case USBC_ATAPI_WRITE_AND_VERIFY:
				//
				//	WRITE(10), WRITE AND VERIFY(10)
				//
				if( usb_gpmsc_Massage.ul_size == 0ul )
				{
					break_with_status( USBC_PMSC_CMD_COMPLETE );
				}
				LED_SET_WRITE();
				usb_gpmsc_AtapiFlashAdr = usb_pmsc_SmpAtapi_EmuWrite( &usb_gpmsc_AtapiFlashSize );
				if( usb_gpmsc_AtapiFlashAdr == NULL )
				{
					// WRITE ERROR
					break_with_status( USBC_PMSC_CMD_FAILED );
				}
				break_with_status( USBC_PMSC_CMD_CONTINUE );

				
			case USBC_ATAPI_READ_CAPACITY:
				//
				//	READ CAPACITY(10)
				//
				if( usb_gpmsc_Massage.ul_size == 0ul )
				{
					break_with_status( USBC_PMSC_CMD_FAILED );
				}
				usb_gpmsc_AtapiFlashAdr = usb_pmsc_SmpAtapi_EmuReadCapacity( &usb_gpmsc_AtapiFlashSize );
				if( usb_gpmsc_AtapiFlashAdr == NULL )
				{
					break_with_status( USBC_PMSC_CMD_FAILED );
				}
				break_with_status( USBC_PMSC_CMD_CONTINUE );


			case USBC_ATAPI_REQUEST_SENSE:
				//
				//	REQUEST SENSE
				//
				if( usb_gpmsc_Massage.ul_size == 0ul )
				{
					break_with_status( USBC_PMSC_CMD_FAILED );
				}
				usb_gpmsc_AtapiFlashAdr = usb_pmsc_SmpAtapi_EmuRequestSense( &usb_gpmsc_AtapiFlashSize );
				if( usb_gpmsc_AtapiFlashAdr == NULL )
				{
					break_with_status( USBC_PMSC_CMD_FAILED );
				}
DBG_printf(("      SNS=%02X-%02X-%02X\n", usb_gpmsc_Atapi_SenseInfoBuffer[2],usb_gpmsc_Atapi_SenseInfoBuffer[12],usb_gpmsc_Atapi_SenseInfoBuffer[13] ));
				break_with_status( USBC_PMSC_CMD_CONTINUE );

				
			case USBC_ATAPI_INQUIRY:
				//
				//	INQUIRY
				//
				if( usb_gpmsc_Massage.ul_size == 0ul )
				{
					break_with_status( USBC_PMSC_CMD_COMPLETE );
				}
				usb_gpmsc_AtapiFlashAdr = usb_pmsc_SmpAtapi_EmuInquiry( &usb_gpmsc_AtapiFlashSize );
				if( usb_gpmsc_AtapiFlashAdr == NULL )
				{
					break_with_status( USBC_PMSC_CMD_FAILED );
				}
				break_with_status( USBC_PMSC_CMD_CONTINUE );
				
				
			case USBC_ATAPI_MODE_SENSE6:
				//
				//	MODE SENSE(6)
				//
				if( usb_gpmsc_Massage.ul_size == 0ul )
				{
					break_with_status( USBC_PMSC_CMD_COMPLETE );
				}
				usb_gpmsc_AtapiFlashAdr = usb_pmsc_SmpAtapi_EmuModeSense6( &usb_gpmsc_AtapiFlashSize );
				if( usb_gpmsc_AtapiFlashAdr == NULL )
				{
					break_with_status( USBC_PMSC_CMD_FAILED );
				}
				break_with_status( USBC_PMSC_CMD_CONTINUE );
				
			case USBC_ATAPI_MODE_SENSE10:
				//
				//	MODE SENSE(10)
				//
				if( usb_gpmsc_Massage.ul_size == 0ul )
				{
					break_with_status( USBC_PMSC_CMD_COMPLETE );
				}
				usb_gpmsc_AtapiFlashAdr = usb_pmsc_SmpAtapi_EmuModeSense10( &usb_gpmsc_AtapiFlashSize );
				if( usb_gpmsc_AtapiFlashAdr == NULL )
				{
					break_with_status( USBC_PMSC_CMD_FAILED );
				}
				break_with_status( USBC_PMSC_CMD_CONTINUE );
				
				
			case USBC_ATAPI_TEST_UNIT_READY:
			case USBC_ATAPI_START_STOP_UNIT:
			case USBC_ATAPI_SEEK:
			case USBC_ATAPI_VERIFY10:
				break_with_status( USBC_PMSC_CMD_COMPLETE );


			default:
				SET_SENSE_KEY_ASC_ASCQ( 0x05, 0x20, 0x00 );	// INVALID COMMAND OPERATION CODE
				break_with_status( USBC_PMSC_CMD_FAILED );
			}
		}
		else //if( usb_gpmsc_AtapiSetupFlg == 1 )
		{
			
			#define	CHECK_DATA_TRANSFER_ERROR(_ok_result_)		\
				if( usb_result != (_ok_result_) )				\
				{												\
					/* LOGICAL UNIT COMMUNICATION FAILURE */	\
					SET_SENSE_KEY_ASC_ASCQ( 0x04, 0x08, 0x00 );	\
					break_with_status( USBC_PMSC_CMD_FAILED );	\
				}												\
			
			//
			//	転送後の処理、次の転送処理
			//
			switch( usb_gpmsc_AtapiCbwcb->s_usb_ptn0.uc_OpCode )
			{
			case USBC_ATAPI_READ10:
				//
				//	READ(10)
				//
				CHECK_DATA_TRANSFER_ERROR( USBC_DATA_NONE )

				usb_gpmsc_AtapiRealData += usb_gpmsc_AtapiFlashSize;
				residue_size = usb_gpmsc_Massage.ul_size - usb_gpmsc_AtapiRealData;

				if( residue_size == 0ul )
				{	/* All data in Device transferred */
					if( usb_gpmsc_Dtl > usb_gpmsc_Massage.ul_size )
					{	/* case 5 */
						usb_gpmsc_Dtl -= usb_gpmsc_Massage.ul_size;
						break_with_status( USBC_PMSC_CMD_FAILED );
					}
					if( usb_gpmsc_Dtl == usb_gpmsc_Massage.ul_size )
					{	/* case 6 */
						usb_gpmsc_Dtl = 0;
						break_with_status( USBC_PMSC_CMD_COMPLETE );
					}
					else
					{	/* case 7*/
						usb_gpmsc_Dtl = (uint32_t)-1;
						break_with_status( USBC_PMSC_CMD_FAILED );
					}
				}
				
				//
				//	次のREAD dataの準備
				//
				usb_gpmsc_AtapiFlashAdr = usb_pmsc_SmpAtapi_EmuRead( &usb_gpmsc_AtapiFlashSize );
				if( usb_gpmsc_AtapiFlashAdr == NULL )
				{
					// READ ERROR
					break_with_status( USBC_PMSC_CMD_FAILED );
				}
			
				break_with_status( USBC_PMSC_CMD_CONTINUE );
				
				
			case USBC_ATAPI_WRITE10:
			case USBC_ATAPI_WRITE_AND_VERIFY:
				//
				//	WRITE(10), WRITE AND VERIFY(10)
				//
				CHECK_DATA_TRANSFER_ERROR( USBC_DATA_OK )

				usb_gpmsc_AtapiRealData += usb_gpmsc_AtapiFlashSize;
				residue_size = usb_gpmsc_Massage.ul_size - usb_gpmsc_AtapiRealData;

				if( !usb_pmsc_SmpAtapi_PostWrite() )
				{
DBG_printf(( "usb_pmsc_SmpAtapiTask: WRITE failure.\n" ));
					break_with_status( USBC_PMSC_CMD_FAILED );
				}

				if( residue_size == 0ul )
				{	/* All data in Device recieved */
					if( usb_gpmsc_Dtl > usb_gpmsc_Massage.ul_size)
					{	/* case 11 */
						usb_gpmsc_Dtl -= usb_gpmsc_Massage.ul_size;
						break_with_status( USBC_PMSC_CMD_FAILED );
					}
					if( usb_gpmsc_Dtl == usb_gpmsc_Massage.ul_size)
					{	/* case 12 */
						usb_gpmsc_Dtl = 0;
						break_with_status( USBC_PMSC_CMD_COMPLETE );
					}
					else
					{	/* case 13*/
						usb_gpmsc_Dtl = (uint32_t)-1;
						break_with_status( USBC_PMSC_CMD_FAILED );
					}
				}
				
				//
				//	次のWRITE dataの準備
				//
				usb_gpmsc_AtapiFlashAdr = usb_pmsc_SmpAtapi_EmuWrite( &usb_gpmsc_AtapiFlashSize );
				break_with_status( USBC_PMSC_CMD_CONTINUE );
				
				
			case USBC_ATAPI_INQUIRY:
			case USBC_ATAPI_REQUEST_SENSE:
			case USBC_ATAPI_MODE_SENSE6:
			case USBC_ATAPI_MODE_SENSE10:
			case USBC_ATAPI_READ_CAPACITY:
				CHECK_DATA_TRANSFER_ERROR( USBC_DATA_NONE )
				
				usb_gpmsc_AtapiRealData += usb_gpmsc_AtapiFlashSize;
				residue_size = usb_gpmsc_Massage.ul_size - usb_gpmsc_AtapiRealData;

				if( residue_size == 0ul )
				{	/* All data in Device transferred */
					if( usb_gpmsc_Dtl > usb_gpmsc_Massage.ul_size )
					{	/* case 5 */
						usb_gpmsc_Dtl -= usb_gpmsc_Massage.ul_size;
						break_with_status( USBC_PMSC_CMD_COMPLETE );
					}
					if( usb_gpmsc_Dtl == usb_gpmsc_Massage.ul_size )
					{	/* case 6 */
						usb_gpmsc_Dtl = 0;
						break_with_status( USBC_PMSC_CMD_COMPLETE );
					}
					else
					{	/* case 7*/
						usb_gpmsc_Dtl = (uint32_t)-1;
						break_with_status( USBC_PMSC_CMD_FAILED );
					}
				}

				// ここでデータ転送は終わるはずなのに。。。
				SET_SENSE_KEY_ASC_ASCQ( 0x04, 0x44, 0x00 );	// INTERNAL TARGET FAILURE
				break_with_status( USBC_PMSC_CMD_FAILED );
				
				
		/* Recieve Data Commands (from Host to Device) */
			case USBC_ATAPI_MODE_SELECT10:
			case USBC_ATAPI_FORMAT_UNIT:
				CHECK_DATA_TRANSFER_ERROR( USBC_DATA_OK )

				usb_gpmsc_AtapiRealData += usb_gpmsc_AtapiFlashSize;
				residue_size = usb_gpmsc_Massage.ul_size - usb_gpmsc_AtapiRealData;

				if( residue_size == 0ul )
				{	/* All data in Device recieved */
					if( usb_gpmsc_Dtl > usb_gpmsc_Massage.ul_size)
					{	/* case 11 */
						usb_gpmsc_Dtl -= usb_gpmsc_Massage.ul_size;
						break_with_status( USBC_PMSC_CMD_FAILED );
					}
					if( usb_gpmsc_Dtl == usb_gpmsc_Massage.ul_size)
					{	/* case 12 */
						usb_gpmsc_Dtl = 0;
						break_with_status( USBC_PMSC_CMD_COMPLETE );
					}
					else
					{	/* case 13*/
						usb_gpmsc_Dtl = (uint32_t)-1;
						break_with_status( USBC_PMSC_CMD_FAILED );
					}
				}
				
				// ここでデータ転送は終わるはずなのに。。。
				SET_SENSE_KEY_ASC_ASCQ( 0x04, 0x44, 0x00 );	// INTERNAL TARGET FAILURE
				break_with_status( USBC_PMSC_CMD_FAILED );

		/* No Send / Recieve Data Commands	-> Error case */
			case USBC_ATAPI_TEST_UNIT_READY:
			case USBC_ATAPI_PREVENT_ALLOW:
			case USBC_ATAPI_START_STOP_UNIT:
			case USBC_ATAPI_SEEK:
			case USBC_ATAPI_VERIFY10:
			default:
				SET_SENSE_KEY_ASC_ASCQ( 0x04, 0x44, 0x00 );	// INTERNAL TARGET FAILURE
				break_with_status( USBC_PMSC_CMD_FAILED );
			}
			
			#undef	CHECK_DATA_TRANSFER_ERROR
		
		} //end of if( usb_gpmsc_AtapiSetupFlg == 1 )
		
		
		usb_gpmsc_AtapiSetupFlg = (status ==USBC_PMSC_CMD_CONTINUE) ? 1 : 0;
		
		if( status == USBC_PMSC_CMD_COMPLETE )
		{
			SET_SENSE_KEY_ASC_ASCQ( 0x00, 0x00, 0x00 );
		}

		/* Set mess members */
		mess->tranadr = usb_gpmsc_AtapiFlashAdr;
		mess->tranlen = usb_gpmsc_AtapiFlashSize;
		mess->status  = status;

		/* CallBack function (SND_MSG) */
		(*mess->complete)(mess);
		
		
		if( status != USBC_PMSC_CMD_CONTINUE )
		{
			LED_RESET();
		}
		
/* Condition compilation by the difference of the operating system */
#if USBC_FW_PP == USBC_FW_NONOS_PP
#else	/* USBC_FW_PP == USBC_FW_NONOS_PP */
	}
#endif	/* USBC_FW_PP == USBC_FW_NONOS_PP */
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pmsc_SmpAtapiInitMedia
Description     : Media Driver open
Arguments       : none
Return value    : USBC_ER_t                  : error
******************************************************************************/
USBC_ER_t usb_pmsc_SmpAtapiInitMedia(void)
{
	USBC_ER_t	err;
	
/* Condition compilation by the difference of the operating system */
#if USBC_FW_PP == USBC_FW_NONOS_PP

	err = USBC_E_OK;

#else	/* USBC_FW_PP == USBC_FW_NONOS_PP */

/* Condition compilation by the difference of the device's operating system */
#if USBC_OS_CRE_MODE_PP == USBC_OS_CRE_USE_PP
	USBC_TSK_t	tskinfo;
	USBC_MBX_t	mbxinfo;
	USBC_MPL_t	mplinfo;

	USBC_PRINTF0("*** Install ATAPI driver ***\n");

	/* Create ATAPI driver task */
	/* Attribute */
	tskinfo.tskatr	= (USBC_ATR_t)(TA_HLNG);
	/* Start address */
	tskinfo.task	= (USBC_FP_t)&usb_pmsc_SmpAtapiTask;
	/* Priority */
	tskinfo.itskpri	= (USBC_PRI_t)(USBC_PFLSH_PRI);
	/* Stack size */
	tskinfo.stksz	= (USBC_SIZ_t)(USBC_TSK_STK);
	/* Stack address */
	tskinfo.stk		= (USBC_VP_t)(NULL);
	err = USBC_CRE_TSK(USB_PFLSH_TSK, &tskinfo);
	if( err != USBC_E_OK )
	{
		USBC_PRINTF1("CRE_TSK USB_PFLSH_TSK Error (%ld)\n", err);
		while( 1 )
		{
			/* do nothing */
		}
	}

	/* Create ATAPI driver mail box */
	/* Attribute */
	mbxinfo.mbxatr	= (USBC_ATR_t)((TA_TFIFO) | (TA_MFIFO));
	/* Priority */
	mbxinfo.maxmpri	= (USBC_PRI_t)(USBC_MBX_PRI);
	/* Header address */
	mbxinfo.mprihd	= (USBC_VP_t)(NULL);
	err = USBC_CRE_MBX(USB_PFLSH_MBX, &mbxinfo);
	if( err != USBC_E_OK )
	{
		USBC_PRINTF1("CRE_MBX USB_PFLSH_MBX Error (%ld)\n", err);
		while( 1 )
		{
			/* do nothing */
		}
	}

	/* Create ATAPI driver memory pool */
	mplinfo.mpfatr	= (USBC_ATR_t)(TA_TFIFO);		/* Attribute */
	mplinfo.blkcnt	= (USBC_UINT_t)(USBC_BLK_CNT);	/* Block count */
	mplinfo.blksz	= (USBC_UINT_t)(USBC_BLK_SIZ);	/* Block size */
	mplinfo.mpf		= (USBC_VP_t)(NULL);				/* Start address */
	err = USBC_CRE_MPL(USB_PFLSH_MPL, &mplinfo);
	if( err != USBC_E_OK )
	{
		USBC_PRINTF1("CRE_MPL USB_PFLSH_MPL Error (%ld)\n", err);
		while( 1 )
		{
			/* do nothing */
		}
	}

	/* Start ATAPI driver task */
	err = USBC_STA_TSK(USB_PFLSH_TSK, 0);
	if( err != USBC_E_OK )
	{
		USBC_PRINTF1("STA_TSK USB_PFLSH_TSK Error (%ld)\n", err);
		while( 1 )
		{
			/* do nothing */
		}
	}
#endif	/* USBC_OS_CRE_MODE_PP == USBC_OS_CRE_USE_PP */

/* Condition compilation by the difference of the device's operating system */
#if USBC_OS_CRE_MODE_PP == USBC_OS_CRE_NOTUSE_PP
	/* Start host FW test driver task */
	err = USBC_ACT_TSK(USB_PFLSH_TSK);
	if( err != USBC_E_OK )
	{
		USBC_PRINTF1("ACT_TSK USB_PFLSH_TSK Error (%ld)\n", err);
		while( 1 )
		{
		}
	}
#endif	/* USBC_OS_CRE_MODE_PP == USBC_OS_CRE_NOTUSE_PP */

#endif /* USBC_FW_PP == USBC_FW_NONOS_PP */

	return err;
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pmsc_SmpAtapiCloseMedia
Description     : Media Driver close
Arguments       : none
Return value    : USBC_ER_t                  : error
******************************************************************************/
USBC_ER_t usb_pmsc_SmpAtapiCloseMedia(void)
{
/* Condition compilation by the difference of the operating system */
#if USBC_FW_PP == USBC_FW_NONOS_PP
	return USBC_E_OK;
#else	/* USBC_FW_PP == USBC_FW_NONOS_PP */
	USBC_ER_t	err;

	USBC_PRINTF0("*** Release ATAPI driver ***\n");

	/* Stop ATAPI driver task */
	err = USBC_TER_TSK(USB_PFLSH_TSK);

/* Condition compilation by the difference of the device's operating system */
#if USBC_OS_CRE_MODE_PP == USBC_OS_CRE_USE_PP
	/* Delete ATAPI driver memory pool */
	err = USBC_DEL_MPL(USB_PFLSH_MPL);

	/* Delete host driver mail box */
	err = USBC_DEL_MBX(USB_PFLSH_MBX);

	/* Delete host driver task */
	err = USBC_DEL_TSK(USB_PFLSH_TSK);
#endif	/* USBC_OS_CRE_MODE_PP == USBC_OS_CRE_USE_PP */
	return err;
#endif	/* USBC_FW_PP == USBC_FW_NONOS_PP */
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pmsc_SmpAtapiCommandExecute
Description     : ATAPI Command Transfer
Arguments       : uint8_t *cbw              : CBW
                : uint16_t status           : status
                : USBC_CB_t complete         : callback
Return value    : USBC_ER_t                  : error
******************************************************************************/
void usb_pmsc_SmpAtapiCommandExecute(uint8_t *cbw, uint16_t status
		, USBC_CB_t complete)
{
	USBC_ER_t	err = USBC_ERROR;

	/* Command Transfer set */
	usb_gpmsc_AtapiMess.msghead		= (USBC_MH_t)NULL;
	usb_gpmsc_AtapiMess.tranadr		= cbw;
	usb_gpmsc_AtapiMess.status		= status;
	usb_gpmsc_AtapiMess.complete	= complete;

	err = USBC_SND_MSG(USB_PFLSH_MBX, (USBC_MSG_t*)&usb_gpmsc_AtapiMess);
	if( err != USBC_E_OK )
	{
		/* Send message failure */
		USBC_PRINTF1("### pmsc_AtapiCommandExecute snd_msg error (%ld)\n"
			, err);
	}
}
/******************************************************************************
End of function
******************************************************************************/




/******************************************************************************
Add code by Medialogic
******************************************************************************/


/******************************************************************************
Function Name   : usb_pmsc_SmpAtapi_EmuInquiry
Description     : Emulate SCSI command: INQUIRY
Arguments       : uint32_t *size            : data size
                : 
Return value    : void*						: data address
******************************************************************************/
static void* usb_pmsc_SmpAtapi_EmuInquiry( uint32_t *size )
{
	static const uint8_t	bInquiryData[40] =
	{
		0x00,0x80,0x02,0x02,	// Removable disk drive
		31,						// Additional length
		0x00,0x00,0x00,

		STR_VENDOR_ID[0],	STR_VENDOR_ID[1],	STR_VENDOR_ID[2],	STR_VENDOR_ID[3],
		STR_VENDOR_ID[4],	STR_VENDOR_ID[5],	STR_VENDOR_ID[6],	STR_VENDOR_ID[7],
		STR_PRODUCT_ID[0],	STR_PRODUCT_ID[1],	STR_PRODUCT_ID[2],	STR_PRODUCT_ID[3],
		STR_PRODUCT_ID[4],	STR_PRODUCT_ID[5],	STR_PRODUCT_ID[6],	STR_PRODUCT_ID[7],
		STR_PRODUCT_ID[8],	STR_PRODUCT_ID[9],	STR_PRODUCT_ID[10],	STR_PRODUCT_ID[11],
		STR_PRODUCT_ID[12],	STR_PRODUCT_ID[13],	STR_PRODUCT_ID[14],	STR_PRODUCT_ID[15],
		STR_PRODUCT_VERSION[0],
		STR_PRODUCT_VERSION[1],
		STR_PRODUCT_VERSION[2],
		STR_PRODUCT_VERSION[3],
	};

	if( usb_gpmsc_AtapiCbwcb->uc[1] & 0x01 )	// EVPD?
	{
		SET_SENSE_KEY_ASC_ASCQ( 5, 0x24, 0x00 );	// INVALID FIELD IN CDB
		return NULL;
	}

	if( usb_gpmsc_Massage.ul_size > sizeof(bInquiryData) )
	{
		*size = sizeof(bInquiryData);
	}
	else
	{
		*size = usb_gpmsc_Massage.ul_size;
	}

	return (void*)bInquiryData;
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pmsc_SmpAtapi_EmuRequestSense
Description     : Emulate SCSI command: REQUEST SENSE
Arguments       : uint32_t *size            : data size
                : 
Return value    : void*						: data address
******************************************************************************/
static void* usb_pmsc_SmpAtapi_EmuRequestSense( uint32_t *size )
{
	if( usb_gpmsc_Massage.ul_size > sizeof(usb_gpmsc_Atapi_SenseInfoBuffer) )
	{
		*size = sizeof(usb_gpmsc_Atapi_SenseInfoBuffer);
	}
	else
	{
		*size = usb_gpmsc_Massage.ul_size;
	}

	return (void*)usb_gpmsc_Atapi_SenseInfoBuffer;
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pmsc_SmpAtapi_EmuModeSense_PageXX
Description     : Get page data for MODE SENSE command
Arguments       : uint8_t*	buf				: data buffer
				: uint8_t	nPageCtrl		: page control
				: uint8_t	bPageData[]		: page data
Return value    : uint16_t					: page data size
******************************************************************************/
static uint16_t usb_pmsc_SmpAtapi_EmuModeSense_PageXX( uint8_t* buf, const uint8_t nPageCtrl, const uint8_t bPageData[] )
{
	const uint16_t	nPageSize = (uint16_t)bPageData[1] + 2;
	if( nPageCtrl == 0x40 )
	{
		memset( buf, 0, nPageSize );
		buf[0] = bPageData[0];
		buf[1] = bPageData[1];
	}
	else
	{
		memcpy( buf, bPageData, nPageSize );
	}
	return nPageSize;
}

static uint16_t usb_pmsc_SmpAtapi_EmuModeSense_Page00( uint8_t* buf, const uint8_t nPageCtrl )
{
	return 0;
}
static uint16_t usb_pmsc_SmpAtapi_EmuModeSense_Page01( uint8_t* buf, const uint8_t nPageCtrl )
{
	//
	//	ReadWrite Error Recovery Page
	//
	static const uint8_t bPageData[] =
	{
		0x01,0x0A, 0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
	};
	
	return usb_pmsc_SmpAtapi_EmuModeSense_PageXX( buf, nPageCtrl, bPageData );
}
static uint16_t usb_pmsc_SmpAtapi_EmuModeSense_Page08( uint8_t* buf, const uint8_t nPageCtrl )
{
	//
	//	Caching page
	//
	static const uint8_t	bPageData[] =
	{
		0x08,0x0A, 0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
	};

	return usb_pmsc_SmpAtapi_EmuModeSense_PageXX( buf, nPageCtrl, bPageData );
}
static uint16_t usb_pmsc_SmpAtapi_EmuModeSense_Page1B( uint8_t* buf, const uint8_t nPageCtrl )
{
	//
	//	Removable Block Access Capacities Page
	//
	static const uint8_t	bPageData[] =
	{
		0x1B,0x0A, 0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
	};
	
	return usb_pmsc_SmpAtapi_EmuModeSense_PageXX( buf, nPageCtrl, bPageData );
}
static uint16_t usb_pmsc_SmpAtapi_EmuModeSense_Page1C( uint8_t* buf, const uint8_t nPageCtrl )
{
	//
	//	Timer & Protect Page
	//
	static const uint8_t	bPageData[] =
	{
		0x1C,0x06, 0x00,0x00,0x00,0x00,0x00,0x00,
	};

	return usb_pmsc_SmpAtapi_EmuModeSense_PageXX( buf, nPageCtrl, bPageData );
}
/******************************************************************************
End of usb_pmsc_SmpAtapi_EmuModeSense_PageXX
******************************************************************************/

/******************************************************************************
Function Name   : usb_pmsc_SmpAtapi_EmuModeSense_MakePageData
Description     : Make page data for MODE SENSE command
Arguments       : uint8_t*	buf				: data buffer
Return value    : uint16_t					: page data size
******************************************************************************/
static uint16_t usb_pmsc_SmpAtapi_EmuModeSense_MakePageData( uint8_t* buf )
{
	const uint8_t	nPageCode	= usb_gpmsc_AtapiCbwcb->uc[2] & 0x3F;
	const uint8_t	nPageCtrl	= usb_gpmsc_AtapiCbwcb->uc[2] & 0xC0;

	if( nPageCode == 0x3F )
	{
		#define	MAKE_PAGE_DATA(_name_)								\
		{															\
			const uint16_t	nPageSize = (_name_)( buf, nPageCtrl );	\
			buf        += nPageSize;								\
			nTotalSize += nPageSize;								\
		}															\

		uint16_t	nTotalSize = 0;
		
		MAKE_PAGE_DATA( usb_pmsc_SmpAtapi_EmuModeSense_Page01 );
		MAKE_PAGE_DATA( usb_pmsc_SmpAtapi_EmuModeSense_Page08 );
		MAKE_PAGE_DATA( usb_pmsc_SmpAtapi_EmuModeSense_Page1B );
		MAKE_PAGE_DATA( usb_pmsc_SmpAtapi_EmuModeSense_Page1C );

		return nTotalSize;

		#undef	MAKE_PAGE_DATA
	}
	
	switch( nPageCode )
	{
	case 0x00:
		return usb_pmsc_SmpAtapi_EmuModeSense_Page00( buf, nPageCtrl );

	case 0x01:
		return usb_pmsc_SmpAtapi_EmuModeSense_Page01( buf, nPageCtrl );
		
	case 0x08:
		return usb_pmsc_SmpAtapi_EmuModeSense_Page08( buf, nPageCtrl );
		
	case 0x1B:
		return usb_pmsc_SmpAtapi_EmuModeSense_Page1B( buf, nPageCtrl );
		
	case 0x1C:
		return usb_pmsc_SmpAtapi_EmuModeSense_Page1C( buf, nPageCtrl );
		
	default:
		return (uint16_t)-1;
	}
}


/******************************************************************************
Function Name   : usb_pmsc_SmpAtapi_EmuModeSense6
Description     : Emulate SCSI command: READ CAPACITY
Arguments       : uint32_t *size            : data size
                : 
Return value    : void*						: data address
******************************************************************************/
static void* usb_pmsc_SmpAtapi_EmuModeSense6( uint32_t *size )
{
	uint8_t* const	pbHeader = usb_gpmsc_AtapiDataBuffer;
	uint8_t*		pbuf = pbHeader;
	uint16_t		nTotalSize = 0, nPagesSize;

	//
	//	Mode parameter header, Block descriptor
	//
	memset( pbHeader, 0, 12 );
	
	if( usb_gpmsc_AtapiCbwcb->uc[1] & 0x08 )	// DBD?
	{
		// Mode parameter header only
		nTotalSize = 4;
	}
	else
	{
		// Mode parameter header + Block descriptor
		nTotalSize = 12;
		pbHeader[ 3] = 0x08;
		pbHeader[ 5] = (uint8_t)((USBC_ATAPI_TOTAL_SECTOR_COUNT >> 16) & 0xFF);
		pbHeader[ 6] = (uint8_t)((USBC_ATAPI_TOTAL_SECTOR_COUNT >>  8) & 0xFF);
		pbHeader[ 7] = (uint8_t)((USBC_ATAPI_TOTAL_SECTOR_COUNT >>  0) & 0xFF);
		pbHeader[ 9] = (uint8_t)((USBC_ATAPI_BLOCK_UNIT >> 16) & 0xFF);
		pbHeader[10] = (uint8_t)((USBC_ATAPI_BLOCK_UNIT >>  8) & 0xFF);
		pbHeader[11] = (uint8_t)((USBC_ATAPI_BLOCK_UNIT >>  0) & 0xFF);
	}
	pbuf += nTotalSize;
	
	//
	//	Page data
	//
	nPagesSize = usb_pmsc_SmpAtapi_EmuModeSense_MakePageData( pbuf );
	if( nPagesSize == (uint16_t)-1 )
	{
		SET_SENSE_KEY_ASC_ASCQ( 5, 0x24, 0x00 );	// INVALID FIELD IN CDB
		return NULL;
	}
	nTotalSize += nPagesSize;
	
	//
	//	Mode parameter length
	//
	pbHeader[0] = (uint8_t)((nTotalSize-1)&0xFF);
	
	if( usb_gpmsc_Massage.ul_size > nTotalSize )
	{
		*size = nTotalSize;
	}
	else
	{
		*size = usb_gpmsc_Massage.ul_size;
	}

	return (void*)pbHeader;
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pmsc_SmpAtapi_EmuModeSense10
Description     : Emulate SCSI command: READ CAPACITY
Arguments       : uint32_t *size            : data size
                : 
Return value    : void*						: data address
******************************************************************************/
static void* usb_pmsc_SmpAtapi_EmuModeSense10( uint32_t *size )
{
	uint8_t* const	pbHeader = usb_gpmsc_AtapiDataBuffer;
	uint8_t*		pbuf = pbHeader;
	uint16_t		nTotalSize = 0, nPagesSize;

	//
	//	Mode parameter header, Block descriptor
	//
	memset( pbHeader, 0, 16 );
	
	if( usb_gpmsc_AtapiCbwcb->uc[1] & 0x08 )	// DBD?
	{
		// Mode parameter header only
		nTotalSize = 8;
	}
	else
	{
		// Mode parameter header + Block descriptor
		nTotalSize = 16;
		pbHeader[ 7] = 0x08;
		pbHeader[ 9] = (uint8_t)((USBC_ATAPI_TOTAL_SECTOR_COUNT >> 16) & 0xFF);
		pbHeader[10] = (uint8_t)((USBC_ATAPI_TOTAL_SECTOR_COUNT >>  8) & 0xFF);
		pbHeader[11] = (uint8_t)((USBC_ATAPI_TOTAL_SECTOR_COUNT >>  0) & 0xFF);
		pbHeader[13] = (uint8_t)((USBC_ATAPI_BLOCK_UNIT >> 16) & 0xFF);
		pbHeader[14] = (uint8_t)((USBC_ATAPI_BLOCK_UNIT >>  8) & 0xFF);
		pbHeader[15] = (uint8_t)((USBC_ATAPI_BLOCK_UNIT >>  0) & 0xFF);
	}
	pbuf += nTotalSize;
	
	//
	//	Page data
	//
	nPagesSize = usb_pmsc_SmpAtapi_EmuModeSense_MakePageData( pbuf );
	if( nPagesSize == (uint16_t)-1 )
	{
		SET_SENSE_KEY_ASC_ASCQ( 5, 0x24, 0x00 );	// INVALID FIELD IN CDB
		return NULL;
	}
	nTotalSize += nPagesSize;
	
	//
	//	Mode parameter length
	//
	pbHeader[0] = (uint8_t)(((nTotalSize-2)>>8)&0xFF);
	pbHeader[1] = (uint8_t)(((nTotalSize-2)>>0)&0xFF);
	
	if( usb_gpmsc_Massage.ul_size > nTotalSize )
	{
		*size = nTotalSize;
	}
	else
	{
		*size = usb_gpmsc_Massage.ul_size;
	}

	return (void*)pbHeader;
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pmsc_SmpAtapi_EmuReadCapacity
Description     : Emulate SCSI command: READ CAPACITY
Arguments       : uint32_t *size            : data size
                : 
Return value    : void*						: data address
******************************************************************************/
static void* usb_pmsc_SmpAtapi_EmuReadCapacity( uint32_t *size )
{
	static const uint8_t	bufData[8] =
	{
		(uint8_t)(((USBC_ATAPI_TOTAL_SECTOR_COUNT-1) >> 24) & 0xFF),
		(uint8_t)(((USBC_ATAPI_TOTAL_SECTOR_COUNT-1) >> 16) & 0xFF),
		(uint8_t)(((USBC_ATAPI_TOTAL_SECTOR_COUNT-1) >>  8) & 0xFF),
		(uint8_t)(((USBC_ATAPI_TOTAL_SECTOR_COUNT-1) >>  0) & 0xFF),
		(uint8_t)((USBC_ATAPI_BLOCK_UNIT >> 24) & 0xFF),
		(uint8_t)((USBC_ATAPI_BLOCK_UNIT >> 16) & 0xFF),
		(uint8_t)((USBC_ATAPI_BLOCK_UNIT >>  8) & 0xFF),
		(uint8_t)((USBC_ATAPI_BLOCK_UNIT >>  0) & 0xFF),
	};
	
	if( usb_gpmsc_Massage.ul_size > sizeof(bufData) )
	{
		*size = sizeof(bufData);
	}
	else
	{
		*size = usb_gpmsc_Massage.ul_size;
	}

	return (void*)bufData;
}
/******************************************************************************
End of function
******************************************************************************/



/******************************************************************************
Function Name   : usb_pmsc_SmpAtapi_AddLba
Description     : Addition to current LBA, Update last LBA
Arguments       : uint32_t count			: addtional count
                : 
Return value    : none
******************************************************************************/
static void usb_pmsc_SmpAtapi_AddLba( const uint32_t count )
{
	usb_gpmsc_Atapi_lbaLast			= usb_gpmsc_Atapi_lbaCurrent;
	usb_gpmsc_Atapi_nLastXferCount	= usb_gpmsc_Atapi_nCurXferCount;
	
	usb_gpmsc_Atapi_lbaCurrent		+= count;
	usb_gpmsc_Atapi_nCurXferCount	-= count;
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pmsc_SmpAtapi_EmuRead_FatArea
Description     : Get read data for FAT area
Arguments       : uint32_t *size            : data size
				: uint32_t lbaBase			: Base LBA of area
				: uint32_t lbaEnd			: End LBA of area
                : 
Return value    : void*						: data address
******************************************************************************/
static void* usb_pmsc_SmpAtapi_EmuRead_FatArea( uint32_t *size, const uint32_t lbaBase, const uint32_t lbaEnd )
{
#if 	ENABLE_KEEP_FILE_INFO
	const uint32_t	addr = USBC_PMSC_DATA_FLASH_ADDR + (usb_gpmsc_Atapi_lbaCurrent - lbaBase) * (uint32_t)USBC_ATAPI_BLOCK_UNIT;
#else
	const uint32_t	addr = (uint32_t)usb_gpmsc_AtapiFatBuffer + (usb_gpmsc_Atapi_lbaCurrent - lbaBase) * (uint32_t)USBC_ATAPI_BLOCK_UNIT;
#endif
		
	uint32_t	count;

	if( (usb_gpmsc_Atapi_lbaCurrent + usb_gpmsc_Atapi_nCurXferCount) > lbaEnd )
	{
		count = lbaEnd - usb_gpmsc_Atapi_lbaCurrent;
	}
	else
	{
		count = usb_gpmsc_Atapi_nCurXferCount;
	}

	usb_pmsc_SmpAtapi_AddLba( count );
		
	*size = count * (uint32_t)USBC_ATAPI_BLOCK_UNIT;
	return (void*)addr;
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pmsc_SmpAtapi_EmuRead_RootDirArea
Description     : Get read data for Root directory entry area
Arguments       : uint32_t *size            : data size
				: uint32_t lbaBase			: Base LBA of area
				: uint32_t lbaEnd			: End LBA of area
                : 
Return value    : void*						: data address
******************************************************************************/
static void* usb_pmsc_SmpAtapi_EmuRead_RootDirArea( uint32_t *size )
{
#if 	ENABLE_KEEP_FILE_INFO
	const uint32_t	addr = USBC_PMSC_DATA_FLASH_ADDR + (usb_gpmsc_Atapi_lbaCurrent - LBA_BASE_ROOT_DIR + FATFS_FAT_SECTOR_COUNT) * (uint32_t)USBC_ATAPI_BLOCK_UNIT;
#else
	const uint32_t	addr = (uint32_t)usb_gpmsc_AtapiRootDirBuffer + (usb_gpmsc_Atapi_lbaCurrent - LBA_BASE_ROOT_DIR) * (uint32_t)USBC_ATAPI_BLOCK_UNIT;
#endif
		
	uint32_t		count;
	const uint32_t	lbaNextArea = LBA_BASE_CLUSTERS;

	if( (usb_gpmsc_Atapi_lbaCurrent + usb_gpmsc_Atapi_nCurXferCount) > lbaNextArea )
	{
		count = lbaNextArea - usb_gpmsc_Atapi_lbaCurrent;
	}
	else
	{
		count = usb_gpmsc_Atapi_nCurXferCount;
	}

	usb_pmsc_SmpAtapi_AddLba( count );
		
	*size = count * (uint32_t)USBC_ATAPI_BLOCK_UNIT;
	return (void*)addr;
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pmsc_SmpAtapi_EmuRead
Description     : Emulate SCSI command: READ(10)
Arguments       : uint32_t *size            : data size
                : 
Return value    : void*						: data address
******************************************************************************/
static void* usb_pmsc_SmpAtapi_EmuRead(uint32_t *size)
{
	DBG_printf(( "READ : LBA=%08Xh(%u) %d\n", usb_gpmsc_Atapi_lbaCurrent, usb_gpmsc_Atapi_nCurXferCount, usb_gpmsc_AtapiSetupFlg ));
	
	//
	//	デバイスの先頭セクタか？
	//
	if( usb_gpmsc_Atapi_lbaCurrent == 0 )
	{
		uint8_t*	buf = usb_gpmsc_AtapiDataBuffer;
		uint8_t*	part = buf + 0x1BE;

		memset( buf, 0, USBC_ATAPI_BLOCK_UNIT );
		part[0]	= 0x00;
		part[1]	= 0x00;
		part[2]	= MBR_SECTOR_COUNT + 1;
		part[3]	= 0x00;
		part[4]	= MBR_PARTITION_ID;
		part[5]	= MBR_END_HEAD_NO;
		part[6]	= MBR_END_SECTOR_NO | ((MBR_END_CYLINDER_NO>>2) & 0xC0);
		part[7]	= MBR_END_CYLINDER_NO & 0xFF;
		*((uint32_t*)(part+8))	= MBR_SECTOR_COUNT;
		*((uint32_t*)(part+12))	= FATFS_VOLUME_TOTAL_SECTOR_COUNT;
		
		buf[0x1FE] = 0x55;
		buf[0x1FF] = 0xAA;

		usb_pmsc_SmpAtapi_AddLba( 1 );

		*size = USBC_ATAPI_BLOCK_UNIT;
		return (void*)usb_gpmsc_AtapiDataBuffer;
	}
	
	//
	//	ボリュームの先頭セクタか？
	//
	else if( usb_gpmsc_Atapi_lbaCurrent == LBA_BASE_VOLUME )
	{
		static const uint8_t	bInitData[] =
		{
			0xEB,0xFE,0x90,
			FATFS_OEM_NAME,						//003: OEM name
			FATFS_SECTOR_SIZE_LO,				//00B: バイト／セクタ
			FATFS_SECTOR_SIZE_HI,
			FATFS_SECTOR_PER_CLUSTER,			//00D: セクタ／クラスタ
			FATFS_RESERVED_SECTOR_COUNT_LO,		//00E: 予約セクタ数
			FATFS_RESERVED_SECTOR_COUNT_HI,			
			2,									//010: FAT数
			FATFS_ROOT_DIR_COUNT_LO,			//011: ルートディレクトリエントリ数
			FATFS_ROOT_DIR_COUNT_HI,
			FATFS_TOTAL_SECTOR_COUNT_LO,		//013: 論理イメージのセクタ数(16bit)
			FATFS_TOTAL_SECTOR_COUNT_HI,
			FATFS_MEDIATYPE,					//015: メディアディスクリプタバイト
			FATFS_FAT_SECTOR_COUNT_LO,			//016: ＦＡＴ／セクタ
			FATFS_FAT_SECTOR_COUNT_HI,
			0,0,								//018: セクタ／トラック
			0,0,								//01A: ヘッド／ユニット
			FATFS_HIDDEN_SECTOR_COUNT_LO,		//01C: 隠れたセクタ数
			FATFS_HIDDEN_SECTOR_COUNT_HI,0,0,
			FATFS_TOTAL_SECTOR_COUNT_EX_0,		//020: 論理イメージのセクタ数(32bit)
			FATFS_TOTAL_SECTOR_COUNT_EX_1,
			FATFS_TOTAL_SECTOR_COUNT_EX_2,
			FATFS_TOTAL_SECTOR_COUNT_EX_3,
			0,									//024: ドライブ番号
			0,0,								//025: reserved
			0,0,0,0,							//027: Volume serial number
			'N','O',' ','N','A','M','E',' ',	//02B: Volume label
			' ',' ',' ',
			FATFS_FILE_SYSTEM_NAME,				//036: File system name
		};
		
		uint8_t*	buf = usb_gpmsc_AtapiDataBuffer;
		memset( buf, 0, USBC_ATAPI_BLOCK_UNIT );
		memcpy( buf, bInitData, sizeof(bInitData) );
		buf[0x1FE] = 0x55;
		buf[0x1FF] = 0xAA;

		usb_pmsc_SmpAtapi_AddLba( 1 );

		*size = USBC_ATAPI_BLOCK_UNIT;
		return (void*)usb_gpmsc_AtapiDataBuffer;
	}
	
	//
	//	予約セクタか？
	//
	else if( usb_gpmsc_Atapi_lbaCurrent < LBA_BASE_FAT1 )
	{
		uint8_t*	buf = usb_gpmsc_AtapiDataBuffer;
		memset( buf, 0, USBC_ATAPI_BLOCK_UNIT );

		usb_pmsc_SmpAtapi_AddLba( 1 );
		
		*size = USBC_ATAPI_BLOCK_UNIT;
		return (void*)usb_gpmsc_AtapiDataBuffer;
	}
	
	//
	//	FAT(1)領域か？
	//
	else if( usb_gpmsc_Atapi_lbaCurrent < LBA_BASE_FAT2 )
	{
		return usb_pmsc_SmpAtapi_EmuRead_FatArea(
					size,
					LBA_BASE_FAT1,
					LBA_BASE_FAT2 );
	}

	//
	//	FAT(2)領域か？
	//
	else if( usb_gpmsc_Atapi_lbaCurrent < LBA_BASE_ROOT_DIR )
	{
		return usb_pmsc_SmpAtapi_EmuRead_FatArea(
					size,
					LBA_BASE_FAT2,
					LBA_BASE_ROOT_DIR );
	}

	//
	//	ルートディレクトリエントリか？
	//
	else if( usb_gpmsc_Atapi_lbaCurrent < LBA_BASE_HTML_FILE_CLUSTER )
	{
		return usb_pmsc_SmpAtapi_EmuRead_RootDirArea( size );
	}

	//
	//	HTMLファイルクラスタ領域か？
	//
	else  if( usb_gpmsc_Atapi_lbaCurrent < LBA_BASE_STORAGE_CLUSTERS )
	{
		static const uint8_t bHtmlFileSectorImage[] =
		{
			HTML_FILE_DATA			
		};
		
		uint32_t		count;
		const uint32_t	lbaNextArea = LBA_BASE_STORAGE_CLUSTERS;
	
		if( (usb_gpmsc_Atapi_lbaCurrent + usb_gpmsc_Atapi_nCurXferCount) > lbaNextArea )
		{
			count = lbaNextArea - usb_gpmsc_Atapi_lbaCurrent;
		}
		else
		{
			count = usb_gpmsc_Atapi_nCurXferCount;
		}

		usb_pmsc_SmpAtapi_AddLba( count );

		*size = count * (uint32_t)USBC_ATAPI_BLOCK_UNIT;
		return (void*)bHtmlFileSectorImage;
	}
	
	//
	//	ストレージ領域だ！
	//
	else 
	{
		const uint32_t	lbaBase	= LBA_BASE_STORAGE_CLUSTERS;
		uint32_t		addr, count;

		addr = USBC_PMSC_STORAGE_FLASH_ADDR + (usb_gpmsc_Atapi_lbaCurrent - lbaBase) * USBC_ATAPI_BLOCK_UNIT;
		
		if( usb_gpmsc_Atapi_nCurXferCount > (USBC_ATAPI_TRANSFER_UNIT/USBC_ATAPI_BLOCK_UNIT) )
		{
			count = (USBC_ATAPI_TRANSFER_UNIT/USBC_ATAPI_BLOCK_UNIT);
		}
		else
		{
			count = usb_gpmsc_Atapi_nCurXferCount;
		}

		usb_pmsc_SmpAtapi_AddLba( count );
		
		*size = count * (uint32_t)USBC_ATAPI_BLOCK_UNIT;
		return (void*)addr;
	}
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pmsc_SmpAtapi_EmuWrite_FatArea
Description     : Get write buffer for FAT area
Arguments       : uint32_t *size            : data size
				: uint32_t lbaEnd			: End LBA of area
                : 
Return value    : void*						: data address
******************************************************************************/
static void* usb_pmsc_SmpAtapi_EmuWrite_FatArea( uint32_t *size, const uint32_t lbaBase, const uint32_t lbaEnd )
{
#if 	ENABLE_KEEP_FILE_INFO
	const uint32_t	addr = usb_gpmsc_AtapiDataBuffer;
#else
	const uint32_t	addr = (uint32_t)usb_gpmsc_AtapiFatBuffer + (usb_gpmsc_Atapi_lbaCurrent - lbaBase) * (uint32_t)USBC_ATAPI_BLOCK_UNIT;
#endif
	uint32_t		count;
	
	if( (usb_gpmsc_Atapi_lbaCurrent + usb_gpmsc_Atapi_nCurXferCount) > lbaEnd )
	{
		count = lbaEnd - usb_gpmsc_Atapi_lbaCurrent;
	}
	else
	{
		count = usb_gpmsc_Atapi_nCurXferCount;
	}

	usb_pmsc_SmpAtapi_AddLba( count );
		
	*size = count * (uint32_t)USBC_ATAPI_BLOCK_UNIT;
	return (void*)addr;
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pmsc_SmpAtapi_PostWrite_FatArea
Description     : Write into Flash ROM for FAT area
Arguments       : uint32_t lba				: LBA of flash rom area
				: uint32_t count			: Block count
                : 
Return value    : BOOL						: true = success, false = write error
******************************************************************************/
static BOOL usb_pmsc_SmpAtapi_PostWrite_FatArea( const uint32_t lba, const uint32_t count )
{
#if 	ENABLE_KEEP_FILE_INFO
	
	uint32_t		addr = USBC_PMSC_DATA_FLASH_ADDR + lba * USBC_ATAPI_BLOCK_UNIT;
	const uint32_t	addrEnd = addr + count * USBC_ATAPI_BLOCK_UNIT;
	uint8_t*		p = usb_gpmsc_AtapiDataBuffer;

	while( addr < addrEnd )
	{
		if( flash_write_dataflash( addr, p ) != FLASH_SUCCESS )
		{
			return FALSE;
		}
		addr += 32;
		p += 32;
	}
	return TRUE;
	
#else

	return TRUE;

#endif
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pmsc_SmpAtapi_EmuWrite_RootDirArea
Description     : Get write buffer for FAT area
Arguments       : uint32_t *size            : data size
				: uint32_t lbaEnd			: End LBA of area
                : 
Return value    : void*						: data address
******************************************************************************/
static void* usb_pmsc_SmpAtapi_EmuWrite_RootDirArea( uint32_t *size )
{
#if 	ENABLE_KEEP_FILE_INFO
	const uint32_t	addr = (uint32_t)usb_gpmsc_AtapiDataBuffer;
#else
	const uint32_t	addr = (uint32_t)usb_gpmsc_AtapiRootDirBuffer + (usb_gpmsc_Atapi_lbaCurrent - LBA_BASE_ROOT_DIR) * (uint32_t)USBC_ATAPI_BLOCK_UNIT;
#endif
	uint32_t		count;
	const uint32_t	lbaNextArea = LBA_BASE_CLUSTERS;
	
	if( (usb_gpmsc_Atapi_lbaCurrent + usb_gpmsc_Atapi_nCurXferCount) > lbaNextArea )
	{
		count = lbaNextArea - usb_gpmsc_Atapi_lbaCurrent;
	}
	else
	{
		count = usb_gpmsc_Atapi_nCurXferCount;
	}

	usb_pmsc_SmpAtapi_AddLba( count );
		
	*size = count * (uint32_t)USBC_ATAPI_BLOCK_UNIT;
	return (void*)addr;
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pmsc_SmpAtapi_PostWrite_RootDirArea
Description     : Write into Flash ROM for FAT area
Arguments       : uint32_t lba				: LBA of flash rom area
				: uint32_t count			: Block count
                : 
Return value    : BOOL						: true = success, false = write error
******************************************************************************/
static BOOL usb_pmsc_SmpAtapi_PostWrite_RootDirArea( const uint32_t lba, const uint32_t count )
{
#if 	ENABLE_KEEP_FILE_INFO
	
	uint32_t		addr = USBC_PMSC_DATA_FLASH_ADDR + (lba + FATFS_FAT_SECTOR_COUNT) * USBC_ATAPI_BLOCK_UNIT;
	const uint32_t	addrEnd = addr + count * USBC_ATAPI_BLOCK_UNIT;
	uint8_t*		p = usb_gpmsc_AtapiDataBuffer;

	while( addr < addrEnd )
	{
		if( flash_write_dataflash( addr, p ) != FLASH_SUCCESS )
		{
			return FALSE;
		}
		addr += 32;
		p += 32;
	}
	return TRUE;
	
#else

	return TRUE;

#endif
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pmsc_SmpAtapi_PostWrite_StorageFlash
Description     : Write into Flash ROM for Storage
Arguments       : uint32_t clus				: Cluster No.
                : 
Return value    : BOOL						: true = success, false = write error
******************************************************************************/
static BOOL usb_pmsc_SmpAtapi_PostWrite_StorageFlash( const uint32_t nClusNo )
{
	const uint32_t	addr = USBC_PMSC_STORAGE_FLASH_ADDR + nClusNo * FATFS_CLUSTER_SIZE;
	
	if( memcmp( (void*)addr, usb_gpmsc_AtapiDataBuffer, FATFS_CLUSTER_SIZE ) == 0 )
	{
		return TRUE;
	}
	
	if( flash_coderom_EraseBlock( addr ) != FLASH_SUCCESS )
	{
		return FALSE;
	}
	if( flash_coderom_WriteData( addr, usb_gpmsc_AtapiDataBuffer, FATFS_CLUSTER_SIZE ) != FLASH_SUCCESS )
	{
		return FALSE;
	}

	return TRUE;
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pmsc_SmpAtapi_EmuWrite
Description     : Get write buffer
Arguments       : uint32_t *size            : data size
                : 
Return value    : void*						: data address
******************************************************************************/
static void* usb_pmsc_SmpAtapi_EmuWrite( uint32_t *size )
{
	DBG_printf(( "WRITE: LBA=%08Xh(%u) %d\n", usb_gpmsc_Atapi_lbaCurrent, usb_gpmsc_Atapi_nCurXferCount, usb_gpmsc_AtapiSetupFlg ));
	
	usb_gpmsc_Atapi_timeLastWrite = timer_GetTimeCount();
	
	//
	//	予約領域か？
	//
	if( usb_gpmsc_Atapi_lbaCurrent < LBA_BASE_FAT1 )
	{
		usb_pmsc_SmpAtapi_AddLba( 1 );
		*size = USBC_ATAPI_BLOCK_UNIT;
		return (void*)usb_gpmsc_AtapiDataBuffer;
	}
	
	//
	//	FAT(1)領域か？
	//
	else if( usb_gpmsc_Atapi_lbaCurrent < LBA_BASE_FAT2 )
	{
		return usb_pmsc_SmpAtapi_EmuWrite_FatArea(
					size,
					LBA_BASE_FAT1,
					LBA_BASE_FAT2 );
	}

	//
	//	FAT(2)領域か？
	//
	else if( usb_gpmsc_Atapi_lbaCurrent < LBA_BASE_ROOT_DIR )
	{
		return usb_pmsc_SmpAtapi_EmuWrite_FatArea(
					size,
					LBA_BASE_FAT2,
					LBA_BASE_ROOT_DIR );
	}

	//
	//	ルートディレクトリエントリか？
	//
	else if( usb_gpmsc_Atapi_lbaCurrent < LBA_BASE_HTML_FILE_CLUSTER )
	{
		return usb_pmsc_SmpAtapi_EmuWrite_RootDirArea( size );
	}

	//
	//	HTMLファイル
	//
	else if( usb_gpmsc_Atapi_lbaCurrent < LBA_BASE_STORAGE_CLUSTERS )
	{
		const uint32_t	addr = (uint32_t)usb_gpmsc_AtapiDataBuffer;	// Dummy
		uint32_t		count;

#if		(HTML_FILE_DATA_CLUSTER_COUNT==1)

		const uint32_t	lbaNextArea = LBA_BASE_STORAGE_CLUSTERS;
	
		if( (usb_gpmsc_Atapi_lbaCurrent + usb_gpmsc_Atapi_nCurXferCount) > lbaNextArea )
		{
			count = lbaNextArea - usb_gpmsc_Atapi_lbaCurrent;
		}
		else
		{
			count = usb_gpmsc_Atapi_nCurXferCount;
		}
#else
		#error Not implemented!
#endif
		usb_pmsc_SmpAtapi_AddLba( count );
		
		*size = count * (uint32_t)USBC_ATAPI_BLOCK_UNIT;
		return (void*)addr;
	}

	//
	//	ストレージ領域だ！
	//
	else 
	{
		const uint32_t	lba		= usb_gpmsc_Atapi_lbaCurrent - LBA_BASE_STORAGE_CLUSTERS;
		const uint32_t	clus	= lba / FATFS_SECTOR_PER_CLUSTER;
		const uint32_t	offsec	= lba % FATFS_SECTOR_PER_CLUSTER;
		uint32_t		count;
		if( usb_gpmsc_Atapi_nCurXferCount > (FATFS_SECTOR_PER_CLUSTER - offsec) )
		{
			count = FATFS_SECTOR_PER_CLUSTER - offsec;
		}
		else
		{
			count = usb_gpmsc_Atapi_nCurXferCount;
		}

		if( (offsec != 0) || (count < FATFS_SECTOR_PER_CLUSTER) )
		{
			memcpy( usb_gpmsc_AtapiDataBuffer,
					USBC_PMSC_STORAGE_FLASH_ADDR + clus * FATFS_CLUSTER_SIZE,
					FATFS_CLUSTER_SIZE );
		}
		
		usb_pmsc_SmpAtapi_AddLba( count );
		*size = count * USBC_ATAPI_BLOCK_UNIT;
		return (void*)(usb_gpmsc_AtapiDataBuffer + offsec * USBC_ATAPI_BLOCK_UNIT);
	}
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
* Function Name	:	usb_pmsc_SmpAtapi_PostWrite
* Description	:	Write into Flash ROM
* Arguments		:	none
* Return value	:	true		= Success
*					false		= Write error
******************************************************************************/
static BOOL usb_pmsc_SmpAtapi_PostWrite( void )
{
	DBG_printf(( "WRITE: LBA=%08Xh(POST)\n", usb_gpmsc_Atapi_lbaLast ));
	
	usb_gpmsc_Atapi_timeLastWrite = timer_GetTimeCount();

	//
	//	予約領域か？
	//
	if( usb_gpmsc_Atapi_lbaLast == 0 )
	{
		return TRUE;
	}
	else if( usb_gpmsc_Atapi_lbaLast < LBA_BASE_FAT1 )
	{
		return TRUE;
	}

	//
	//	FAT(1)領域か？
	//
	else if( usb_gpmsc_Atapi_lbaLast < LBA_BASE_FAT2 )
	{
		if( !usb_pmsc_SmpAtapi_PostWrite_FatArea(
				usb_gpmsc_Atapi_lbaLast - LBA_BASE_FAT1,
				usb_gpmsc_Atapi_nLastXferCount ) )
		{
			goto __WriteError;
		}
	}

	//
	//	FAT(2)領域か？
	//
	else if( usb_gpmsc_Atapi_lbaLast < LBA_BASE_ROOT_DIR )
	{
		if( !usb_pmsc_SmpAtapi_PostWrite_FatArea(
				usb_gpmsc_Atapi_lbaLast - LBA_BASE_FAT2,
				usb_gpmsc_Atapi_nLastXferCount ) )
		{
			goto __WriteError;
		}
	}

	//
	//	ルートディレクトリエントリか？
	//
	else if( usb_gpmsc_Atapi_lbaLast < LBA_BASE_HTML_FILE_CLUSTER )
	{
		if( !usb_pmsc_SmpAtapi_PostWrite_RootDirArea(
				usb_gpmsc_Atapi_lbaLast - LBA_BASE_ROOT_DIR,
				usb_gpmsc_Atapi_nLastXferCount ) )
		{
			goto __WriteError;
		}
	}

	//
	//	HTMLファイルか？
	//
	else if( usb_gpmsc_Atapi_lbaLast < LBA_BASE_STORAGE_CLUSTERS )
	{
		goto __WriteError;
	}

	//
	//	ストレージ領域だ！
	//
	else 
	{
		const uint32_t	clus = (usb_gpmsc_Atapi_lbaLast - LBA_BASE_STORAGE_CLUSTERS) / FATFS_SECTOR_PER_CLUSTER;
		if( !usb_pmsc_SmpAtapi_PostWrite_StorageFlash( clus ) )
		{
			goto __WriteError;
		}
	}

	return TRUE;
	
	
__WriteError:
	SET_SENSE_KEY_ASC_ASCQ( 0x03, 0x0C, 0x02 );	// WRITE ERROR - AUTO REALLOCATION FAILED
	return FALSE;	
}
/******************************************************************************
End of function
******************************************************************************/





/******************************************************************************
* Function Name	:	fatfs_FindUserApplicationFileInfo()
* Description	:	Search file information of user application.
* Argument		:	none
* Return		:	not NULL	= Found file information.
*				:	NULL		= not found.
******************************************************************************/
PDIRENT fatfs_FindUserApplicationFileInfo( void )
{
	uint16_t	i;

	//
	//	ルートディレクトリエントリを検索する
	//
	PDIRENT	pdeFound = NULL;
#if 	ENABLE_KEEP_FILE_INFO
	PDIRENT	pde = (PDIRENT)(USBC_PMSC_DATA_FLASH_ADDR + (FATFS_FAT_SECTOR_COUNT * (uint32_t)USBC_ATAPI_BLOCK_UNIT));
#else
	PDIRENT	pde = (PDIRENT)usb_gpmsc_AtapiRootDirBuffer;
#endif
	for( i = 0; i < FATFS_ROOT_DIR_COUNT; i ++, pde ++ )
	{
		if( pde->bAttributes == 0x0F )
		{
			continue;	// Long filename
		}
		if( pde->bAttributes == 0x08 )
		{
			continue;	// Volume label
		}
		if( pde->bAttributes & 0x16 )
		{
			continue;	// Sub directory, System file, Hidden file
		}
		if( pde->bFilename[0] == 0xE5 )
		{
			continue;	// Deleted
		}
		if( pde->bFilename[0] == 0x00 )
		{
			continue;	// unused
		}
		if( ((pde->bExtension[0]&0xDF) == 'B') &&
			((pde->bExtension[1]&0xDF) == 'I') &&
			((pde->bExtension[2]&0xDF) == 'N') )
		{
			if( pdeFound == NULL )
			{
				pdeFound = pde;
			}
			else if( pde->wLastWriteDate > pdeFound->wLastWriteDate )
			{
				pdeFound = pde;
			}
			else if( pde->wLastWriteDate == pdeFound->wLastWriteDate )
			{
				if( pde->wLastWriteTime >= pdeFound->wLastWriteTime )
				{
					pdeFound = pde;
				}
			}
		}
		
	} //end of for( i = 0; i < FATFS_ROOT_DIR_COUNT; i ++, pde ++ )
	
	return pdeFound;
}
/******************************************************************************
End of function
******************************************************************************/

	
/******************************************************************************
* Function Name	:	fatfs_GetNextClusterNo()
* Description	:	Get next cluster No.
* Argument		:	Current cluster No.
* Return		:	Next cluster No.
******************************************************************************/
uint16_t fatfs_GetNextClusterNo( const uint16_t clusCur )
{
#if 	ENABLE_FAT16

	{
		//
		//	FAT16の場合
		//
		const uint32_t	ofst = (clusCur * 2);
#if 	ENABLE_KEEP_FILE_INFO
		const uint16_t*	pwFat = (uint16_t*)(USBC_PMSC_DATA_FLASH_ADDR + ofst);
#else
		const uint16_t*	pwFat = (uint16_t*)&usb_gpmsc_AtapiFatBuffer[ofst];
#endif
		uint16_t		clusNext = *pwFat;

		if( clusNext >= 0xFFF8 )
		{
			clusNext = (uint16_t)-1;
		}
		return clusNext;
	}
	
#else	//ENABLE_FAT16

	{
		//
		//	FAT12の場合
		//
		const uint32_t	ofst = (clusCur / 2) * 3;
#if 	ENABLE_KEEP_FILE_INFO
		const uint8_t*	pbFat = (uint8_t*)(USBC_PMSC_DATA_FLASH_ADDR + ofst);
#else
		const uint8_t*	pbFat = &usb_gpmsc_AtapiFatBuffer[ofst];
#endif
		uint16_t		clusNext;
		if( clusCur & 1 )
		{
			clusNext = ((uint16_t)pbFat[2] << 4) | ((uint16_t)(pbFat[1]&0xF0) >> 4);
		}
		else
		{
			clusNext = ((uint16_t)(pbFat[1]&0x0F) << 8) | (uint16_t)pbFat[0];
		}
		if( clusNext >= 0x0FF8 )
		{
			clusNext = (uint16_t)-1;
		}
		return clusNext;
	}
	
#endif	//ENABLE_FAT16	
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
* Function Name	:	fatfs_GetClusterData()
* Description	:	Get cluster data
* Argument		:	Cluster No.
* Return		:	Data buffer
******************************************************************************/
void* fatfs_GetClusterData( const uint16_t nClusNo )
{
	const uint32_t	nStorageClusNo = (uint32_t)(nClusNo - 2 - HTML_FILE_DATA_CLUSTER_COUNT);
	return (void*)(USBC_PMSC_STORAGE_FLASH_ADDR + nStorageClusNo * FATFS_CLUSTER_SIZE);
}
/******************************************************************************
End of function
******************************************************************************/



/******************************************************************************
End  Of File
******************************************************************************/
