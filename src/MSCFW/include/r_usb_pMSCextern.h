/******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized.
* This software is owned by Renesas Electronics Corporation and is  protected
* under all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES
* REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY,
* INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR  A
* PARTICULAR PURPOSE AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE  EXPRESSLY
* DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE  LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES
* FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS
* AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this
* software and to discontinue the availability of this software.
* By using this software, you agree to the additional terms and
* conditions found by accessing the following link:
* http://www.renesas.com/disclaimer
******************************************************************************
* Copyright (C) 2010 Renesas Electronics Corporation. All rights reserved.
******************************************************************************
* File Name    : r_usb_cATAPIdefine.h
* Version      : 1.00
* Device(s)    : Renesas SH-Series, RX-Series
* Tool-Chain   : Renesas SuperH RISC engine Standard Toolchain
*              : Renesas RX Standard Toolchain
* OS           : Common to None and uITRON 4.0 Spec
* H/W Platform : Independent
* Description  : USB Host and Peripheral Interrupt code
******************************************************************************
* History : DD.MM.YYYY Version Description
*         : 04.02.2010 1.00    First Release
*         : 02.08.2010 0.91    Updated comments
*         : 29.10.2010 1.00    Mass Production Release
******************************************************************************/
#ifndef __R_USB_PMSCEXTERN_H__
#define __R_USB_PMSCEXTERN_H__


/******************************************************************************
Includes   <System Includes> , "Project Includes"
******************************************************************************/


/*****************************************************************************
Macro definitions
******************************************************************************/


/*****************************************************************************
Enumerated Types
******************************************************************************/


/******************************************************************************
Typedef definitions
******************************************************************************/


/******************************************************************************
External variables and functions
******************************************************************************/

extern uint16_t			*usb_gpmsc_EpPtr[];
extern uint16_t			usb_gpmsc_EpTbl1[];
extern uint16_t			usb_gpmsc_EpTbl2[];
extern uint16_t			usb_gpmsc_EpTbl3[];
extern uint16_t			usb_gpmsc_EpTbl4[];
extern uint16_t			usb_gpmsc_EpTbl5[];
extern uint16_t			usb_gpmsc_Inpipe;
extern uint16_t			usb_gpmsc_Outpipe;
extern uint32_t			usb_gpmsc_Dtl;
extern uint32_t			usb_gpmsc_Tag;
extern uint8_t			*usb_gpmsc_ConPtr[];
extern uint8_t			*usb_gpmsc_ConPtrOther[];
extern uint8_t			*usb_gpmsc_StrPtr[];
extern uint8_t			usb_gpmsc_CbwCbLength;
extern uint8_t			usb_gpmsc_ConfigrationF1[];
extern uint8_t			usb_gpmsc_ConfigrationH1[];
extern uint8_t			usb_gpmsc_DeviceDescriptor[];
extern uint8_t			usb_gpmsc_InterfaceSubClass[];
extern uint8_t			usb_gpmsc_QualifierDescriptor[];
extern uint8_t			usb_gpmsc_Seq;
extern uint8_t			usb_gpmsc_StringDescriptor0[];
extern uint8_t			usb_gpmsc_StringDescriptor1[];
extern uint8_t			usb_gpmsc_StringDescriptor2[];
extern uint8_t			usb_gpmsc_StringDescriptor3[];
extern uint8_t			usb_gpmsc_StringDescriptor4[];
extern uint8_t			usb_gpmsc_StringDescriptor5[];
extern uint8_t			usb_gpmsc_StringDescriptor6[];
extern USBC_MSC_CBW_t	usb_gpmsc_Cbw;
extern USBC_MSC_CSW_t	usb_gpmsc_Csw;
extern USBC_PMSC_CBM_t	usb_gpmsc_Massage;
extern USBC_UTR_t		usb_gpmsc_Mess;
extern uint8_t			usb_gpmsc_MediaArea[];

uint16_t	R_usb_pmsc_ProcessWaitTmo(uint16_t tmo);
uint8_t		usb_pmsc_CheckCase13(uint32_t ul_size, uint8_t *uc_case);
uint8_t		usb_pmsc_CheckMeaning(uint8_t seq);
uint8_t		usb_pmsc_CheckValid(uint32_t length);
uint8_t		usb_pmsc_CommandCheck(uint8_t seq);
uint8_t		usb_pmsc_TransferMatrix(uint8_t uc_pmsc_case);
USBC_ER_t	R_usb_pmsc_Close(uint16_t data1, uint16_t data2);
USBC_ER_t	R_usb_pmsc_Open(uint16_t data1, uint16_t data2);
USBC_ER_t	usb_pmsc_SmpAtapiInitMedia(void);
void		R_usb_pmsc_ControlTrans0(USBC_REQUEST_t *req);
void		R_usb_pmsc_ControlTrans1(USBC_REQUEST_t *req);
void		R_usb_pmsc_ControlTrans2(USBC_REQUEST_t *req);
void		R_usb_pmsc_ControlTrans3(USBC_REQUEST_t *req);
void		R_usb_pmsc_ControlTrans4(USBC_REQUEST_t *req);
void		R_usb_pmsc_ControlTrans5(USBC_REQUEST_t *req);
void		R_usb_pmsc_DataTrans(uint16_t pipe, uint32_t size
				, uint8_t *table, USBC_CB_t complete);
void		R_usb_pmsc_DescriptorChange(uint16_t mode, uint16_t data2);
void		R_usb_pmsc_GetMaxLun(USBC_CB_INFO_t complete);
void		R_usb_pmsc_MassStrageReset(USBC_CB_INFO_t complete);
void		R_usb_pmsc_Registration(void);
void		R_usb_pmsc_SetConfig(void);
void		R_usb_pmsc_SetInterface(uint16_t data1, uint16_t data2);
void		R_usb_pmsc_StallClearResult(uint16_t data,uint16_t dummy);
void		R_usb_pmsc_StrgTaskClose(void);
void		R_usb_pmsc_StrgTaskOpen(void);
void		R_usb_pmsc_TransResult(USBC_UTR_t *mess);
void		usb_pmsc_AtapiTransResult(USBC_UTR_t *mess);
void		usb_pmsc_CswSet(uint8_t ar_resp, uint32_t ul_size);
void		usb_pmsc_Error0(void);
void		usb_pmsc_Error1(void);
void		usb_pmsc_Error2(void);
void		usb_pmsc_Error3(void);
void		usb_pmsc_Error4(void);
void		usb_pmsc_Error5(void);
void		usb_pmsc_GetMaxLun(uint16_t value, uint16_t index
				, uint16_t length);
void		usb_pmsc_MassStrageReset(uint16_t value, uint16_t index
				, uint16_t length);
void		usb_pmsc_Task(USBC_VP_INT_t);
void		usb_pmsc_UsbExecute(USBC_UTR_t *mess);
void		usb_pstd_RequestBackup(USBC_REQUEST_t *req);
void		usb_pmsc_SmpAtapiAnalyzeCbwCb(uint8_t *cbwcb);
void		usb_pmsc_SmpAtapiCommandExecute(uint8_t *cbw, uint16_t status
				, USBC_CB_t complete);

/* Condition compilation by the difference of the operating system */
#if USBC_FW_PP == USBC_FW_NONOS_PP
void		usb_pmsc_UsbExecuteCont(uint16_t, uint16_t);
void		usb_pmsc_ErrCswNg(void);
void		usb_pmsc_ErrPhaseErr(void);
void		usb_pmsc_ErrSetStall(void);
void		usb_pmsc_ReqTransEnd(uint16_t, uint16_t);
#endif	/* USBC_FW_PP == USBC_FW_NONOS_PP */


#endif	/* __R_USB_PMSCEXTERN_H__ */
/******************************************************************************
End  Of File
******************************************************************************/
