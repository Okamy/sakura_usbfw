/******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized.
* This software is owned by Renesas Electronics Corporation and is  protected
* under all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES
* REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY,
* INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR  A
* PARTICULAR PURPOSE AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE  EXPRESSLY
* DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE  LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES
* FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS
* AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this
* software and to discontinue the availability of this software.
* By using this software, you agree to the additional terms and
* conditions found by accessing the following link:
* http://www.renesas.com/disclaimer
*******************************************************************************
* Copyright (C) 2010(2011) Renesas Electronics Corpration
* and Renesas Solutions Corp. All rights reserved.
*******************************************************************************
* File Name    : r_usb_smp_cSub.c
* Version      : 1.10
* Device(s)    : Renesas SH-Series, RX-Series
* Tool-Chain   : Renesas SuperH RISC engine Standard Toolchain
*              : Renesas RX Standard Toolchain
* OS           : Common to None and uITRON 4.0 Spec
* H/W Platform : Independent
* Description  : USB Host and Peripheral common sample code
******************************************************************************
* History : DD.MM.YYYY Version Description
*         : 17.03.2010 0.80    First Release
*         : 30.07.2010 0.90    Updated comments
*         : 02.08.2010 0.91    Updated comments
*         : 29.10.2010 1.00    Mass Production Release
*         : 01.06.2011 1.10    Version 1.10 Release
******************************************************************************/


/******************************************************************************
Includes   <System Includes> , "Project Includes"
******************************************************************************/
#include "r_usbc_cTypedef.h"		/* Type define */
#include "r_usb_cDefUsr.h"			/* USB-H/W register set (user define) */
#include "r_usbc_cKernelId.h"		/* Kernel ID definition */
#include "r_usbc_cDefUSBIP.h"		/* USB-FW Library Header */
#include "r_usbc_cMacPrint.h"		/* Standard IO macro */
#include "r_usbc_cMacSystemcall.h"	/* uITRON system call macro */
#include "r_usb_cExtern.h"			/* USB-FW global define */


/******************************************************************************
Section    <Section Definition> , "Project Sections"
******************************************************************************/
#pragma section _usblib


/******************************************************************************
Constant macro definitions
******************************************************************************/


/******************************************************************************
External variables and functions
******************************************************************************/
void			usb_cstd_ClassTransResult(USBC_UTR_t *mess);
uint16_t		usb_cstd_ClassTransWaitTmo(uint16_t tmo);
void			usb_cstd_ClassProcessResult(uint16_t data,uint16_t Dummy);
uint16_t		usb_cstd_ClassProcessWaitTmo(uint16_t tmo);
void			usb_cstd_DummyFunction(uint16_t data1, uint16_t data2);


/******************************************************************************
Private global variables and functions
******************************************************************************/


/******************************************************************************
Renesas Abstracted common Driver functions
******************************************************************************/

/******************************************************************************
Function Name   : usb_cstd_ClassTransResult
Description     : send message
Arguments       : USBC_UTR_t *mess           : message
Return value    : none
******************************************************************************/
void usb_cstd_ClassTransResult(USBC_UTR_t *mess)
{
/* Condition compilation by the difference of the operating system */
#if USBC_FW_PP == USBC_FW_OS_PP
	USBC_ER_t		err;

	err = USBC_SND_MSG(USB_CLS_MBX, (USBC_MSG_t*)mess);
	if( err != USBC_E_OK )
	{
		USBC_PRINTF1("### ClassTransResult snd_msg error (%ld)\n", err);
	}
#endif	/* USBC_FW_OS_PP */
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_ClassTransWaitTmo
Description     : receive message
Arguments       : uint16_t tmo              : time out
Return value    : uint16_t                  : status
******************************************************************************/
uint16_t usb_cstd_ClassTransWaitTmo(uint16_t tmo)
{
/* Condition compilation by the difference of the operating system */
#if USBC_FW_PP == USBC_FW_OS_PP
	USBC_UTR_t		*mess;
	USBC_ER_t		err;

	err = USBC_TRCV_MSG(USB_CLS_MBX, (USBC_MSG_t**)&mess, (USBC_TM_t)tmo);
	if( err == USBC_E_TMOUT )
	{
		return (USBC_DATA_TMO);
	}
	if( err != USBC_E_OK )
	{
		USBC_PRINTF1("### lib_ClassTransWaitTmo trcv_msg error (%ld)\n"
			, err);
		return (USBC_DATA_ERR);
	}
	return (mess->status);
#endif	/* USBC_FW_OS_PP */
/* Condition compilation by the difference of the operating system */
#if USBC_FW_PP == USBC_FW_NONOS_PP
	return USBC_E_OK;
#endif	/* USBC_FW_NONOS_PP */
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_ClassProcessResult
Description     : callback function
Arguments       : uint16_t data             : status
                : uint16_t dummy            : not use
Return value    : none
******************************************************************************/
void usb_cstd_ClassProcessResult(uint16_t data, uint16_t dummy)
{
/* Condition compilation by the difference of the operating system */
#if USBC_FW_PP == USBC_FW_OS_PP
	USBC_MH_t		p_blf;
	USBC_ER_t		err, err2;
	USBC_UTR_t		*ptr;

	/* Get mem pool blk */
	err = USBC_PGET_BLK(USB_CLS_MPL, &p_blf);
	if( err == USBC_E_OK )
	{
		ptr = (USBC_UTR_t*)p_blf; 
		ptr->msghead	= (USBC_MH_t)USBC_NULL;
		ptr->msginfo	= (uint16_t)0u;
		ptr->keyword	= (uint16_t)0u;
		ptr->status		= data;
		/* Send message */
		err = USBC_SND_MSG(USB_CLS_MBX, (USBC_MSG_t*)p_blf);
		if( err != USBC_E_OK )
		{
			USBC_PRINTF1("### ClassProcessResult snd_msg error (%ld)\n"
				, err);
			err2 = USBC_REL_BLK(USB_CLS_MPL, (USBC_MH_t)p_blf);
			if( err2 != USBC_E_OK )
			{
				USBC_PRINTF1("### ClassProcessResult rel_blk error (%ld)\n"
					, err2);
			}
		}
	}
	else
	{
		USBC_PRINTF1("### ClassProcessResult pget_blk error (%ld)\n", err);
	}
#endif	/* USBC_FW_OS_PP */
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_ClassProcessWaitTmo
Description     : receive message
Arguments       : uint16_t tmo              : time out
Return value    : uint16_t                  : status
******************************************************************************/
uint16_t usb_cstd_ClassProcessWaitTmo(uint16_t tmo)
{
/* Condition compilation by the difference of the operating system */
#if USBC_FW_PP == USBC_FW_OS_PP
	USBC_UTR_t		*mess;
	USBC_ER_t		err;
	uint16_t		status;

	err = USBC_TRCV_MSG(USB_CLS_MBX, (USBC_MSG_t**)&mess, (USBC_TM_t)tmo);
	if( err == USBC_E_TMOUT )
	{
		return (USBC_ERROR);
	}
	if( err != USBC_E_OK )
	{
		USBC_PRINTF1("### lib_ClassProcessWaitTmo trcv_msg error (%ld)\n"
			, err);
		return (USBC_ERROR);
	}
	status = mess->status;
	err = USBC_REL_BLK(USB_CLS_MPL, (USBC_MH_t)mess);
	if( err != USBC_E_OK )
	{
		USBC_PRINTF2(
			"### lib_ClassProcessWaitTmo (%x) rel_blk error (%ld)\n"
			, status, err);
		return (USBC_ERROR);
	}
	return (status);
#endif	/* USBC_FW_OS_PP */
/* Condition compilation by the difference of the operating system */
#if USBC_FW_PP == USBC_FW_NONOS_PP
	return USBC_E_OK;
#endif	/* USBC_FW_NONOS_PP */
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_DummyFunction
Description     : dummy function
Arguments       : uint16_t data1            : not use
                : uint16_t data2            : not use
Return value    : none
******************************************************************************/
void usb_cstd_DummyFunction(uint16_t data1, uint16_t data2)
{
}
/******************************************************************************
End of function
******************************************************************************/

/******************************************************************************
Function Name   : usb_cstd_DummyTrn
Description     : dummy function
Arguments       : USBC_REQUEST_t *data1      : not use
                : uint16_t data2            : not use
Return value    : none
******************************************************************************/
void usb_cstd_DummyTrn(USBC_REQUEST_t *data1, uint16_t data2)
{
}
/******************************************************************************
End of function
******************************************************************************/

/******************************************************************************
End  Of File
******************************************************************************/
