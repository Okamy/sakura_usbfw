/******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized.
* This software is owned by Renesas Electronics Corporation and is  protected
* under all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES
* REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY,
* INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR  A
* PARTICULAR PURPOSE AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE  EXPRESSLY
* DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE  LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES
* FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS
* AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this
* software and to discontinue the availability of this software.
* By using this software, you agree to the additional terms and
* conditions found by accessing the following link:
* http://www.renesas.com/disclaimer
*******************************************************************************
* Copyright (C) 2010(2011) Renesas Electronics Corpration
* and Renesas Solutions Corp. All rights reserved.
*******************************************************************************
* File Name    : r_usb_pClassVendor.c
* Version      : 1.10
* Device(s)    : Renesas SH-Series, RX-Series
* Tool-Chain   : Renesas SuperH RISC engine Standard Toolchain
*              : Renesas RX Standard Toolchain
* OS           : Common to None and uITRON 4.0 Spec
* H/W Platform : Independent
* Description  : USB Peripheral Vendor Class code
*******************************************************************************
* History : DD.MM.YYYY Version Description
*         : 17.03.2010 0.80    First Release
*         : 30.07.2010 0.90    Updated comments
*         : 02.08.2010 0.91    Updated comments
*         : 29.10.2010 1.00    Mass Production Release
*         : 01.06.2011 1.10    Version 1.10 Release
******************************************************************************/


/******************************************************************************
Includes   <System Includes> , "Project Includes"
******************************************************************************/
#include "r_usbc_cTypedef.h"		/* Type define */
#include "r_usb_cDefUsr.h"			/* USB-H/W register set (user define) */
#include "r_usbc_cDefUSBIP.h"		/* USB-FW Library Header */
#include "r_usb_cExtern.h"			/* USB-FW global define */
#include "r_usbc_cMacPrint.h"		/* Standard IO macro */


/******************************************************************************
Section    <Section Definition> , "Project Sections"
******************************************************************************/
#pragma section _pcd


/******************************************************************************
Constant macro definitions
******************************************************************************/


/******************************************************************************
External variables and functions
******************************************************************************/


/******************************************************************************
Private global variables and functions
******************************************************************************/
void	usb_pstd_ClassReq0(USBC_REQUEST_t *);
void	usb_pstd_ClassReq1(USBC_REQUEST_t *);
void	usb_pstd_ClassReq2(USBC_REQUEST_t *);
void	usb_pstd_ClassReq3(USBC_REQUEST_t *);
void	usb_pstd_ClassReq4(USBC_REQUEST_t *);
void	usb_pstd_ClassReq5(USBC_REQUEST_t *);
void	usb_pstd_VendorReq0(USBC_REQUEST_t *);
void	usb_pstd_VendorReq1(USBC_REQUEST_t *);
void	usb_pstd_VendorReq2(USBC_REQUEST_t *);
void	usb_pstd_VendorReq3(USBC_REQUEST_t *);
void	usb_pstd_VendorReq4(USBC_REQUEST_t *);
void	usb_pstd_VendorReq5(USBC_REQUEST_t *);


/******************************************************************************
Renesas Abstracted Peripheral Vendor Class Driver functions
******************************************************************************/

/******************************************************************************
Function Name   : usb_pstd_UsrCtrlTransFunction
Description     : Class request (idle or setup stage)
Arguments       : USBC_REQUEST_t *ptr        : request
                : uint16_t stginfo           : stage info
Return value    : none
******************************************************************************/
void usb_pstd_UsrCtrlTransFunction(USBC_REQUEST_t *ptr, uint16_t stginfo )
{
	if( ptr->ReqTypeType == USBC_CLASS )
	{
		switch( stginfo )
		{
		/* Idle or setup stage */
		case USBC_CS_IDST:	usb_pstd_ClassReq0(ptr);	break;
		/* Control read data stage */
		case USBC_CS_RDDS:	usb_pstd_ClassReq1(ptr);	break;
		/* Control write data stage */
		case USBC_CS_WRDS:	usb_pstd_ClassReq2(ptr);	break;
		/* Control write nodata status stage */
		case USBC_CS_WRND:	usb_pstd_ClassReq3(ptr);	break;
		/* Control read status stage */
		case USBC_CS_RDSS:	usb_pstd_ClassReq4(ptr);	break;
		/* Control write status stage */
		case USBC_CS_WRSS:	usb_pstd_ClassReq5(ptr);	break;

		/* Control sequence error */
		case USBC_CS_SQER:
			R_usb_pstd_ControlEnd((uint16_t)USBC_DATA_ERR);
			break;
		/* Illegal */
		default:
			R_usb_pstd_ControlEnd((uint16_t)USBC_DATA_ERR);
			break;
		}
	}
	else if( ptr->ReqTypeType == USBC_VENDOR )
	{
		switch( stginfo )
		{
		/* Idle or setup stage */
		case USBC_CS_IDST:	usb_pstd_VendorReq0(ptr);	break;
		/* Control read data stage */
		case USBC_CS_RDDS:	usb_pstd_VendorReq1(ptr);	break;
		/* Control write data stage */
		case USBC_CS_WRDS:	usb_pstd_VendorReq2(ptr);	break;
		/* Control write nodata status stage */
		case USBC_CS_WRND:	usb_pstd_VendorReq3(ptr);	break;
		/* Control read status stage */
		case USBC_CS_RDSS:	usb_pstd_VendorReq4(ptr);	break;
		/* Control write status stage */
		case USBC_CS_WRSS:	usb_pstd_VendorReq5(ptr);	break;

		/* Control sequence error */
		case USBC_CS_SQER:
			R_usb_pstd_ControlEnd((uint16_t)USBC_DATA_ERR);
			break;
		/* Illegal */
		default:
			R_usb_pstd_ControlEnd((uint16_t)USBC_DATA_ERR);
			break;
		}
	}
	else
	{
		USBC_PRINTF2("usb_pstd_UsrCtrlTransFunction Err: %x %x\n"
			, ptr->ReqTypeType, stginfo );
		R_usb_pstd_PcdChangeDeviceState( USBC_DO_STALL, USBC_PIPE0
			,(USBC_CB_INFO_t)usb_cstd_DummyFunction );
	}
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pstd_ClassReq0
Description     : Class request (idle or setup stage)
Arguments       : USBC_REQUEST_t *ptr        : request
Return value    : none
******************************************************************************/
void usb_pstd_ClassReq0(USBC_REQUEST_t *ptr)
{
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pstd_ClassReq1
Description     : Class request (control read data stage)
Arguments       : USBC_REQUEST_t *ptr        : request
Return value    : none
******************************************************************************/
void usb_pstd_ClassReq1(USBC_REQUEST_t *ptr)
{
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pstd_ClassReq2
Description     : Class Request (control write data stage)
Arguments       : USBC_REQUEST_t *ptr        : request
Return value    : none
******************************************************************************/
void usb_pstd_ClassReq2(USBC_REQUEST_t *ptr)
{
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pstd_ClassReq3
Description     : Class request (control write nodata status stage)
Arguments       : USBC_REQUEST_t *ptr        : request
Return value    : none
******************************************************************************/
void usb_pstd_ClassReq3(USBC_REQUEST_t *ptr)
{
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pstd_ClassReq4
Description     : Class request (control read status stage)
Arguments       : USBC_REQUEST_t *ptr        : request
Return value    : none
******************************************************************************/
void usb_pstd_ClassReq4(USBC_REQUEST_t *ptr)
{
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pstd_ClassReq5
Description     : Class request (control write status stage)
Arguments       : USBC_REQUEST_t *ptr        : request
Return value    : none
******************************************************************************/
void usb_pstd_ClassReq5(USBC_REQUEST_t *ptr)
{
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pstd_VendorReq0
Description     : Vendor request (idle or setup stage)
Arguments       : USBC_REQUEST_t *ptr        : request
Return value    : none
******************************************************************************/
void usb_pstd_VendorReq0(USBC_REQUEST_t *ptr)
{
	R_usb_pstd_PcdChangeDeviceState( USBC_DO_STALL, USBC_PIPE0
		, (USBC_CB_INFO_t)usb_cstd_DummyFunction );
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pstd_VendorReq1
Description     : Vendor request (control read data stage)
Arguments       : USBC_REQUEST_t *ptr        : request
Return value    : none
******************************************************************************/
void usb_pstd_VendorReq1(USBC_REQUEST_t *ptr)
{
	R_usb_pstd_PcdChangeDeviceState( USBC_DO_STALL, USBC_PIPE0
		, (USBC_CB_INFO_t)usb_cstd_DummyFunction );
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pstd_VendorReq2
Description     : Vendor request (control write data stage)
Arguments       : USBC_REQUEST_t *ptr        : request
Return value    : none
******************************************************************************/
void usb_pstd_VendorReq2(USBC_REQUEST_t *ptr)
{
	R_usb_pstd_PcdChangeDeviceState( USBC_DO_STALL, USBC_PIPE0
		, (USBC_CB_INFO_t)usb_cstd_DummyFunction );
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pstd_VendorReq3
Description     : Vendor request (control write nodata status stage)
Arguments       : USBC_REQUEST_t *ptr        : request
Return value    : none
******************************************************************************/
void usb_pstd_VendorReq3(USBC_REQUEST_t *ptr)
{
	R_usb_pstd_PcdChangeDeviceState( USBC_DO_STALL, USBC_PIPE0
		, (USBC_CB_INFO_t)usb_cstd_DummyFunction );
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pstd_VendorReq4
Description     : Vendor request (control read status stage)
Arguments       : USBC_REQUEST_t *ptr        : request
Return value    : none
******************************************************************************/
void usb_pstd_VendorReq4(USBC_REQUEST_t *ptr)
{
	R_usb_pstd_PcdChangeDeviceState( USBC_DO_STALL, USBC_PIPE0
		, (USBC_CB_INFO_t)usb_cstd_DummyFunction );
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pstd_VendorReq5
Description     : Vendor request (control write status stage)
Arguments       : USBC_REQUEST_t *ptr        : request
Return value    : none
******************************************************************************/
void usb_pstd_VendorReq5(USBC_REQUEST_t *ptr)
{
	R_usb_pstd_PcdChangeDeviceState( USBC_DO_STALL, USBC_PIPE0
			,(USBC_CB_INFO_t)usb_cstd_DummyFunction );
}
/******************************************************************************
End of function
******************************************************************************/

/******************************************************************************
End  Of File
******************************************************************************/
