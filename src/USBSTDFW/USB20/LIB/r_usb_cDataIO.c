/******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized.
* This software is owned by Renesas Electronics Corporation and is  protected
* under all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES
* REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY,
* INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR  A
* PARTICULAR PURPOSE AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE  EXPRESSLY
* DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE  LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES
* FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS
* AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this
* software and to discontinue the availability of this software.
* By using this software, you agree to the additional terms and
* conditions found by accessing the following link:
* http://www.renesas.com/disclaimer
*******************************************************************************
* Copyright (C) 2010(2011) Renesas Electronics Corpration
* and Renesas Solutions Corp. All rights reserved.
*******************************************************************************
* File Name    : r_usb_cDataIO.c
* Version      : 1.10
* Device(s)    : Renesas SH-Series, RX-Series
* Tool-Chain   : Renesas SuperH RISC engine Standard Toolchain
*              : Renesas RX Standard Toolchain
* OS           : Common to None and uITRON 4.0 Spec
* H/W Platform : Independent
* Description  : USB Host and Peripheral data I/O code
*******************************************************************************
* History : DD.MM.YYYY Version Description
*         : 17.03.2010 0.80    First Release
*         : 30.07.2010 0.90    Updated comments
*         : 02.08.2010 0.91    Updated comments
*         : 29.10.2010 1.00    Mass Production Release
*         : 01.06.2011 1.10    Version 1.10 Release
******************************************************************************/

/******************************************************************************
Includes   <System Includes> , "Project Includes"
******************************************************************************/
#include "r_usbc_cTypedef.h"		/* Type define */
#include "r_usb_cDefUsr.h"			/* USB-H/W register set (user define) */
#include "r_usbc_cDefUSBIP.h"		/* USB-FW Library Header */
#include "r_usbc_cMacPrint.h"		/* Standard IO macro */
#include "r_usb_cExtern.h"			/* USB-FW global define */


/******************************************************************************
Section    <Section Definition> , "Project Sections"
******************************************************************************/
#pragma section _usblib


/******************************************************************************
Constant macro definitions
******************************************************************************/


/******************************************************************************
External variables and functions
******************************************************************************/


/******************************************************************************
Private global variables and functions
******************************************************************************/
void usb_cstd_SetTransactCount(uint16_t pipe, uint16_t useport
	, uint32_t length, uint16_t mxps);
void usb_cstd_D0FifoselSet(void);

/******************************************************************************
Renesas Abstracted common data I/O functions
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_SendStart
Description     : Send Data start
Arguments       : uint16_t pipe			; Pipe Number
Return value    : none
******************************************************************************/
void usb_cstd_SendStart(uint16_t pipe)
{
	USBC_UTR_t		*ptr;
	uint32_t		length;
	uint16_t		useport;

	/* Evacuation pointer */
	ptr		= usb_gcstd_Pipe[pipe];
	length	= ptr->tranlen;

	/* Check transfer count */
	if( ptr->segment == USBC_TRAN_CONT )
	{
		/* Sequence toggle */
		usb_cstd_DoSqtgl(pipe, ptr->pipectr);
	}

	/* Select NAK */
	usb_cstd_SelectNak(pipe);
	/* Set data count */
	usb_gcstd_DataCnt[pipe] = length;
	/* Set data pointer */
	usb_gcstd_DataPtr[pipe] = (uint8_t*)ptr->tranadr;
	/* Ignore count clear */
	usb_ghstd_IgnoreCnt[pipe] = (uint16_t)0;

	/* BEMP Status Clear */
	USB_CLR_STS(BEMPSTS, USBC_BITSET(pipe));
	/* BRDY Status Clear */
	USB_CLR_STS(BRDYSTS, USBC_BITSET(pipe));

	/* Pipe number to FIFO port select */
	useport = usb_cstd_Pipe2Fport(pipe);
	/* Check use FIFO access */
	switch( useport )
	{
	/* CFIFO use */
	case USBC_CUSE:
		/* Buffer to FIFO data write */
		usb_cstd_Buf2Fifo(pipe, useport);
		/* Set BUF */
		usb_cstd_SetBuf(pipe);
		break;
	/* D0FIFO use */
	case USBC_D0USE:
		/* D0 FIFO access is NG */
		USBC_PRINTF1("### USB-ITRON is not support(SND-D0USE:pipe%d)\n"
			, pipe);
		usb_cstd_ForcedTermination(pipe, (uint16_t)USBC_DATA_ERR);
		break;
	/* D1FIFO use */
	case USBC_D1USE:
		/* Buffer to FIFO data write */
		usb_cstd_Buf2Fifo(pipe, useport);
		/* Set BUF */
		usb_cstd_SetBuf(pipe);
		break;
	/* D0FIFO DMA */
	case USBC_D0DMA:
		/* Setting for use PIPE number */
		usb_gcstd_Dma0Pipe	= pipe;
		/* PIPE direction */
		usb_gcstd_Dma0Dir	= usb_cstd_GetPipeDir(pipe);
		/* Buffer size */
		usb_gcstd_Dma0Fifo	= usb_cstd_GetBufSize(pipe);
		/* Check data count */
		if( usb_gcstd_DataCnt[usb_gcstd_Dma0Pipe] <= usb_gcstd_Dma0Fifo )
		{
			/* Transfer data size */
			usb_gcstd_Dma0Size
				 = (uint16_t)usb_gcstd_DataCnt[usb_gcstd_Dma0Pipe];
			/* Enable Empty Interrupt */
			usb_cstd_BempEnable(usb_gcstd_Dma0Pipe);
		}
		else
		{
			/* Data size == FIFO size */
			usb_gcstd_Dma0Size = usb_gcstd_Dma0Fifo;
		}

		/* Transfer size check */
		if( usb_gcstd_Dma0Size != 0 )
		{
/* Condition compilation by the difference of FIFO access width */
#if USBC_BUSSIZE_PP == USBC_BUSSIZE_32_PP
			/* 32bit access */
			if( (usb_gcstd_Dma0Size & 0x0003u) == 0u )
			{
#if USBC_TRANS_MODE_PP == USBC_TRANS_DMA_PP
				if( (uint32_t)usb_gcstd_DataPtr[usb_gcstd_Dma0Pipe] >= 0x08000000 && 
					(uint32_t)usb_gcstd_DataPtr[usb_gcstd_Dma0Pipe] < 0x0A000000 )
				{
					/* DMA access Buffer to FIFO start */
					USB_WR(DMA0CFG, USBC_BURST|USBC_CPU_DACK_ONLY);
					usb_cstd_Buf2D0fifoStartDma(USB_D0FIFO_32_ADR);
				}
				else
				{
					/* CPU access of the D0FIFO */
					usb_cstd_Buf2D0fifo(pipe);
				}
#else
				/* DMA access Buffer to FIFO start */
				usb_cstd_Buf2D0fifoStartDma(USB_D0FIFO_32_ADR);
#endif
			}
			else
#endif	/* USBC_BUSSIZE_32_PP */
			/* 16bit access */
			if ( (usb_gcstd_Dma0Size & 0x0001u) == 0u )
			{
#if USBC_TRANS_MODE_PP == USBC_TRANS_DMA_PP
				if( (uint32_t)usb_gcstd_DataPtr[usb_gcstd_Dma0Pipe] >= 0x08000000 && 
					(uint32_t)usb_gcstd_DataPtr[usb_gcstd_Dma0Pipe] < 0x0A000000 )
				{
					/* DMA access Buffer to FIFO start */
					USB_WR(DMA0CFG, USBC_BURST|USBC_CPU_DACK_ONLY);
					usb_cstd_Buf2D0fifoStartDma(USB_D0FIFO_16_ADR);
				}
				else
				{
					/* CPU access of the D0FIFO */
					usb_cstd_Buf2D0fifo(pipe);
				}
#else
				/* DMA access Buffer to FIFO start */
				usb_cstd_Buf2D0fifoStartDma(USB_D0FIFO_16_ADR);
#endif
			}
			else
			{
				/* 8bit access */
#if USBC_TRANS_MODE_PP == USBC_TRANS_DMA_PP
				if( (uint32_t)usb_gcstd_DataPtr[usb_gcstd_Dma0Pipe] >= 0x08000000 && 
					(uint32_t)usb_gcstd_DataPtr[usb_gcstd_Dma0Pipe] < 0x0A000000 )
				{
					/* DMA access Buffer to FIFO start */
					USB_WR(DMA0CFG, USBC_BURST|USBC_CPU_DACK_ONLY);
					usb_cstd_Buf2D0fifoStartDma(USB_D0FIFO_08_ADR);
				}
				else
				{
					/* CPU access of the D0FIFO */
					usb_cstd_Buf2D0fifo(pipe);
				}
#else
				/* DMA access Buffer to FIFO start */
				usb_cstd_Buf2D0fifoStartDma(USB_D0FIFO_08_ADR);
#endif
			}
			/* Changes the FIFO port by the pipe. */
			usb_cstd_FPortChange2(pipe,useport, USBC_NO);
			/* Enable Not Ready Interrupt */
			usb_cstd_NrdyEnable(pipe);
			/* CPU access Buffer to FIFO start */
			usb_cstd_Buf2D0fifoStartUsb();
		}
		else
		{
			/* Buffer to FIFO data write */
			usb_cstd_Buf2Fifo(pipe, useport);
		}
		/* Set BUF */
		usb_cstd_SetBuf(pipe);
		break;
	/* D1FIFO DMA */
	case USBC_D1DMA:
		/* D1 FIFO access is NG */
		USBC_PRINTF1("### USB-ITRON is not support(SND-D1DMA:pipe%d)\n"
			, pipe);
		usb_cstd_ForcedTermination(pipe, (uint16_t)USBC_DATA_ERR);
		break;
	default:
		/* Access is NG */
		USBC_PRINTF1("### USB-ITRON is not support(SND-else:pipe%d)\n"
			, pipe);
		usb_cstd_ForcedTermination(pipe, (uint16_t)USBC_DATA_ERR);
		break;
	}
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_Buf2Fifo
Description     : Buffer to FIFO data write
Arguments       : uint16_t pipe			; Pipe Number
Return value    : none
******************************************************************************/
void usb_cstd_Buf2Fifo(uint16_t pipe, uint16_t useport)
{
	uint16_t	end_flag;

	/* Disable Ready Interrupt */
	usb_cstd_BrdyDisable(pipe);
	/* Ignore count clear */
	usb_ghstd_IgnoreCnt[pipe] = (uint16_t)0;
	/*  */
	/* Check use FIFO access */
	switch( useport )
	{
	/* CFIFO use */
	case USBC_CUSE:
		/* Buffer to CFIFO write */
		end_flag = usb_cstd_Buf2Cfifo(pipe);
		break;
	/* D0FIFO use */
	case USBC_D0USE:
		/* D0FIFO access is NG */
		USBC_PRINTF0("### CPU access of the D0FIFO is not support \n");
		break;
	/* D1FIFO use */
	case USBC_D1USE:
		/* Buffer to D1FIFO write */
		end_flag = usb_cstd_Buf2D1fifo(pipe);
		break;
	/* D0FIFO DMA */
	case USBC_D0DMA:
		/* This case is to send zero length packet */
		/* continue */
	/* D1FIFO DMA */
	case USBC_D1DMA:
		/* This case is to send zero length packet */
		/* Buffer to D1FIFO write */
		end_flag = usb_cstd_Buf2D1fifo(pipe);
		break;
	default:
		/* Access is NG */
		USBC_PRINTF0("### Not support of FIFO-port access command \n");
		end_flag = USBC_ERROR;
		break;
	}

	/* Check FIFO access sequence */
	switch( end_flag )
	{
	case USBC_WRITING:
		/* Continue of data write */
		/* Enable Ready Interrupt */
		usb_cstd_BrdyEnable(pipe);
		/* Enable Not Ready Interrupt */
		usb_cstd_NrdyEnable(pipe);
		break;
	case USBC_WRITEEND:
		/* End of data write */
		/* continue */
	case USBC_WRITESHRT:
		/* End of data write */
		/* Enable Empty Interrupt */
		usb_cstd_BempEnable(pipe);
		/* Enable Not Ready Interrupt */
		usb_cstd_NrdyEnable(pipe);
		break;
	case USBC_FIFOERROR:
		/* FIFO access error */
		USBC_PRINTF0("### FIFO access error \n");
		usb_cstd_ForcedTermination(pipe, (uint16_t)USBC_DATA_ERR);
		break;
	default:
		usb_cstd_ForcedTermination(pipe, (uint16_t)USBC_DATA_ERR);
		break;
	}
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_Buf2Cfifo
Description     : Buffer to CFIFO data write
Arguments       : uint16_t pipe			; Pipe Number
Return value    : uint16_t end_flag
******************************************************************************/
uint16_t usb_cstd_Buf2Cfifo(uint16_t pipe)
{
	uint16_t	size, count, even, buffer, mxps;
	uint16_t	end_flag;

	/* Check use PIPE */
	if( pipe == USBC_PIPE0 )
	{
		/* Changes FIFO port by the pipe. */
		buffer = usb_cstd_FPortChange1(pipe, (uint16_t)USBC_CUSE
			, (uint16_t)USBC_ISEL);
	}
	else
	{
		/* Changes FIFO port by the pipe. */
		buffer = usb_cstd_FPortChange1(pipe, (uint16_t)USBC_CUSE, USBC_NO);
	}
	/* Check error */
	if( buffer == USBC_FIFOERROR )
	{
		/* FIFO access error */
		return (USBC_FIFOERROR);
	}
	/* Data buffer size */
	size = usb_cstd_GetBufSize(pipe);
	/* Max Packet Size */
	mxps = usb_cstd_GetMaxPacketSize(pipe);

	/* Data size check */
	if( usb_gcstd_DataCnt[pipe] <= (uint32_t)size )
	{
		count = (uint16_t)usb_gcstd_DataCnt[pipe];
		/* Data count check */
		if( count == 0 )
		{
			/* Null Packet is end of write */
			end_flag = USBC_WRITESHRT;
		}
		else if( (count % mxps) != 0 )
		{
			/* Short Packet is end of write */
			end_flag = USBC_WRITESHRT;
		}
		else if( pipe == USBC_PIPE0 )
		{
			/* Just Send Size */
			end_flag = USBC_WRITING;
		}
		else
		{
			/* Write continues */
			end_flag = USBC_WRITEEND;
		}
	}
	else
	{
		/* Write continues */
		end_flag = USBC_WRITING;
		count = size;
	}
/* Condition compilation by the difference of FIFO access width */
#if USBC_BUSSIZE_PP == USBC_BUSSIZE_32_PP
	/* Check access size */
	for( even = (uint16_t)(count >> 2u); (even != 0u); --even )
	{
		/* 32bit access */
		/* FIFO write */
		USB_WR_FF(USB_CFIFO_32, *((uint32_t*)usb_gcstd_DataPtr[pipe]));
		/* Renewal write pointer */
		usb_gcstd_DataPtr[pipe]++;
		usb_gcstd_DataPtr[pipe]++;
		usb_gcstd_DataPtr[pipe]++;
		usb_gcstd_DataPtr[pipe]++;
	}
	if( (count & (uint16_t)0x0002u) != 0u )
	{
		/* 16bit access */
		/* count == 3 or 2 */
		/* Change FIFO access width */
		USB_MDF_PAT(CFIFOSEL, USBC_MBW_16, USBC_MBW);
		/* FIFO write */
		USB_WR_FF(USB_CFIFO_16, *((uint16_t*)usb_gcstd_DataPtr[pipe]));
		/* Return FIFO access width */
		USB_MDF_PAT(CFIFOSEL, USB_CFIFO_USBC_MBW, USBC_MBW);
		/* Renewal write pointer */
		usb_gcstd_DataPtr[pipe]++;
		usb_gcstd_DataPtr[pipe]++;
	}
#else	/* !USBC_BUSSIZE_32_PP */
	for( even = (uint16_t)(count >> 1); (even != 0); --even )
	{
		/* 16bit access */
		USB_WR_FF(USB_CFIFO_16, *((uint16_t*)usb_gcstd_DataPtr[pipe]));
		/* Renewal write pointer */
		usb_gcstd_DataPtr[pipe]++;
		usb_gcstd_DataPtr[pipe]++;
	}
#endif	/* USBC_BUSSIZE_32_PP */

	if( (count & (uint16_t)0x0001u) != 0u )
	{
		/* 8bit access */
		/* count == odd */
		/* Change FIFO access width */
		USB_MDF_PAT(CFIFOSEL, USBC_MBW_8, USBC_MBW);
		/* FIFO write */
		USB_WR_FF(USB_CFIFO_08, *usb_gcstd_DataPtr[pipe]);
		/* Return FIFO access width */
		USB_MDF_PAT(CFIFOSEL, USB_CFIFO_MBW, USBC_MBW);
		/* Renewal write pointer */
		usb_gcstd_DataPtr[pipe]++;
	}

	/* Check data count to remain */
	if( usb_gcstd_DataCnt[pipe] < (uint32_t)size )
	{
		/* Clear data count */
		usb_gcstd_DataCnt[pipe] = (uint32_t)0u;
		/* Read CFIFOCTR */
		USB_RD(CFIFOCTR, buffer);
		/* Check BVAL */
		if( (buffer & USBC_BVAL) == 0u )
		{
			/* Short Packet */
			USB_WR(CFIFOCTR, USBC_BVAL);
		}
	}
	else
	{
		/* Total data count - count */
		usb_gcstd_DataCnt[pipe] -= count;
	}
	/* End or Err or Continue */
	return end_flag;
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_Buf2D0fifo
Description     : Buffer to D0FIFO data write
Arguments       : uint16_t pipe			; Pipe Number
Return value    : uint16_t end_flag
******************************************************************************/
uint16_t usb_cstd_Buf2D0fifo(uint16_t pipe)
{
	uint16_t	size, count, even, buffer, mxps;
	uint16_t	end_flag;

	/* Changes FIFO port by the pipe. */
	buffer = usb_cstd_FPortChange1(pipe, (uint16_t)USBC_D0USE, USBC_NO);
	/* Check error */
	if( buffer == USBC_FIFOERROR )
	{
		/* FIFO access error */
		return (USBC_FIFOERROR);
	}
	/* Data buffer size */
	size = usb_cstd_GetBufSize(pipe);
	/* Max Packet Size */
	mxps = usb_cstd_GetMaxPacketSize(pipe);

	/* Data size check */
	if( usb_gcstd_DataCnt[pipe] <= (uint32_t)size )
	{
		count = (uint16_t)usb_gcstd_DataCnt[pipe];
		/* Data count check */
		if( count == 0 )
		{
			/* Null Packet is end of write */
			end_flag = USBC_WRITESHRT;
		}
		else if( (count % mxps) != 0 )
		{
			/* Short Packet is end of write */
			end_flag = USBC_WRITESHRT;
		}
		else
		{
			/* Write continues */
			end_flag = USBC_WRITEEND;
		}
	}
	else
	{
		/* Write continues */
		end_flag = USBC_WRITING;
		count = size;
	}
/* Condition compilation by the difference of FIFO access width */
#if USBC_BUSSIZE_PP == USBC_BUSSIZE_32_PP
	/* Check access size */
	for( even = (uint16_t)(count >> 2); (even != 0); --even )
	{
		/* 32bit access */
		/* FIFO write */
		USB_WR_FF(USB_D0FIFO_32, *((uint32_t*)usb_gcstd_DataPtr[pipe]));
		/* Renewal write pointer */
		usb_gcstd_DataPtr[pipe]++;
		usb_gcstd_DataPtr[pipe]++;
		usb_gcstd_DataPtr[pipe]++;
		usb_gcstd_DataPtr[pipe]++;
	}
	if( (count & 0x0002u) != 0u )
	{
		/* 16bit access */
		/* count == 3 or 2 */
		/* Change FIFO access width */
		USB_MDF_PAT(D0FIFOSEL, USBC_MBW_16, USBC_MBW);
		/* FIFO write */
		USB_WR_FF(USB_D0FIFO_16, *((uint16_t*)usb_gcstd_DataPtr[pipe]));
		/* Return FIFO access width */
		USB_MDF_PAT(D0FIFOSEL, USB_D0FIFO_MBW, USBC_MBW);
		/* Renewal write pointer */
		usb_gcstd_DataPtr[pipe]++;
		usb_gcstd_DataPtr[pipe]++;
	}
#else	/* !USBC_BUSSIZE_32_PP */
	for( even = (uint16_t)(count >> 1); (even != 0); --even )
	{
		/* 16bit access */
		/* FIFO write */
		USB_WR_FF(USB_D0FIFO_16, *((uint16_t*)usb_gcstd_DataPtr[pipe]));
		/* Renewal write pointer */
		usb_gcstd_DataPtr[pipe]++;
		usb_gcstd_DataPtr[pipe]++;
	}
#endif	/* USBC_BUSSIZE_32_PP */

	if( (count & 0x0001u) != 0u )
	{
		/* 8bit access */
		/* count == odd */
		/* Change FIFO access width */
		USB_MDF_PAT(D0FIFOSEL, USBC_MBW_8, USBC_MBW);
		/* FIFO write */
		USB_WR_FF(USB_D0FIFO_08, *usb_gcstd_DataPtr[pipe]);
		/* Return FIFO access width */
		USB_MDF_PAT(D0FIFOSEL, USB_D0FIFO_MBW, USBC_MBW);
		/* Renewal write pointer */
		usb_gcstd_DataPtr[pipe]++;
	}

	/* Check data count to remain */
	if( usb_gcstd_DataCnt[pipe] < (uint32_t)size)
	{
		/* Clear data count */
		usb_gcstd_DataCnt[pipe] = (uint32_t)0u;
		/* Read D0FIFOCTR */
		USB_RD(D0FIFOCTR, buffer);
		/* Check BVAL */
		if( (buffer & USBC_BVAL) == 0u )
		{
			/* Short Packet */
			USB_WR(D0FIFOCTR, USBC_BVAL);
		}
	}
	else
	{
		/* Total data count - count */
		usb_gcstd_DataCnt[pipe] -= count;
	}
	/* End or Err or Continue */
	return (end_flag);
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_Buf2D1fifo
Description     : Buffer to D1FIFO data write
Arguments       : uint16_t pipe			; Pipe Number
Return value    : uint16_t end_flag
******************************************************************************/
uint16_t usb_cstd_Buf2D1fifo(uint16_t pipe)
{
	uint16_t	size, count, even, buffer, mxps;
	uint16_t	end_flag;

	/* Changes FIFO port by the pipe. */
	buffer = usb_cstd_FPortChange1(pipe, (uint16_t)USBC_D1USE, USBC_NO);
	/* Check error */
	if( buffer == USBC_FIFOERROR )
	{
		/* FIFO access error */
		return (USBC_FIFOERROR);
	}
	/* Data buffer size */
	size = usb_cstd_GetBufSize(pipe);
	/* Max Packet Size */
	mxps = usb_cstd_GetMaxPacketSize(pipe);

	/* Data size check */
	if( usb_gcstd_DataCnt[pipe] <= (uint32_t)size )
	{
		count = (uint16_t)usb_gcstd_DataCnt[pipe];
		/* Data count check */
		if( count == 0 )
		{
			/* Null Packet is end of write */
			end_flag = USBC_WRITESHRT;
		}
		else if( (count % mxps) != 0 )
		{
			/* Short Packet is end of write */
			end_flag = USBC_WRITESHRT;
		}
		else
		{
			/* Write continues */
			end_flag = USBC_WRITEEND;
		}
	}
	else
	{
		/* Write continues */
		end_flag = USBC_WRITING;
		count = size;
	}
/* Condition compilation by the difference of FIFO access width */
#if USBC_BUSSIZE_PP == USBC_BUSSIZE_32_PP
	/* Check access size */
	for( even = (uint16_t)(count >> 2); (even != 0); --even )
	{
		/* 32bit access */
		/* FIFO write */
		USB_WR_FF(USB_D1FIFO_32, *((uint32_t*)usb_gcstd_DataPtr[pipe]));
		/* Renewal write pointer */
		usb_gcstd_DataPtr[pipe]++;
		usb_gcstd_DataPtr[pipe]++;
		usb_gcstd_DataPtr[pipe]++;
		usb_gcstd_DataPtr[pipe]++;
	}
	if( (count & 0x0002u) != 0u )
	{
		/* 16bit access */
		/* count == 3 or 2 */
		/* Change FIFO access width */
		USB_MDF_PAT(D1FIFOSEL, USBC_MBW_16, USBC_MBW);
		/* FIFO write */
		USB_WR_FF(USB_D1FIFO_16, *((uint16_t*)usb_gcstd_DataPtr[pipe]));
		/* Return FIFO access width */
		USB_MDF_PAT(D1FIFOSEL, USB_D1FIFO_MBW, USBC_MBW);
		/* Renewal write pointer */
		usb_gcstd_DataPtr[pipe]++;
		usb_gcstd_DataPtr[pipe]++;
	}
#else	/* !USBC_BUSSIZE_32_PP */
	for( even = (uint16_t)(count >> 1); (even != 0); --even )
	{
		/* 16bit access */
		/* FIFO write */
		USB_WR_FF(USB_D1FIFO_16, *((uint16_t*)usb_gcstd_DataPtr[pipe]));
		/* Renewal write pointer */
		usb_gcstd_DataPtr[pipe]++;
		usb_gcstd_DataPtr[pipe]++;
	}
#endif	/* USBC_BUSSIZE_32_PP */

	if( (count & 0x0001u) != 0u )
	{
		/* 8bit access */
		/* count == odd */
		/* Change FIFO access width */
		USB_MDF_PAT(D1FIFOSEL, USBC_MBW_8, USBC_MBW);
		/* FIFO write */
		USB_WR_FF(USB_D1FIFO_08, *usb_gcstd_DataPtr[pipe]);
		/* Return FIFO access width */
		USB_MDF_PAT(D1FIFOSEL, USB_D1FIFO_MBW, USBC_MBW);
		/* Renewal write pointer */
		usb_gcstd_DataPtr[pipe]++;
	}

	/* Check data count to remain */
	if( usb_gcstd_DataCnt[pipe] < (uint32_t)size)
	{
		/* Clear data count */
		usb_gcstd_DataCnt[pipe] = (uint32_t)0u;
		/* Read D1FIFOCTR */
		USB_RD(D1FIFOCTR, buffer);
		/* Check BVAL */
		if( (buffer & USBC_BVAL) == 0u )
		{
			/* Short Packet */
			USB_WR(D1FIFOCTR, USBC_BVAL);
		}
	}
	else
	{
		/* Total data count - count */
		usb_gcstd_DataCnt[pipe] -= count;
	}
	/* End or Err or Continue */
	return (end_flag);
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_ReceiveStart
Description     : Receive Data start
Arguments       : uint16_t pipe			; Pipe Number
Return value    : none
******************************************************************************/
void usb_cstd_ReceiveStart(uint16_t pipe)
{
	USBC_UTR_t		*ptr;
	uint32_t		length;
	uint16_t		mxps, useport;

	/* Evacuation pointer */
	ptr		= usb_gcstd_Pipe[pipe];
	length	= ptr->tranlen;

	/* Check transfer count */
	if( ptr->segment == USBC_TRAN_CONT )
	{
		/* Sequence toggle */
		usb_cstd_DoSqtgl(pipe, ptr->pipectr);
	}

	/* Select NAK */
	usb_cstd_SelectNak(pipe);
	/* Set data count */
	usb_gcstd_DataCnt[pipe] = length;
	/* Set data pointer */
	usb_gcstd_DataPtr[pipe] = (uint8_t*)ptr->tranadr;

	/* Ignore count clear */
	usb_ghstd_IgnoreCnt[pipe] = (uint16_t)0u;

	/* Pipe number to FIFO port select */
	useport	= usb_cstd_Pipe2Fport(pipe);

/* Condition compilation by the difference of transfer type */
#if USBC_PERIODIC_MODE_PP == USBC_NONPERIODIC_PP
	/* Nothing */
#else	/* !USBC_NONPERIODIC_PP */
	if( usb_cstd_ReadBsts(pipe) != 0u )
	{
		usb_cstd_Fifo2Buf(pipe, useport);
	}
#endif	/* USBC_NONPERIODIC_PP */

	/* Check use FIFO access */
	switch( useport )
	{
	/* D0FIFO use */
	case USBC_D0USE:
		/* D0 FIFO access is NG */
		USBC_PRINTF1("### USB-ITRON is not support(RCV-D0USE:pipe%d)\n"
			, pipe);
		usb_cstd_ForcedTermination(pipe, (uint16_t)USBC_DATA_ERR);
		break;
	/* CFIFO use */
	case USBC_CUSE:
		/* continue */
	/* D1FIFO use */
	case USBC_D1USE:
		/* Changes the FIFO port by the pipe. */
		usb_cstd_FPortChange2(pipe,useport, USBC_NO);
		/* Max Packet Size */
		mxps = usb_cstd_GetMaxPacketSize(pipe);
		if( length != (uint32_t)0u )
		{
			usb_cstd_SetTransactCount(pipe, useport, length, mxps);
		}
		/* Set BUF */
		usb_cstd_SetBuf(pipe);
		/* Enable Ready Interrupt */
		usb_cstd_BrdyEnable(pipe);
		/* Enable Not Ready Interrupt */
		usb_cstd_NrdyEnable(pipe);
		break;
	/* D0FIFO DMA */
	case USBC_D0DMA:
		/* Setting for use PIPE number */
		usb_gcstd_Dma0Pipe	= pipe;
		/* PIPE direction */
		usb_gcstd_Dma0Dir	= usb_cstd_GetPipeDir(pipe);
		/* Buffer size */
		usb_gcstd_Dma0Fifo	= usb_cstd_GetBufSize(pipe);
		/* Check data count */
		if( usb_gcstd_DataCnt[usb_gcstd_Dma0Pipe] < usb_gcstd_Dma0Fifo )
		{
			/* Transfer data size */
			usb_gcstd_Dma0Size
				 = (uint16_t)usb_gcstd_DataCnt[usb_gcstd_Dma0Pipe];
		}
		else
		{
			/* Data size == FIFO size */
			usb_gcstd_Dma0Size = usb_gcstd_Dma0Fifo;
		}
		/* Data size check */
		if( usb_gcstd_Dma0Size != 0u )
		{
/* Condition compilation by the difference of FIFO access width */
#if USBC_BUSSIZE_PP == USBC_BUSSIZE_32_PP
			/* 32bit access */
			if( (usb_gcstd_Dma0Size & 0x0003u) == 0u )
			{
				/* D0FIFO to Buffer DMA read start */
				usb_cstd_D0fifo2BufStartDma(USB_D0FIFO_32_ADR);
			}
			else
#endif	/* USBC_BUSSIZE_32_PP */
			/* 16bit access */
			if( (usb_gcstd_Dma0Size & 0x0001u) == 0u )
			{
				/* D0FIFO to Buffer DMA read start */
				usb_cstd_D0fifo2BufStartDma(USB_D0FIFO_16_ADR);
			}
			else
			{
				/* 8bit access */
				/* D0FIFO to Buffer DMA read start */
				usb_cstd_D0fifo2BufStartDma(USB_D0FIFO_08_ADR);
			}

			/* Changes the FIFO port by the pipe. */
			usb_cstd_FPortChange2(pipe,useport, USBC_NO);
			/* Max Packet Size */
			mxps = usb_cstd_GetMaxPacketSize(pipe);
			if( length != (uint32_t)0u )
			{
				usb_cstd_SetTransactCount(pipe, useport, length, mxps);
			}
			/* Set BUF */
			usb_cstd_SetBuf(pipe);
			/* Enable Ready Interrupt */
			usb_cstd_BrdyEnable(pipe);
			/* Enable Not Ready Interrupt */
			usb_cstd_NrdyEnable(pipe);
			usb_cstd_D0fifo2BufStartUsb();
		}
		else
		{
			/* Changes the FIFO port by the pipe. */
			usb_cstd_FPortChange2(pipe, useport, USBC_NO);
			/* DMA buffer clear mode set */
			USB_SET_PAT(D0FIFOSEL, USBC_DCLRM);
			/* Set BUF */
			usb_cstd_SetBuf(pipe);
			/* Enable Ready Interrupt */
			usb_cstd_BrdyEnable(pipe);
			/* Enable Not Ready Interrupt */
			usb_cstd_NrdyEnable(pipe);
		}
		break;
	/* D1FIFO DMA */
	case USBC_D1DMA:
		/* D1 FIFO access is NG */
		USBC_PRINTF1("### USB-ITRON is not support(RCV-D1DMA:pipe%d)\n"
			, pipe);
		usb_cstd_ForcedTermination(pipe, (uint16_t)USBC_DATA_ERR);
		break;
	default:
		USBC_PRINTF1("### USB-ITRON is not support(RCV-else:pipe%d)\n"
			, pipe);
		usb_cstd_ForcedTermination(pipe, (uint16_t)USBC_DATA_ERR);
		break;
	}
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_Fifo2Buf
Description     : FIFO to Buffer data read
                : CFIFO		: Operation on the conditions of CNTMD=0 is possible
                : D1FIFO	: Ends by the transaction counter or short-packet
Arguments       : uint16_t pipe			; Pipe Number
Return value    : none
******************************************************************************/
void usb_cstd_Fifo2Buf(uint16_t pipe, uint16_t useport)
{
/* Condition compilation by the difference of transfer type */
#if USBC_PERIODIC_MODE_PP == USBC_PERIODIC_PP
	uint16_t	size;
#endif	/* USBC_PERIODIC_PP */
	uint16_t	end_flag;

	/* Ignore count clear */
	usb_ghstd_IgnoreCnt[pipe] = (uint16_t)0;
	end_flag = USBC_ERROR;
	/* Check use FIFO */
	switch( useport )
	{
	/* CFIFO use */
	case USBC_CUSE:
		/* CFIFO to buffer read */
		end_flag = usb_cstd_Cfifo2Buf(pipe);
		break;
	/* D0FIFO use */
	case USBC_D0USE:
		USBC_PRINTF0("### CPU access of the D0FIFO is not support \n");
		break;
	/* D1FIFO use */
	case USBC_D1USE:
		/* D1FIFO to buffer read */
		end_flag = usb_cstd_D1fifo2Buf(pipe);
		break;
	/* D0FIFO DMA */
	case USBC_D0DMA:
		/* continue */
	/* D1FIFO DMA */
	case USBC_D1DMA:
		USBC_PRINTF0("### DMA read error \n");
		break;
	default:
		USBC_PRINTF0("### Not support of FIFO-port access command \n");
		break;
	}

	/* Check FIFO access sequence */
	switch( end_flag )
	{
	case USBC_READING:
		/* Continue of data read */
		break;
	case USBC_READEND:
		/* End of data read */
		usb_cstd_DataEnd(pipe, (uint16_t)USBC_DATA_OK);
		break;
	case USBC_READSHRT:
		/* End of data read */
		usb_cstd_DataEnd(pipe, (uint16_t)USBC_DATA_SHT);
		break;
	case USBC_READOVER:
		/* Buffer over */
/* Condition compilation by the difference of transfer type */
#if USBC_PERIODIC_MODE_PP == USBC_NONPERIODIC_PP
		USBC_PRINTF1("### Receive data over PIPE%d\n",pipe);
		usb_cstd_ForcedTermination(pipe, (uint16_t)USBC_DATA_OVR);
#else	/* !USBC_NONPERIODIC_PP */
		/* Data buffer size */
		size = usb_cstd_GetBufSize(pipe);
		if( usb_gcstd_DataCnt[pipe] != size )
		{
			USBC_PRINTF1("### Receive data over PIPE%d\n", pipe);
			usb_cstd_ForcedTermination(pipe, USBC_DATA_OVR);
		}
#endif	/* USBC_NONPERIODIC_PP */
		break;
	case USBC_FIFOERROR:
		/* FIFO access error */
		USBC_PRINTF0("### FIFO access error \n");
		usb_cstd_ForcedTermination(pipe, (uint16_t)USBC_DATA_ERR);
		break;
	default:
		usb_cstd_ForcedTermination(pipe, (uint16_t)USBC_DATA_ERR);
		break;
	}
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_Cfifo2Buf
Description     : CFIFO to Buffer data read
                : Operation on the conditions of DBLB=0/CNTMD=0 is possible.
Arguments       : uint16_t pipe			; Pipe Number
Return value    : uint16_t end_flag
******************************************************************************/
uint16_t usb_cstd_Cfifo2Buf(uint16_t pipe)
{
	uint16_t	count, even, buffer, mxps, dtln;
	uint16_t	end_flag;

	/* Changes FIFO port by the pipe. */
	buffer = usb_cstd_FPortChange1(pipe, (uint16_t)USBC_CUSE, USBC_NO);
	if( buffer == USBC_FIFOERROR )
	{
		/* FIFO access error */
		return (USBC_FIFOERROR);
	}
	dtln = (uint16_t)(buffer & USBC_DTLN);
	/* Max Packet Size */
	mxps = usb_cstd_GetMaxPacketSize(pipe);

	if( usb_gcstd_DataCnt[pipe] < dtln )
	{
		/* Buffer Over ? */
		end_flag = USBC_READOVER;
		/* Set NAK */
		usb_cstd_SetNak(pipe);
		count = (uint16_t)usb_gcstd_DataCnt[pipe];
		usb_gcstd_DataCnt[pipe] = dtln;
	}
	else if( usb_gcstd_DataCnt[pipe] == dtln )
	{
		/* Just Receive Size */
		count = dtln;
		if( (pipe == USBC_PIPE0) && ((dtln % mxps) == 0) )
		{
			/* Just Receive Size */
			if( usb_cstd_FunctionUsbip() == USBC_NO )
			{
				/* Peripheral Function */
				end_flag = USBC_READING;
			}
			else
			{
				/* Host Function */
				end_flag = USBC_READEND;
				/* Set NAK */
				usb_cstd_SelectNak(pipe);
			}
		}
		else
		{
			end_flag = USBC_READEND;
			/* Set NAK */
			usb_cstd_SelectNak(pipe);
		}
	}
	else
	{
		/* Continus Receive data */
		count = dtln;
		end_flag = USBC_READING;
		if( count == 0 )
		{
			/* Null Packet receive */
			end_flag = USBC_READSHRT;
			/* Select NAK */
			usb_cstd_SelectNak(pipe);
		}
		if( (count % mxps) != 0 )
		{
			/* Null Packet receive */
			end_flag = USBC_READSHRT;
			/* Select NAK */
			usb_cstd_SelectNak(pipe);
		}
	}

	if( dtln == 0 )
	{
		/* 0 length packet */
		/* Clear BVAL */
		USB_WR(CFIFOCTR, USBC_BCLR);
	}
	else
	{
/* Condition compilation by the difference of FIFO access width */
#if USBC_BUSSIZE_PP == USBC_BUSSIZE_32_PP
		for( even = (uint16_t)(count >> 2); (even != 0); --even )
		{
			/* 32bit FIFO access */
			USB_RD_FF(USB_CFIFO_32, *((uint32_t*)usb_gcstd_DataPtr[pipe]));
			/* Renewal read pointer */
			usb_gcstd_DataPtr[pipe]++;
			usb_gcstd_DataPtr[pipe]++;
			usb_gcstd_DataPtr[pipe]++;
			usb_gcstd_DataPtr[pipe]++;
		}
		if( (count & (uint16_t)0x0003) == 3 )
		{
			/* 16bit FIFO access */
			USB_RD_FF(USB_CFIFO_16, *((uint16_t*)usb_gcstd_DataPtr[pipe]));
			/* Renewal read pointer */
			usb_gcstd_DataPtr[pipe]++;
			usb_gcstd_DataPtr[pipe]++;
			/* 8bit FIFO access */
			USB_RD_FF(USB_CFIFO_08, *usb_gcstd_DataPtr[pipe]);
			/* Renewal read pointer */
			usb_gcstd_DataPtr[pipe]++;
		}
		else if( (count & (uint16_t)0x0002) != 0 )
		{
			/* 16bit FIFO access */
			USB_RD_FF(USB_CFIFO_16, *((uint16_t*)usb_gcstd_DataPtr[pipe]));
			/* Renewal read pointer */
			usb_gcstd_DataPtr[pipe]++;
			usb_gcstd_DataPtr[pipe]++;
		}
		else if( (count & (uint16_t)0x0001) != 0 )
		{
			/* 8bit FIFO access */
			USB_RD_FF(USB_CFIFO_08, *usb_gcstd_DataPtr[pipe]);
			/* Renewal read pointer */
			usb_gcstd_DataPtr[pipe]++;
		}
#else	/* !USBC_BUSSIZE_32_PP */
		for( even = (uint16_t)(count >> 1); (even != 0); --even )
		{
			/* 16bit FIFO access */
			USB_RD_FF(USB_CFIFO_16, *((uint16_t*)usb_gcstd_DataPtr[pipe]));
			/* Renewal read pointer */
			usb_gcstd_DataPtr[pipe]++;
			usb_gcstd_DataPtr[pipe]++;
		}
		if( (count & (uint16_t)0x0001) != 0 )
		{
			/* Change FIFO access width */
			USB_MDF_PAT(CFIFOSEL, USBC_MBW_8, USBC_MBW);
			
			/* 8bit FIFO access */
			USB_RD_FF(USB_CFIFO_08, *usb_gcstd_DataPtr[pipe]);
			
			/* Return FIFO access width */
			USB_MDF_PAT(CFIFOSEL, USB_CFIFO_MBW, USBC_MBW);
			
			/* Renewal read pointer */
			usb_gcstd_DataPtr[pipe]++;
		}
#endif	/* USBC_BUSSIZE_32_PP */

	}
	usb_gcstd_DataCnt[pipe] -= count;
	/* End or Err or Continue */
	return (end_flag);
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_D1fifo2Buf
Description     : D1FIFO to Buffer data read
Arguments       : uint16_t pipe			; Pipe Number
Return value    : uint16_t end_flag
Note            : Ends by the transaction counter or short-packet
******************************************************************************/
uint16_t usb_cstd_D1fifo2Buf(uint16_t pipe)
{
	uint16_t	count, even, buffer, mxps, dtln;
	uint16_t	end_flag;

	/* Changes FIFO port by the pipe. */
	buffer = usb_cstd_FPortChange1(pipe, (uint16_t)USBC_D1USE, USBC_NO);
	if( buffer == USBC_FIFOERROR )
	{
		/* FIFO access error */
		return (USBC_FIFOERROR);
	}
	dtln = (uint16_t)(buffer & USBC_DTLN);
	/* Max Packet Size */
	mxps = usb_cstd_GetMaxPacketSize(pipe);

	/* Check receive data length */
	if( usb_gcstd_DataCnt[pipe] < dtln )
	{
		/* Buffer Over ? */
		end_flag = USBC_READOVER;
		/* Set NAK */
		usb_cstd_SetNak(pipe);
		count = (uint16_t)usb_gcstd_DataCnt[pipe];
		usb_gcstd_DataCnt[pipe] = dtln;
	}
	else if( usb_gcstd_DataCnt[pipe] == dtln )
	{
		/* Just Receive Size */
		end_flag = USBC_READEND;
		/* Select NAK */
		usb_cstd_SelectNak(pipe);
		count = dtln;
	}
	else
	{
		/* Continus Receive data */
		end_flag = USBC_READING;
		count = dtln;
		if( count == 0 )
		{
			/* Null Packet receive */
			end_flag = USBC_READSHRT;
			/* Select NAK */
			usb_cstd_SelectNak(pipe);
		}
		if( (count % mxps) != 0 )
		{
			/* Short Packet receive */
			end_flag = USBC_READSHRT;
			/* Select NAK */
			usb_cstd_SelectNak(pipe);
		}
	}

	if( dtln == 0 )
	{
		/* 0 length packet */
		/* Clear BVAL */
		USB_WR(D1FIFOCTR, USBC_BCLR);
	}
	else
	{
/* Condition compilation by the difference of FIFO access width */
#if USBC_BUSSIZE_PP == USBC_BUSSIZE_32_PP
		for( even = (uint16_t)(count >> 2); (even!=0) ; --even )
		{
			/* 32bit FIFO access */
			USB_RD_FF(USB_D1FIFO_32, *((uint32_t*)usb_gcstd_DataPtr[pipe]));
			/* Renewal read pointer */
			usb_gcstd_DataPtr[pipe]++;
			usb_gcstd_DataPtr[pipe]++;
			usb_gcstd_DataPtr[pipe]++;
			usb_gcstd_DataPtr[pipe]++;
		}
		if( (count & (uint16_t)0x0003) == 3 )
		{
			/* 16bit FIFO access */
			USB_RD_FF(USB_D1FIFO_16, *((uint16_t*)usb_gcstd_DataPtr[pipe]));
			/* Renewal read pointer */
			usb_gcstd_DataPtr[pipe]++;
			usb_gcstd_DataPtr[pipe]++;
			/* 8bit FIFO access */
			USB_RD_FF(USB_D1FIFO_08, *usb_gcstd_DataPtr[pipe]);
			/* Renewal read pointer */
			usb_gcstd_DataPtr[pipe]++;
		}
		else if( (count & (uint16_t)0x0002) != 0 )
		{
			/* 16bit FIFO access */
			USB_RD_FF(USB_D1FIFO_16, *((uint16_t*)usb_gcstd_DataPtr[pipe]));
			/* Renewal read pointer */
			usb_gcstd_DataPtr[pipe]++;
			usb_gcstd_DataPtr[pipe]++;
		}
		else if( (count & (uint16_t)0x0001) != 0 )
		{
			/* 8bit FIFO access */
			USB_RD_FF(USB_D1FIFO_08, *usb_gcstd_DataPtr[pipe]);
			/* Renewal read pointer */
			usb_gcstd_DataPtr[pipe]++;
		}
#else	/* !USBC_BUSSIZE_32_PP */
		for( even = (uint16_t)(count >> 1u); (even != 0u); --even )
		{
			/* 16bit FIFO access */
			USB_RD_FF(USB_D1FIFO_16, *((uint16_t*)usb_gcstd_DataPtr[pipe]));
			/* Renewal read pointer */
			usb_gcstd_DataPtr[pipe]++;
			usb_gcstd_DataPtr[pipe]++;
		}
		if( (count & (uint16_t)0x0001) != 0 )
		{
			/* 8bit FIFO access */
			USB_RD_FF(USB_D1FIFO_08, *usb_gcstd_DataPtr[pipe]);
			/* Renewal read pointer */
			usb_gcstd_DataPtr[pipe]++;
		}
#endif	/* USBC_BUSSIZE_32_PP */

	}

	usb_gcstd_DataCnt[pipe] -= count;
	/* End or Err or Continue */
	return (end_flag);
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_DataEnd
Description     : End of data transfer (IN/OUT)
                : In the case of timeout status, it does not call back.
Arguments       : uint16_t pipe			; Pipe Number
                : uint16_t status		; Transfer status type
Return value    : none
******************************************************************************/
void usb_cstd_DataEnd(uint16_t pipe, uint16_t status)
{
	uint16_t	useport;

	/* PID = NAK */
	/* Set NAK */
	usb_cstd_SelectNak(pipe);
	/* Pipe number to FIFO port select */
	useport = usb_cstd_Pipe2Fport(pipe);

	/* Disable Interrupt */
	/* Disable Ready Interrupt */
	usb_cstd_BrdyDisable(pipe);
	/* Disable Not Ready Interrupt */
	usb_cstd_NrdyDisable(pipe);
	/* Disable Empty Interrupt */
	usb_cstd_BempDisable(pipe);

	/* Disable Transaction count */
	usb_cstd_ClrTransactionCounter(pipe);

	/* Check use FIFO */
	switch( useport )
	{
	/* CFIFO use */
	case USBC_CUSE:
		break;
	/* D0FIFO use */
	case USBC_D0USE:
		break;
	/* D0FIFO DMA */
	case USBC_D0DMA:
		/* DMA buffer clear mode clear */
		USB_CLR_PAT(D0FIFOSEL, USBC_DCLRM);
		USB_MDF_PAT(D0FIFOSEL, USB_D0FIFO_MBW, USBC_MBW);
/* Condition compilation by the difference of the devices */
#if USBC_TARGET_CHIP_PP == USBC_ASSP_PP
		USB_WR(DMA0CFG, USBC_CPU_ADR_RD_WR);
#endif
		break;
	/* D1FIFO use */
	case USBC_D1USE:
		break;
	/* D1FIFO DMA */
	case USBC_D1DMA:
		/* continue */
	default:
		break;
	}

	/* Call Back */
	if( usb_gcstd_Pipe[pipe] != USBC_NULL )
	{
		/* Check PIPE TYPE */
		if( usb_cstd_GetPipeType(pipe) != USBC_ISO )
		{
			/* Transfer information set */
			usb_gcstd_Pipe[pipe]->tranlen	= usb_gcstd_DataCnt[pipe];
			usb_gcstd_Pipe[pipe]->status	= status;
			usb_gcstd_Pipe[pipe]->pipectr	= usb_cstd_ReadPipectr(pipe);
			usb_gcstd_Pipe[pipe]->errcnt
				= (uint8_t)usb_ghstd_IgnoreCnt[pipe];
			(*usb_gcstd_Pipe[pipe]->complete)(usb_gcstd_Pipe[pipe]);
			usb_gcstd_Pipe[pipe] = (USBC_UTR_t*)USBC_NULL;
		}
		else
		{
			/* Transfer information set */
			usb_gcstd_Pipe[pipe]->tranlen	= usb_gcstd_DataCnt[pipe];
			usb_gcstd_Pipe[pipe]->pipectr	= usb_cstd_ReadPipectr(pipe);
			usb_gcstd_Pipe[pipe]->errcnt
				= (uint8_t)usb_ghstd_IgnoreCnt[pipe];
			/* Data Transfer (restart) */
			if( usb_cstd_GetPipeDir(pipe) == USBC_BUF2FIFO )
			{
				/* OUT Transfer */
				usb_gcstd_Pipe[pipe]->status	= USBC_DATA_WRITING;
				(*usb_gcstd_Pipe[pipe]->complete)(usb_gcstd_Pipe[pipe]);
			}
			else
			{
				/* IN Transfer */
				usb_gcstd_Pipe[pipe]->status	= USBC_DATA_READING;
				(*usb_gcstd_Pipe[pipe]->complete)(usb_gcstd_Pipe[pipe]);
			}
		}
	}
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_ForcedTermination
Description     : End of data transfer (IN/OUT)
Arguments       : uint16_t pipe			; Pipe Number
                : uint16_t status		; Transfer status type
Return value    : none
Note            : In the case of timeout status, it does not call back.
******************************************************************************/
void usb_cstd_ForcedTermination(uint16_t pipe, uint16_t status)
{
	uint16_t	buffer;

	/* PID = NAK */
	/* Set NAK */
	usb_cstd_SetNak(pipe);

	/* Disable Interrupt */
	/* Disable Ready Interrupt */
	usb_cstd_BrdyDisable(pipe);
	/* Disable Not Ready Interrupt */
	usb_cstd_NrdyDisable(pipe);
	/* Disable Empty Interrupt */
	usb_cstd_BempDisable(pipe);

	usb_cstd_ClrTransactionCounter(pipe);

	/* Clear D1FIFO-port */
	USB_RD(CFIFOSEL, buffer);
	if( (buffer & USBC_CURPIPE) == pipe )
	{
		USB_MDF_PAT(CFIFOSEL, USB_CFIFO_MBW, USBC_MBW);
		/* Changes the FIFO port by the pipe. */
		usb_cstd_FPortChange2((uint16_t)USBC_PIPE0, (uint16_t)USBC_CUSE
			, USBC_NO);
	}
	/* Clear D0FIFO-port */
	USB_RD(D0FIFOSEL, buffer);
	if( (buffer & USBC_CURPIPE) == pipe )
	{
		/* Stop DMA,FIFO access */
		usb_cstd_StopDma();
		usb_cstd_D0fifoStopUsb();
		usb_cstd_D0FifoselSet();
/* Condition compilation by the difference of the devices */
#if USBC_TARGET_CHIP_PP == USBC_ASSP_PP
		USB_WR(DMA0CFG, USBC_CPU_ADR_RD_WR);
#endif	/* USBC_ASSP_PP */
		/* Changes the FIFO port by the pipe. */
		usb_cstd_FPortChange2((uint16_t)USBC_PIPE0, (uint16_t)USBC_D0USE
			, USBC_NO);
	}
	/* Clear D1FIFO-port */
	USB_RD(D1FIFOSEL, buffer);
	if( (buffer & USBC_CURPIPE) == pipe )
	{
		USB_MDF_PAT(D1FIFOSEL, USB_D1FIFO_MBW, USBC_MBW);
		/* Changes the FIFO port by the pipe. */
		usb_cstd_FPortChange2((uint16_t)USBC_PIPE0, (uint16_t)USBC_D1USE
			, USBC_NO);
	}

	/* Changes the FIFO port by the pipe. */
	usb_cstd_FPortChange2(pipe, (uint16_t)USBC_CUSE, USBC_NO);
	USB_RD(CFIFOCTR, buffer);
	if( (uint16_t)(buffer & USBC_FRDY) == USBC_FRDY )
	{
		/* Clear BVAL */
		USB_WR(CFIFOCTR, USBC_BCLR);
	}

	/* FIFO buffer SPLIT transaction initialized */
	usb_cstd_FPortChange2((uint16_t)USBC_PIPE0, (uint16_t)USBC_CUSE, USBC_NO);
	usb_cstd_DoCsclr(pipe);

	/* Call Back */
	if( (status != USBC_DATA_TMO) && (usb_gcstd_Pipe[pipe] != USBC_NULL) )
	{
		/* Transfer information set */
		usb_gcstd_Pipe[pipe]->tranlen	= usb_gcstd_DataCnt[pipe];
		usb_gcstd_Pipe[pipe]->status	= status;
		usb_gcstd_Pipe[pipe]->pipectr	= usb_cstd_ReadPipectr(pipe);
		usb_gcstd_Pipe[pipe]->errcnt
			= (uint8_t)usb_ghstd_IgnoreCnt[pipe];
		(*usb_gcstd_Pipe[pipe]->complete)(usb_gcstd_Pipe[pipe]);
		usb_gcstd_Pipe[pipe] = (USBC_UTR_t*)USBC_NULL;
	}
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_FPortChange1
Description     : Changes the specified FIFO port by the specified pipe.
                : Please change the wait time for your MCU
Arguments       : uint16_t pipe			; Pipe Number
                : uint16_t fifosel		; FIFO select
                : uint16_t isel			; ISEL bit status
Return value    : uint16_t FRDY status
******************************************************************************/
uint16_t usb_cstd_FPortChange1(uint16_t pipe, uint16_t fifosel, uint16_t isel)
{
	uint16_t	buffer, i;

	/* Changes the FIFO port by the pipe. */
	usb_cstd_FPortChange2(pipe, fifosel, isel);

	for( i = 0; i < 4; i++ )
	{
		switch( fifosel )
		{
		case USBC_CUSE:		USB_RD(CFIFOCTR, buffer);	break;
		case USBC_D0USE:		/* continue */
		case USBC_D0DMA:		USB_RD(D0FIFOCTR, buffer);	break;
		case USBC_D1USE:		/* continue */
		case USBC_D1DMA:		USB_RD(D1FIFOCTR, buffer);	break;
		default:			buffer = 0;					break;
		}
		if( (uint16_t)(buffer & USBC_FRDY) == USBC_FRDY )
		{
			return (buffer);
		}
		USBC_PRINTF1("*** FRDY wait pipe = %d\n", pipe);
	/* Cautions !!!
	 * Depending on the external bus speed of CPU, you may need to wait
	 * for 100ns here.
	 * For details, please look at the data sheet.	 */
	/***** The example of reference. *****/
/* Condition compilation by the difference of the devices */
#if USBC_TARGET_CHIP_PP == USBC_RX600_PP
		/* Wait for About 60ns */
		USB_RD(SYSCFG, buffer);
#else	/* USBC_TARGET_CHIP_PP == USBC_RX600_PP */
		/* Wait for About 60ns */
		USB_RD(SYSCFG0, buffer);
#endif	/* USBC_TARGET_CHIP_PP == USBC_RX600_PP */
		/* Wait for About 60ns */
		USB_RD(SYSSTS0, buffer);
	/*************************************/
	}
	return (USBC_FIFOERROR);
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_FPortChange2
Description     : Changes the specified FIFO port by the specified pipe.
Arguments       : uint16_t pipe			; Pipe Number
                : uint16_t fifosel		; FIFO select
                : uint16_t isel			; ISEL bit status
Return value    : none
******************************************************************************/
void usb_cstd_FPortChange2(uint16_t pipe, uint16_t fifosel, uint16_t isel)
{
	uint16_t	buffer;

	/* Select FIFO */
	switch( fifosel )
	{
	/* CFIFO use */
	case USBC_CUSE:
		/* ISEL=1, CURPIPE=0 */
		USB_MDF_PAT(CFIFOSEL, (uint16_t)(USBC_RCNT|isel|pipe)
			, (uint16_t)(USBC_RCNT|USBC_ISEL|USBC_CURPIPE));
		do
		{
			USB_RD(CFIFOSEL, buffer);
		}
		while( (buffer & (uint16_t)(USBC_ISEL|USBC_CURPIPE))
			!= (uint16_t)(isel|pipe) );
		break;
	/* D0FIFO use */
	case USBC_D0USE:
		/* continue */
	/* D0FIFO DMA */
	case USBC_D0DMA:
		/* D0FIFO pipe select */
		USB_MDF_PAT(D0FIFOSEL, pipe, USBC_CURPIPE);
		do
		{
			USB_RD(D0FIFOSEL, buffer);
		}
		while( (uint16_t)(buffer & USBC_CURPIPE) != pipe );
		break;
	/* D1FIFO use */
	case USBC_D1USE:
		/* continue */
	/* D1FIFO DMA */
	case USBC_D1DMA:
		/* D1FIFO pipe select */
		USB_MDF_PAT(D1FIFOSEL, pipe, USBC_CURPIPE);
		do
		{
			USB_RD(D1FIFOSEL, buffer);
		}
		while( (uint16_t)(buffer & USBC_CURPIPE) != pipe );
		break;
	default:
		break;
	}
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_FifoClr
Description     : FIFO buffer clear
Arguments       : uint16_t pipe			; Pipe Number
Return value    : none
******************************************************************************/
void usb_cstd_FifoClr(uint16_t pipe)
{
	uint16_t		buf, i;

	if( pipe == USBC_USEPIPE )
	{
		/* Changes the FIFO port by the pipe. */
		usb_cstd_FPortChange2((uint16_t)USBC_PIPE0
			, (uint16_t)USBC_D0USE, USBC_NO);
		/* Changes the FIFO port by the pipe. */
		usb_cstd_FPortChange2((uint16_t)USBC_PIPE0
			, (uint16_t)USBC_D1USE, USBC_NO);
		for( i = USBC_PIPE1; i <= USBC_MAX_PIPE_NO; i++ )
		{
			/* Do pipe ACLRM */
			usb_cstd_DoAclrm(i);
		}
	}
	else
	{
		USB_RD(D0FIFOSEL, buf);
		if( (buf & USBC_CURPIPE) == pipe )
		{
			/* Changes the FIFO port by the pipe. */
			usb_cstd_FPortChange2((uint16_t)USBC_PIPE0, (uint16_t)USBC_D0USE
				, USBC_NO);
		}
		USB_RD(D1FIFOSEL, buf);
		if( (buf & USBC_CURPIPE) == pipe )
		{
			/* Changes the FIFO port by the pipe. */
			usb_cstd_FPortChange2((uint16_t)USBC_PIPE0, (uint16_t)USBC_D1USE
				, USBC_NO);
		}
		/* Do pipe ACLRM */
		usb_cstd_DoAclrm(pipe);
	}
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_Buf2D0fifoStartUsb
Description     : Buffer to D0FIFO data write DMA start
Arguments       : none
Return value    : none
******************************************************************************/
void usb_cstd_Buf2D0fifoStartUsb(void)
{
	uint16_t	mbw;

	/* Write (MEMORY -> FIFO) : USB register set */
/* Condition compilation by the difference of FIFO access width */
#if USBC_BUSSIZE_PP == USBC_BUSSIZE_32_PP
	if( (usb_gcstd_Dma0Size & (uint16_t)0x0003) == 0 )
	{
		mbw = USBC_MBW_32;
	} else
#endif	/* USBC_BUSSIZE_32_PP */
	if( (usb_gcstd_Dma0Size & (uint16_t)0x0001) != 0 )
	{
		mbw = USBC_MBW_8;
	}
	else
	{
		mbw = USBC_MBW_16;
	}
	/* Change MBW setting */
	USB_MDF_PAT(D0FIFOSEL, mbw, USBC_MBW);
#if USBC_TARGET_CHIP_PP == USBC_RX600_PP
	if((usb_gcstd_DataCnt[usb_gcstd_Dma0Pipe] % usb_gcstd_Dma0Fifo) == 0)
	{
		/* D0FIFO interrupt disable */
		usb_cpu_DisableDma();
		usb_cstd_BempEnable(usb_gcstd_Dma0Pipe);
	}
	else
	{
		/* DTC(D0FIFO) interrupt enable */
		usb_cpu_EnableDma();
	}
#endif /* USBC_TARGET_CHIP_PP == USBC_RX600_PP */
	/* Set DREQ enable */
	USB_SET_PAT(D0FIFOSEL, USBC_DREQE);
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_D0fifo2BufStartUsb
Description     : D0FIFO to Buffer data read DMA start
Arguments       : none
Return value    : none
******************************************************************************/
void usb_cstd_D0fifo2BufStartUsb(void)
{
#if USBC_TARGET_CHIP_PP != USBC_RX600_PP
	uint16_t	mbw;
#endif /* USBC_TARGET_CHIP_PP == USBC_RX600_PP */

	/* Read (FIFO -> MEMORY) : USB register set */
/* Condition compilation by the difference of FIFO access width */
#if USBC_BUSSIZE_PP == USBC_BUSSIZE_32_PP
	if( (usb_gcstd_Dma0Size & (uint16_t)0x0003) == 0 )
	{
		mbw = USBC_MBW_32;
	} else
#endif	/* USBC_BUSSIZE_32_PP */

#if USBC_TARGET_CHIP_PP == USBC_RX600_PP
	/* DMA buffer clear mode & MBW set */
	USB_MDF_PAT(D0FIFOSEL, (uint16_t)(USBC_MBW_16),
		 (uint16_t)(USBC_DCLRM|USBC_MBW));
#else /* USBC_TARGET_CHIP_PP == USBC_RX600_PP */
	if( (usb_gcstd_Dma0Size & (uint16_t)0x0001) != 0 )
	{
		mbw = USBC_MBW_8;
	}
	else
	{
		mbw = USBC_MBW_16;
	}
	/* DMA buffer clear mode & MBW set */
	USB_MDF_PAT(D0FIFOSEL, (uint16_t)(USBC_DCLRM|mbw),
		 (uint16_t)(USBC_DCLRM|USBC_MBW));
#endif /* USBC_TARGET_CHIP_PP == USBC_RX600_PP */
	/* Set DREQ enable */
	USB_SET_PAT(D0FIFOSEL, USBC_DREQE);
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_D0fifoStopUsb
Description     : D0FIFO access DMA stop
Arguments       : none
Return value    : none
******************************************************************************/
void usb_cstd_D0fifoStopUsb(void)
{
	USB_CLR_PAT(D0FIFOSEL, USBC_DREQE);

	/* Direction check */
	if( usb_gcstd_Dma0Dir == USBC_BUF2FIFO )
	{
		/* Buffer to FIFO */
		if( usb_gcstd_DataCnt[usb_gcstd_Dma0Pipe] < usb_gcstd_Dma0Size )
		{
			/* >yes then set BVAL */
			usb_gcstd_DataPtr[usb_gcstd_Dma0Pipe]
				= (uint8_t*)((uint32_t)usb_gcstd_DataPtr[usb_gcstd_Dma0Pipe]
					+ usb_gcstd_DataCnt[usb_gcstd_Dma0Pipe]);
			usb_gcstd_DataCnt[usb_gcstd_Dma0Pipe] = (uint32_t)0u;
			/* Disable Ready Interrupt */
			usb_cstd_BrdyDisable(usb_gcstd_Dma0Pipe);
			/* Set BVAL */
			USB_WR(D0FIFOCTR, USBC_BVAL);
		}
		else
		{
			usb_gcstd_DataPtr[usb_gcstd_Dma0Pipe]
				= (uint8_t*)((uint32_t)usb_gcstd_DataPtr[usb_gcstd_Dma0Pipe]
					+ usb_gcstd_Dma0Size);
			/* Set data count to remain */
			usb_gcstd_DataCnt[usb_gcstd_Dma0Pipe] -= usb_gcstd_Dma0Size;
		}
	}
	else
	{
		/* FIFO to Buffer */
		usb_gcstd_DataPtr[usb_gcstd_Dma0Pipe]
			= (uint8_t*)((uint32_t)usb_gcstd_DataPtr[usb_gcstd_Dma0Pipe]
				+ (uint32_t)usb_gcstd_Dma0Size);
		/* Set data count to remain */
		usb_gcstd_DataCnt[usb_gcstd_Dma0Pipe] -= usb_gcstd_Dma0Size;
	}
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : Select periodic pipe NAK set
Description     : void usb_cstd_SelectNak(uint16_t pipe)
Arguments       : uint16_t pipe			: pipe number
Return value    : none
******************************************************************************/
void usb_cstd_SelectNak(uint16_t pipe)
{
/* Condition compilation by the difference of USB function */
#if USB_FUNCSEL_PP == USBC_PERI_PP
/* Condition compilation by the difference of transfer type */
  #if USBC_PERIODIC_MODE_PP == USBC_NONPERIODIC_PP
	/* Check PIPE TYPE */
	if( usb_cstd_GetPipeType(pipe) != USBC_ISO )
	{
		/* Set pipe PID_NAK */
		usb_cstd_SetNak(pipe);
	}
  #endif	/* USBC_NONPERIODIC_PP */
#endif	/* USBC_PERI_PP */
/* Condition compilation by the difference of USB function */
#if USB_FUNCSEL_PP == USBC_HOST_PP
	/* Check PIPE TYPE */
	if( usb_cstd_GetPipeType(pipe) != USBC_ISO )
	{
		/* Set pipe PID_NAK */
		usb_cstd_SetNak(pipe);
	}
#endif	/* USBC_HOST_PP */
/* Condition compilation by the difference of USB function */
#if USB_FUNCSEL_PP == USBC_HOST_PERI_PP
	/* Check PIPE TYPE */
	if( usb_cstd_GetPipeType(pipe) != USBC_ISO )
	{
		/* Check current function */
		if( usb_cstd_FunctionUsbip() == USBC_NO )
		{
			/* Peripheral Function */
/* Condition compilation by the difference of transfer type */
  #if USBC_PERIODIC_MODE_PP == USBC_NONPERIODIC_PP
			/* Set pipe PID_NAK */
			usb_cstd_SetNak(pipe);
  #endif	/* USBC_NONPERIODIC_PP */
		}
		else
		{
			/* Set pipe PID_NAK */
			usb_cstd_SetNak(pipe);
		}
	}
#endif	/* USBC_HOST_PERI_PP */
}
/******************************************************************************
End of function
******************************************************************************/



/*===========================================================================*/
/* Condition compilation by the difference of IP */
#if USBC_IPSEL_PP == USBC_597IP_PP

/******************************************************************************
Function Name   : usb_cstd_SetTransactionCounter
Description     : Set Transaction counter
Arguments       : uint16_t trnreg		; Pipe Number
                : uint16_t trncnt		: Transaction counter
Return value    : none
******************************************************************************/
void usb_cstd_SetTransactionCounter(uint16_t trnreg, uint16_t trncnt)
{
	/* Check PIPE */
	switch( trnreg )
	{
	case USBC_PIPE1:
		USB_SET_PAT(PIPE1TRE, USBC_TRCLR);
		/* Pipe1 transaction counter reg set & count enable */
/* Condition compilation by the difference of the devices */
#if USBC_TARGET_CHIP_PP == USBC_RX600_PP
		USB_WRW(PIPE1TRN, trncnt);
#else	/* !USBC_RX600_PP */
		USB_WR(PIPE1TRN, trncnt);
#endif	/* USBC_RX600_PP */
		USB_SET_PAT(PIPE1TRE, USBC_TRENB);
		break;
	case USBC_PIPE2:
		USB_SET_PAT(PIPE2TRE, USBC_TRCLR);
		/* Pipe2 transaction counter reg set & count enable  */
/* Condition compilation by the difference of the devices */
#if USBC_TARGET_CHIP_PP == USBC_RX600_PP
		USB_WRW(PIPE2TRN, trncnt);
#else	/* !USBC_RX600_PP */
		USB_WR(PIPE2TRN, trncnt);
#endif	/* USBC_RX600_PP */
		USB_SET_PAT(PIPE2TRE, USBC_TRENB);
		break;
	case USBC_PIPE3:
		USB_SET_PAT(PIPE3TRE, USBC_TRCLR);
		/* Pipe3 transaction counter reg set & count enable  */
/* Condition compilation by the difference of the devices */
#if USBC_TARGET_CHIP_PP == USBC_RX600_PP
		USB_WRW(PIPE3TRN, trncnt);
#else	/* !USBC_RX600_PP */
		USB_WR(PIPE3TRN, trncnt);
#endif	/* USBC_RX600_PP */
		USB_SET_PAT(PIPE3TRE, USBC_TRENB);
		break;
	case USBC_PIPE4:
		USB_SET_PAT(PIPE4TRE, USBC_TRCLR);
		/* Pipe4 transaction counter reg set & count enable  */
/* Condition compilation by the difference of the devices */
#if USBC_TARGET_CHIP_PP == USBC_RX600_PP
		USB_WRW(PIPE4TRN, trncnt);
#else	/* !USBC_RX600_PP */
		USB_WR(PIPE4TRN, trncnt);
#endif	/* USBC_RX600_PP */
		USB_SET_PAT(PIPE4TRE, USBC_TRENB);
		break;
	case USBC_PIPE5:
		USB_SET_PAT(PIPE5TRE, USBC_TRCLR);
		/* Pipe5 transaction counter reg set & count enable  */
/* Condition compilation by the difference of the devices */
#if USBC_TARGET_CHIP_PP == USBC_RX600_PP
		USB_WRW(PIPE5TRN, trncnt);
#else	/* !USBC_RX600_PP */
		USB_WR(PIPE5TRN, trncnt);
#endif	/* USBC_RX600_PP */
		USB_SET_PAT(PIPE5TRE, USBC_TRENB);
		break;
	default:
		break;
	}
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_ClrTransactionCounter
Description     : Clear Transaction counter
Arguments       : uint16_t trnreg		; Pipe Number
Return value    : none
******************************************************************************/
void usb_cstd_ClrTransactionCounter(uint16_t trnreg)
{
	/* Check PIPE */
	switch( trnreg )
	{
	case USBC_PIPE1:
		/* Pipe1 transaction counter reg clear & count disable  */
		USB_CLR_PAT(PIPE1TRE, USBC_TRENB);
		USB_SET_PAT(PIPE1TRE, USBC_TRCLR);
		break;
	case USBC_PIPE2:
		/* Pipe2 transaction counter reg clear & count disable  */
		USB_CLR_PAT(PIPE2TRE, USBC_TRENB);
		USB_SET_PAT(PIPE2TRE, USBC_TRCLR);
		break;
	case USBC_PIPE3:
		/* Pipe3 transaction counter reg clear & count disable  */
		USB_CLR_PAT(PIPE3TRE, USBC_TRENB);
		USB_SET_PAT(PIPE3TRE, USBC_TRCLR);
		break;
	case USBC_PIPE4:
		/* Pipe4 transaction counter reg clear & count disable  */
		USB_CLR_PAT(PIPE4TRE, USBC_TRENB);
		USB_SET_PAT(PIPE4TRE, USBC_TRCLR);
		break;
	case USBC_PIPE5:
		/* Pipe5 transaction counter reg clear & count disable  */
		USB_CLR_PAT(PIPE5TRE, USBC_TRENB);
		USB_SET_PAT(PIPE5TRE, USBC_TRCLR);
		break;
	default:
		break;
	}
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_SetTransactCount
Description     : Transact Count Set
Arguments       : uint16_t pipe         : pipe number
                : uint16_t useport      : not use
                : uint32_t length       : Transaction data length
                : uint16_t mxps         : max packet size
Return value    : none
******************************************************************************/
void usb_cstd_SetTransactCount(uint16_t pipe, uint16_t useport
	, uint32_t length, uint16_t mxps)
{
	/* Data length check */
	if( (length % mxps) == (uint32_t)0u )
	{
		/* Set Transaction counter */
		usb_cstd_SetTransactionCounter(pipe, (uint16_t)(length / mxps));
	}
	else
	{
		/* Set Transaction counter */
		usb_cstd_SetTransactionCounter(pipe
			, (uint16_t)((length / mxps) + (uint32_t)1u));
	}
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_D0FifoselSet
Description     : D0FIFO port select reg set(endian & mbw)
Arguments       : none
Return value    : none
******************************************************************************/
void usb_cstd_D0FifoselSet(void)
{
	/* Big endian mode set */
	USB_MDF_PAT(D0FIFOSEL, USB_FIFOENDIAN, USBC_BIGEND);
	/* DMA buffer clear mode set */
	USB_CLR_PAT(D0FIFOSEL, USBC_DCLRM);
	/* Maximum bit width for FIFO access set */
	USB_MDF_PAT(D0FIFOSEL, USB_D0FIFO_MBW, USBC_MBW);
}

/******************************************************************************
Function Name   : usb_cstd_D0fifoInt
Description     : D0FIFO interrupt process
Arguments       : none
Return value    : none
******************************************************************************/
void usb_cstd_D0fifoInt(void)
{
#if USBC_TARGET_CHIP_PP == USBC_RX600_PP
	uint16_t	usb_DMA0bval = 0;

	USB_CLR_PAT(D0FIFOSEL, USBC_DREQE);

	/* Transfer count check */
	if ( usb_gcstd_DataCnt[usb_gcstd_Dma0Pipe] > usb_gcstd_Dma0Fifo )
	{
		/* Transfer count update */
		usb_gcstd_Dma0Size
			= usb_gcstd_DataCnt[usb_gcstd_Dma0Pipe] % usb_gcstd_Dma0Fifo;
	}
	else
	{
		if ( usb_gcstd_Dma0Size != 0 )
		{
			usb_DMA0bval = 1;
		}

		usb_gcstd_Dma0Size = 0;
	}
	usb_gcstd_DataCnt[usb_gcstd_Dma0Pipe] = usb_gcstd_Dma0Size;

	/* Transfer continue check */
	if( usb_gcstd_Dma0Size != 0 )
	{
		if( (usb_gcstd_Dma0Size & 0x0001u) != 0u )
		{
			/* if count == odd */
			USB_MDF_PAT(D0FIFOSEL, USBC_MBW_8, USBC_MBW);
		}
		/* DMA Restart */
		usb_cstd_RestartDma();
		USB_SET_PAT(D0FIFOSEL, USBC_DREQE);
	}
	else
	{
		/* Transfer complete */
		usb_cstd_StopDma();

		USB_CLR_STS(BEMPSTS, USBC_BITSET(usb_gcstd_Dma0Pipe));
		/* Enable Empty Interrupt */
		usb_cstd_BempEnable(usb_gcstd_Dma0Pipe);
		if ( usb_DMA0bval != 0 )
		{
			/* DataSize % MXPS */
			USB_WR(D0FIFOCTR, USBC_BVAL);
		}
	}
#else	/* USBC_TARGET_CHIP_PP == USBC_RX600_PP */
	USB_CLR_PAT(D0FIFOSEL, USBC_DREQE);
	usb_cstd_StopDma();

	if( usb_gcstd_Dma0Dir == USBC_BUF2FIFO )
	{
		/* Enable Empty Interrupt */
		usb_cstd_BempEnable(usb_gcstd_Dma0Pipe);
		if( (usb_gcstd_Dma0Size % usb_gcstd_Dma0Fifo) != 0 )
	{
			/* DataSize % MXPS */
			USB_WR(D0FIFOCTR, USBC_BVAL);
		}
	}
	else
	{
		usb_gcstd_DataCnt[usb_gcstd_Dma0Pipe] = 0;
	}
#endif	/* USBC_TARGET_CHIP_PP == USBC_RX600_PP */
}
/******************************************************************************
End of function
******************************************************************************/


#endif	/* USBC_597IP_PP */
/******************************************************************************
End of function
******************************************************************************/



/*===========================================================================*/
/* Condition compilation by the difference of IP */
#if USBC_IPSEL_PP == USBC_596IP_PP

/******************************************************************************
Function Name   : usb_cstd_SetTransactionCounter
Description     : Set Transaction counter
Arguments       : uint16_t trnreg		; FIFO port
 *				  uint16_t trncnt		: Transaction counter
Return value    : none
******************************************************************************/
void usb_cstd_SetTransactionCounter(uint16_t trnreg, uint16_t trncnt)
{
	/* Check use FIFO */
	switch( trnreg )
	{
	case USBC_D0USE:
		/* continue */
	case USBC_D0DMA:
		/* D0FIFO transaction counter reg clear */
		USB_SET_PAT(D0FIFOSEL, USBC_TRCLR);
		/* D0FIFO transaction counter set  */
		USB_WR(D0FIFOTRN, trncnt);
		/* D0FIFO transaction counter enable  */
		USB_SET_PAT(D0FIFOSEL, USBC_TRENB);
		break;
	case USBC_D1USE:
		/* continue */
	case USBC_D1DMA:
		/* D1FIFO transaction counter reg clear */
		USB_SET_PAT(D1FIFOSEL, USBC_TRCLR);
		/* D1FIFO transaction counter set  */
		USB_WR(D1FIFOTRN, trncnt);
		/* D1FIFO transaction counter enable  */
		USB_SET_PAT(D1FIFOSEL, USBC_TRENB);
		break;
	default:
		USBC_PRINTF0("### Not support of FIFO-port access command \n");
		break;
	}
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_ClrTransactionCounter
Description     : Clear Transaction counter
Arguments       : uint16_t trnreg		; FIFO port
Return value    : none
******************************************************************************/
void usb_cstd_ClrTransactionCounter(uint16_t trnreg)
{
	uint16_t	useport;

	/* Pipe number to FIFO port select */
	useport = usb_cstd_Pipe2Fport(trnreg);

	/* Check use FIFO */
	switch( trnreg )
	{
	case USBC_D0USE:
		/* continue */
	case USBC_D0DMA:
		/* D0FIFO transaction counter enable  */
		USB_CLR_PAT(D0FIFOSEL, USBC_TRENB);
		/* D0FIFO transaction counter reg clear */
		USB_SET_PAT(D0FIFOSEL, USBC_TRCLR);
		break;
	case USBC_D1USE:
		/* continue */
	case USBC_D1DMA:
		/* D0FIF1 transaction counter enable  */
		USB_CLR_PAT(D1FIFOSEL, USBC_TRENB);
		/* D0FIF1 transaction counter reg clear */
		USB_SET_PAT(D1FIFOSEL, USBC_TRCLR);
		break;
	default:
		USBC_PRINTF0("### Not support of FIFO-port access command \n");
		break;
	}
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_SetTransactCount
Description     : Transact Count Set
Arguments       : uint16_t pipe         : pipe number
                : uint16_t useport      : not use
                : uint32_t length       : Transaction data length
                : uint16_t mxps         : max packet size
Return value    : none
******************************************************************************/
void usb_cstd_SetTransactCount(uint16_t pipe, uint16_t useport
	, uint32_t length, uint16_t mxps)
{
	/* Data length check */
	if( (length % mxps) == 0u )
	{
		/* Set Transaction counter */
		usb_cstd_SetTransactionCounter(useport, (uint16_t)(length / mxps));
	}
	else
	{
		/* Set Transaction counter */
		usb_cstd_SetTransactionCounter(useport
			, (uint16_t)((length / mxps) + 1u));
	}
}


/******************************************************************************
Function Name   : usb_cstd_D0FifoselSet
Description     : D0FIFO port select reg set(mbw)
Arguments       : none
Return value    : none
******************************************************************************/
void usb_cstd_D0FifoselSet(void)
{
	/* DMA buffer clear mode set */
	USB_CLR_PAT(D0FIFOSEL, USBC_DCLRM);
	/* Maximum bit width for FIFO access set */
	USB_MDF_PAT(D0FIFOSEL, USB_D0FIFO_MBW, USBC_MBW);
}


#endif	/* == USBC_596IP_PP */
/******************************************************************************
End of function
******************************************************************************/



/*===========================================================================*/
/* Condition compilation by the difference of IP */
#if USBC_IPSEL_PP == USBC_592IP_PP

/******************************************************************************
Function Name   : usb_cstd_SetTransactionCounter
Description     : Set Transaction counter
Arguments       : uint16_t trnreg		; FIFO port
 *				  uint16_t trncnt		: Transaction counter
Return value    : none
******************************************************************************/
void usb_cstd_SetTransactionCounter(uint16_t trnreg, uint16_t trncnt)
{
	/* Check use FIFO */
	switch( trnreg )
	{
	case USBC_D0USE:
		/* continue */
	case USBC_D0DMA:
		/* D0FIFO transaction counter reg clear */
		USB_SET_PAT(D0FIFOSEL, USBC_TRCLR);
		/* D0FIFO transaction counter set  */
		USB_WR(D0FIFOTRN, trncnt);
		/* D0FIFO transaction counter enable  */
		USB_SET_PAT(D0FIFOSEL, USBC_TRENB);
		break;
	case USBC_D1USE:
		/* continue */
	case USBC_D1DMA:
		/* D1FIFO transaction counter reg clear */
		USB_SET_PAT(D1FIFOSEL, USBC_TRCLR);
		/* D1FIFO transaction counter set  */
		USB_WR(D1FIFOTRN, trncnt);
		/* D1FIFO transaction counter enable  */
		USB_SET_PAT(D1FIFOSEL, USBC_TRENB);
		break;
	default:
		USBC_PRINTF0("### Not support of FIFO-port access command \n");
		break;
	}
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : Clear Transaction counter
Description     : void usb_cstd_ClrTransactionCounter(uint16_t trnreg)
Arguments       : uint16_t trnreg		; FIFO port
Return value    : none
******************************************************************************/
void usb_cstd_ClrTransactionCounter(uint16_t trnreg)
{
	uint16_t	useport;

	/* Pipe number to FIFO port select */
	useport = usb_cstd_Pipe2Fport(trnreg);

	/* Check use FIFO */
	switch( trnreg )
	{
	case USBC_D0USE:
		/* continue */
	case USBC_D0DMA:
		/* D0FIFO transaction counter enable  */
		USB_CLR_PAT(D0FIFOSEL, USBC_TRENB);
		/* D0FIFO transaction counter reg clear */
		USB_SET_PAT(D0FIFOSEL, USBC_TRCLR);
		break;
	case USBC_D1USE:
		/* continue */
	case USBC_D1DMA:
		/* D1FIFO transaction counter enable  */
		USB_CLR_PAT(D1FIFOSEL, USBC_TRENB);
		/* D1FIFO transaction counter reg clear */
		USB_SET_PAT(D1FIFOSEL, USBC_TRCLR);
		break;
	default:
		USBC_PRINTF0("### Not support of FIFO-port access command \n");
		break;
	}
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_SetTransactCount
Description     : Transact Count Set
Arguments       : uint16_t pipe         : pipe number
                : uint16_t useport      : not use
                : uint32_t length       : Transaction data length
                : uint16_t mxps         : max packet size
Return value    : none
******************************************************************************/
void usb_cstd_SetTransactCount(uint16_t pipe, uint16_t useport
	, uint32_t length, uint16_t mxps)
{
	/* Data length check */
	if( (length % mxps) == 0u )
	{
		/* Set Transaction counter */
		usb_cstd_SetTransactionCounter(useport, (uint16_t)(length / mxps));
	}
	else
	{
		/* Set Transaction counter */
		usb_cstd_SetTransactionCounter(useport
			, (uint16_t)((length / mxps) + 1u));
	}
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_D0FifoselSet
Description     : D0FIFO port select reg set(mbw)
Arguments       : none
Return value    : none
******************************************************************************/
void usb_cstd_D0FifoselSet(void)
{
	/* DMA buffer clear mode set */
	USB_CLR_PAT(D0FIFOSEL, USBC_DCLRM);
	/* Maximum bit width for FIFO access set */
	USB_MDF_PAT(D0FIFOSEL, USB_D0FIFO_MBW, USBC_MBW);
}
/******************************************************************************
End of function
******************************************************************************/
#endif	/* == USBC_592IP_PP */
/******************************************************************************
End  Of File
******************************************************************************/
