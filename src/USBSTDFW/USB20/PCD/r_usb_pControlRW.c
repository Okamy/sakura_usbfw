/******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized.
* This software is owned by Renesas Electronics Corporation and is  protected
* under all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES
* REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY,
* INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR  A
* PARTICULAR PURPOSE AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE  EXPRESSLY
* DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE  LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES
* FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS
* AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this
* software and to discontinue the availability of this software.
* By using this software, you agree to the additional terms and
* conditions found by accessing the following link:
* http://www.renesas.com/disclaimer
*******************************************************************************
* Copyright (C) 2010(2011) Renesas Electronics Corpration
* and Renesas Solutions Corp. All rights reserved.
*******************************************************************************
* File Name    : r_usb_pControlRW.c
* Version      : 1.10
* Device(s)    : Renesas SH-Series, RX-Series
* Tool-Chain   : Renesas SuperH RISC engine Standard Toolchain
*              : Renesas RX Standard Toolchain
* OS           : Common to None and uITRON 4.0 Spec
* H/W Platform : Independent
* Description  : USB Peripheral control transfer API code
*******************************************************************************
* History : DD.MM.YYYY Version Description
*         : 17.03.2010 0.80    First Release
*         : 30.07.2010 0.90    Updated comments
*         : 02.08.2010 0.91    Updated comments
*         : 29.10.2010 1.00    Mass Production Release
*         : 01.06.2011 1.10    Version 1.10 Release
******************************************************************************/


/******************************************************************************
Includes   <System Includes> , "Project Includes"
******************************************************************************/
#include "r_usbc_cTypedef.h"		/* Type define */
#include "r_usb_cDefUsr.h"			/* USB-H/W register set (user define) */
#include "r_usbc_cDefUSBIP.h"		/* USB-FW Library Header */
#include "r_usb_cExtern.h"			/* USB-FW global definition */


/******************************************************************************
Section    <Section Definition> , "Project Sections"
******************************************************************************/
#pragma section _pcd

/******************************************************************************
Renesas Abstracted Peripheral Control RW API functions
******************************************************************************/

/******************************************************************************
Function Name   : R_usb_pstd_ControlRead
Description     : Control read start
Arguments       : uint32_t bsize
                : uint8_t *table
Return value    : uint16_t
******************************************************************************/
uint16_t R_usb_pstd_ControlRead(uint32_t bsize, uint8_t *table)
{
	uint16_t	end_flag;

	usb_gcstd_DataCnt[USBC_PIPE0] = bsize;
	usb_gcstd_DataPtr[USBC_PIPE0] = table;

	usb_cstd_FPortChange2((uint16_t)USBC_PIPE0, (uint16_t)USBC_CUSE
		, (uint16_t)USBC_ISEL);
	/* Buffer clear */
	USB_WR(CFIFOCTR, USBC_BCLR);

	USB_CLR_STS(BEMPSTS, USBC_BIT0);

	/* Peripheral Control sequence */
	end_flag = usb_cstd_Buf2Cfifo((uint16_t)USBC_PIPE0);

	/* Peripheral control sequence */
	switch( end_flag )
	{
	/* End of data write */
	case USBC_WRITESHRT:
		/* Enable not ready interrupt */
		usb_cstd_NrdyEnable((uint16_t)USBC_PIPE0);
		/* Set PID=BUF */
		usb_cstd_SetBuf((uint16_t)USBC_PIPE0);
		break;
	/* End of data write (not null) */
	case USBC_WRITEEND:
		/* Continue */
	/* Continue of data write */
	case USBC_WRITING:
		/* Enable empty interrupt */
		usb_cstd_BempEnable((uint16_t)USBC_PIPE0);
		/* Enable not ready interrupt */
		usb_cstd_NrdyEnable((uint16_t)USBC_PIPE0);
		/* Set PID=BUF */
		usb_cstd_SetBuf((uint16_t)USBC_PIPE0);
		break;
	/* FIFO access error */
	case USBC_FIFOERROR:
		break;
	default:
		break;
	}
	/* End or error or continue */
	return (end_flag);
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : R_usb_pstd_ControlWrite
Description     : Control write start
Arguments       : uint32_t bsize
                : uint8_t *table
Return value    : none
******************************************************************************/
void R_usb_pstd_ControlWrite(uint32_t bsize, uint8_t *table)
{
	usb_gcstd_DataCnt[USBC_PIPE0] = bsize;
	usb_gcstd_DataPtr[USBC_PIPE0] = table;

	usb_cstd_FPortChange2((uint16_t)USBC_PIPE0, (uint16_t)USBC_CUSE
		, USBC_NO);
	/* Buffer clear */
	USB_WR(CFIFOCTR, USBC_BCLR);

	/* Interrupt enable */
	/* Enable ready interrupt */
	usb_cstd_BrdyEnable((uint16_t)USBC_PIPE0);
	/* Enable not ready interrupt */
	usb_cstd_NrdyEnable((uint16_t)USBC_PIPE0);

	/* Set PID=BUF */
	usb_cstd_SetBuf((uint16_t)USBC_PIPE0);
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : R_usb_pstd_ControlEnd
Description     : Control transfer stop(end)
Arguments       : uint16_t status
Return value    : none
******************************************************************************/
void R_usb_pstd_ControlEnd(uint16_t status)
{
	/* Interrupt disable */
	/* BEMP0 disable */
	usb_cstd_BempDisable((uint16_t)USBC_PIPE0);
	/* BRDY0 disable */
	usb_cstd_BrdyDisable((uint16_t)USBC_PIPE0);
	/* NRDY0 disable */
	usb_cstd_NrdyDisable((uint16_t)USBC_PIPE0);

	USB_MDF_PAT(CFIFOSEL, USB_CFIFO_MBW, USBC_MBW);

	if( (status == USBC_DATA_ERR) || (status == USBC_DATA_OVR) )
	{
		/* Request error */
		usb_cstd_SetStall((uint16_t)USBC_PIPE0);
	}
	else if( status == USBC_DATA_STOP )
	{
		/* Pipe stop */
		usb_cstd_SetNak((uint16_t)USBC_PIPE0);
	}
	else
	{
		/* Set CCPL bit */
		USB_SET_PAT(DCPCTR, USBC_CCPL);
	}
}
/******************************************************************************
End of function
******************************************************************************/

/******************************************************************************
End  Of File
******************************************************************************/
