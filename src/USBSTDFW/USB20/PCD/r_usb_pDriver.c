/******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized.
* This software is owned by Renesas Electronics Corporation and is  protected
* under all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES
* REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY,
* INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR  A
* PARTICULAR PURPOSE AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE  EXPRESSLY
* DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE  LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES
* FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS
* AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this
* software and to discontinue the availability of this software.
* By using this software, you agree to the additional terms and
* conditions found by accessing the following link:
* http://www.renesas.com/disclaimer
*******************************************************************************
* Copyright (C) 2010(2011) Renesas Electronics Corpration
* and Renesas Solutions Corp. All rights reserved.
*******************************************************************************
* File Name    : r_usb_pDriver.c
* Version      : 1.10
* Device(s)    : Renesas SH-Series, RX-Series
* Tool-Chain   : Renesas SuperH RISC engine Standard Toolchain
*              : Renesas RX Standard Toolchain
* OS           : Common to None and uITRON 4.0 Spec
* H/W Platform : Independent
* Description  : USB Peripheral driver code
*******************************************************************************
* History : DD.MM.YYYY Version Description
*         : 17.03.2010 0.80    First Release
*         : 30.07.2010 0.90    Updated comments
*         : 02.08.2010 0.91    Updated comments
*         : 29.10.2010 1.00    Mass Production Release
*         : 01.06.2011 1.10    Version 1.10 Release
******************************************************************************/

/******************************************************************************
Includes   <System Includes> , "Project Includes"
******************************************************************************/
#include "r_usbc_cTypedef.h"		/* Type define */
#include "r_usb_cDefUsr.h"			/* USB-H/W register set (user define) */
#include "r_usbc_cKernelId.h"		/* Kernel ID definition */
#include "r_usbc_cDefUSBIP.h"		/* USB-FW Library Header */
#include "r_usbc_cMacPrint.h"		/* Standard IO macro */
#include "r_usbc_cMacSystemcall.h"	/* System call macro */
#include "r_usb_cExtern.h"			/* USB-FW global definition */


/******************************************************************************
Section    <Section Definition> , "Project Sections"
******************************************************************************/
#pragma section _pcd

/******************************************************************************
Private global variables and functions
******************************************************************************/
/* Pcd Task receive message */
USBC_PCDINFO_t	*usb_gpstd_PcdMsg;
/* Stall Pipe info */
uint16_t		usb_gpstd_StallPipe[USBC_MAX_PIPE_NO + 1u];
/* Stall Callback function */
USBC_CB_INFO_t	usb_gpstd_StallCB;

/* Configuration number */
uint16_t	usb_gpstd_ConfigNum = 0;
/* Alternate number */
uint16_t	usb_gpstd_AltNum[USBC_ALT_NO];
/* Remote wakeup enable flag */
uint16_t	usb_gpstd_RemoteWakeup = USBC_NO;
/* Test mode selectors */
uint16_t	usb_gpstd_TestModeSelect;
/* Test mode flag */
uint16_t	usb_gpstd_TestModeFlag = USBC_NO;
/* Index of endpoint information table */
uint16_t	usb_gpstd_EpTblIndex[USBC_MAX_EP_NO + 1u];
/* Request type */
uint16_t	usb_gpstd_ReqType = (uint16_t)-1;
/* Request type TYPE */
uint16_t	usb_gpstd_ReqTypeType;
/* Request type RECIPIENT */
uint16_t	usb_gpstd_ReqTypeRecip;
/* Request */
uint16_t	usb_gpstd_ReqRequest;
/* Value */
uint16_t	usb_gpstd_ReqValue;
/* Index */
uint16_t	usb_gpstd_ReqIndex;
/* Length */
uint16_t	usb_gpstd_ReqLength;

/* INTSTS0 */
uint16_t	usb_gpstd_intsts0;

/* Driver registration */
USBC_PCDREG_t	usb_gpstd_Driver = 
{
	/* Pipe define table address */
	(uint16_t**)&usb_cstd_DummyFunction,
	/* Device descriptor table address */
	(uint8_t*)	&usb_cstd_DummyFunction,
	/* Qualifier descriptor table address */
	(uint8_t*)	&usb_cstd_DummyFunction,
	/* Configuration descriptor table address */
	(uint8_t**) &usb_cstd_DummyFunction,
	/* Other configuration descriptor table address */
	(uint8_t**) &usb_cstd_DummyFunction,
	/* String descriptor table address */
	(uint8_t**) &usb_cstd_DummyFunction,
	/* Driver init */
	&usb_cstd_DummyFunction,
	/* Device default */
	&usb_cstd_DummyFunction,
	/* Device configured */
	&usb_cstd_DummyFunction,
	/* Device detach */
	&usb_cstd_DummyFunction,
	/* Device suspend */
	&usb_cstd_DummyFunction,
	/* Device resume */
	&usb_cstd_DummyFunction,
	/* Interfaced change */
	&usb_cstd_DummyFunction,
	/* Control transfer */
	&usb_cstd_DummyTrn,
};

/* Device Request - Request structure */
USBC_REQUEST_t	usb_gpstd_ReqReg;


/******************************************************************************
Renesas Abstracted Peripheral Driver functions
******************************************************************************/

/******************************************************************************
Function Name   : usb_pstd_PcdSndMbx
Description     : PCD Send Mailbox
Arguments       : uint16_t msginfo
                : uint16_t keyword
                : USBC_CB_INFO_t complete ; callback functionuint16_t keyword
Return value    : USBC_ER_t Error Info
******************************************************************************/
USBC_ER_t usb_pstd_PcdSndMbx(uint16_t msginfo, uint16_t keyword
	, USBC_CB_INFO_t complete)
{
	USBC_MH_t		p_blf;
	USBC_ER_t		err, err2;
	USBC_PCDINFO_t	*ptr;

	/* Get Memory pool for send message */
	err = USBC_PGET_BLK(USB_PCD_MPL, &p_blf);
	if( err == USBC_E_OK )
	{
		ptr = (USBC_PCDINFO_t*)p_blf;
		ptr->msghead		= (USBC_MH_t)USBC_NULL;
		ptr->msginfo		= msginfo;
		ptr->keyword		= keyword;
		ptr->complete		= complete;

		/* Send message for usb_pstd_PcdTask */
		err = USBC_SND_MSG(USB_PCD_MBX, (USBC_MSG_t*)p_blf);
		if( err != USBC_E_OK )
		{
			USBC_PRINTF1("### pPcdSndMbx snd_msg error (%ld)\n", err);
			err2 = USBC_REL_BLK(USB_PCD_MPL,(USBC_MH_t)p_blf);
			if( err2 != USBC_E_OK )
			{
				USBC_PRINTF1("### pPcdSndMbx rel_blk error (%ld)\n", err2);
			}
		}
	}
	else
	{
		USBC_PRINTF1("### pPcdSndMbx pget_blk error\n", err);
	}
	return err;
}
/******************************************************************************
End of function
******************************************************************************/

/******************************************************************************
Function Name   : usb_pstd_PcdRelMpl
Description     : PCD REL_BLK send
Arguments       : uint16_t n
Return value    : none
******************************************************************************/
void usb_pstd_PcdRelMpl(uint16_t n)
{
	USBC_ER_t		err;

	/* PCD memory pool release */
	err = USBC_REL_BLK(USB_PCD_MPL, (USBC_MH_t)usb_gpstd_PcdMsg);
	if( err != USBC_E_OK )
	{
		USBC_PRINTF2("### usb_pstd_PcdRelMpl (%d) rel_blk error: %d\n"
			, n, err);
	}
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pstd_PcdTask
Description     : USB Peripheral control driver Task
Arguments       : USBC_VP_INT stacd
Return value    : none
******************************************************************************/
void usb_pstd_PcdTask(USBC_VP_INT stacd)
{
	USBC_UTR_t		*mess, *ptr;
	/* Error code */
	USBC_ER_t		err;
	uint16_t		pipenum;
/* Condition compilation by the difference of the operating system */
#if USBC_FW_PP == USBC_FW_NONOS_PP
		err = USBC_TRCV_MSG(USB_PCD_MBX, (USBC_MSG_t**)&mess
			, (USBC_TM_t)10000);
		if( (err != USBC_E_OK) )
		{
			return;
		}
#else	/* USBC_FW_PP == USBC_FW_NONOS_PP */
	while( 1 )
	{
		/* Pcd Task message receive */
		do
		{
			err = USBC_TRCV_MSG(USB_PCD_MBX, (USBC_MSG_t**)&mess
					, (USBC_TM_t)10000);
			if( (err != USBC_E_OK) && (err != USBC_E_TMOUT) )
			{
				USBC_PRINTF1("### pPcdTask rcv_msg error (%ld)\n", err);
			}
		}
		while( err != USBC_E_OK );
#endif	/* USBC_FW_PP == USBC_FW_NONOS_PP */

		usb_gpstd_PcdMsg = (USBC_PCDINFO_t*)mess;
		pipenum = usb_gpstd_PcdMsg->keyword;

		/* Detach is all device */
		switch( usb_gpstd_PcdMsg->msginfo )
		{
		case USBC_MSG_PCD_INT:
			/* USB INT */
			usb_pstd_Interrupt((USBC_UTR_t*)usb_gpstd_PcdMsg);
			break;
		case USBC_MSG_PCD_PCUTINT:
			/* Start Oscillation : Interrupt wakeup */
			usb_cstd_InterruptClock();
			ptr = (USBC_UTR_t*)usb_gpstd_PcdMsg;
			/* USB interrupt Handler */
			usb_pstd_InterruptHandler(&ptr->keyword, &ptr->status);
			/* USB INT */
			usb_pstd_Interrupt((USBC_UTR_t*)usb_gpstd_PcdMsg);
			ptr->msginfo = USBC_MSG_PCD_INT;
			break;

		case USBC_MSG_PCD_SUBMITUTR:
			/* USB Submit utr */
			usb_pstd_SetSubmitutr((USBC_UTR_t*)usb_gpstd_PcdMsg);
			break;
		case USBC_MSG_PCD_REMOTEWAKEUP:
			usb_cstd_SelfClock();
			usb_pstd_RemoteWakeup();
			/* Process Done Callback function */
			(*usb_gpstd_PcdMsg->complete)((uint16_t)USBC_NO_ARG
				, USBC_MSG_PCD_REMOTEWAKEUP);
			/* PCD memory pool release */
			usb_pstd_PcdRelMpl((uint16_t)1u);
			break;

		case USBC_MSG_PCD_CLRSEQBIT:
			usb_cstd_DoSqclr(pipenum);
			/* Process Done Callback function */
			(*usb_gpstd_PcdMsg->complete)((uint16_t)USBC_NO_ARG
				, USBC_MSG_PCD_CLRSEQBIT);
			/* PCD memory pool release */
			usb_pstd_PcdRelMpl((uint16_t)2u);
			break;
		case USBC_MSG_PCD_SETSTALL:
			usb_cstd_SetStall(pipenum);
			usb_gpstd_StallPipe[pipenum] = USBC_YES;
			usb_gpstd_StallCB = usb_gpstd_PcdMsg->complete;
			/* PCD memory pool release */
			usb_pstd_PcdRelMpl((uint16_t)3u);
			break;

		case USBC_MSG_PCD_TRANSEND1:
			/* End of all pipes */
			if( usb_gcstd_Pipe[pipenum] != USBC_NULL )
			{
				/* Transfer timeout */
				usb_cstd_ForcedTermination(pipenum
					, (uint16_t)USBC_DATA_TMO);
			}
			else
			{
				USBC_PRINTF1("### Peri not transferd-1 %d\n", pipenum);
			}
			/* PCD memory pool release */
			usb_pstd_PcdRelMpl((uint16_t)4u);
			break;
		case USBC_MSG_PCD_TRANSEND2:
			/* End of all pipes */
			if( usb_gcstd_Pipe[pipenum] != USBC_NULL )
			{
				/* Transfer stop */
				usb_cstd_ForcedTermination(pipenum
					, (uint16_t)USBC_DATA_STOP);
			}
			else
			{
				USBC_PRINTF1("### Peri not transferd-2 %d\n", pipenum);
			}
			/* PCD memory pool release */
			usb_pstd_PcdRelMpl((uint16_t)5u);
			break;

		case USBC_MSG_PCD_DETACH:
			usb_cstd_SelfClock();
			/* USB detach */
			usb_pstd_DetachProcess();
			/* Process Done Callback function */
			(*usb_gpstd_PcdMsg->complete)((uint16_t)USBC_NO_ARG
				, USBC_MSG_PCD_DETACH);
			/* PCD memory pool release */
			usb_pstd_PcdRelMpl((uint16_t)6u);
			break;
		case USBC_MSG_PCD_ATTACH:
			usb_cstd_SelfClock();
			usb_pstd_AttachProcess();
			/* Process Done Callback function */
			(*usb_gpstd_PcdMsg->complete)((uint16_t)USBC_NO_ARG
				, USBC_MSG_PCD_ATTACH);
			/* PCD memory pool release */
			usb_pstd_PcdRelMpl((uint16_t)7u);
			break;


		case USBC_MSG_PCD_DP_ENABLE:
			usb_pstd_DpEnable();
			/* Process Done Callback function */
			(*usb_gpstd_PcdMsg->complete)((uint16_t)USBC_NO_ARG
				, USBC_MSG_PCD_DP_ENABLE);
			/* PCD memory pool release */
			usb_pstd_PcdRelMpl((uint16_t)8u);
			break;
		case USBC_MSG_PCD_DP_DISABLE:
			usb_pstd_DpDisable();
			/* Process Done Callback function */
			(*usb_gpstd_PcdMsg->complete)((uint16_t)USBC_NO_ARG
				, USBC_MSG_PCD_DP_DISABLE);
			/* PCD memory pool release */
			usb_pstd_PcdRelMpl((uint16_t)9u);
			break;
		case USBC_MSG_PCD_DM_ENABLE:
			usb_pstd_DmEnable();
			/* Process Done Callback function */
			(*usb_gpstd_PcdMsg->complete)((uint16_t)USBC_NO_ARG
				, USBC_MSG_PCD_DM_ENABLE);
			/* PCD memory pool release */
			usb_pstd_PcdRelMpl((uint16_t)10u);
			break;
		case USBC_MSG_PCD_DM_DISABLE:
			usb_pstd_DmDisable();
			/* Process Done Callback function */
			(*usb_gpstd_PcdMsg->complete)((uint16_t)USBC_NO_ARG
				, USBC_MSG_PCD_DM_DISABLE);
			/* PCD memory pool release */
			usb_pstd_PcdRelMpl((uint16_t)11u);
			break;

#if USBC_TARGET_CHIP_PP == USBC_RX600_PP || USBC_TARGET_CHIP_PP == USBC_ASSP_PP
		case USBC_MSG_PCD_D0FIFO_INT:
			usb_cstd_D0fifoInt();
			break;

		case USBC_MSG_PCD_D1FIFO_INT:
			break;

		case USBC_MSG_PCD_RESM_INT:
			break;
#endif
/* USBC_TARGET_CHIP_PP == USBC_RX600_PP || USBC_TARGET_CHIP_PP == USBC_ASSP_PP */

		default:
			while( 1 )
			{
			};
			break;
		}
/* Condition compilation by the difference of the operating system */
#if USBC_FW_PP == USBC_FW_NONOS_PP
#else	/* USBC_FW_PP == USBC_FW_NONOS_PP */
	}
#endif	/* USBC_FW_PP == USBC_FW_NONOS_PP */
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pstd_SetSubmitutr
Description     : USB Submit utr
Arguments       : USBC_UTR_t *utrmsg
Return value    : USBC_ER_t
******************************************************************************/
USBC_ER_t usb_pstd_SetSubmitutr(USBC_UTR_t *utrmsg)
{
	uint16_t	pipenum;

	pipenum = utrmsg->keyword;
	usb_gcstd_Pipe[pipenum] = utrmsg;

	/* Check state ( Configured ) */
	if( usb_pstd_ChkConfigured() == USBC_YES )
	{
		/* Data transfer */
		usb_pstd_SetReTransfer(pipenum);
	}
	else
	{
		/* Transfer stop */
		usb_cstd_ForcedTermination(pipenum, (uint16_t)USBC_DATA_ERR);
	}
	return USBC_DONE;
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pstd_SetReTransfer
Description     : Data Transfer start
Arguments       : uint16_t pipe
Return value    : none
******************************************************************************/
void usb_pstd_SetReTransfer(uint16_t pipe)
{
	/* Data transfer */
	if( usb_cstd_GetPipeDir(pipe) == USBC_DIR_P_OUT )
	{	/* Out transfer */
		usb_cstd_ReceiveStart(pipe);
	}
	else
	{
		/* In transfer */
		usb_cstd_SendStart(pipe);
	}
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pstd_Interrupt
Description     : USB INT
Arguments       : USBC_UTR_t *p
Return value    : none
******************************************************************************/
void usb_pstd_Interrupt(USBC_UTR_t *p)
{
	uint16_t	intsts, status, stginfo;

	intsts = p->keyword;
	status = p->status;

	USB_RD(INTSTS0, usb_gpstd_intsts0);

	/* check interrupt status */
	switch( intsts )
	{

	/* BRDY, BEMP, NRDY */
	case USBC_INT_BRDY:
		usb_pstd_BrdyPipe(status);
		break;
	case USBC_INT_BEMP:
		usb_pstd_BempPipe(status);
		break;
	case USBC_INT_NRDY:
		usb_pstd_NrdyPipe(status);
		break;
	/* Resume */
	case USBC_INT_RESM:
		USBC_PRINTF0("RESUME int peri\n");
		/* Callback */
		(*usb_gpstd_Driver.devresume)((uint16_t)USBC_NO_ARG
			, (uint16_t)USBC_NO_ARG);
		usb_pstd_ResumeProcess();
		break;
	/* VBUS */
	case USBC_INT_VBINT:
		if( usb_pstd_ChkVbsts() == USBC_ATTACH )
		{
			USBC_PRINTF0("VBUS int attach\n");
			/* USB attach */
			usb_pstd_AttachProcess();
		}
		else
		{
			USBC_PRINTF0("VBUS int detach\n");
			usb_pstd_DetachFunction();
			/* USB detach */
			usb_pstd_DetachProcess();
		}
		break;
	/* SOF */
	case USBC_INT_SOFR:
		/* User program */
		break;

	/* DVST */
	case USBC_INT_DVST:
		switch( (uint16_t)(status & USBC_DVSQ) )
		{
		/* Power state	*/
		case USBC_DS_POWR:
			break;
		/* Default state  */
		case USBC_DS_DFLT:
			USBC_PRINTF0("USB-reset int peri\n");
			usb_pstd_BusReset();
			break;
		/* Address state  */
		case USBC_DS_ADDS:
			break;
		/* Configured state	 */
		case USBC_DS_CNFG:
			USBC_PRINTF0("Device configuration int peri\n");
			break;
		/* Power suspend state */
		case USBC_DS_SPD_POWR:
			/* Continue */
		/* Default suspend state */
		case USBC_DS_SPD_DFLT:
			/* Continue */
		/* Address suspend state */
		case USBC_DS_SPD_ADDR:
			/* Continue */
		/* Configured Suspend state */
		case USBC_DS_SPD_CNFG:
			USBC_PRINTF0("SUSPEND int peri\n");
			usb_pstd_SuspendProcess();
			(*usb_gpstd_Driver.devsuspend)((uint16_t)USBC_NO_ARG
				, (uint16_t)USBC_NO_ARG);	/* Callback */
			break;
		/* Error */
		default:
			break;
		}
		break;

	/* CTRT */
	case USBC_INT_CTRT:
		stginfo = (uint16_t)(status & USBC_CTSQ);
		if( (stginfo == USBC_CS_IDST) )
		{
			/* check Test mode */
			if( usb_gpstd_TestModeFlag == USBC_YES )
			{
				/* Test mode */
				usb_pstd_TestMode();
			}
		}
		else
		{
			if( ((stginfo == USBC_CS_RDDS) || (stginfo == USBC_CS_WRDS))
				|| (stginfo == USBC_CS_WRND) )
			{
				/* Save request register */
				usb_pstd_SaveRequest();
			}
		}

		if( usb_gpstd_ReqTypeType == USBC_STANDARD )
		{
			/* check CTSQ */
			switch( stginfo )
			{
			/* Idle or setup stage */
			case USBC_CS_IDST:	usb_pstd_StandReq0();	break;
			/* Control read data stage */
			case USBC_CS_RDDS:	usb_pstd_StandReq1();	break;
			/* Control write data stage */
			case USBC_CS_WRDS:	usb_pstd_StandReq2();	break;
			/* Control write nodata status stage */
			case USBC_CS_WRND:	usb_pstd_StandReq3();	break;
			/* Control read status stage */
			case USBC_CS_RDSS:	usb_pstd_StandReq4();	break;
			/* Control write status stage */
			case USBC_CS_WRSS:	usb_pstd_StandReq5();	break;
			/* Control sequence error */
			case USBC_CS_SQER:	R_usb_pstd_ControlEnd(
								(uint16_t)USBC_DATA_ERR);	break;
			default:		R_usb_pstd_ControlEnd((uint16_t)USBC_DATA_ERR);
							/* Illegal */
							break;
			}
		}
		else
		{
			/* Vender Specific */
			usb_gpstd_ReqReg.ReqType		= usb_gpstd_ReqType;
			usb_gpstd_ReqReg.ReqTypeType	= usb_gpstd_ReqTypeType;
			usb_gpstd_ReqReg.ReqTypeRecip	= usb_gpstd_ReqTypeRecip;
			usb_gpstd_ReqReg.ReqRequest		= usb_gpstd_ReqRequest;
			usb_gpstd_ReqReg.ReqValue		= usb_gpstd_ReqValue;
			usb_gpstd_ReqReg.ReqIndex		= usb_gpstd_ReqIndex;
			usb_gpstd_ReqReg.ReqLength		= usb_gpstd_ReqLength;
			(*usb_gpstd_Driver.ctrltrans)
				((USBC_REQUEST_t *)&usb_gpstd_ReqReg, stginfo);
		}
		break;

	/* Error */
	case USBC_INT_UNKNOWN:
		USBC_PRINTF0("pINT_UNKNOWN\n");
		break;
	default:
		USBC_PRINTF1("pINT_default %X\n", intsts);
		break;
	}
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pstd_SaveRequest
Description     : save request register
Arguments       : none
Return value    : none
******************************************************************************/
void usb_pstd_SaveRequest(void)
{
	uint16_t	buf;

	/* Valid clear */
	USB_CLR_STS(INTSTS0, USBC_VALID);
	USB_RD(USBREQ, buf);

	usb_gpstd_ReqType	   = (uint16_t)(buf & USBC_BMREQUESTTYPE);
	usb_gpstd_ReqTypeType  = (uint16_t)(buf & USBC_BMREQUESTTYPETYPE);
	usb_gpstd_ReqTypeRecip = (uint16_t)(buf & USBC_BMREQUESTTYPERECIP);
	usb_gpstd_ReqRequest   = (uint16_t)(buf & USBC_BREQUEST);

/* Condition compilation by the difference of the devices */
#if USBC_TARGET_CHIP_PP == USBC_RX600_PP
	/* Save USB Request Value Register */
	USB_RDW(USBVAL,	 usb_gpstd_ReqValue);
	/* Save USB Request Index Register */
	USB_RDW(USBINDX, usb_gpstd_ReqIndex);
	/* Save USB Request Length Register */
	USB_RDW(USBLENG, usb_gpstd_ReqLength);
#else	/* USBC_TARGET_CHIP_PP == USBC_RX600_PP */
	/* Save USB Request Value Register */
	USB_RD(USBVAL,	usb_gpstd_ReqValue);
	/* Save USB Request Index Register */
	USB_RD(USBINDX, usb_gpstd_ReqIndex);
	/* Save USB Request Length Register */
	USB_RD(USBLENG, usb_gpstd_ReqLength);
#endif	/* USBC_TARGET_CHIP_PP == USBC_RX600_PP */
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pstd_ClearAlt
Description     : Alternate table clear
Arguments       : none
Return value    : none
******************************************************************************/
void usb_pstd_ClearAlt(void)
{
	uint16_t	i;

	for( i = 0; i < USBC_ALT_NO; ++i )
	{
		/* Alternate table clear */
		usb_gpstd_AltNum[i] = 0;
	}
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pstd_ClearMem
Description     : Memory clear
Arguments       : none
Return value    : none
******************************************************************************/
void usb_pstd_ClearMem(void)
{
	/* Configuration number */
	usb_gpstd_ConfigNum = 0;
	/* Remote wakeup enable flag */
	usb_gpstd_RemoteWakeup = USBC_NO;
	usb_gcstd_XckeMode = USBC_NO;
	/* Alternate setting clear */
	usb_pstd_ClearAlt();
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pstd_SetConfigNum
Description     : Memory configuration
Arguments       : uint16_t value          : Configuration Number
Return value    : none
******************************************************************************/
void usb_pstd_SetConfigNum(uint16_t value)
{
	/* Configuration number set */
	usb_gpstd_ConfigNum = value;
	/* Alternate setting clear */
	usb_pstd_ClearAlt();
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pstd_ClearEpTblIndex
Description     : Endpoint index table clear
Arguments       : none
Return value    : none
******************************************************************************/
void usb_pstd_ClearEpTblIndex(void)
{
	uint16_t	i;

	for( i = 0; i <= USBC_MAX_EP_NO; ++i )
	{
		/* Endpoint index table clear */
		usb_gpstd_EpTblIndex[i] = USBC_ERROR;
	}
}
/******************************************************************************
End of function
******************************************************************************/



/******************************************************************************
Function Name   : usb_pstd_GetConfigNum
Description     : Get Configuration number
Arguments       : none
Return value    : uint16_t ; Number of possible configurations 
                : (bNumConfigurations)
******************************************************************************/
uint16_t usb_pstd_GetConfigNum(void)
{
	/* Configuration Number */
	return (uint16_t)(usb_gpstd_Driver.devicetbl[USBC_DEV_NUM_CONFIG]);
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pstd_GetInterfaceNum
Description     : Get Interface number
Arguments       : uint16_t con_num ; Configuration Number
Return value    : uint16_t ; Number of this interface 
                : (bNumInterfaces)
******************************************************************************/
uint16_t usb_pstd_GetInterfaceNum(uint16_t con_num)
{
	uint16_t	conf;

	conf = con_num;
	if( conf < (uint16_t)1 )
	{
		/* Address state */
		conf = (uint16_t)1;
	}
	/* Interface number */
	return (uint16_t)(*(uint8_t*)
		((uint32_t)usb_gpstd_Driver.configtbl[conf - 1u] + (uint16_t)4u));
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pstd_GetAlternateNum
Description     : Get Alternate setting number
Arguments       : uint16_t con_num ; Configuration Number
                : uint16_t int_num ; Interface Number
Return value    : uint16_t ; Value used to select this alternate
                : (bAlternateSetting)
******************************************************************************/
uint16_t usb_pstd_GetAlternateNum(uint16_t con_num, uint16_t int_num)
{
	uint16_t	i, conf;
	uint16_t	alt_num = 0;
	uint8_t		*ptr;
	uint16_t	length;

	conf = con_num;
	if( conf < (uint16_t)1 )
	{
		/* Address state */
		conf = (uint16_t)1;
	}

	ptr = usb_gpstd_Driver.configtbl[conf - 1];
	i = ptr[0];
	/* Interface descriptor[0] */
	ptr = (uint8_t*)((uint32_t)ptr + ptr[0]);
	length
		= (uint16_t)
		(*(uint8_t*)((uint32_t)	usb_gpstd_Driver.configtbl[conf-1u]
		+ (uint16_t)2u));
	length
		|= (uint16_t)((uint16_t)(*(uint8_t*)
		((uint32_t)usb_gpstd_Driver.configtbl[conf-1u]
		+ (uint16_t)3u)) << 8u);
	
	/* Search descriptor table size */
	for(  ; i < length;	 )
	{
		/* Descriptor type ? */
		switch( ptr[1] )
		{
		/* Interface */
		case USBC_DT_INTERFACE:
			if( int_num == ptr[2] )
			{
				/* Alternate number count */
				alt_num = (uint16_t)ptr[3];
			}
			i += ptr[0];
			/* Interface descriptor[0] */
			ptr =(uint8_t*)((uint32_t)ptr + ptr[0]);
			break;
		/* Device */
		case USBC_DT_DEVICE:
			/* Continue */
		/* Configuration */
		case USBC_DT_CONFIGURATION:
			/* Continue */
		/* String */
		case USBC_DT_STRING:
			/* Continue */
		/* Endpoint */
		case USBC_DT_ENDPOINT:
			/* Continue */
		/* Class, Vendor, else */
		default:
			i += ptr[0];
			/* Interface descriptor[0] */
			ptr =(uint8_t*)((uint32_t)ptr + ptr[0]);
			break;
		}
	}
	return alt_num;
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pstd_SetEpTblIndex
Description     : Endpoint table initialize
Arguments       : uint16_t con_num ; Configuration Number
                : uint16_t int_num ; Interface Number
                : uint16_t alt_num ; Alternate Setting
Return value    : none
******************************************************************************/
void usb_pstd_SetEpTblIndex(uint16_t con_num, uint16_t int_num
	, uint16_t alt_num)
{
	uint8_t			*ptr;
	uint16_t		i, j, length, conf;
	uint16_t		start, numbers, ep;

	conf = con_num;
	if( conf < (uint16_t)1 )
	{
		/* Address state */
		conf = (uint16_t)1;
	}

	/* Configuration descriptor */
	ptr = usb_gpstd_Driver.configtbl[conf - 1u];
	i = *ptr;
	length = (uint16_t)(*(uint8_t*)((uint32_t)ptr + (uint32_t)3u));
	length = (uint16_t)(length << 8);
	length += (uint16_t)(*(uint8_t*)((uint32_t)ptr + (uint32_t)2u));
	ptr =(uint8_t*)((uint32_t)ptr + *ptr);
	start = 0;
	numbers = 0;
	j = 0;

	for(  ; i < length;	 )
	{
		/* Descriptor type ? */
		switch(*(uint8_t*)((uint32_t)ptr + (uint32_t)1u) )
		{
		/* Interface */
		case USBC_DT_INTERFACE:
			if((*(uint8_t*)((uint32_t)ptr + (uint32_t)2u) == int_num)
				&& (*(uint8_t*)((uint32_t)ptr + (uint32_t)3u) == alt_num))
			{
				numbers = *(uint8_t*)((uint32_t)ptr + (uint32_t)4u);
			}
			else
			{
				start += *(uint8_t*)((uint32_t)ptr + (uint32_t)4u);
			}
			i += *ptr;
			ptr =(uint8_t*)((uint32_t)ptr + *ptr);
			break;
		/* Endpoint */
		case USBC_DT_ENDPOINT:
			if( j < numbers )
			{
				ep = (uint16_t)*(uint8_t*)((uint32_t)ptr + (uint32_t)2u);
				ep &= (uint16_t)0x0f;
				usb_gpstd_EpTblIndex[ep] = (uint8_t)(start + j);
				++j;
			}
			i += *ptr;
			ptr = (uint8_t*)((uint32_t)ptr + *ptr);
			break;
		/* Device */
		case USBC_DT_DEVICE:
			/* Continue */
		/* Configuration */
		case USBC_DT_CONFIGURATION:
			/* Continue */
		/* String */
		case USBC_DT_STRING:
			/* Continue */
		/* Class, Vendor, else */
		default:
			i += *ptr;
			ptr = (uint8_t*)((uint32_t)ptr + *ptr);
			break;
		}
	}
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pstd_ChkRemote
Description     : Check remote wakeup bit (bmAttributes)
Arguments       : none
Return value    : uint16_t ; remote wakeup status(YES/NO)
******************************************************************************/
uint16_t usb_pstd_ChkRemote(void)
{
	uint8_t	atr;

	if( usb_gpstd_ConfigNum == 0 )
	{
		return USBC_NO;
	}

	/* Get Configuration Descriptor - bmAttributes */
	atr = *(uint8_t*)((uint32_t)usb_gpstd_Driver.configtbl
			[usb_gpstd_ConfigNum - 1u] + (uint32_t)7u);
	/* Remote Wakeup check(= D5) */
	if( (atr & USBC_CF_RWUP) == USBC_CF_RWUP )
	{
		return USBC_YES;
	}
	return USBC_NO;
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pstd_GetCurrentPower
Description     : Get currently power
Arguments       : none
Return value    : uint8_t ; Currently power status(GS_SELFPOWERD/GS_BUSPOWERD)
******************************************************************************/
uint8_t usb_pstd_GetCurrentPower(void)
{
	/*
	 * Please answer the currently power of your system.
	 */

	uint8_t	tmp, currentpower, conf;

	conf = (uint8_t)usb_gpstd_ConfigNum;
	if( conf < (uint8_t)1 )
	{
		/* Address state */
		conf = (uint8_t)1;
	}

	/* Standard configuration descriptor */
	tmp = *(uint8_t*)((uint32_t)usb_gpstd_Driver.configtbl
			[(conf - 1u)] + (uint32_t)7u);
	if( (tmp & USBC_CF_SELF) == USBC_CF_SELF )
	{
		/* Self Powered */
		currentpower = USBC_GS_SELFPOWERD;
	}
	else
	{
		/* Bus Powered */
		currentpower = USBC_GS_BUSPOWERD;
	}

	/* Check currently powered */

	return currentpower;
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pstd_ChkConfigured
Description     : Check state ( Configured )
Arguments       : none
Return value    : uint16_t ; Configuration state (YES/NO)
******************************************************************************/
uint16_t usb_pstd_ChkConfigured(void)
{
	uint16_t	buf;

	USB_RD(INTSTS0, buf);
	/* Device Status - Configured check */
	if( (buf & USBC_DVSQ) == USBC_DS_CNFG )
	{
		/* Configured */
		return USBC_YES;
	}
	else
	{
		/* not Configured */
		return USBC_NO;
	}
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Renesas Abstracted Peripheral Driver functions
******************************************************************************/
/* Condition compilation by the difference of IP */
#if USBC_IPSEL_PP == USBC_597IP_PP

/******************************************************************************
Function Name   : usb_pstd_InterruptHandler
Description     : USB interrupt Handler
Arguments       : uint16_t *keyword : Interrupt type
                : uint16_t *status : Interrupt status
Return value    : none
******************************************************************************/
void usb_pstd_InterruptHandler(uint16_t *keyword, uint16_t *status)
{
	uint16_t	intsts0, intenb0, ists0;
	uint16_t	intsts1, intenb1, ists1;
	uint16_t	brdysts, brdyenb, bsts;
	uint16_t	nrdysts, nrdyenb, nsts;
	uint16_t	bempsts, bempenb, ests;

	/* Register Save */
	USB_RD(INTSTS0, intsts0);
/* Condition compilation by the difference of USB function */
#if	USB_FUNCSEL_PP != USBC_PERI_PP
	USB_RD(INTSTS1, intsts1);
#endif	/* USBC_PERI_PP */
	USB_RD(BRDYSTS, brdysts);
	USB_RD(NRDYSTS, nrdysts);
	USB_RD(BEMPSTS, bempsts);
	USB_RD(INTENB0, intenb0);
/* Condition compilation by the difference of USB function */
#if	USB_FUNCSEL_PP != USBC_PERI_PP
	USB_RD(INTENB1, intenb1);
#endif	/* USBC_PERI_PP */
	USB_RD(BRDYENB, brdyenb);
	USB_RD(NRDYENB, nrdyenb);
	USB_RD(BEMPENB, bempenb);

	*keyword = USBC_INT_UNKNOWN;
	*status	 = 0;
	/* Interrupt status get */
	ists0 = (uint16_t)(intsts0 & intenb0);
	ists1 = (uint16_t)(intsts1 & intenb1);
	bsts  = (uint16_t)(brdysts & brdyenb);
	nsts  = (uint16_t)(nrdysts & nrdyenb);
	ests  = (uint16_t)(bempsts & bempenb);

	if( (intsts0 & (USBC_VBINT|USBC_RESM|USBC_SOFR|USBC_DVST|USBC_CTRT|USBC_BEMP|USBC_NRDY|USBC_BRDY)) == 0u )
	{
		usb_pstd_IntHandFunction(ists1);
		return;
	}

	/***** Processing USB bus signal *****/
	/***** Resume signal *****/
	if( (ists0 & USBC_RESM) == USBC_RESM )
	{
		USB_CLR_STS(INTSTS0, USBC_RESM);
		*keyword = USBC_INT_RESM;
	}
	/***** Vbus change *****/
	else if( (ists0 & USBC_VBINT) == USBC_VBINT )
	{
		/* Status clear */
		usb_cstd_VbintClearSts();
		*keyword = USBC_INT_VBINT;
	}
	/***** SOFR change *****/
	else if( (ists0 & USBC_SOFR) == USBC_SOFR )
	{
		/* SOFR Clear */
		USB_CLR_STS(INTSTS0, USBC_SOFR);
		*keyword = USBC_INT_SOFR;
	}

	/***** Processing device state *****/
	/***** DVST change *****/
	else if( (ists0 & USBC_DVST) == USBC_DVST )
	{
		/* DVST clear */
		USB_CLR_STS(INTSTS0, USBC_DVST);
		*keyword = USBC_INT_DVST;
		*status	 = intsts0;
	}

	/***** Processing PIPE0 data *****/
	else if( ((ists0 & USBC_BRDY) == USBC_BRDY) && ((bsts & USBC_BRDY0) == USBC_BRDY0) )
	{
		USB_CLR_STS(BRDYSTS, USBC_BRDY0);
		*keyword = USBC_INT_BRDY;
		*status	 = USBC_BRDY0;
	}
	else if( ((ists0 & USBC_BEMP) == USBC_BEMP) && ((ests & USBC_BEMP0) == USBC_BEMP0) )
	{
		USB_CLR_STS(BEMPSTS, USBC_BEMP0);
		*keyword = USBC_INT_BEMP;
		*status	 = USBC_BRDY0;
	}
	else if( ((ists0 & USBC_NRDY) == USBC_NRDY) && ((nsts & USBC_NRDY0) == USBC_NRDY0) )
	{
		USB_CLR_STS(NRDYSTS, USBC_NRDY0);
		*keyword = USBC_INT_NRDY;
		*status	 = USBC_BRDY0;
	}

	/***** Processing setup transaction *****/
	else if( (ists0 & USBC_CTRT) == USBC_CTRT )
	{
		/* CTSQ bit changes later than CTRT bit for ASSP. */
		/* CTSQ reloading */
		USB_RD(INTSTS0, *status);
		/* USBC_CTRT clear */
		USB_CLR_STS(INTSTS0, USBC_CTRT);
		*keyword = USBC_INT_CTRT;
	}

	/***** Processing PIPE1-MAX_PIPE_NO data *****/
	/***** EP0-7 BRDY *****/
	else if( (ists0 & USBC_BRDY) == USBC_BRDY )
	{
		USB_CLR_STS(BRDYSTS, bsts);
		*keyword = USBC_INT_BRDY;
		*status	 = bsts;
	}
	/***** EP0-7 BEMP *****/
	else if( (ists0 & USBC_BEMP) == USBC_BEMP )
	{
		USB_CLR_STS(BEMPSTS, ests);
		*keyword = USBC_INT_BEMP;
		*status	 = ests;
	}
	/***** EP0-7 NRDY *****/
	else if( (ists0 & USBC_NRDY) == USBC_NRDY )
	{
		USB_CLR_STS(NRDYSTS, nsts);
		*keyword = USBC_INT_NRDY;
		*status	 = nsts;
	}
	else
	{
		usb_pstd_IntHandFunction(ists1);
	}
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pstd_InterruptEnable
Description     : Enable USB ASSP interrupt
Arguments       : none
Return value    : none
******************************************************************************/
void usb_pstd_InterruptEnable(void)
{
	/* Enable VBSE, DVSE, CTRE */
	USB_SET_PAT(INTENB0, (USBC_VBSE | USBC_DVSE | USBC_CTRE ));
}
#endif /* USBC_IPSEL_PP == USBC_597IP_PP */
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Renesas Abstracted Peripheral Driver API functions
******************************************************************************/
/* Condition compilation by the difference of IP */
#if USBC_IPSEL_PP == USBC_596IP_PP

/******************************************************************************
Function Name   : usb_pstd_InterruptHandler
Description     : USB interrupt Handler
Arguments       : uint16_t *keyword : Interrupt type
                : uint16_t *status : Interrupt status
Return value    : none
******************************************************************************/
void usb_pstd_InterruptHandler(uint16_t *keyword, uint16_t *status)
{
	uint16_t	intsts0, intenb0, ists0;
	uint16_t	intsts1, intenb1, ists1;
	uint16_t	brdysts, brdyenb, bsts;
	uint16_t	nrdysts, nrdyenb, nsts;
	uint16_t	bempsts, bempenb, ests;

	/* Register Save */
	USB_RD(INTSTS0, intsts0);
/* Condition compilation by the difference of USB function */
#if	USB_FUNCSEL_PP != USBC_PERI_PP
	USB_RD(INTSTS1, intsts1);
#endif	/* !USBC_PERI_PP */
	USB_RD(BRDYSTS, brdysts);
	USB_RD(NRDYSTS, nrdysts);
	USB_RD(BEMPSTS, bempsts);
	USB_RD(INTENB0, intenb0);
/* Condition compilation by the difference of USB function */
#if	USB_FUNCSEL_PP != USBC_PERI_PP
	USB_RD(INTENB1, intenb1);
#endif	/* != USBC_PERI_PP */
	USB_RD(BRDYENB, brdyenb);
	USB_RD(NRDYENB, nrdyenb);
	USB_RD(BEMPENB, bempenb);

	*keyword = USBC_INT_UNKNOWN;
	*status	 = 0;
	/* Interrupt status get */
	ists0 = (uint16_t)(intsts0 & intenb0);
	ists1 = (uint16_t)(intsts1 & intenb1);
	bsts  = (uint16_t)(brdysts & brdyenb);
	nsts  = (uint16_t)(nrdysts & nrdyenb);
	ests  = (uint16_t)(bempsts & bempenb);

	if( (intsts0 & (USBC_VBINT|USBC_RESM|USBC_SOFR|USBC_DVST|USBC_CTRT|USBC_BEMP|USBC_NRDY|USBC_BRDY)) == 0u )
	{
		usb_pstd_IntHandFunction(ists1);
		return;
	}

	/***** Processing USB bus signal *****/
	/***** Resume signal *****/
	if( (ists0 & USBC_RESM) == USBC_RESM )
	{
		USB_CLR_STS(INTSTS0, USBC_RESM);
		*keyword = USBC_INT_RESM;
	}
	/***** Vbus change *****/
	else if( (ists0 & USBC_VBINT) == USBC_VBINT )
	{
		/* Status clear */
		usb_cstd_VbintClearSts();
		*keyword = USBC_INT_VBINT;
	}
	/***** SOFR change *****/
	else if( (ists0 & USBC_SOFR) == USBC_SOFR )
	{
		/* SOFR clear */
		USB_CLR_STS(INTSTS0, USBC_SOFR);
		*keyword = USBC_INT_SOFR;
	}

	/***** Processing device state *****/
	/***** DVST change *****/
	else if( (ists0 & USBC_DVST) == USBC_DVST )
	{
		/* DVST clear */
		USB_CLR_STS(INTSTS0, USBC_DVST);
		*keyword = USBC_INT_DVST;
		*status	 = intsts0;
	}

	/***** Processing PIPE0 data *****/
	else if( ((ists0 & USBC_BRDY) == USBC_BRDY) && ((bsts & USBC_BRDY0) == USBC_BRDY0) )
	{
		USB_CLR_STS(BRDYSTS, USBC_BRDY0);
		*keyword = USBC_INT_BRDY;
		*status	 = USBC_BRDY0;
	}
	else if( ((ists0 & USBC_BEMP) == USBC_BEMP) && ((ests & USBC_BEMP0) == USBC_BEMP0) )
	{
		USB_CLR_STS(BEMPSTS, USBC_BEMP0);
		*keyword = USBC_INT_BEMP;
		*status	 = USBC_BRDY0;
	}
	else if( ((ists0 & USBC_NRDY) == USBC_NRDY) && ((nsts & USBC_NRDY0) == USBC_NRDY0) )
	{
		USB_CLR_STS(NRDYSTS, USBC_NRDY0);
		*keyword = USBC_INT_NRDY;
		*status	 = USBC_BRDY0;
	}

	/***** Processing setup transaction *****/
	else if( (ists0 & USBC_CTRT) == USBC_CTRT )
	{
		/* CTSQ bit changes later than CTRT bit for ASSP. */
		/* CTSQ reloading */
		USB_RD(INTSTS0, *status);
		/* CTRT clear */
		USB_CLR_STS(INTSTS0, USBC_CTRT);
		*keyword = USBC_INT_CTRT;
	}

	/***** Processing PIPE1-MAX_PIPE_NO data *****/
	/***** EP0-7 BRDY *****/
	else if( (ists0 & USBC_BRDY) == USBC_BRDY )
	{
		USB_CLR_STS(BRDYSTS, bsts);
		*keyword = USBC_INT_BRDY;
		*status	 = bsts;
	}
	/***** EP0-7 BEMP *****/
	else if( (ists0 & USBC_BEMP) == USBC_BEMP )
	{
		USB_CLR_STS(BEMPSTS, ests);
		*keyword = USBC_INT_BEMP;
		*status	 = ests;
	}
	/***** EP0-7 NRDY *****/
	else if( (ists0 & USBC_NRDY) == USBC_NRDY )
	{
		USB_CLR_STS(NRDYSTS, nsts);
		*keyword = USBC_INT_NRDY;
		*status	 = nsts;
	}
	else
	{
		usb_pstd_IntHandFunction(ists1);
	}
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pstd_InterruptEnable
Description     : Enable USB ASSP interrupt
Arguments       : none
Return value    : none
******************************************************************************/
void usb_pstd_InterruptEnable(void)
{
	/* Enable Interrupt */
	USB_SET_PAT(INTENB0, (USBC_VBSE | USBC_DVSE | USBC_CTRE |
						  USBC_URST | USBC_SADR | USBC_SCFG |
						  USBC_SUSP | USBC_WDST | USBC_RDST | USBC_CMPL));
}
#endif /* USBC_IPSEL_PP == USBC_596IP_PP */
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Renesas Abstracted Peripheral Driver functions
******************************************************************************/
/* Condition compilation by the difference of IP */
#if USBC_IPSEL_PP == USBC_592IP_PP

/******************************************************************************
Function Name   : usb_pstd_InterruptHandler
Description     : USB interrupt Handler
Arguments       : uint16_t *keyword : Interrupt type
                : uint16_t *status : Interrupt status
Return value    : none
******************************************************************************/
void usb_pstd_InterruptHandler(uint16_t *keyword, uint16_t *status)
{
	uint16_t	intsts0, intenb0, ists0;
	uint16_t	intsts1, intenb1, ists1;
	uint16_t	brdysts, brdyenb, bsts;
	uint16_t	nrdysts, nrdyenb, nsts;
	uint16_t	bempsts, bempenb, ests;

	/* Register Save */
	USB_RD(INTSTS0, intsts0);
	USB_RD(BRDYSTS, brdysts);
	USB_RD(NRDYSTS, nrdysts);
	USB_RD(BEMPSTS, bempsts);
	USB_RD(INTENB0, intenb0);
	USB_RD(BRDYENB, brdyenb);
	USB_RD(NRDYENB, nrdyenb);
	USB_RD(BEMPENB, bempenb);

	*keyword = USBC_INT_UNKNOWN;
	*status	 = 0;
	/* Interrupt status get */
	ists0 = (uint16_t)(intsts0 & intenb0);
	bsts  = (uint16_t)(brdysts & brdyenb);
	nsts  = (uint16_t)(nrdysts & nrdyenb);
	ests  = (uint16_t)(bempsts & bempenb);

	if( (intsts0 & (USBC_VBINT|USBC_RESM|USBC_SOFR|USBC_DVST|USBC_CTRT|USBC_BEMP|USBC_NRDY|USBC_BRDY)) == 0u )
	{
		usb_pstd_IntHandFunction(ists1);
		return;
	}

	/***** Processing USB bus signal *****/
	/***** Resume signal *****/
	if( (ists0 & USBC_RESM) == USBC_RESM )
	{
		USB_CLR_STS(INTSTS0, USBC_RESM);
		*keyword = USBC_INT_RESM;
	}
	/***** Vbus change *****/
	else if( (ists0 & USBC_VBINT) == USBC_VBINT )
	{
		/* Status clear */
		usb_cstd_VbintClearSts();
		*keyword = USBC_INT_VBINT;
	}
	/***** SOFR change *****/
	else if( (ists0 & USBC_SOFR) == USBC_SOFR )
	{
		/* SOFR clear */
		USB_CLR_STS(INTSTS0, USBC_SOFR);
		*keyword = USBC_INT_SOFR;
	}

	/***** Processing device state *****/
	/***** DVST change *****/
	else if( (ists0 & USBC_DVST) == USBC_DVST )
	{
		/* DVST clear */
		USB_CLR_STS(INTSTS0, USBC_DVST);
		*keyword = USBC_INT_DVST;
		*status	 = intsts0;
	}

	/***** Processing PIPE0 data *****/
	else if( ((ists0 & USBC_BRDY) == USBC_BRDY) && ((bsts & USBC_BRDY0) == USBC_BRDY0) )
	{
		USB_CLR_STS(BRDYSTS, USBC_BRDY0);
		*keyword = USBC_INT_BRDY;
		*status	 = USBC_BRDY0;
	}
	else if( ((ists0 & USBC_BEMP) == USBC_BEMP) && ((ests & USBC_BEMP0) == USBC_BEMP0) )
	{
		USB_CLR_STS(BEMPSTS, USBC_BEMP0);
		*keyword = USBC_INT_BEMP;
		*status	 = USBC_BRDY0;
	}
	else if( ((ists0 & USBC_NRDY) == USBC_NRDY) && ((nsts & USBC_NRDY0) == USBC_NRDY0) )
	{
		USB_CLR_STS(NRDYSTS, USBC_NRDY0);
		*keyword = USBC_INT_NRDY;
		*status	 = USBC_BRDY0;
	}

	/***** Processing Setup transaction *****/
	else if( (ists0 & USBC_CTRT) == USBC_CTRT )
	{
		/* CTSQ bit changes later than CTRT bit for ASSP. */
		/* CTSQ reloading */
		USB_RD(INTSTS0, *status);
		/* CTRT clear */
		USB_CLR_STS(INTSTS0, USBC_CTRT);
		*keyword = USBC_INT_CTRT;
	}

	/***** Processing PIPE1-MAX_PIPE_NO data *****/
	else if( (ists0 & USBC_BRDY) == USBC_BRDY )
	{			/***** EP0-7 BRDY *****/
		USB_CLR_STS(BRDYSTS, bsts);
		*keyword = USBC_INT_BRDY;
		*status	 = bsts;
	}
	/***** EP0-7 BEMP *****/
	else if( (ists0 & USBC_BEMP) == USBC_BEMP )
	{
		USB_CLR_STS(BEMPSTS, ests);
		*keyword = USBC_INT_BEMP;
		*status	 = ests;
	}
	/***** EP0-7 NRDY *****/
	else if( (ists0 & USBC_NRDY) == USBC_NRDY )
	{
		USB_CLR_STS(NRDYSTS, nsts);
		*keyword = USBC_INT_NRDY;
		*status	 = nsts;
	}
	else
	{
		usb_pstd_IntHandFunction(ists1);
	}
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pstd_InterruptEnable
Description     : Enable USB ASSP interrupt
Arguments       : none
Return value    : none
******************************************************************************/
void usb_pstd_InterruptEnable(void)
{
	/* Enable Interrupt */
	USB_SET_PAT(INTENB0, (USBC_VBSE | USBC_DVSE | USBC_CTRE |
						  URST | SADR | SCFG |
						  SUSP | WDST | RDST | CMPL));
}

#endif /* USBC_IPSEL_PP == USBC_592IP_PP */
/******************************************************************************
End of function
******************************************************************************/

/******************************************************************************
End  Of File
******************************************************************************/
