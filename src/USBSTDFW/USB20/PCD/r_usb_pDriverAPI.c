/******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized.
* This software is owned by Renesas Electronics Corporation and is  protected
* under all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES
* REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY,
* INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR  A
* PARTICULAR PURPOSE AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE  EXPRESSLY
* DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE  LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES
* FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS
* AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this
* software and to discontinue the availability of this software.
* By using this software, you agree to the additional terms and
* conditions found by accessing the following link:
* http://www.renesas.com/disclaimer
*******************************************************************************
* Copyright (C) 2010(2011) Renesas Electronics Corpration
* and Renesas Solutions Corp. All rights reserved.
*******************************************************************************
* File Name    : r_usb_pDriverAPI.c
* Version      : 1.10
* Device(s)    : Renesas SH-Series, RX-Series
* Tool-Chain   : Renesas SuperH RISC engine Standard Toolchain
*              : Renesas RX Standard Toolchain
* OS           : Common to None and uITRON 4.0 Spec
* H/W Platform : Independent
* Description  : USB Peripheral Driver API code
*******************************************************************************
* History : DD.MM.YYYY Version Description
*         : 17.03.2010 0.80    First Release
*         : 30.07.2010 0.90    Updated comments
*         : 02.08.2010 0.91    Updated comments
*         : 29.10.2010 1.00    Mass Production Release
*         : 01.06.2011 1.10    Version 1.10 Release
******************************************************************************/

/******************************************************************************
Includes   <System Includes> , "Project Includes"
******************************************************************************/
#include "r_usbc_cTypedef.h"		/* Type define */
#include "r_usb_cDefUsr.h"			/* USB-H/W register set (user define) */
#include "r_usbc_cKernelId.h"		/* Kernel ID definition */
#include "r_usbc_cDefUSBIP.h"		/* USB-FW Library Header */
#include "r_usbc_cMacPrint.h"		/* Standard IO macro */
#include "r_usbc_cMacSystemcall.h"	/* System call macro */
#include "r_usb_cExtern.h"			/* USB-FW global definition */

/******************************************************************************
Section    <Section Definition> , "Project Sections"
******************************************************************************/
#pragma section _pcd


extern	uint16_t	usb_gpstd_StallPipe[];
extern	uint8_t		ConfigurationF_1[];
extern	uint8_t		ConfigurationH_1[];
extern	uint8_t		*ConPtr[];
extern	uint8_t		*ConPtr_Other[];
extern	uint16_t	EPtbl_1[];
extern	uint16_t	outpipe;
extern	uint16_t	inpipe;
extern	uint16_t	usb_gpstd_intsts0;

/******************************************************************************
Renesas Abstracted Peripheral Driver API functions
******************************************************************************/

/******************************************************************************
Function Name   : R_usb_pstd_PcdOpen
Description     : Usb Driver Open
Arguments       : none
Return value    : USBC_ER_t Error Info
******************************************************************************/
USBC_ER_t R_usb_pstd_PcdOpen(void)
{
/* Condition compilation by the difference of the operating system */
#if USBC_FW_PP == USBC_FW_NONOS_PP
	uint16_t	i;

	for( i = USBC_PIPE0; i <= USBC_MAX_PIPE_NO; i++ )
	{
		usb_gpstd_StallPipe[i]	= USBC_DONE;
		usb_gcstd_Pipe[i]		= (USBC_UTR_t*)USBC_NULL;
	}

	return USBC_E_OK;
#else	/* USBC_FW_PP == USBC_FW_NONOS_PP */
	uint16_t	i;
	USBC_ER_t	err;
	USBC_RTST_t	refinfo;
/* Condition compilation by the difference of the device's operating system */
#if USBC_OS_CRE_MODE_PP == USBC_OS_CRE_USE_PP
	USBC_TSK_t	tskinfo;
	USBC_MBX_t	mbxinfo;
	USBC_MPL_t	mplinfo;

	/* Check task status */
	err = USBC_REF_TST(USB_PCD_TSK, &refinfo);
	/* Is HCD task opened ? */
	if( err == USBC_E_OK )
	{
		USBC_PRINTF1("Already started USB_PCD_TSK (%ld)\n", err);
		/* >Yes no process */
		return	USBC_ERROR;
	}
#endif	/* USBC_OS_CRE_MODE_PP == USBC_OS_CRE_USE_PP */
/* Condition compilation by the difference of the device's operating system */
#if USBC_OS_CRE_MODE_PP == USBC_OS_CRE_NOTUSE_PP
	/* Check task status */
	err = USBC_REF_TST(USB_PCD_TSK, &refinfo);
	/* Is HCD task opened ? */
	if( err != USBC_E_OK )
	{
		USBC_PRINTF1("REF_TSK USB_PCD_TSK Error (%ld)\n", err);
		while( 1 )
		{
		}
	}
	if( refinfo.tskstat != USBC_TTS_DMT )
	{
		USBC_PRINTF1("Already started USB_PCD_TSK (%ld)\n"
			, refinfo.tskstat);
		/* >Yes no process */
		return	USBC_ERROR;
	}
#endif	/* USBC_OS_CRE_MODE_PP == USBC_OS_CRE_NOTUSE_PP */

	for( i = USBC_PIPE0; i <= USBC_MAX_PIPE_NO; i++ )
	{
		usb_gpstd_StallPipe[i]	= USBC_DONE;
		usb_gcstd_Pipe[i]		= (USBC_UTR_t*)USBC_NULL;
	}

	USBC_PRINTF0("*** Install USB-PCD ***\n");
/* Condition compilation by the difference of the device's operating system */
#if USBC_OS_CRE_MODE_PP == USBC_OS_CRE_USE_PP
	/* Create peripheral driver task */
	/* Attribute */
	tskinfo.tskatr	= (uint32_t)USBC_TA_HLNG;
	/* Start address */
	tskinfo.task	= (USBC_FP_t)&usb_pstd_PcdTask;
	/* Priority */
	tskinfo.itskpri	= (USBC_PRI_t)(USB_PCD_PRI);
	/* Stack size */
	tskinfo.stksz	= (USBC_SIZ_t)(USB_TSK_STK);
	/* Stack address */
	tskinfo.stk		= (USBC_VP_t)(USBC_NULL);
	err = USBC_CRE_TSK(USB_PCD_TSK, &tskinfo);
	if( err != USBC_E_OK )
	{
		USBC_PRINTF1("CRE_TSK USB_PCD_TSK Error (%ld)\n", err);
		while( 1 )
		{
		}
	}

	/* Create peripheral driver mailbox */
	/* Attribute */
	mbxinfo.mbxatr	= (USBC_ATR_t)((USBC_TA_TFIFO) | (USBC_TA_MFIFO));
	/* Priority */
	mbxinfo.maxmpri	= (USBC_PRI_t)(USB_MBX_PRI);
	/* Header address */
	mbxinfo.mprihd	= (USBC_VP_t)(USBC_NULL);
	err = USBC_CRE_MBX(USB_PCD_MBX, &mbxinfo);
	if( err != USBC_E_OK )
	{
		USBC_PRINTF1("CRE_MBX USB_PCD_MBX Error (%ld)\n", err);
		while( 1 )
		{
		}
	}

	/* Create peripheral driver memory pool */
	/* Attribute */
	mplinfo.mpfatr	= (USBC_ATR_t)(USBC_TA_TFIFO );
	/* Block count */
	mplinfo.blkcnt	= (USBC_UINT_t)(USB_BLK_CNT);
	/* Block size */
	mplinfo.blksz	= (USBC_UINT_t)(USB_BLK_SIZ);
	/* Start address */
	mplinfo.mpf		= (USBC_VP_t)(USBC_NULL);
	err = USBC_CRE_MPL(USB_PCD_MPL, &mplinfo);
	if( err != USBC_E_OK )
	{
		USBC_PRINTF1("CRE_MPL USB_PCD_MPL Error (%ld)\n", err);
		while( 1 )
		{
		}
	}

	/* Start peripheral driver task */
	err = USBC_STA_TSK(USB_PCD_TSK, 0);
	if( err != USBC_E_OK )
	{
		USBC_PRINTF1("STA_TSK USB_PCD_TSK Error (%ld)\n", err);
		while( 1 )
		{
		}
	}
#endif	/* USBC_OS_CRE_MODE_PP == USBC_OS_CRE_USE_PP */

/* Condition compilation by the difference of the device's operating system */
#if USBC_OS_CRE_MODE_PP == USBC_OS_CRE_NOTUSE_PP
	/* Start peripheral driver task */
	err = USBC_ACT_TSK(USB_PCD_TSK);
	if( err != USBC_E_OK )
	{
		USBC_PRINTF1("ACT_TSK USB_PCD_TSK Error (%ld)\n", err);
		while( 1 )
		{
		}
	}
#endif	/* USBC_OS_CRE_MODE_PP == USBC_OS_CRE_NOTUSE_PP */

	return err;
#endif /* USBC_FW_PP == USBC_FW_NONOS_PP */
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : R_usb_pstd_PcdClose
Description     : Usb Driver Close
Arguments       : none
Return value    : USBC_ER_t Error Info
******************************************************************************/
USBC_ER_t R_usb_pstd_PcdClose(void)
{
/* Condition compilation by the difference of the operating system */
#if USBC_FW_PP == USBC_FW_NONOS_PP
	return USBC_E_OK;
#else	/* USBC_FW_PP == USBC_FW_NONOS_PP */
	USBC_ER_t		err;

	USBC_PRINTF0("*** Release USB-PCD ***\n");

	/* Stop peripheral driver task */
	err = USBC_TER_TSK(USB_PCD_TSK);
	if( err != USBC_E_OK )
	{
		USBC_PRINTF1("TER_TSK USB_PCD Error (%ld)\n", err);
		while( 1 )
		{
		}
	}
/* Condition compilation by the difference of the device's operating system */
#if USBC_OS_CRE_MODE_PP == USBC_OS_CRE_USE_PP
	/* Delete peripheral driver memory pool */
	err = USBC_DEL_MPL(USB_PCD_MPL);
	if( err != USBC_E_OK )
	{
		USBC_PRINTF1("DEL_MPL USB_PCD Error (%ld)\n", err);
		while( 1 )
		{
		}
	}

	/* Delete peripheral driver mailbox */
	err = USBC_DEL_MBX(USB_PCD_MBX);
	if( err != USBC_E_OK )
	{
		USBC_PRINTF1("DEL_MBX USB_PCD Error (%ld)\n", err);
		while( 1 )
		{
		}
	}

	/* Delete peripheral driver task */
	err = USBC_DEL_TSK(USB_PCD_TSK);
	if( err != USBC_E_OK )
	{
		USBC_PRINTF1("DEL_TSK USB_PCD Error (%ld)\n", err);
		while( 1 )
		{
		}
	}
#endif	/* USBC_OS_CRE_MODE_PP == USBC_OS_CRE_USE_PP */

	return err;
#endif /* USBC_FW_PP == USBC_FW_NONOS_PP */
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : R_usb_pstd_TransferStart
Description     : Data transfer request
Arguments       : USBC_UTR_t *utr_table : message
Return value    : USBC_ER_t Error Info
******************************************************************************/
USBC_ER_t R_usb_pstd_TransferStart(USBC_UTR_t *utr_table)
{
	USBC_ER_t		err;
	uint16_t		pipenum;

	pipenum = utr_table->keyword;
	if( usb_gcstd_Pipe[pipenum] != USBC_NULL )
	{
		/* Get PIPE TYPE */
		if( usb_cstd_GetPipeType(pipenum) != USBC_ISO )
		{
			USBC_PRINTF1("### R_usb_pstd_TransferStart overlaps %d\n"
				, pipenum);
			return USBC_E_QOVR;
		}
	}

	/* Check state ( Configured ) */
	if( usb_pstd_ChkConfigured() != USBC_YES )
	{
		USBC_PRINTF0("### R_usb_pstd_TransferStart not configured\n");
		return USBC_E_ERROR;
	}

	if( pipenum == USBC_PIPE0 )
	{
		USBC_PRINTF0("### R_usb_pstd_TransferStart PIPE0 is not support\n");
		return USBC_E_ERROR;
	}

	utr_table->msghead	= (USBC_MH_t)USBC_NULL;
	utr_table->msginfo	= USBC_MSG_PCD_SUBMITUTR;
	/* Send message */
	err = USBC_SND_MSG(USB_PCD_MBX, (USBC_MSG_t*)utr_table);
	if( err != USBC_E_OK )
	{
		USBC_PRINTF1("### pTransferStart snd_msg error (%ld)\n", err);
	}
	return err;
}
/******************************************************************************
End of function
******************************************************************************/

/******************************************************************************
Function Name   : R_usb_pstd_SetStall
Description     : Set pipe stall request
Arguments       : USBC_CB_INFO_t complete ; callback function
                : uint16_t pipe ; pipe number
Return value    : USBC_ER_t Error Info
******************************************************************************/
USBC_ER_t R_usb_pstd_SetStall(USBC_CB_INFO_t complete, uint16_t pipe)
{
	/* PCD Send Mailbox */
	return usb_pstd_PcdSndMbx((uint16_t)USBC_MSG_PCD_SETSTALL, pipe
		, complete);
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : R_usb_pstd_TransferEnd
Description     : Data transfer end request
Arguments       : uint16_t pipe ; pipe number
Return value    : USBC_ER_t Error Info
******************************************************************************/
USBC_ER_t R_usb_pstd_TransferEnd(uint16_t pipe, uint16_t status)
{
	uint16_t		info;

	if( usb_gcstd_Pipe[pipe] == USBC_NULL )
	{
		USBC_PRINTF0("### R_usb_pstd_TransferEnd overlaps\n");
		return USBC_E_QOVR;
	}

	/* check Time out */
	if( status == USBC_DATA_TMO )
	{
		info = USBC_MSG_PCD_TRANSEND1;
	}
	else
	{
		info = USBC_MSG_PCD_TRANSEND2;
	}

	/* PCD Send Mailbox */
	return usb_pstd_PcdSndMbx(info, pipe, &usb_cstd_DummyFunction);
}
/******************************************************************************
End of function
******************************************************************************/

/*""FUNC COMMENT""*************************************************************
Function Name   : R_usb_pstd_PcdChangeDeviceState
Description     : Change Device State
Arguments       : uint16_t msginfo          : new device status
                : uint16_t member           : pipe number etc
                : USBC_CB_INFO_t complete   : Callback function
Return value    : USBC_ER_t                 : Error code
*""FUNC COMMENT END""*********************************************************/
USBC_ER_t R_usb_pstd_PcdChangeDeviceState(uint16_t msginfo, uint16_t member
	, USBC_CB_INFO_t complete)
{
	USBC_ER_t		err;

	/* PCD Send Mailbox */
	err = usb_pstd_PcdSndMbx(msginfo, member, complete);
	return err;
}
/******************************************************************************
End of function
******************************************************************************/


/*""FUNC COMMENT""*************************************************************
Function Name   : R_usb_pstd_DeviceInformation
Description     : Get Device Information
Arguments       : uint16_t *tbl    : Device Information Store Pointer TBL
Return value    : none
*""FUNC COMMENT END""*********************************************************/
void R_usb_pstd_DeviceInformation(uint16_t *tbl)
{
	/* Device status */
	tbl[0] = usb_gpstd_intsts0 & (uint16_t)(USBC_VBSTS|USBC_DVSQ);

	/* Speed */
	tbl[1] = usb_cstd_PortSpeed((uint16_t)USBC_PORT0);

	/* Configuration number */
	tbl[2] = usb_gpstd_ConfigNum;

	/* Interface number */
	tbl[3] = usb_pstd_GetInterfaceNum(usb_gpstd_ConfigNum);

	/* Remote Wakeup Flag */
	tbl[4] = usb_gpstd_RemoteWakeup;
}
/******************************************************************************
End of function
******************************************************************************/

/******************************************************************************
Function Name   : R_usb_pstd_DriverRegistration
Description     : Callback registration
Arguments       : USBC_PCDREG_t *registinfo : class driver structure
Return value    : none
******************************************************************************/
void R_usb_pstd_DriverRegistration(USBC_PCDREG_t *registinfo)
{
	USBC_PCDREG_t	*driver;

	driver = &usb_gpstd_Driver;
	/* Pipe define table address */
	driver->pipetbl		= registinfo->pipetbl;
	/* Device descriptor table address */
	driver->devicetbl	= registinfo->devicetbl;
	/* Qualifier descriptor table address */
	driver->qualitbl	= registinfo->qualitbl;
	/* Configuration descriptor table address */
	driver->configtbl	= registinfo->configtbl;
	/* Other configuration descriptor table address */
	driver->othertbl	= registinfo->othertbl;
	/* String descriptor table address */
	driver->stringtbl	= registinfo->stringtbl;
	/* Driver init */
	driver->classinit	= registinfo->classinit;
	/* Device default */
	driver->devdefault	= registinfo->devdefault;
	/* Device configured */
	driver->devconfig	= registinfo->devconfig;
	/* Device detach */
	driver->devdetach	= registinfo->devdetach;
	/* Device suspend */
	driver->devsuspend	= registinfo->devsuspend;
	/* Device resume */
	driver->devresume	= registinfo->devresume;
	/* Interfaced change */
	driver->interface	= registinfo->interface;
	/* Control transfer */
	driver->ctrltrans	= registinfo->ctrltrans;
	/* Initialized device driver */
	(*driver->classinit)((uint16_t)USBC_NO_ARG, (uint16_t)USBC_NO_ARG);
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : R_usb_pstd_DriverRelease
Description     : Class driver release
Arguments       : none
Return value    : none
******************************************************************************/
void R_usb_pstd_DriverRelease(void)
{
	USBC_PCDREG_t	*driver;

	driver = &usb_gpstd_Driver;
	/* Pipe define table address */
	driver->pipetbl		= (uint16_t**)0u;
	/* Device descriptor table address */
	driver->devicetbl	= (uint8_t*)0u;
	/* Qualifier descriptor table address */
	driver->qualitbl	= (uint8_t*)0u;
	/* Configuration descriptor table address */
	driver->configtbl	= (uint8_t**)0u;
	/* Other configuration descriptor table address */
	driver->othertbl	= (uint8_t**)0u;
	/* String descriptor table address */
	driver->stringtbl	= (uint8_t**)0u;
	/* Driver init */
	driver->classinit	= &usb_cstd_DummyFunction;
	/* Device default */
	driver->devdefault	= &usb_cstd_DummyFunction;
	/* Device configured */
	driver->devconfig	= &usb_cstd_DummyFunction;
	/* Device detach */
	driver->devdetach	= &usb_cstd_DummyFunction;
	/* Device suspend */
	driver->devsuspend	= &usb_cstd_DummyFunction;
	/* Device resume */
	driver->devresume	= &usb_cstd_DummyFunction;
	/* Interfaced change */
	driver->interface	= &usb_cstd_DummyFunction;
	/* Control transfer */
	driver->ctrltrans	= &usb_cstd_DummyTrn;
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
End  Of File
******************************************************************************/
