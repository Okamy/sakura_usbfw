/******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized.
* This software is owned by Renesas Electronics Corporation and is  protected
* under all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES
* REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY,
* INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR  A
* PARTICULAR PURPOSE AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE  EXPRESSLY
* DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE  LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES
* FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS
* AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this
* software and to discontinue the availability of this software.
* By using this software, you agree to the additional terms and
* conditions found by accessing the following link:
* http://www.renesas.com/disclaimer
*******************************************************************************
* Copyright (C) 2010(2011) Renesas Electronics Corpration
* and Renesas Solutions Corp. All rights reserved.
*******************************************************************************
* File Name    : r_usb_pLibUSBIP.c
* Version      : 1.10
* Device(s)    : Renesas SH-Series, RX-Series
* Tool-Chain   : Renesas SuperH RISC engine Standard Toolchain
*              : Renesas RX Standard Toolchain
* OS           : Common to None and uITRON 4.0 Spec
* H/W Platform : Independent
* Description  : USB Peripheral library USB-IP code
*******************************************************************************
* History : DD.MM.YYYY Version Description
*         : 17.03.2010 0.80    First Release
*         : 30.07.2010 0.90    Updated comments
*         : 02.08.2010 0.91    Updated comments
*         : 29.10.2010 1.00    Mass Production Release
*         : 01.06.2011 1.10    Version 1.10 Release
******************************************************************************/

/******************************************************************************
Includes   <System Includes> , "Project Includes"
******************************************************************************/
#include "r_usbc_cTypedef.h"		/* Type define */
#include "r_usb_cDefUsr.h"			/* USB-H/W register set (user define) */
#include "r_usbc_cDefUSBIP.h"		/* USB-FW Library Header */
#include "r_usbc_cMacPrint.h"		/* Standard IO macro */
#include "r_usb_cExtern.h"			/* USB-FW global definition */


/******************************************************************************
Section    <Section Definition> , "Project Sections"
******************************************************************************/
#pragma section _pcd

/******************************************************************************
Renesas Abstracted Peripheral library USB-IP functions
******************************************************************************/

/******************************************************************************
Function Name   : R_usb_pstd_SetPipeRegister
Description     : Set pipe configuration register
Arguments       : uint16_t pipe_number ; pipe number
                : uint16_t *tbl ; DEF_EP table pointer
Return value    : none
******************************************************************************/
void R_usb_pstd_SetPipeRegister(uint16_t pipe_number, uint16_t *tbl)
{
	uint16_t		i, pipe, ep, buf;

	switch( pipe_number )
	{
	/* All pipe initialized */
	case USBC_USEPIPE:
		/* Current FIFO port Clear */
		usb_cstd_FPortChange2((uint16_t)USBC_PIPE0
			, (uint16_t)USBC_CUSE,	USBC_NO);
		usb_cstd_FPortChange2((uint16_t)USBC_PIPE0
			, (uint16_t)USBC_D0USE, USBC_NO);
		usb_cstd_FPortChange2((uint16_t)USBC_PIPE0
			, (uint16_t)USBC_D1USE, USBC_NO);
		for( i = 0; tbl[i] != USBC_PDTBLEND; i += USBC_EPL )
		{
			/* Pipe number */
			pipe = (uint16_t)(tbl[i + 0] & USBC_CURPIPE);
			usb_cstd_PipeClr(pipe, tbl, i);
		}
		break;
	/* Peripheral pipe initialized */
	case USBC_PERIPIPE:
		/* Current FIFO port Clear */
		usb_cstd_FPortChange2((uint16_t)USBC_PIPE0
			, (uint16_t)USBC_CUSE,	USBC_NO);
		usb_cstd_FPortChange2((uint16_t)USBC_PIPE0
			, (uint16_t)USBC_D0USE, USBC_NO);
		usb_cstd_FPortChange2((uint16_t)USBC_PIPE0
			, (uint16_t)USBC_D1USE, USBC_NO);
		for( ep = USBC_EP1; ep <= USBC_MAX_EP_NO; ++ep )
		{
			if( usb_gpstd_EpTblIndex[ep] != USBC_ERROR )
			{
				i = (uint16_t)(USBC_EPL * usb_gpstd_EpTblIndex[ep]);
				/* Pipe number */
				pipe = (uint16_t)(tbl[i + 0] & USBC_CURPIPE);
				usb_cstd_PipeClr(pipe, tbl, i);
			}
		}
		break;
	/* Pipe initialized */
	default:
		/* Current FIFO port clear */
		usb_cstd_FPortChange2((uint16_t)USBC_PIPE0
			, (uint16_t)USBC_CUSE, USBC_NO);
		/* D0FIFO */
		USB_RD(D0FIFOSEL, buf);
		if( (buf & USBC_CURPIPE) == pipe_number )
		{
			usb_cstd_FPortChange2((uint16_t)USBC_PIPE0, (uint16_t)USBC_D0USE
				, USBC_NO);
		}
		/* D1FIFO */
		USB_RD(D1FIFOSEL, buf);
		if( (buf & USBC_CURPIPE) == pipe_number )
		{
			usb_cstd_FPortChange2((uint16_t)USBC_PIPE0, (uint16_t)USBC_D1USE
				, USBC_NO);
		}
		for( i = 0; tbl[i] != USBC_PDTBLEND; i += USBC_EPL )
		{
			/* Pipe number */
			pipe = (uint16_t)(tbl[i + 0] & USBC_CURPIPE);
			if( pipe == pipe_number )
			{
				usb_cstd_PipeClr(pipe, tbl, i);
			}
		}
		break;
	}
}
/******************************************************************************
End of function
******************************************************************************/

/******************************************************************************
End  Of File
******************************************************************************/
