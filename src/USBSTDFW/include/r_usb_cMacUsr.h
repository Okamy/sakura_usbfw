/******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized.
* This software is owned by Renesas Electronics Corporation and is  protected
* under all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES
* REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY,
* INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR  A
* PARTICULAR PURPOSE AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE  EXPRESSLY
* DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE  LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES
* FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS
* AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this
* software and to discontinue the availability of this software.
* By using this software, you agree to the additional terms and
* conditions found by accessing the following link:
* http://www.renesas.com/disclaimer
*******************************************************************************
* Copyright (C) 2010(2011) Renesas Electronics Corpration
* and Renesas Solutions Corp. All rights reserved.
*******************************************************************************
* File Name    : r_usb_cMacUsr.h
* Version      : 1.10
* Device(s)    : Renesas SH-Series, RX-Series
* Tool-Chain   : Renesas SuperH RISC engine Standard Toolchain
*              : Renesas RX Standard Toolchain
* OS           : Common to None and uITRON 4.0 Spec
* H/W Platform : Independent
* Description  : Macro Definition Header File
*******************************************************************************
* History : DD.MM.YYYY Version Description
*         : 17.03.2010 0.80    First Release
*         : 30.07.2010 0.90    Updated comments
*         : 02.08.2010 0.91    Updated comments
*         : 29.10.2010 1.00    Mass Production Release
*         : 01.06.2011 1.10    Version 1.10 Release
*******************************************************************************/
#ifndef __R_USB_CMACUSR_H__
#define __R_USB_CMACUSR_H__


/*****************************************************************************
Macro definitions
******************************************************************************/
/* Condition compilation by the difference of the devices */
#if USBC_TARGET_CHIP_PP == USBC_RX600_PP
/*------------------------------------------------------------
 * USB register access macro (Basic)
 *------------------------------------------------------------*/
/* Read/Write USB register */
#define  USB_RD( r, v )		do { (( v ) = ( USB_IP.r.WORD )); } while(0)
#define  USB_WR( r, v )		do { (( USB_IP.r.WORD ) = ( v )); } while(0)

#define  USB_RDW( r, v )		do { (( v ) = ( USB_IP.r )); } while(0)
#define  USB_WRW( r, v )		do { (( USB_IP.r ) = ( v )); } while(0)

/*------------------------------------------------------------
 * USB FIFO register access macro
 *------------------------------------------------------------*/
/* Read/Write USB FIFO register */
#define  USB_RD_FF( r, v )		do { (( v ) = ( USB_IP.r )); } while(0)
#define  USB_WR_FF( r, v )		do { (( USB_IP.r ) = ( v )); } while(0)

/*------------------------------------------------------------
 * USB register bit access macro
 *------------------------------------------------------------*/
/* Set bit(s) of USB register	 */
/* r : USB register				 */
/* v : Value to set				 */
#define  USB_SET_PAT( r, v ) \
	do { (( USB_IP.r.WORD ) |= ( v )); } while(0)


/* Reset bit(s) of USB register  */
/* r : USB register				 */
/* m : Bit pattern to reset		 */
#define  USB_CLR_PAT( r, m ) \
	do { (( USB_IP.r.WORD ) &= ( (uint16_t)(~(m)) )); } while(0)


/* modify bit(s) of USB register */
/* r : USB register				 */
/* v : Value to set				 */
/* m : Bit pattern to modify	 */
#define  USB_MDF_PAT( r, v, m )	do {								\
									uint16_t mtmp;					\
									USB_RD( r, mtmp );				\
									mtmp &= ( (uint16_t)(~(m)) );	\
									mtmp |= ( (uint16_t)(v & m) );	\
									USB_WR( r, mtmp );				\
								} while(0)

/* Reset bit(s) of USB status	 */
/* r : USB register				 */
/* m : Bit pattern to reset		 */
#define  USB_CLR_STS( r, m )	USB_WR( r, ( (uint16_t)(~(m)) ) )

/* Set bit(s) of USB status		 */
/* r : USB register				 */
/* m : Dummy					 */
/* Condition compilation by the difference of IP */
#if USBC_IPSEL_PP != USBC_597IP_PP
#define  USB_SET_STS( r, m )	USB_WR( r, 0xFFFFu )
#endif	/* != USBC_597IP_PP */

#endif /* USBC_RX600_PP */


/* Condition compilation by the difference of the devices */
#if USBC_TARGET_CHIP_PP == USBC_ASSP_PP

/*------------------------------------------------------------
 * USB register access macro (Basic)
 *------------------------------------------------------------*/
/* Read/Write USB register */
#define	 USB_RD( r, v )		\
	do { (( v ) = ( usb_gcstd_UsbReg->r )); } while(0)
#define	 USB_WR( r, v )		\
	do { (( usb_gcstd_UsbReg->r ) = ( v )); } while(0)

/*------------------------------------------------------------
 * USB FIFO register access macro
 *------------------------------------------------------------*/
/* Read/Write USB FIFO register */
#define	 USB_RD_FF( r, v )	\
	do { (( v ) = ( usb_gcstd_UsbReg->r )); } while(0)
#define	 USB_WR_FF( r, v )	\
	do { (( usb_gcstd_UsbReg->r ) = ( v )); } while(0)

/*------------------------------------------------------------
 * USB register bit access macro
 *------------------------------------------------------------*/
/* Set bit(s) of USB register	 */
/* r : USB register				 */
/* v : Value to set				 */
#define	 USB_SET_PAT( r, v )	\
	do { (( usb_gcstd_UsbReg->r ) |= ( v )); } while(0)


/* Reset bit(s) of USB register	 */
/* r : USB register				 */
/* m : Bit pattern to reset		 */
#define	 USB_CLR_PAT( r, m )	\
	do { (( usb_gcstd_UsbReg->r ) &= ( (uint16_t)(~(m)) )); } while(0)


/* Modify bit(s) of USB register */
/* r : USB register				 */
/* v : Value to set				 */
/* m : Bit pattern to modify	 */
#define	 USB_MDF_PAT( r, v, m ) do {								\
									uint16_t mtmp;					\
									USB_RD( r, mtmp );				\
									mtmp &= ( (uint16_t)(~(m)) );	\
									mtmp |= ( (uint16_t)(v & m) );	\
									USB_WR( r, mtmp );				\
								} while(0)


/* reset bit(s) of USB status	 */
/* r : USB register				 */
/* m : Bit pattern to reset		 */
#define	 USB_CLR_STS( r, m )	USB_WR( r, ( (uint16_t)(~(m)) ) )


/* Set bit(s) of USB status		 */
/* r : USB register				 */
/* m : Dummy					 */
/* Condition compilation by the difference of IP */
#if USBC_IPSEL_PP != USBC_597IP_PP
#define	 USB_SET_STS( r, m )	USB_WR( r, 0xFFFFu )
#endif	/* USBC_IPSEL_PP != USBC_597IP_PP */

#endif /* USBC_TARGET_CHIP_PP == USBC_ASSP_PP */


#endif	/* __R_USB_CMACUSR_H__ */
/******************************************************************************
End  Of File
******************************************************************************/
