/******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized.
* This software is owned by Renesas Electronics Corporation and is  protected
* under all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES
* REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY,
* INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR  A
* PARTICULAR PURPOSE AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE  EXPRESSLY
* DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE  LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES
* FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS
* AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this
* software and to discontinue the availability of this software.
* By using this software, you agree to the additional terms and
* conditions found by accessing the following link:
* http://www.renesas.com/disclaimer
******************************************************************************
* Copyright (C) 2010(2011) Renesas Electronics Corpration
* and Renesas Solutions Corp. All rights reserved.
******************************************************************************
* File Name    : r_usb_cExtern.h
* Version      : 1.10
* Device(s)    : Renesas SH-Series, RX-Series
* Tool-Chain   : Renesas SuperH RISC engine Standard Toolchain
*              : Renesas RX Standard Toolchain
* OS           : Common to None and uITRON 4.0 Spec
* H/W Platform : Independent
* Description  : USB common extern header
******************************************************************************
* History : DD.MM.YYYY Version Description
*         : 17.03.2010 0.80    First Release
*         : 30.07.2010 0.90    Updated comments
*         : 02.08.2010 0.91    Updated comments
*         : 29.10.2010 1.00    Mass Production Release
*         : 01.06.2011 1.10    Version 1.10 Release
******************************************************************************/
#ifndef __R_USB_CEXTERN_H__
#define __R_USB_CEXTERN_H__


/*****************************************************************************
Public Variables
******************************************************************************/

extern volatile struct USBC_REGISTER	*usb_gcstd_UsbReg;	/*	usb register
															 define */

extern uint32_t			usb_gcstd_DataCnt[];			/* PIPEn Buffer
															 counter */
extern uint16_t			usb_gcstd_Dma0Dir;				/* DMA0 direction */
extern uint16_t			usb_gcstd_Dma0Size;				/* DMA0 buffer size */
extern uint16_t			usb_gcstd_Dma0Fifo;				/* DMA0 FIFO buffer
															 size */
extern uint16_t			usb_gcstd_Dma0Pipe;				/* DMA0 pipe number */
extern uint8_t			*usb_gcstd_DataPtr[];			/* PIPEn Buffer
															 pointer(8bit) */
extern USBC_UTR_t		*usb_gcstd_Pipe[];				/* Message pipe */
extern uint16_t			usb_gcstd_PcutMode;				/* PCUT Mode Flag */

extern uint16_t			usb_gcstd_XckeMode;				/* XCKE Mode Flag */
extern uint16_t			usb_gcstd_HsEnable;				/* Hi-speed enable */

extern uint16_t			usb_gpstd_ConfigNum;			/* Configuration
															 Number */
extern uint16_t			usb_gpstd_AltNum[];				/* Alternate */
extern uint16_t			usb_gpstd_RemoteWakeup;			/* Remote Wakeup Enable
															 Flag */
extern uint16_t			usb_gpstd_TestModeSelect;		/* Test Mode
															 Selectors */
extern uint16_t			usb_gpstd_TestModeFlag;			/* Test Mode Flag */
extern uint16_t			usb_gpstd_EpTblIndex[];			/* Index of Endpoint
															 Information
															  table */

extern USBC_REQUEST_t	usb_gpstd_ReqReg;				/* Request variable */
extern uint16_t			usb_gpstd_ReqType;				/* Request type */
extern uint16_t			usb_gpstd_ReqTypeType;			/* Request type TYPE */
extern uint16_t			usb_gpstd_ReqTypeRecip;			/* Request type
															 RECIPIENT */
extern uint16_t			usb_gpstd_ReqRequest;			/* Request */
extern uint16_t			usb_gpstd_ReqValue;				/* Value */
extern uint16_t			usb_gpstd_ReqIndex;				/* Index */
extern uint16_t			usb_gpstd_ReqLength;			/* Length */

extern USBC_PCDREG_t		usb_gpstd_Driver;

extern uint16_t			usb_ghstd_Ctsq;					/* Control transfer
														 stage management */
extern uint16_t			usb_ghstd_MgrMode[];			/* Manager mode */
extern uint16_t			usb_ghstd_DcpRegister[];		/* DEVSEL & DCPMAXP
															 (Multiple
															  device) */
extern uint16_t			usb_ghstd_DeviceAddr;			/* Device address */
extern uint16_t			usb_ghstd_DeviceSpeed;			/* Reset handshake
															 result */
extern uint16_t			usb_ghstd_DeviceNum;			/* Device driver
															 number */
extern uint16_t			usb_ghstd_DeviceInfo[][8];		/* Information : port
														, status, config num
														, interface class
														, speed, */
extern USBC_HCDREG_t		usb_ghstd_DeviceDrv[];
extern uint16_t			usb_ghstd_IgnoreCnt[];			/* Ignore count */

extern uint16_t			usb_gcstd_RhstBit;
extern uint16_t			usb_gcstd_DvsqBit;
extern uint16_t			usb_gcstd_AddrBit;
extern uint16_t			usb_gcstd_SqmonBit;

/* Condition compilation by the difference of the operating system */
#if USBC_FW_PP == USBC_FW_NONOS_PP
extern uint8_t			usb_ghstd_EnuWait;				/* Class check TaskID */
extern uint16_t			usb_ghstd_CheckEnuResult;		/* Enumeration result
															 check */
#endif // USBC_FW_PP == USBC_FW_NONOS_PP


/*****************************************************************************
Public Functions
******************************************************************************/

void			usb_pstd_StandReq0(void);
void			usb_pstd_StandReq1(void);
void			usb_pstd_StandReq2(void);
void			usb_pstd_StandReq3(void);
void			usb_pstd_StandReq4(void);
void			usb_pstd_StandReq5(void);
void			usb_pstd_GetStatus1(void);
void			usb_pstd_GetDescriptor1(void);
void			usb_pstd_GetConfiguration1(void);
void			usb_pstd_GetInterface1(void);
void			usb_pstd_ClearFeature0(void);
void			usb_pstd_ClearFeature3(void);
void			usb_pstd_SetFeature0(void);
void			usb_pstd_SetFeature3(void);
void			usb_pstd_SetAddress0(void);
void			usb_pstd_SetAddress3(void);
void			usb_pstd_SetDescriptor2(void);
void			usb_pstd_SetConfiguration0(void);
void			usb_pstd_SetConfiguration3(void);
void			usb_pstd_SetInterface0(void);
void			usb_pstd_SetInterface3(void);
void			usb_pstd_SynchFrame1(void);
void			usb_pstd_SetFeatureFunction(void);
void			usb_pstd_DtchFunction(void);
void			usb_pstd_IntHandFunction(uint16_t ists1);
void			usb_pstd_AttachFunction(void);
void			usb_pstd_DetachFunction(void);
void			usb_pstd_BusresetFunction(void);
void			usb_pstd_SuspendFunction(void);
uint16_t		usb_pstd_InitFunction(void);
void			usb_pstd_AttachProcess(void);
void			usb_pstd_DetachProcess(void);
void			usb_pstd_BusReset(void);
void			usb_pstd_RemoteWakeup(void);
void			usb_pstd_SuspendProcess(void);
void			usb_pstd_InitConnect(void);
uint16_t		usb_pstd_ChkVbsts(void);
void			usb_pstd_TestMode(void);
void			usb_pstd_ResumeProcess(void);
void			usb_pstd_ResumeProcess(void);
void			usb_pstd_SuspendClock(void);
void			usb_pstd_DetachClock(void);
void			usb_pstd_BrdyPipe(uint16_t bitsts);
void			usb_pstd_NrdyPipe(uint16_t bitsts);
void			usb_pstd_BempPipe(uint16_t bitsts);
USBC_ER_t		usb_pstd_Remote(USBC_CB_INFO_t complete);
USBC_ER_t		usb_pstd_ClearSeqbit(USBC_CB_INFO_t complete
					, uint16_t pipe);
USBC_ER_t		usb_pstd_Detach(USBC_CB_INFO_t complete);
USBC_ER_t		usb_pstd_Attach(USBC_CB_INFO_t complete);
USBC_ER_t		usb_pstd_PcdSndMbx(uint16_t msginfo, uint16_t keyword
					, USBC_CB_INFO_t complete);
USBC_ER_t		usb_pstd_SetSubmitutr(USBC_UTR_t *utrmsg);
void			usb_pstd_SetReTransfer(uint16_t pipe);
void			usb_pstd_Interrupt(USBC_UTR_t *p);
void			usb_pstd_SaveRequest(void);
void			usb_pstd_InterruptHandler(uint16_t *keyword
					, uint16_t *status);
void			usb_pstd_ClearAlt(void);
void			usb_pstd_ClearMem(void);
void			usb_pstd_SetConfigNum(uint16_t Value);
void			usb_pstd_ClearEpTblIndex(void);
uint16_t		usb_pstd_GetConfigNum(void);
uint16_t		usb_pstd_GetInterfaceNum(uint16_t Con_Num);
uint16_t		usb_pstd_GetAlternateNum(uint16_t Con_Num
					, uint16_t Int_Num);
void			usb_pstd_SetEpTblIndex(uint16_t Con_Num, uint16_t Int_Num
					, uint16_t Alt_Num);
uint16_t		usb_pstd_ChkRemote(void);
uint8_t			usb_pstd_GetCurrentPower(void);
void			usb_pstd_InterruptEnable(void);
uint16_t		usb_pstd_ChkConfigured(void);
void			usb_cstd_SetHwFunction(uint16_t function);
void			usb_cstd_SetHwFunctionSub(uint16_t function);
void			usb_cstd_AsspConfig(void);
void			usb_cstd_Pinconfig(void);
void			usb_cstd_InitialClock(void);
void			usb_cstd_InterruptClock(void);
void			usb_cstd_SelfClock(void);
void			usb_cstd_StopClock(void);
void			usb_cstd_RegBackup(void);
void			usb_hstd_RegRecover(void);
void			usb_pstd_RegRecover(void);
void			usb_cstd_WaitUsbip(void);
void			usb_cstd_BrdyEnable(uint16_t Pipe);
void			usb_cstd_BrdyDisable(uint16_t Pipe);
void			usb_cstd_BempEnable(uint16_t Pipe);
void			usb_cstd_BempDisable(uint16_t Pipe);
void			usb_cstd_NrdyEnable(uint16_t Pipe);
void			usb_cstd_NrdyDisable(uint16_t Pipe);
uint16_t		usb_cstd_PortSpeed(uint16_t port);
uint16_t		usb_cstd_HiSpeedEnable(uint16_t port);
void			usb_cstd_BerneEnable(void);
uint16_t		usb_cstd_Epadr2Pipe(uint16_t Dir_Ep);
uint8_t			usb_cstd_Pipe2Epadr(uint16_t Pipe);
uint16_t		usb_cstd_Pipe2Fport(uint16_t Pipe);
void			usb_cstd_SwReset(void);
uint16_t		usb_cstd_GetDeviceAddress(uint16_t Pipe);
uint16_t		usb_cstd_ReadBsts(uint16_t Pipe);
uint16_t		usb_cstd_ReadInbufm(uint16_t Pipe);
void			usb_cstd_DoCsclr(uint16_t Pipe);
void			usb_cstd_DoAclrm(uint16_t Pipe);
void			usb_cstd_SetAclrm(uint16_t Pipe);
void			usb_cstd_ClrAclrm(uint16_t Pipe);
void			usb_cstd_DoSqclr(uint16_t Pipe);
void			usb_cstd_DoSqset(uint16_t Pipe);
void			usb_cstd_DoSqtgl(uint16_t Pipe, uint16_t toggle);
void			usb_cstd_SetBuf(uint16_t Pipe);
void			usb_cstd_SetNak(uint16_t Pipe);
void			usb_cstd_SetStall(uint16_t Pipe);
void			usb_cstd_ClrStall(uint16_t Pipe);
uint16_t		usb_cstd_GetPid(uint16_t Pipe);
uint16_t		usb_cstd_ReadPipectr(uint16_t Pipe);
uint16_t		usb_cstd_GetBufSize(uint16_t Pipe);
uint16_t		usb_cstd_GetMaxPacketSize(uint16_t Pipe);
uint16_t		usb_cstd_GetDeviceAddress(uint16_t Pipe);
void			usb_cstd_SetHse(uint16_t port, uint16_t speed);
uint16_t		usb_cstd_GetPipeDir(uint16_t Pipe);
uint16_t		usb_cstd_GetPipeType(uint16_t Pipe);
void			usb_cstd_PipeClr(uint16_t pipe, uint16_t *tbl
					, uint16_t ofs);
void			usb_cstd_ClrPipeCnfg(uint16_t PipeNo);
void			usb_cstd_VbintClearSts(void);
void			usb_cstd_ResmClearSts(void);
void			usb_cstd_InitUsbMessage(uint16_t function);
void			usb_cstd_UsbHandler(void);
void			usb_cstd_DmaHandler(void);
void			usb_cstd_BrdyPipe(uint16_t bitsts);
void			usb_cstd_NrdyPipe(uint16_t bitsts);
void			usb_cstd_BempPipe(uint16_t bitsts);
void			usb_cstd_SendStart(uint16_t Pipe);
void			usb_cstd_Buf2Fifo(uint16_t Pipe, uint16_t useport);
uint16_t		usb_cstd_Buf2Cfifo(uint16_t Pipe);
uint16_t		usb_cstd_Buf2D0fifo(uint16_t Pipe);
uint16_t		usb_cstd_Buf2D1fifo(uint16_t Pipe);
void			usb_cstd_ReceiveStart(uint16_t Pipe);
void			usb_cstd_Fifo2Buf(uint16_t Pipe, uint16_t useport);
uint16_t		usb_cstd_Cfifo2Buf(uint16_t Pipe);
uint16_t		usb_cstd_D1fifo2Buf(uint16_t Pipe);
void			usb_cstd_DataEnd(uint16_t Pipe, uint16_t Status);
void			usb_cstd_ForcedTermination(uint16_t Pipe
					, uint16_t Status);
uint16_t		usb_cstd_FPortChange1(uint16_t Pipe, uint16_t fifosel
					, uint16_t isel);
void			usb_cstd_FPortChange2(uint16_t Pipe, uint16_t fifosel
					, uint16_t isel);
void			usb_cstd_FifoClr(uint16_t pipe);
void			usb_cstd_SetTransactionCounter(uint16_t trnreg
					, uint16_t trncnt);
void			usb_cstd_ClrTransactionCounter(uint16_t trnreg);
void			usb_cstd_SetTransactionCounter(uint16_t trnreg
					, uint16_t trncnt);
void			usb_cstd_ClrTransactionCounter(uint16_t trnreg);
void			usb_cstd_Buf2D0fifoStartUsb(void);
void			usb_cstd_D0fifo2BufStartUsb(void);
void			usb_cstd_D0fifoStopUsb(void);
void			usb_cstd_SelectNak(uint16_t pipe);

void			usb_cstd_GetDescriptorInterface(USBC_REQUEST_t *parm);
uint16_t		usb_hstd_CheckDescriptor(uint8_t *table, uint16_t spec);
/* Condition compilation by the difference of IP */
#if USBC_FW_PP == USBC_FW_OS_PP
uint16_t		usb_hstd_ClearStall(uint16_t pipe);
uint16_t		usb_hstd_GetConfigDesc(uint16_t addr, uint16_t length);
uint16_t		usb_hstd_SetFeature(uint16_t addr, uint16_t epnum);
uint16_t		usb_hstd_ClearFeature(uint16_t addr, uint16_t epnum);
#else	/* USBC_FW_PP == USBC_FW_OS_PP */
uint16_t		usb_hstd_ClearStall(uint16_t pipe, USBC_CB_t complete);
uint16_t		usb_hstd_GetConfigDesc(uint16_t addr, uint16_t length
					, USBC_CB_t complete);
uint16_t		usb_hstd_SetFeature(uint16_t addr, uint16_t epnum
					, USBC_CB_t complete);
uint16_t		usb_hstd_ClearFeature(uint16_t addr, uint16_t epnum
					, USBC_CB_t complete);
#endif	/* USBC_FW_PP == USBC_FW_OS_PP */
uint16_t		usb_hstd_GetStringDescriptor1(uint16_t devaddr
					, uint16_t index, USBC_CB_t complete);
uint16_t		usb_hstd_GetStringDescriptor1Check(uint16_t errcheck);

uint16_t		usb_hstd_GetStringDescriptor2(uint16_t devaddr
					, uint16_t index, USBC_CB_t complete);
uint16_t		usb_hstd_GetStringDescriptor2Check(uint16_t errcheck);
uint16_t		usb_hstd_ClearStallCheck(uint16_t errcheck);
uint16_t		usb_hstd_StdReqCheck(uint16_t errcheck);

void			usb_hstd_Ovrcr0Function(void);
void			usb_hstd_Attch0Function(void);
void			usb_hstd_Bchg0Function(void);
void			usb_hstd_Dtch0Function(void);
void			usb_hstd_LsConnectFunction(void);
void			usb_hstd_AttachFunction(void);
uint16_t		usb_hstd_EnumFunction1(void);
uint16_t		usb_hstd_EnumFunction2(uint16_t* enummode);
void			usb_hstd_EnumFunction3(uint16_t devaddr
					, uint16_t enum_seq);
void			usb_hstd_EnumFunction4(uint16_t* reqnum, uint16_t* enummode
					, uint16_t devaddr);
void			usb_hstd_EnumFunction5(void);
void			usb_hstd_DetachFunction(uint16_t port);
void			usb_hstd_DetachProcFunction(uint16_t port);
void			usb_hstd_AttachProcFunction(uint16_t port, uint16_t *buf);
void			usb_hstd_VbusFunction(uint16_t port, uint16_t command);
void			usb_hstd_SuspendFunction(uint16_t port);
uint16_t		usb_hstd_ChkLnstFunction(uint16_t port);
uint16_t		usb_hstd_OvrcreFunction(uint16_t port);
void			usb_hstd_AttachProcess(uint16_t port);
void			usb_hstd_DetachProcess(uint16_t port);
void			usb_hstd_ReadLnst(uint16_t port, uint16_t *buf);
void			usb_hstd_VbusControl(uint16_t port, uint16_t command);
void			usb_hstd_BusReset(uint16_t port);
void			usb_hstd_ResumeProcess(uint16_t port);
void			usb_hstd_SuspendProcess(uint16_t port);
void			usb_hstd_ChkSof(uint16_t port);
void			usb_hstd_TestUactControl(uint16_t port, uint16_t command);
void			usb_hstd_TestVbusControl(uint16_t port, uint16_t command);
void			usb_hstd_TestBusReset(uint16_t port);
void			usb_hstd_TestSuspend(uint16_t port);
void			usb_hstd_TestResume(uint16_t port);
void			usb_hstd_TestStop(uint16_t port);
void			usb_hstd_TestSignal(uint16_t port, uint16_t command);
void			usb_hstd_Attach(uint16_t result, uint16_t port);
void			usb_hstd_Detach(uint16_t port);
void			usb_hstd_InitConnect(uint16_t port);
void			usb_hstd_ChkClk(uint16_t port, uint16_t event);
uint16_t		usb_hstd_ChkAttach(uint16_t port);
void			usb_hstd_Attach(uint16_t result, uint16_t port);
void			usb_hstd_Detach(uint16_t port);
void			usb_hstd_InitConnect(uint16_t port);
void			usb_hstd_ChkClk(uint16_t port, uint16_t event);
uint16_t		usb_hstd_ChkAttach(uint16_t port);
void			usb_hstd_SuspendClock(void);
void			usb_hstd_DetachClock(void);
void			usb_hstd_MgrReset(uint16_t addr);
void			usb_hstd_MgrResume(uint16_t);
void			usb_hstd_MgrSuspend(uint16_t);
void			usb_hstd_DeviceStateControl(uint16_t devaddr
					, uint16_t msginfo);
void			usb_hstd_DeviceStateControl2(USBC_CB_INFO_t complete
					, uint16_t devaddr, uint16_t msginfo
					, uint16_t mgr_msginfo);
uint16_t		usb_hstd_Enumeration(void);
void			usb_hstd_EnumerationErr(uint16_t Rnum);
uint16_t		usb_hstd_ChkDeviceClass(USBC_HCDREG_t *driver
					, uint16_t port);
void			usb_hstd_NotifAtorDetach(uint16_t result, uint16_t port);
void			usb_hstd_OvcrNotifiation(uint16_t port);
void			usb_hstd_StatusResult(uint16_t port, uint16_t result);
void			usb_hstd_SubmitResult(uint16_t *utr_table);
void			usb_hstd_TransferEndResult(uint16_t result, uint16_t pipe);
void			usb_hstd_DetachsContrlTrans(uint16_t addr);
void			usb_hstd_SetReDetachControl(uint16_t addr);
void			usb_hstd_EnumGetDescriptor(uint16_t addr
					, uint16_t CntValue);
void			usb_hstd_EnumSetAddress(uint16_t addr, uint16_t setaddr);
void			usb_hstd_EnumSetConfiguration(uint16_t addr
					, uint16_t confnum);
void			usb_hstd_EnumDummyRequest(uint16_t addr
					, uint16_t CntValue);
void			usb_hstd_SetDetachDetect(uint16_t addr, uint16_t CntValue);
uint16_t		usb_cstd_FunctionUsbip(void);
void			usb_hstd_BchgClearSts(uint16_t port);
void			usb_hstd_BchgEnable(uint16_t port);
void			usb_hstd_BchgDisable(uint16_t port);
void			usb_hstd_SetUact(uint16_t port);
void			usb_hstd_RwupeEnable(uint16_t port);
void			usb_hstd_RwupeDisable(uint16_t port);
void			usb_hstd_OvrcrClearSts(uint16_t port);
void			usb_hstd_OvrcrEnable(uint16_t port);
void			usb_hstd_OvrcrDisable(uint16_t port);
void			usb_hstd_AttchClearSts(uint16_t port);
void			usb_hstd_AttchEnable(uint16_t port);
void			usb_hstd_AttchDisable(uint16_t port);
void			usb_hstd_DtchClearSts(uint16_t port);
void			usb_hstd_DtchEnable(uint16_t port);
void			usb_hstd_DtchDisable(uint16_t port);
void			usb_hstd_SetDevAddr(uint16_t addr, uint16_t speed
					, uint16_t port);
void			usb_hstd_SetHubPort(uint16_t addr, uint16_t upphub
					, uint16_t hubport);
void			usb_hstd_SetPipeRegister(uint16_t PipeNo, uint16_t *tbl);
uint16_t		usb_hstd_GetRootport(uint16_t addr);
uint16_t		usb_hstd_ChkDevAddr(uint16_t addr, uint16_t rootport);
uint16_t		usb_hstd_GetDevSpeed(uint16_t addr);
uint16_t		usb_hstd_GetDevAddr(uint16_t addr);
void			usb_hstd_BrdyPipe(uint16_t bitsts);
void			usb_hstd_NrdyPipe(uint16_t bitsts);
void			usb_hstd_BempPipe(uint16_t bitsts);
uint16_t		usb_hstd_MfrMode1(uint16_t devaddr);
uint16_t		usb_hstd_MfrMode2(uint16_t devclass);
uint16_t		usb_hstd_MfrMode3(uint16_t devclass);
uint16_t		usb_hstd_MfrMode4(uint16_t devclass);
uint8_t			*usb_hstd_DevDescriptor(void);
uint8_t			*usb_hstd_ConDescriptor(void);
uint16_t		usb_hstd_HsFsResult(void);
void			usb_hstd_DeviceAttach(uint16_t devaddr);
void			usb_hstd_DeviceDetach(uint16_t devaddr);
void			usb_hstd_DeviceUsbReset(uint16_t devaddr);
void			usb_hstd_DeviceSuspend(uint16_t devaddr);
void			usb_hstd_DeviceResume(uint16_t devaddr);
void			usb_hstd_DeviceRemote(uint16_t devaddr);
void			usb_hstd_DeviceVbon(uint16_t devaddr);
void			usb_hstd_DeviceVboff(uint16_t devaddr);
USBC_ER_t		usb_hstd_HcdSndMbx(uint16_t msginfo, uint16_t dat
					, uint16_t *adr, USBC_CB_INFO_t callback);
void			usb_hstd_MgrSndMbx(uint16_t msginfo, uint16_t dat
					, uint16_t res);
void			usb_hhub_Release(void);
void			usb_hhub_Initial(uint16_t data1, uint16_t data2);
void			usb_hhub_ChkClass(uint16_t **table);
uint16_t		usb_hhub_ChkConfig(uint16_t **table, uint16_t spec);
uint16_t		usb_hhub_ChkInterface(uint16_t **table, uint16_t spec);
uint16_t		usb_hhub_PipeInfo(uint8_t *table, uint16_t offset
					, uint16_t speed, uint16_t length);
void			usb_hhub_InputStatus(void);
void			usb_hhub_PortDetach(uint16_t hubaddr, uint16_t portnum);
void			usb_hhub_SelectiveDetach(uint16_t devaddr);
void			usb_hhub_TransStart(uint16_t hubaddr, uint32_t size
					, uint8_t *table, USBC_CB_t complete);
void			usb_hhub_TransResult(USBC_UTR_t *mess);
void			usb_hhub_ProcessResult(uint16_t data,uint16_t dummy);
uint16_t		usb_hhub_TransWaitTmo(uint16_t tmo);
uint16_t		usb_hhub_ProcessWaitTmo(uint16_t tmo);
USBC_ER_t		usb_hhub_ChangeState(uint16_t devaddr, uint16_t msginfo
					, USBC_CB_INFO_t callback);
uint16_t		usb_hhub_GetNewDevaddr(void);
uint16_t		usb_hhub_GetHubaddr(uint16_t pipenum);
uint16_t		usb_hhub_GetCnnDevaddr(uint16_t hubaddr, uint16_t portnum);
uint16_t		usb_hhub_ChkTblIndx1(uint16_t hubaddr);
uint16_t		usb_hhub_ChkTblIndx2(uint16_t hubaddr);
uint16_t		usb_hhub_ChkTblIndx3(uint16_t pipenum);
void			usb_hstd_SetupStart(void);
uint16_t		usb_hstd_ControlWriteStart(uint32_t bsize, uint8_t *table);
void			usb_hstd_ControlReadStart(uint32_t bsize, uint8_t *table);
void			usb_hstd_StatusStart(void);
void			usb_hstd_ControlEnd(uint16_t Status);
void			usb_hstd_Suspend(uint16_t port);
USBC_ER_t		usb_hstd_SetSubmitutr(USBC_UTR_t *utrmsg);
void			usb_hstd_SetReTransfer(uint16_t pipe);
void			usb_hstd_Interrupt(USBC_UTR_t *p);
void			usb_hstd_InterruptHandler(uint16_t *keyword
					, uint16_t *status);
void			usb_hstd_BusIntDisable(uint16_t port);
void			usbc_cpu_DelayXms(uint16_t time);
void			usbc_cpu_Delay1us(uint16_t time);
void			usb_cstd_D0fifo2BufStartDma(uint32_t SourceAddr);
void			usb_cstd_Buf2D0fifoStartDma(uint32_t DistAdr);
void			usb_cstd_StopDma(void);
void			usb_pstd_StandReq0 (void);
void			usb_pstd_StandReq1 (void);
void			usb_pstd_StandReq2 (void);
void			usb_pstd_StandReq3 (void);
void			usb_pstd_StandReq4 (void);
void			usb_pstd_StandReq5 (void);
uint16_t		usb_cstd_SmpTestPort(void);
void			usb_pstd_DpEnable(void);
void			usb_pstd_DpDisable(void);
void			usb_pstd_DmEnable(void);
void			usb_pstd_DmDisable(void);
void			usb_cstd_TargetInit(void);
void			usb_cstd_TargetBoardInit(void);
uint16_t		usbc_cpu_KeyWait(void);
USBC_ER_t		usb_pmsc_SmpAtapiInitMedia(void);
USBC_ER_t		usb_pmsc_SmpAtapiCloseMedia(void);
uint16_t		usb_cstd_Sw(void);
uint16_t		usb_cstd_GetDevsel(uint16_t Pipe);
void			usb_cstd_RegClrPipeCtr(uint16_t Pipe, uint16_t bit);
void			usb_cstd_RegSetPipeCtr(uint16_t Pipe, uint16_t bit);
uint16_t		usb_cstd_RegRdCtr(uint16_t Pipe);
void			usb_pstd_PcdRelMpl(uint16_t);
void			usb_cstd_DummyFunction(uint16_t data1, uint16_t data2);
void			usb_cstd_DummyTrn(USBC_REQUEST_t *data1, uint16_t data2);
void			usb_cstd_ClassProcessResult(uint16_t data,uint16_t dummy);
void			usb_cstd_ClassTransResult(USBC_UTR_t *mess);

void			usb_pstd_PcdTask(USBC_VP_INT_t);
void			usb_hstd_MgrTask(USBC_VP_INT_t);
void			usb_hhub_Task(USBC_VP_INT_t);
void			usb_hstd_HcdTask(USBC_VP_INT_t);
void			usb_cstd_MainTask(USBC_VP_INT_t);
void			usb_hstd_HetTask(USBC_VP_INT_t);
void			usb_pstd_MainTask(USBC_VP_INT_t);

/* Condition compilation by the difference of the operating system */
#if USBC_FW_PP == USBC_FW_OS_PP
void			usb_hstd_MainTask(USBC_VP_INT_t);	/* OS */
#else	/* !USBC_FW_OS_PP */
void			usb_hstd_MainTask(USBC_UTR_t *mess);	/* nonOS */
#endif	/* USBC_FW_OS_PP */


USBC_ER_t		usb_hstd_ChgSeqbit(uint16_t pipe, uint16_t toggle
					, USBC_CB_INFO_t complete);
void			usb_hhub_InitDownPort(uint16_t hubaddr
					, USBC_CLSINFO_t *mess);
void			usb_hhub_NewConnect(uint16_t hubaddr, uint16_t portnum
					, USBC_CLSINFO_t *mess);
uint16_t		usb_hhub_PortAttach(uint16_t hubaddr, uint16_t portnum
					, USBC_CLSINFO_t *mess);
void			usb_hhub_PortReset(uint16_t hubaddr, uint16_t portnum
					, USBC_CLSINFO_t *mess);
uint16_t		usb_hhub_PortSetFeature(uint16_t hubaddr, uint16_t port
					, uint16_t command, USBC_CB_t complete);
uint16_t		usb_hhub_PortClrFeature(uint16_t hubaddr, uint16_t port
					, uint16_t command, USBC_CB_t complete);
/* Condition compilation by the difference of the operating system */
#if USBC_FW_PP == USBC_FW_OS_PP
#endif // USBC_FW_PP == USBC_FW_OS_PP


/* Condition compilation by the difference of the operating system */
#if USBC_FW_PP == USBC_FW_OS_PP
uint16_t		usb_hstd_ClearFeature(uint16_t addr, uint16_t epnum);
uint16_t		usb_hstd_ClearStall(uint16_t pipe);
#else	/* !USBC_FW_OS_PP */
uint16_t		usb_hstd_ClearFeature(uint16_t addr, uint16_t epnum
					, USBC_CB_t complete);
uint16_t		usb_hstd_ClearStall(uint16_t pipe, USBC_CB_t complete);
void			usb_hstd_ClearStallResult(uint16_t *utr_table);
#endif	/* USBC_FW_OS_PP */


/* Condition compilation by the difference of the operating system */
#if USBC_FW_PP == USBC_FW_NONOS_PP
uint16_t		usb_hstd_InitConnect0(uint16_t port);
void			usb_hstd_InitConnect2(void);
void			usb_hstd_SuspCont(uint16_t devaddr, uint16_t rootport);
void			usb_hstd_ResuCont(uint16_t devaddr, uint16_t rootport);
void			usb_hhub_Enumeration(USBC_CLSINFO_t *mess);
void			usb_hhub_SubmitResult(USBC_CLSINFO_t *mess
					, uint16_t status);
void			usb_hhub_Ivent(USBC_CLSINFO_t *mess);
void			usb_hhub_SpecifiedPath(USBC_CLSINFO_t *mess);
void			usb_hhub_SpecifiedPathWait(USBC_CLSINFO_t *mess
					, uint16_t times);
void			usb_hhub_CheckResult(USBC_UTR_t *mess);
void			usb_hhub_WaitRequest(USBC_UTR_t *mess);
void			usb_hhub_ClearStallResult(USBC_UTR_t *mess);
void			usb_hhub_CheckRequest(uint16_t result);
uint16_t		usb_hhub_RequestResult(uint16_t checkerr);
void			usb_cstd_IntEnable(void);
void			usb_cstd_IntDisable(void);
#endif // USBC_FW_PP == USBC_FW_NONOS_PP


/*****************************************************************************
Public Functions (API)
******************************************************************************/

USBC_ER_t		R_usb_pstd_PcdOpen(void);
USBC_ER_t		R_usb_pstd_PcdClose(void);
USBC_ER_t		R_usb_pstd_TransferStart(USBC_UTR_t *utr_table);
USBC_ER_t		R_usb_pstd_SetStall(USBC_CB_INFO_t complete, uint16_t pipe);
USBC_ER_t		R_usb_pstd_TransferEnd(uint16_t pipe, uint16_t status);
void			R_usb_pstd_DriverRegistration(USBC_PCDREG_t *callback);
void			R_usb_pstd_DriverRelease(void);
USBC_ER_t		R_usb_pstd_PcdChangeDeviceState(uint16_t msginfo
					, uint16_t member, USBC_CB_INFO_t complete);
void			R_usb_pstd_DeviceInformation(uint16_t *tbl);
void			R_usb_pstd_SetPipeRegister(uint16_t PipeNo, uint16_t *tbl);
uint16_t		R_usb_pstd_ControlRead(uint32_t Bsize, uint8_t *Table);
void			R_usb_pstd_ControlWrite(uint32_t Bsize, uint8_t *Table);
void			R_usb_pstd_ControlEnd(uint16_t status);


void			R_usb_cstd_ClearHwFunction(void);
/* Condition compilation by the difference of the operating system */
#if USBC_FW_PP == USBC_FW_NONOS_PP
void			R_usbc_cstd_Scheduler(void);
void			R_usbc_cstd_SetTaskPri(uint8_t tasknum, uint8_t pri);
uint8_t			R_usbc_cstd_CheckSchedule(void);
#endif // USBC_FW_PP == USBC_FW_NONOS_PP


USBC_ER_t		R_usb_hstd_HcdOpen(void);
USBC_ER_t		R_usb_hstd_HcdClose(void);
USBC_ER_t		R_usb_hstd_TransferStart(USBC_UTR_t *utr_table);
USBC_ER_t		R_usb_hstd_SetPipeRegistration(uint16_t *table
					, uint16_t pipe);
USBC_ER_t		R_usb_hstd_TransferEnd(uint16_t pipe, uint16_t status);
USBC_ER_t		R_usb_hstd_ChangeDeviceState(USBC_CB_INFO_t complete
					, uint16_t msginfo, uint16_t rootport);

void			R_usb_hstd_SmplRegistration(void);
void			R_usb_hstd_SmplClassCheck(uint16_t **table);
void			R_usb_hstd_SmplOpen(uint16_t data1, uint16_t data2);
void			R_usb_hstd_SmplClose(uint16_t data1, uint16_t data2);

USBC_ER_t		R_usb_hstd_MgrOpen(void);
USBC_ER_t		R_usb_hstd_MgrClose(void);
void			R_usb_hstd_DriverRegistration(USBC_HCDREG_t *callback);
void			R_usb_hstd_DriverRelease(uint8_t devclass);
USBC_ER_t		R_usb_hstd_MgrChangeDeviceState(USBC_CB_INFO_t complete
					, uint16_t msginfo, uint16_t devaddr);
void			R_usb_hstd_DeviceInformation(uint16_t addr, uint16_t *tbl);
/* Condition compilation by the difference of the operating system */
#if USBC_FW_PP == USBC_FW_NONOS_PP
void			R_usb_hstd_ReturnEnuMGR(uint16_t cls_result);
void			R_usb_hstd_EnuWait(uint8_t taskID);
#endif // USBC_FW_PP == USBC_FW_NONOS_PP
uint16_t		R_usb_hstd_DetachControl(uint16_t port);


USBC_ER_t		R_usb_hhub_Open(uint16_t devaddr, uint16_t data2);
USBC_ER_t		R_usb_hhub_Close(uint16_t hubaddr, uint16_t data2);
void			R_usb_hhub_Registration(USBC_HCDREG_t *callback);
USBC_ER_t		R_usb_hhub_ChangeDeviceState(USBC_CB_INFO_t complete
					, uint16_t msginfo, uint16_t devaddr);
uint16_t		R_usb_hhub_GetHubInformation(uint16_t hubaddr
					, USBC_CB_t complete);
uint16_t		R_usb_hhub_GetPortInformation(uint16_t hubaddr
					, uint16_t port, USBC_CB_t complete);

void			R_usb_pmsc_StrgTaskOpen(void);

uint16_t		R_usb_hstd_ChkPipeInfo(uint16_t speed, uint16_t *EpTbl
					, uint8_t *Descriptor);

void			R_usb_hstd_SetPipeInfo(uint16_t *EpTbl, uint16_t *TmpTbl
					, uint16_t length);

#endif	/* __R_USB_CEXTERN_H__ */
/******************************************************************************
End  Of File
******************************************************************************/
