/******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized.
* This software is owned by Renesas Electronics Corporation and is  protected
* under all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES
* REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY,
* INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR  A
* PARTICULAR PURPOSE AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE  EXPRESSLY
* DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE  LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES
* FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS
* AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this
* software and to discontinue the availability of this software.
* By using this software, you agree to the additional terms and
* conditions found by accessing the following link:
* http://www.renesas.com/disclaimer
*******************************************************************************
* Copyright (C) 2010(2011) Renesas Electronics Corpration
* and Renesas Solutions Corp. All rights reserved.
*******************************************************************************
* File Name    : main.c
* Version      : 1.10
* Device(s)    : Renesas SH-Series, RX-Series
* Tool-Chain   : Renesas SuperH RISC engine Standard Toolchain
*              : Renesas RX Standard Toolchain
* OS           : Common to None and uITRON 4.0 Spec
* H/W Platform : Independent
* Description  : main process
******************************************************************************
* History : DD.MM.YYYY Version Description
*         : 17.03.2010 0.80    First Release
*         : 30.07.2010 0.90    Updated comments
*         : 02.08.2010 0.91    Updated comments
*         : 29.10.2010 1.00    Mass Production Release
*         : 01.06.2011 1.10    Version 1.10 Release
******************************************************************************/


/******************************************************************************
Includes   <System Includes> , "Project Includes"
******************************************************************************/
#include <machine.h>

#include "r_usbc_cTypedef.h"		/* Type define */
#include "r_usb_cDefUsr.h"			/* USB-H/W register (user define) */
#include "r_usbc_cKernelId.h"		/* Kernel ID definition */
#include "r_usbc_cMacSystemcall.h"	/* uITRON system call macro */
#include "r_usbc_cMacPrint.h"		/* Standard IO macro */
#include "r_usbc_cDefUSBIP.h"		/* USB-FW Library Header */


/******************************************************************************
Section    <Section Definition> , "Project Sections"
******************************************************************************/
#pragma section _smpl


/******************************************************************************
External variables and functions
******************************************************************************/


/******************************************************************************
Private global variables and functions
******************************************************************************/
void	usbc_cstd_IdleTaskStart(void);
void	usb_cstd_PrintKernelSysdwnInfo(signed long w, signed long er
			, signed long i1, signed long i2);

/* Condition compilation by the difference of the operating system */
#if USBC_FW_PP == USBC_FW_NONOS_PP
/* Condition compilation by the difference of the devices */
 #if (USBC_LPWR_MODE_PP == USBC_LPWR_USE_PP)
extern uint16_t usb_gcpu_RemoteProcess;
 #endif /* (USBC_LPWR_MODE_PP == USBC_LPWR_USE_PP) */
#endif /* USBC_FW_PP == USBC_FW_NONOS_PP */


/******************************************************************************
Section    <Section Definition> , "Project Sections"
******************************************************************************/
#pragma section _smpl


/******************************************************************************
Function Name   : usb_cstd_PrintKernelSysdwnInfo
Description     : System down Information print
Arguments       : signed long w         : error kind
                : signed long er        : error code
                : signed long i1        : system down information 1
                : signed long i2        : system down information 2
Return value    : none
******************************************************************************/
void usb_cstd_PrintKernelSysdwnInfo(signed long w, signed long er
	, signed long i1, signed long i2)
{
	USBC_PRINTF0("\nkernel_sysdwn ==========\n");
	USBC_PRINTF4("  %ld,%ld,%lx,%lx\n", w, er, i1, i2);
	USBC_PRINTF0("========================\n");
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usbc_cstd_IdleTaskStart
Description     : Idle Task Start process
Arguments       : none
Return value    : none
******************************************************************************/
void usbc_cstd_IdleTaskStart(void)
{
/* Condition compilation by the difference of the devices */
#if (USBC_LPWR_MODE_PP == USBC_LPWR_USE_PP)
/* Condition compilation by the difference of the operating system */
 #if USBC_FW_PP == USBC_FW_OS_PP
	USBC_ER_t	err;
/* Condition compilation by the difference of the device's operating system */
  #if USBC_OS_CRE_MODE_PP == USBC_OS_CRE_USE_PP
	USBC_TSK_t	tskinfo;

	/* Create application task */
	/* Attribute */
	tskinfo.tskatr	= (USBC_ATR_t)(USBC_TA_HLNG);
	/* Start address */
	tskinfo.task	= (USBC_FP_t)&usbc_cstd_IdleTask;
	/* Priority */
	tskinfo.itskpri	= (USBC_PRI_t)(USB_IDLE_PRI);
	/* Stack size */
	tskinfo.stksz	= (USBC_SIZ_t)(USB_TSK_STK);
	/* Stack address */
	tskinfo.stk		= (USBC_VP_t)(USBC_NULL);
	err = USBC_CRE_TSK(USBC_IDLE_TSK, &tskinfo);
	if( err != USBC_E_OK )
	{
		USBC_PRINTF1("CRE_TSK USBC_IDLE_TSK Error (%ld)\n", err);
		while( 1 )
		{
			/* do nothing */
		}
	}

	/* Start task */
	err = USBC_STA_TSK(USBC_IDLE_TSK, 0L);
	if( err != USBC_E_OK )
	{
		USBC_PRINTF1("STA_TSK USBC_IDLE_TSK Error (%ld)\n", err);
		while( 1 )
		{
			/* do nothing */
		}
	}
  #endif	/* USBC_OS_CRE_MODE_PP == USBC_OS_CRE_USE_PP */

/* Condition compilation by the difference of the device's operating system */
  #if USBC_OS_CRE_MODE_PP == USBC_OS_CRE_NOTUSE_PP
	/* Start Task */
	err = USBC_ACT_TSK(USBC_IDLE_TSK);
	if( err != USBC_E_OK )
	{
		USBC_PRINTF1("ACT_TSK USBC_IDLE_TSK Error (%ld)\n", err);
		while( 1 )
		{
		}
	}
  #endif	/* USBC_OS_CRE_MODE_PP == USBC_OS_CRE_NOTUSE_PP */
 #else /* USBC_FW_PP != USBC_FW_OS_PP */
	USBC_SND_MSG(USBC_IDL_TSK, 0);
 #endif /* USBC_FW_PP != USBC_FW_OS_PP */
#endif /* (USBC_LPWR_MODE_PP == USBC_LPWR_USE_PP) */
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usbc_cstd_IdleTask
Description     : Idle Task (sleep sample)
Arguments       : USBC_VP_INT stacd      : task start code(not use)
Return value    : none
******************************************************************************/
void usbc_cstd_IdleTask(USBC_VP_INT stacd)
{
/* Condition compilation by the difference of USB function */
#if USB2_FUNCSEL_PP == USBC_HOST_PP
	/* nothing */
#else /* USB2_FUNCSEL_PP == USBC_HOST_PP */
/* Condition compilation by the difference of the devices */
 #if (USBC_LPWR_MODE_PP == USBC_LPWR_USE_PP)
	void		usbc_cpu_GoLpwrSleepMode(void);
	void		usbc_cpu_GoDeepStbyMode(void);
	uint16_t	res[8], sts0;
	uint8_t		stby;
/* Condition compilation by the difference of the operating system */
  #if USBC_FW_PP == USBC_FW_NONOS_PP
	USBC_UTR_t	*mess;
	USBC_ER_t	err;
	uint16_t	usb_idle;

	usb_idle = USBC_OFF;
  #endif	/* USBC_FW_PP == USBC_FW_NONOS_PP */

/* Condition compilation by the difference of the operating system */
  #if USBC_FW_PP == USBC_FW_OS_PP
	while( 1 )
	{
  #endif /* USBC_FW_PP == USBC_FW_OS_PP */

/* Condition compilation by the difference of the operating system */
  #if USBC_FW_PP == USBC_FW_NONOS_PP
	/* Idle Task message receive (port0) */
	err = USBC_TRCV_MSG(USBC_IDL_TSK, (USBC_MSG_t**)&mess, (USBC_TM_t)0);
	if( (err != USBC_E_OK) && (err != USBC_E_TMOUT) )
	{
		USBC_PRINTF1("### HSMP driver rcv_msg error %ld\n", err);
	}
	else
	{
		/* Send message to IDL_TSK */
		usb_idle = USBC_ON;
		USBC_SND_MSG(USBC_IDL_TSK, 0);
	}

	if( usb_idle == USBC_ON )
	{
  #endif /* USBC_FW_PP == USBC_FW_NONOS_PP */


/* Condition compilation by the difference of USB function */
  #ifdef USB_FUNCSEL_PP
		R_usb_pstd_DeviceInformation((uint16_t *)res);
  #endif /* USB_FUNCSEL_PP */
/* Condition compilation by the difference of USB function */

		sts0 = res[0];
		if( (sts0 & USBC_VBSTS) == 0 )
		{
			sts0 = USBC_DETACHED;	/* Port0 detach */
		}
		else if( (sts0 & USBC_DS_SPD_CNFG) == USBC_DS_SPD_CNFG )
		{
			sts0 = USBC_SUSPENDED;	/* Port0 suspend */
		}
		else
		{
			sts0 = USBC_PORTOFF;
		}

		if( sts0 == USBC_DETACHED )
		{
			/* Detach */
/* Condition compilation by the difference of the operating system */
  #if USBC_FW_PP == USBC_FW_NONOS_PP
			usb_gcpu_RemoteProcess = USBC_OFF;
  #endif /* USBC_FW_PP == USBC_FW_NONOS_PP */
  			/* Get Deep Software Standby Reset Flag */
			stby = usbc_cpu_GetDPSRSTF();
			if( stby == 0 )
			{
				/* Deep Standby shift */
				usbc_cpu_GoLpwrDeepStby();
			}
		}
		else if( sts0 == USBC_SUSPENDED )
		{
			/* Suspend */
			usbc_cpu_GoLpwrSleep();
		}
		else
		{
/* Condition compilation by the difference of the operating system */
  #if USBC_FW_PP != USBC_FW_OS_PP
			usb_gcpu_RemoteProcess = USBC_OFF;
  #endif /* USBC_FW_PP != USBC_FW_OS_PP */
		}
	}
 #else /* (USBC_LPWR_MODE_PP == USBC_LPWR_USE_PP) */
/* Condition compilation by the difference of the operating system */
  #if USBC_FW_PP == USBC_FW_OS_PP
	while( 1 )
	{
	}
  #endif	/* USBC_FW_PP == USBC_FW_OS_PP */

 #endif /* (USBC_LPWR_MODE_PP == USBC_LPWR_USE_PP) */
#endif /* USB2_FUNCSEL_PP == USBC_HOST_PP */
}
/******************************************************************************
End of function
******************************************************************************/

/******************************************************************************
End  Of File
******************************************************************************/
