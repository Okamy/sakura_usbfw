/******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized.
* This software is owned by Renesas Electronics Corporation and is  protected
* under all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES
* REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY,
* INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR  A
* PARTICULAR PURPOSE AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE  EXPRESSLY
* DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE  LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES
* FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS
* AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this
* software and to discontinue the availability of this software.
* By using this software, you agree to the additional terms and
* conditions found by accessing the following link:
* http://www.renesas.com/disclaimer
*******************************************************************************
* Copyright (C) 2010(2011) Renesas Electronics Corpration
* and Renesas Solutions Corp. All rights reserved.
*******************************************************************************
* File Name    : RX630RSK.c
* Version      : 1.10
* Device(s)    : Renesas SH-Series, RX-Series
* Tool-Chain   : Renesas SuperH RISC engine Standard Toolchain
*              : Renesas RX Standard Toolchain
* OS           : Common to None and uITRON 4.0 Spec
* H/W Platform : Independent
* Description  : RX630 RSK processing
******************************************************************************
* History : DD.MM.YYYY Version Description
*         : 17.03.2010 0.80    First Release
*         : 30.07.2010 0.90    Updated comments
*         : 02.08.2010 0.91    Updated comments
*         : 29.10.2010 1.00    Mass Production Release
*         : 01.06.2011 1.10    Version 1.10 Release
******************************************************************************/


/******************************************************************************
Includes   <System Includes> , "Project Includes"
******************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <machine.h>
#include "RX63nSAKURA_Define.h"		/* RX63nSAKURA Define Header */
#include "RX63nSAKURA_Extern.h"		/* RX63nSAKURA Extern Header */

#include "r_usbc_cTypedef.h"		/* Type define */
#include "r_usb_cDefUsr.h"			/* USB-H/W register (user define) */
#include "r_usbc_cDefUSBIP.h"		/* USB-FW Library Header */
#include "r_usbc_cMacPrint.h"		/* Standard IO macro */
#include "r_usbc_cMacSystemcall.h"	/* System call macro */
#include "r_usb_cExtern.h"			/* USB-FW global define */


/* Condition compilation by the difference of the operating system */
#if USBC_FW_PP == USBC_FW_OS_PP
#include "kernel_id.h"				/* Kernel ID definition */
#else	/* USBC_FW_PP == USBC_FW_OS_PP */
#include "r_usbc_cKernelId.h"		/* Kernel ID definition */
#endif	/* USBC_FW_PP == USBC_FW_OS_PP */

/******************************************************************************
External variables and functions
******************************************************************************/


/******************************************************************************
Constant macro definitions
******************************************************************************/

/* ----- LED Defines -------- */
#define	LED_0					0
#define	LED_1					1
#define	LED_2					2
#define	LED_3					3


/******************************************************************************
Section    <Section Definition> , "Project Sections"
******************************************************************************/
#pragma address MDEreg=0xffffff80 /* MDE register (Single Chip Mode) */
#ifdef __BIG
	const unsigned long MDEreg = 0xfffffff8; /* big */
#else
	const unsigned long MDEreg = 0xffffffff; /* little */
#endif

/* Condition compilation by the difference of the devices */
#if USBC_TRANS_MODE_PP == USBC_TRANS_DTC_PP
#pragma section _DTCtable


/******************************************************************************
Typedef definitions
******************************************************************************/

/******************************************************************************
Bit Order Definition "LEFT"
******************************************************************************/
#pragma bit_order left

/* DTC Vector table */ 
typedef struct
{
/* Condition compilation by the difference of the endian */
  #if USBC_CPUBYTE_PP == USBC_BYTE_LITTLE_PP
	unsigned short WORD;
    union
	{
        unsigned char BYTE;
        struct
		{
            unsigned char CHNE:1;
            unsigned char CHNS:1;
            unsigned char DISEL:1;
            unsigned char DTS:1;
            unsigned char DM:2;
            unsigned char :2;
        }
		BIT;
    }
	MRB;						/* Mode Register B */
    union
	{
        unsigned char BYTE;
        struct
		{
            unsigned char MD:2;
            unsigned char SZ:2;
            unsigned char SM:2;
            unsigned char :2;
        }
		BIT;
    }
	MRA;						/* Mode Register A */
	unsigned long  SAR;			/* Source Address */
	unsigned long  DAR;			/* Destination Address */
	unsigned short CRB;			/* Count Register A */
	unsigned short CRA;			/* Count Register B */
  #endif /* USBC_CPUBYTE_PP == USBC_BYTE_LITTLE_PP */

/* Condition compilation by the difference of the endian */
  #if USBC_CPUBYTE_PP == USBC_BYTE_BIG_PP
    union
	{
        unsigned char BYTE;
        struct
		{
            unsigned char MD:2;
            unsigned char SZ:2;
            unsigned char SM:2;
            unsigned char :2;
        }
		BIT;
    }
	MRA;						/* Mode Register A */
    union
	{
        unsigned char BYTE;
        struct
		{
            unsigned char CHNE:1;
            unsigned char CHNS:1;
            unsigned char DISEL:1;
            unsigned char DTS:1;
            unsigned char DM:2;
            unsigned char :2;
        }
		BIT;
    }
	MRB;						/* Mode Register B */
	unsigned short WORD;
	unsigned long  SAR;			/* Source Address */
	unsigned long  DAR;			/* Destination Address */
	unsigned short CRA;			/* Count Register A */
	unsigned short CRB;			/* Count Register B */
  #endif /* USBC_CPUBYTE_PP == USBC_BYTE_BIG_PP */
} USB_DTC_t;


/******************************************************************************
Bit Order Definition default
******************************************************************************/
#pragma bit_order

typedef struct st_usb		USB_STBY_t;


/******************************************************************************
Private global variables and functions
******************************************************************************/
/*
*  Must be set 0 by the Address Low 2bit. (4byte alignment)
*/
/* DTC Control Register */
USB_DTC_t	usb_dtcreg[2u];

/*
*  Must be set 0 by the Address Low 12bit. (0x???? ?000)
*/
/* DTC VECTOR Table  */
uint32_t	usb_dtcctable[48] = 
{
	(uint32_t)0xFFFFFFFF,		/* DTC VECTOR  0 */
	(uint32_t)0xFFFFFFFF,		/* DTC VECTOR  1 */
	(uint32_t)0xFFFFFFFF,		/* DTC VECTOR  2 */
	(uint32_t)0xFFFFFFFF,		/* DTC VECTOR  3 */
	(uint32_t)0xFFFFFFFF,		/* DTC VECTOR  4 */
	(uint32_t)0xFFFFFFFF,		/* DTC VECTOR  5 */
	(uint32_t)0xFFFFFFFF,		/* DTC VECTOR  6 */
	(uint32_t)0xFFFFFFFF,		/* DTC VECTOR  7 */
	(uint32_t)0xFFFFFFFF,		/* DTC VECTOR  8 */
	(uint32_t)0xFFFFFFFF,		/* DTC VECTOR  9 */
	(uint32_t)0xFFFFFFFF,		/* DTC VECTOR 10 */
	(uint32_t)0xFFFFFFFF,		/* DTC VECTOR 11 */
	(uint32_t)0xFFFFFFFF,		/* DTC VECTOR 12 */
	(uint32_t)0xFFFFFFFF,		/* DTC VECTOR 13 */
	(uint32_t)0xFFFFFFFF,		/* DTC VECTOR 14 */
	(uint32_t)0xFFFFFFFF,		/* DTC VECTOR 15 */
	(uint32_t)0xFFFFFFFF,		/* DTC VECTOR 16 */
	(uint32_t)0xFFFFFFFF,		/* DTC VECTOR 17 */
	(uint32_t)0xFFFFFFFF,		/* DTC VECTOR 18 */
	(uint32_t)0xFFFFFFFF,		/* DTC VECTOR 19 */
	(uint32_t)0xFFFFFFFF,		/* DTC VECTOR 20 */
	(uint32_t)0xFFFFFFFF,		/* DTC VECTOR 21 */
	(uint32_t)0xFFFFFFFF,		/* DTC VECTOR 22 */
	(uint32_t)0xFFFFFFFF,		/* DTC VECTOR 23 */
	(uint32_t)0xFFFFFFFF,		/* DTC VECTOR 24 */
	(uint32_t)0xFFFFFFFF,		/* DTC VECTOR 25 */
	(uint32_t)0xFFFFFFFF,		/* DTC VECTOR 26 */
	(uint32_t)0x00000000,		/* DTC VECTOR 27 (SWINT) */
	(uint32_t)0x00000000,		/* DTC VECTOR 28 (CMT0) */
	(uint32_t)0x00000000,		/* DTC VECTOR 29 (CMT1) */
	(uint32_t)0x00000000,		/* DTC VECTOR 30 (CMT2) */
	(uint32_t)0x00000000,		/* DTC VECTOR 31 (CMT3) */
	(uint32_t)0xFFFFFFFF,		/* DTC VECTOR 32 */
	(uint32_t)&usb_dtcreg[0],	/* DTC VECTOR 33 (USB0-D0FIFO) */
	(uint32_t)0x00000000,		/* DTC VECTOR 34 (USB0-D1FIFO) */
	(uint32_t)0xFFFFFFFF,		/* DTC VECTOR 35 */
	(uint32_t)&usb_dtcreg[1],	/* DTC VECTOR 36 */
	(uint32_t)0x00000000,		/* DTC VECTOR 37 */
	(uint32_t)0xFFFFFFFF,		/* DTC VECTOR 38 */
	(uint32_t)0xFFFFFFFF,		/* DTC VECTOR 39 */
	(uint32_t)0xFFFFFFFF,		/* DTC VECTOR 40 */
	(uint32_t)0xFFFFFFFF,		/* DTC VECTOR 41 */
	(uint32_t)0xFFFFFFFF,		/* DTC VECTOR 42 */
	(uint32_t)0xFFFFFFFF,		/* DTC VECTOR 43 */
	(uint32_t)0xFFFFFFFF,		/* DTC VECTOR 44 */
	(uint32_t)0x00000000,		/* DTC VECTOR 45 (SPRI0) */
	(uint32_t)0x00000000,		/* DTC VECTOR 46 (SPTI1) */
	(uint32_t)0xFFFFFFFF		/* DTC VECTOR 47 */
};
#endif	/* USBC_TRANS_MODE_PP == USBC_TRANS_DTC_PP */


/******************************************************************************
Section    <Section Definition> , "Project Sections"
******************************************************************************/
#pragma section _rx62n

/******************************************************************************
Private global variables and functions
******************************************************************************/
/* global variable prototype definition */
uint16_t	usb_gcstd_KeyTime[2];
uint8_t		usb_gcstd_KeyBuff[4];

uint32_t volatile	timer_nCount_ms = 0;


/******************************************************************************
Private global variables and functions
******************************************************************************/
/*=== SYSTEM ================================================================*/
void		usb_cstd_TargetInit(void);
void		usbc_cpu_FunctionUSB0IP(void);
/*=== LED ===================================================================*/
void		usbc_cpu_LedInit(void);
void		usbc_cpu_LedReinit(void);
void		usbc_cpu_LedSet(uint8_t data);
void		usbc_cpu_LedSetBit(uint8_t bit, uint8_t data);
void		usbc_cpu_LedLoop(void);
/*=== Interrupt =============================================================*/
void		usbc_cpu_UsbintInit(void);
void		usbc_cpu_DmaintInit(void);
void		usb_cstd_UsbIntHand(void);
void		usb_cstd_D0fifoIntHand(void);
/* Condition compilation by the difference of the operating system */
#if USBC_FW_PP == USBC_FW_NONOS_PP
void		usb_cstd_IntEnable(void);
void		usb_cstd_IntDisable(void);
#endif	/* USBC_FW_PP == USBC_FW_NONOS_PP */
/*=== DMA ===================================================================*/
void		usb_cstd_D0fifo2BufStartDma(uint32_t SourceAddr);
void		usb_cstd_Buf2D0fifoStartDma(uint32_t DistAdr);
void		usb_cstd_Buf2D0fifoRestartDma(void);
void		usb_cstd_StopDma(void);
/*=== TIMER =================================================================*/
void		timer_Initialize( void );
void		timer_IntHandler( void );
uint32_t	timer_GetTimeCount( void );
uint32_t	timer_GetPastTime( const uint32_t timeLast );
void		usbc_cpu_Delay1us(uint16_t time);
void		usbc_cpu_DelayXms(uint16_t time);
/*=== KEY ===================================================================*/
void		usbc_cpu_KeyInit(void);
uint16_t	usbc_cpu_KeyWait(void);
uint16_t	usbc_cpu_GetKeyNo(void);
void		usbc_cpu_AcquisitionKeyNo(void);
uint8_t		usbc_cpu_KeepKeyNo(void);
uint8_t		usbc_cpu_SingleKeyNo(void);
/*=== Standby control =======================================================*/
void		usbc_cpu_GoDeepStbyMode(void);
void		usbc_cpu_GoWait(void);
void		usbc_cpu_GoDeepStbyMode0(void);
void		usb_cpu_RegRecovEnable(void);
void		usb_cpu_RegRecovDisable(void);
/*=== IRQ ===================================================================*/
void		usbc_cpu_IRQ2_Enable(void);
void		usbc_cpu_IRQ2_DisEnable(void);
void		usbc_cpu_IRQ8_Enable(void);
void		usbc_cpu_IRQ8_DisEnable(void);
void		usbc_cpu_IRQ2Int(void);
void		usbc_cpu_IRQ8Int(void);


/* Condition compilation by the difference of the operating system */
#if USBC_FW_PP == USBC_FW_NONOS_PP
/* Condition compilation by the difference of the devices */
 #if (USBC_LPWR_MODE_PP == USBC_LPWR_USE_PP)
uint16_t	usb_gcpu_RemoteProcess = USBC_OFF;
 #endif /* (USBC_LPWR_MODE_PP == USBC_LPWR_USE_PP) */
#endif /* USBC_FW_PP == USBC_FW_NONOS_PP */

/******************************************************************************
Renesas Abstracted RSK functions
******************************************************************************/

/******************************************************************************
Function Name   : usb_cstd_TargetInit
Description     : Target System Initialize
Arguments       : none
Return value    : none
******************************************************************************/
void usb_cstd_TargetInit(void)
{
	/* Enable Interrupt */
	timer_Initialize();				// Initialize Timer interrupt */
	usbc_cpu_UsbintInit();			/* Initialized USB interrupt  */
	usbc_cpu_DmaintInit();			/* Initialized DMA interrupt  */

	usbc_cpu_LedInit();				/* Initialized RSK LED */
	usbc_cpu_KeyInit();				/* Initialized RSK KEY */

//	usbc_cpu_LcdInit();				/* Initialized RSK LCD */
//	usbc_cpu_LcdDisp( 0, "        ");
//	usbc_cpu_LcdDisp(16, "        ");
}
/******************************************************************************
End of function
******************************************************************************/

/******************************************************************************
Function Name   : usbc_cpu_TargetLcdClear
Description     : Target System Lcd Clear
Arguments       : none
Return value    : none
******************************************************************************/
void usbc_cpu_TargetLcdClear(void)
{
}
/******************************************************************************
End of function
******************************************************************************/

/******************************************************************************
LED function
******************************************************************************/

/******************************************************************************
Function Name   : usbc_cpu_LedInit
Description     : LED Initialize
Arguments       : none
Return value    : none
******************************************************************************/
void usbc_cpu_LedInit(void)
{
	/* Data direction register
	b0 B0 Pn0 in/out select bit
	b1 B1 Pn1 in/out select bit
	b2 B2 Pn2 in/out select bit
	b3 B3 Pn3 in/out select bit
	b4 B4 Pn4 in/out select bit
	b5 B5 Pn5 in/out select bit
	b6 B6 Pn6 in/out select bit
	b7 B7 Pn7 in/out select bit
	*/
	PORTA.PDR.BIT.B0	= 1;			/* LED1 PA0 */
	PORTA.PDR.BIT.B1	= 1;			/* LED2 PA1 */
	PORTA.PDR.BIT.B2	= 1;			/* LED3 PA3 */
	PORTA.PDR.BIT.B6	= 1;			/* LED4 PA6 */

	/* Port output data register (LED off)
	b0 B0 Pn0 output data bit
	b1 B1 Pn1 output data bit
	b2 B2 Pn2 output data bit
	b3 B3 Pn3 output data bit
	b4 B4 Pn4 output data bit
	b5 B5 Pn5 output data bit
	b6 B6 Pn6 output data bit
	b7 B7 Pn7 output data bit
	*/
	PORTA.PODR.BIT.B0	= 1;			/* LED1 PA1 */
	PORTA.PODR.BIT.B1	= 1;			/* LED2 PA2 */
	PORTA.PODR.BIT.B2	= 1;			/* LED3 PA3 */
	PORTA.PODR.BIT.B6	= 1;			/* LED4 PA6 */
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usbc_cpu_LedReinit
Description     : LED Initialize
Arguments       : none
Return value    : none
******************************************************************************/
void usbc_cpu_LedReinit(void)
{
	usbc_cpu_LedSet((uint8_t)0);

	/* Data direction register
	b0 B0 Pn0 in/out select bit
	b1 B1 Pn1 in/out select bit
	b2 B2 Pn2 in/out select bit
	b3 B3 Pn3 in/out select bit
	b4 B4 Pn4 in/out select bit
	b5 B5 Pn5 in/out select bit
	b6 B6 Pn6 in/out select bit
	b7 B7 Pn7 in/out select bit
	*/
	PORTA.PDR.BIT.B0	= 1;			/* LED1 PA1 */
	PORTA.PDR.BIT.B1	= 1;			/* LED2 PA2 */
	PORTA.PDR.BIT.B2	= 1;			/* LED3 PA3 */
	PORTA.PDR.BIT.B6	= 1;			/* LED4 PA6 */
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usbc_cpu_LedSet
Description     : LED Set process
Arguments       : uint8_t data          : LED Set data
Return value    : none
******************************************************************************/
void usbc_cpu_LedSet(uint8_t data)
{
	usbc_cpu_LedSetBit((uint8_t)1, (data & 0x01));	/* LED1 */
	usbc_cpu_LedSetBit((uint8_t)2, (data & 0x02));	/* LED2 */
	usbc_cpu_LedSetBit((uint8_t)3, (data & 0x04));	/* LED3 */
	usbc_cpu_LedSetBit((uint8_t)4, (data & 0x08));	/* LED4 */
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usbc_cpu_LedSetBit
Description     : LED Bit Set process
Arguments       : uint8_t bit          : Led Set Position
                : uint8_t data         : Led On/Off
Return value    : none
******************************************************************************/
void usbc_cpu_LedSetBit(uint8_t bit, uint8_t data)
{
	uint8_t	dat;

	dat = 0;									/* OFF */
	if( data != 0 )
	{
		dat = 1;								/* ON */
	}

	switch( bit )
	{
	/* Data register (LED off)
	b0 B0 Pn0 output data bit
	b1 B1 Pn1 output data bit
	b2 B2 Pn2 output data bit
	b3 B3 Pn3 output data bit
	b4 B4 Pn4 output data bit
	b5 B5 Pn5 output data bit
	b6 B6 Pn6 output data bit
	b7 B7 Pn7 output data bit
	*/
	case 1:	PORTA.PODR.BIT.B0	= dat;	break;	/* LED1 */
	case 2:	PORTA.PODR.BIT.B1	= dat;	break;	/* LED2 */
	case 3:	PORTA.PODR.BIT.B2	= dat;	break;	/* LED3 */
	case 4:	PORTA.PODR.BIT.B6	= dat;	break;	/* LED4 */
	default:						break;
	}
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usbc_cpu_LedLoop
Description     : Led All On & Loop process
Arguments       : none
Return value    : none
******************************************************************************/
void usbc_cpu_LedLoop(void)
{
	usbc_cpu_LedSet(0xff);		/* LED ALL ON */
	while( 1 );					/* Eternal loop */
}
/******************************************************************************
End of function
******************************************************************************/



/******************************************************************************
Interrupt function
******************************************************************************/

/******************************************************************************
Function Name   : usb_cstd_UsbIntHand
Description     : USB interrupt Handler
Arguments       : void
Return value    : void
******************************************************************************/
void usb_cstd_UsbIntHand(void)
{
/* Condition compilation by the difference of USB function */
#ifdef USB_FUNCSEL_PP
	usb_cstd_UsbHandler();		/* Call interrupt routine */
#endif /* USB_FUNCSEL_PP */
}
/******************************************************************************
End of function
******************************************************************************/

/******************************************************************************
Function Name   : usb_cstd_RestartDma
Description     : DMA Restart
Arguments       : none
Return value    : none
******************************************************************************/
void usb_cstd_RestartDma( void )
{
}
/******************************************************************************
End of function
******************************************************************************/

/******************************************************************************
Function Name   : usb_cpu_EnableDma
Description     : DTC(D0FIFO) interrupt enable (Interrupt priority 5 set)
Arguments       : none
Return value    : none
******************************************************************************/
void usb_cpu_EnableDma(void)
{
}
/******************************************************************************
End of function
******************************************************************************/

/******************************************************************************
Function Name   : usb_cpu_DisableDma
Description     : D0FIFO interrupt disable (Interrupt priority 0 set)
Arguments       : none
Return value    : none
******************************************************************************/
void usb_cpu_DisableDma(void)
{
}
/******************************************************************************
End of function
******************************************************************************/
/******************************************************************************
Function Name   : usb_cstd_D0fifoIntHand
Description     : D0FIFO interrupt Handler
Arguments       : none
Return value    : none
******************************************************************************/
void usb_cstd_D0fifoIntHand(void)
{
/* Condition compilation by the difference of USB function */
#ifdef USB_FUNCSEL_PP
	usb_cstd_DmaHandler();		/* Call interrupt routine */
#endif /* USB_FUNCSEL_PP */
}
/******************************************************************************
End of function
******************************************************************************/

/******************************************************************************
Function Name   : usb_cstd_D1fifoIntHand
Description     : D1FIFO interrupt Handler
Arguments       : none
Return value    : none
******************************************************************************/
void usb_cstd_D1fifoIntHand(void)
{
	/* Please add the processing for the system. */
}
/******************************************************************************
End of function
******************************************************************************/

/******************************************************************************
Function Name   : usb_cstd_UsbIntHandR
Description     : RESUME interrupt Handler
Arguments       : none
Return value    : none
******************************************************************************/
void usb_cstd_UsbIntHandR(void)
{
	/* Please add the processing for the system. */
}
/******************************************************************************
End of function
******************************************************************************/

/* Condition compilation by the difference of the operating system */
#if USBC_FW_PP == USBC_FW_NONOS_PP
/******************************************************************************
Private global variables and functions
******************************************************************************/
uint16_t usb_gcstd_D0fifo = 0;		/* D0fifo0 Interrupt Request enable */
uint16_t usb_gcstd_D1fifo = 0;		/* D1fifo0 Interrupt Request enable */

/******************************************************************************
Renesas Abstracted RSK functions
******************************************************************************/

/******************************************************************************
Function Name   : usb_cstd_IntEnable
Description     : USB Interrupt Enable
Arguments       : void
Return value    : void
******************************************************************************/
void usb_cstd_IntEnable(void)
{
/* Condition compilation by the difference of USB function */
#if	USB_FUNCSEL_PP == USBC_PERI_PP
	/* Interrupt enable register (USB0 USBIO enable)
	b0 IEN0 Interrupt enable bit
	b1 IEN1 Interrupt enable bit
	b2 IEN2 Interrupt enable bit
	b3 IEN3 Interrupt enable bit
	b4 IEN4 Interrupt enable bit
	b5 IEN5 Interrupt enable bit
	b6 IEN6 Interrupt enable bit
	b7 IEN7 Interrupt enable bit
	*/
	ICU.IER[4].BIT.IEN3		= 1;
	ICU.IER[4].BIT.IEN1		= usb_gcstd_D0fifo;
	ICU.IER[4].BIT.IEN2		= usb_gcstd_D1fifo;
#endif	/* USB_FUNCSEL_PP == USBC_PERI_PP */
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_IntDisable
Description     : USB Interrupt disable
Arguments       : void
Return value    : void
******************************************************************************/
void usb_cstd_IntDisable(void)
{
/* Condition compilation by the difference of USB function */
#if	USB_FUNCSEL_PP == USBC_PERI_PP
	/* Interrupt enable register (USB0 USBIO disable)
	b0 IEN0 Interrupt enable bit
	b1 IEN1 Interrupt enable bit
	b2 IEN2 Interrupt enable bit
	b3 IEN3 Interrupt enable bit
	b4 IEN4 Interrupt enable bit
	b5 IEN5 Interrupt enable bit
	b6 IEN6 Interrupt enable bit
	b7 IEN7 Interrupt enable bit
	*/
	ICU.IER[4].BIT.IEN3			= 0;
	usb_gcstd_D0fifo			= ICU.IER[4].BIT.IEN1;
	ICU.IER[4].BIT.IEN1			= 0;
	usb_gcstd_D1fifo			= ICU.IER[4].BIT.IEN2;
	ICU.IER[4].BIT.IEN2			= 0;
#endif	/* USB_FUNCSEL_PP == USBC_PERI_PP */
}
/******************************************************************************
End of function
******************************************************************************/
#endif /* USBC_FW_PP == USBC_FW_NONOS_PP */


/******************************************************************************
Function Name   : usbc_cpu_UsbintInit
Description     : USB interrupt Initialize
Arguments       : void
Return value    : void
******************************************************************************/
void usbc_cpu_UsbintInit(void)
{
	/* Deep standby USB monitor register
	b0      SRPC0    USB0 single end control
	b3-b1   Reserved 0
	b4      FIXPHY0  USB0 transceiver output fix
	b7-b5   Reserved 0
	b8      SRPC1    USB1 single end control
	b11-b9  Reserved 0
	b12     FIXPHY1  USB1 transceiver output fix
	b15-b13 Reserved 0
	b16     DP0      USB0 DP input
	b17     DM0      USB0 DM input
	b19-b18 Reserved 0
	b20     DOVCA0   USB0 OVRCURA input
	b21     DOVCB0   USB0 OVRCURB input
	b22     Reserved 0
	b23     DVBSTS0  USB1 VBUS input
	b24     DP1      USB1 DP input
	b25     DM1      USB1 DM input
	b27-b26 Reserved 0
	b28     DOVCA1   USB1 OVRCURA input
	b29     DOVCB1   USB1 OVRCURB input
	b30     Reserved 0
	b31     DVBSTS1  USB1 VBUS input
	*/
	USB.DPUSR0R.BIT.FIXPHY0	= 0u;	/* USB0 Transceiver Output fixed */

	/* Interrupt enable register
	b0 IEN0 Interrupt enable bit
	b1 IEN1 Interrupt enable bit
	b2 IEN2 Interrupt enable bit
	b3 IEN3 Interrupt enable bit
	b4 IEN4 Interrupt enable bit
	b5 IEN5 Interrupt enable bit
	b6 IEN6 Interrupt enable bit
	b7 IEN7 Interrupt enable bit
	*/
	ICU.IER[0x04].BIT.IEN1	= 0u;	/* D0FIFO0 disable */
	ICU.IER[0x04].BIT.IEN2	= 0u;	/* D1FIFO0 disable */
	ICU.IER[0x04].BIT.IEN3	= 1u;	/* USBI0 enable */
	ICU.IER[0x0B].BIT.IEN2	= 0u;	/* USBR0 disable */

	/* Interrupt priority register
	b3-b0 IPR      Interrupt priority
	b7-b4 Reserved 0
	*/
	ICU.IPR[33].BYTE	= 0x00;	/* D0FIFO0 */
	ICU.IPR[34].BYTE	= 0x00;	/* D1FIFO0 */
	ICU.IPR[35].BYTE	= 0x03;	/* USBI0 */
	ICU.IPR[90].BYTE	= 0x00;	/* USBR0 */

	usbc_cpu_FunctionUSB0IP();
}
/******************************************************************************
End of function
******************************************************************************/

/******************************************************************************
Function Name   : usbc_cpu_FunctionUSB0IP
Description     : USB0 port mode and Switch mode Initialize
Arguments       : none
Return value    : none
******************************************************************************/
void usbc_cpu_FunctionUSB0IP(void)
{
	/* Write protect register
	b5-b0 Reserved 0
	b6 PFSWE    PFS register write enable bit
	b7 BOWI     PFSWE bit write disable bit
	*/
	MPC.PWPR.BIT.B0WI	= 0u;
	MPC.PWPR.BIT.PFSWE	= 1u;

	
	/* Port mode register
	b0    B0       Pm0 port mode control bit
	b1    B1       Pm1 port mode control bit
	b2    B2       Pm2 port mode control bit
	b3    B3       Pm3 port mode control bit
	b4    B4       Pm4 port mode control bit
	b5    B5       Pm5 port mode control bit
	b6    B6       Pm6 port mode control bit
	b7    B7       Pm7 port mode control bit
	*/
	PORT1.PMR.BIT.B4	= 1u;
	PORT1.PMR.BIT.B6	= 1u;

	/* Pmm port function select register
	b0-b4 PSEL     port function select bit
	b5    Reserved 0
	b6    ISEL     Interrupt select bit
	b7    Reserved 0
	*/
	MPC.P14PFS.BYTE	= 0x11;	/* USB0_DPUPE */
	MPC.P16PFS.BYTE	= 0x11;	/* USB0_VBUS */

	/* USB0 control register
	b1-b0 Reserved 0
	b2    PUPHZS   PUPHZS select bit
	b3    PDHZS    PDHZS select bit
	b4-b7 Reserved 0
	*/
	MPC.PFUSB0.BIT.PUPHZS	= 1u;

	/* Write protect register
	b5-b0 Reserved 0
	b6 PFSWE    PFS register write enable bit
	b7 BOWI     PFSWE bit write disable bit
	*/
	MPC.PWPR.BIT.PFSWE	= 0u;
	MPC.PWPR.BIT.B0WI	= 1u;
}
/******************************************************************************
End of function
******************************************************************************/

/******************************************************************************
Function Name   : usbc_cpu_DmaintInit
Description     : DMA interrupt Initialize
Arguments       : void
Return value    : void
******************************************************************************/
void usbc_cpu_DmaintInit(void)
{
	/* Protect register
	b0    PRC0     Protect bit0
	b1    PRC1     Protect bit1
	b2    Reserved 0
	b3    PRC3     Protect bit3
	b7-b4 Reserved 0
	b15-b8 PRKEY   PRC Key code bit
	*/
	SYSTEM.PRCR.WORD	= 0xA503;	/* Protect off */

	/* Module stop control register (Enable DTC module)
	b3-b0   Reserved 0
	b4	    MSTPA4   8bit timer3,2 stop bit
	b5      MSTPA5   8bit timer1,0 stop bit
	b8-b6   Reserved 0
	b9      MSTPA9   Multifunction timer unit0 stop bit
	b10     MSTPA10  Programmable pulse unit1 stop bit
	b11     MSTPA11  Programmable pulse unit0 stop bit
	b12     MSTPA12  16Bit timer pulse unit1 stop bit
	b13     MSTPA13  16Bit timer pulse unit0 stop bit
	b14     MSTPA14  Compare timer unit1 stop bit
	b15     MSTPA15  Compare timer unit0 stop bit
	b16     Reserved 0
	b17     MSTPA17  12bit AD stop bit
	b18     Reserved 0
	b19     MSTPA19  DA stop bit
	b22-b20 Reserved 0
	b23     MSTPA23  10bit AD unit0 stop bit
	b24     MSTPA24  Module stop A24 set bit
	b26-b25 Reserved 0
	b27     MSTPA27  Module stop A27 set bit
	b28     MSTPA28  DMA/DTC stop bit
	b29     MSTPA29  Module stop A29 set bit
	b30     Reserved 0
	b31     ACSE     All clock stop bit
	*/
	SYSTEM.MSTPCRA.BIT.MSTPA28	= 0;

	/* Protect register
	b0    PRC0     Protect bit0
	b1    PRC1     Protect bit1
	b2    Reserved 0
	b3    PRC3     Protect bit3
	b7-b4 Reserved 0
	b15-b8 PRKEY   PRC Key code bit
	*/
	SYSTEM.PRCR.WORD	= 0xA500;	/* Protect on */

	/* DTC vector register
	b21-b0 DTCVBR Vector table address
	*/
	DTC.DTCVBR					= (unsigned long)&usb_dtcctable;
}
/******************************************************************************
End of function
******************************************************************************/



/******************************************************************************
DMA & DTC function
******************************************************************************/

/******************************************************************************
Function Name   : usb_cstd_D0fifo2BufStartDma
Description     : FIFO to Buffer data read DMA start
Arguments       : uint32_t SourceAddr	; Source address
Return value    : void
******************************************************************************/
void usb_cstd_D0fifo2BufStartDma(uint32_t SourceAddr)
{
}
/******************************************************************************
End of function
******************************************************************************/

/******************************************************************************
Function Name   : usb_cstd_Buf2D0fifoStartDma
Description     : Buffer to FIFO data write DMA start
Arguments       : uint32_t DistAddr		; Destination address
Return value    : void
******************************************************************************/
void usb_cstd_Buf2D0fifoStartDma(uint32_t DistAdr)
{
}
/******************************************************************************
End of function
******************************************************************************/

/******************************************************************************
Function Name   : usb_cstd_StopDma
Description     : DMA stop
Arguments       : void
Return value    : void
******************************************************************************/
void usb_cstd_StopDma(void)
{
}
/******************************************************************************
End of function
******************************************************************************/

/******************************************************************************
TIMER function
******************************************************************************/

/******************************************************************************
Function Name   : timer_Initialize
Description     : Timer interrupt Initialize
Arguments       : none
Return value    : none
******************************************************************************/
void timer_Initialize(void)
{
	MSTP_CMT0 = 0u;		// Module stopを解除する
	
	//
	//	カウンタ設定
	//
	CMT.CMSTR0.BIT.STR0	= 0;
	CMT0.CMCOR			= 48000000ul / 8 / 1000;	// 1ms周期(PCLK=12MHz x16/4)
	CMT0.CMCR.WORD		= 0x80 /*rsv*/ | 0x40 /*CMIE*/ | 0x00 /*PCLK/8*/;
	CMT.CMSTR0.BIT.STR0	= 1;
	
	//
	//	タイマーカウント開始
	//
	ICU.IPR[4].BYTE			= 10;	// 優先度
	ICU.IER[0x03].BIT.IEN4	= 1;	// CMT0 interrupt
}
/******************************************************************************
End of function
******************************************************************************/

/******************************************************************************
Function Name   : timer_IntHandler(CMT0)
Description     : Timer interrupt Handler at 1ms period.
Arguments       : none
Return value    : none
******************************************************************************/
void timer_IntHandler(void)
{
	timer_nCount_ms ++;
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : timer_GetTimeCount
Description     : Get time counter.
Arguments       : none
Return value    : uint32_t		: Time counter
******************************************************************************/
uint32_t timer_GetTimeCount(void)
{
	const uint32_t	t = timer_nCount_ms;
	return (t == 0) ? 1 : t;	// time=0は都合が悪い場合があるのでちょっとずらす
}
/******************************************************************************
End of function
******************************************************************************/

/******************************************************************************
Function Name   : timer_GetPastTime
Description     : Get past time
Arguments       : uint32_t timeLast	: Last time count
				:
Return value    : uint32_t			: Time [ms]
******************************************************************************/
uint32_t timer_GetPastTime( const uint32_t timeLast )
{
	return timer_nCount_ms - timeLast;
}
/******************************************************************************
End of function
******************************************************************************/



/******************************************************************************
Function Name   : usbc_cpu_Delay1us
Description     : 1us Delay timer
Arguments       : uint16_t	time		; Delay time(*1us)
Return value    : none
Note            : Please change for your MCU
******************************************************************************/
void usbc_cpu_Delay1us(uint16_t time)
{
	volatile register uint16_t	i, xx;

	/* Wait 1us (Please change for your MCU) */
	for( i = 0; i < (5 * time); ++i )
	{
		xx = i;
	};
}
/******************************************************************************
End of function
******************************************************************************/

/******************************************************************************
Function Name   : usbc_cpu_DelayXms
Description     : xms Delay timer
Arguments       : uint16_t	time		; Delay time(*1ms)
Return value    : void
Note            : Please change for your MCU
******************************************************************************/
void usbc_cpu_DelayXms(uint16_t time)
{
	/* Wait xms (Please change for your MCU) */
/* Condition compilation by the difference of the operating system */
#if USBC_FW_PP == USBC_FW_NONOS_PP
	volatile register uint32_t	i;

	/* Wait 1ms */
	for( i = 0; i < (6400 * time); ++i )
	{
	};
	i = 0;
		/* When "ICLK=48MHz" is set, this code is waiting for 1ms.
		  Please change this code with CPU Clock mode. */

#else	/* USBC_FW_PP == USBC_FW_NONOS_PP */
	USBC_DLY_TSK(time);
#endif	/* USBC_FW_PP == USBC_FW_NONOS_PP */
}
/******************************************************************************
End of function
******************************************************************************/

/******************************************************************************
KEY function
******************************************************************************/

/******************************************************************************
Function Name   : usbc_cpu_KeyInit
Description     : Input key port initialize
Arguments       : none
Return value    : none
******************************************************************************/
void usbc_cpu_KeyInit(void)
{
	/* Write protect register
	b5-b0 Reserved 0
	b6 PFSWE    PFS register write enable bit
	b7 BOWI     PFSWE bit write disable bit
	*/
	MPC.PWPR.BIT.B0WI	= 0u;
	MPC.PWPR.BIT.PFSWE	= 1u;

	/* Port Pin function select register  
	b4-b0 PSEL  Pin function select bit
	b5 Reserved 0
	b6 ISEL     Interrupt input function select bit
	b7 Reserved 0
	*/
	MPC.P32PFS.BIT.ISEL = 1;	/* IRQ2 use (SW1) */
	MPC.P00PFS.BIT.ISEL = 1;	/* IRQ8 use (SW2) */

	/* Write protect register
	b5-b0 Reserved 0
	b6 PFSWE    PFS register write enable bit
	b7 BOWI     PFSWE bit write disable bit
	*/
	MPC.PWPR.BIT.PFSWE	= 0u;
	MPC.PWPR.BIT.B0WI	= 1u;
}
/******************************************************************************
End of function
******************************************************************************/

/******************************************************************************
Function Name   : usbc_cpu_KeyWait
Description     : wait input key
Arguments       : none
Return value    : uint16_t              : key_no
Note            : Please change for your MCU
******************************************************************************/
uint16_t usbc_cpu_KeyWait(void)
{
	uint16_t		key_data;

	USBC_PRINTF0("\n--- Please push any main key ---");
	while( 1 )
	{
		/* 1ms wait */
		usbc_cpu_DelayXms(1);
		key_data = usbc_cpu_GetKeyNo();		/* SW */
		if( key_data != 0 )
		{
			break;
		}
	}
	USBC_PRINTF0("\n");
	return key_data;	/* SW no */
}
/******************************************************************************
End of function
******************************************************************************/

#if 1	// DEMO_PRG
/******************************************************************************
Function Name   : usbc_cpu_KeyScan
Description     : input key port
Arguments       : none
Return value    : uint16_t              : key_no
******************************************************************************/
uint16_t usbc_cpu_KeyScan(void)
{
	/* Port input register
	b0 B0 Pn0 bit
	b1 B1 Pn1 bit
	b2 B2 Pn2 bit
	b3 B3 Pn3 bit
	b4 B4 Pn4 bit
	b5 B5 Pn5 bit
	b6 B6 Pn6 bit
	b7 B7 Pn7 bit
	*/
	if( PORTA.PIDR.BIT.B7 == 0 )
	{
		return (uint16_t)USBC_ON;		// SW2 ON
	}
	return (uint16_t)USBC_OFF;			// SW2 OFF
}
/******************************************************************************
End of function
******************************************************************************/
#endif

/******************************************************************************
Function Name   : usbc_cpu_GetKeyNo
Description     : input key port
Arguments       : none
Return value    : uint16_t              : key_no
******************************************************************************/
uint16_t usbc_cpu_GetKeyNo(void)
{
#if 1	// DEMO_PRG
	uint8_t	i;

	for( i = 0; i < 6; ++i )
	{
		if( usbc_cpu_KeyScan() == USBC_OFF )
			return USBC_OFF;

		usbc_cpu_DelayXms(50);		/* 100ms wait */
	}
	return USBC_ON;

#else	// DEMO_PRG
	uint8_t		key_buf, key_buf1;

	key_buf = 0;

	if( PORTA.PIDR.BIT.B7 == 0 )
	{
		key_buf |= 0x04;	/* SW2 */
	}

	if( key_buf != 0 )
	{
		do
		{
			key_buf1 = 0;	/* Port read */
			
			/* Port register
			b0 B0 Pn0 bit
			b1 B1 Pn1 bit
			b2 B2 Pn2 bit
			b3 B3 Pn3 bit
			b4 B4 Pn4 bit
			b5 B5 Pn5 bit
			b6 B6 Pn6 bit
			b7 B7 Pn7 bit
			*/
			if( PORTA.PIDR.BIT.B7 == 0 )
			{
				key_buf1 |= 0x04;	/* SW2 */
			}

			usbc_cpu_DelayXms(100);	/* 100ms wait */

			key_buf = 0;	/* Port read */

			/* Port register
			b0 B0 Pn0 bit
			b1 B1 Pn1 bit
			b2 B2 Pn2 bit
			b3 B3 Pn3 bit
			b4 B4 Pn4 bit
			b5 B5 Pn5 bit
			b6 B6 Pn6 bit
			b7 B7 Pn7 bit
			*/
			if( PORTA.PIDR.BIT.B7 == 0 )
			{
				key_buf |= 0x04;	/* SW3 */
			}
		}
		while( key_buf != 0 );
		return (uint16_t)key_buf1;	/* Port data */
	}
	else
	{
		return (uint16_t)0;
	}
#endif	// DEMO_PRG
}
/******************************************************************************
End of function
******************************************************************************/

/******************************************************************************
Function Name   : usbc_cpu_AcquisitionKeyNo
Description     : input key data
Arguments       : none
Return value    : none
Note            : Please change for your MCU
******************************************************************************/
void usbc_cpu_AcquisitionKeyNo(void)
{
	usb_gcstd_KeyTime[0]++;
	switch(usb_gcstd_KeyTime[0])
	{
	case	10:
		usb_gcstd_KeyTime[0] = 0;	/* Initialized counter */
		usb_gcstd_KeyBuff[0] = usb_gcstd_KeyBuff[1];	/* Backup key data & input key data */

		usb_gcstd_KeyBuff[1] = 0;	/* Port read */
	
		/* Port register
		b0 B0 Pn0 bit
		b1 B1 Pn1 bit
		b2 B2 Pn2 bit
		b3 B3 Pn3 bit
		b4 B4 Pn4 bit
		b5 B5 Pn5 bit
		b6 B6 Pn6 bit
		b7 B7 Pn7 bit
		*/
		if( PORT3.PIDR.BIT.B2 == 0)
		{
			usb_gcstd_KeyBuff[1] |= 0x01;	/* SW1 */
		}
		if( PORT0.PIDR.BIT.B0 == 0 )
		{
			usb_gcstd_KeyBuff[1] |= 0x02;	/* SW2 */
		}
		if( PORT0.PIDR.BIT.B7 == 0 )
		{
			usb_gcstd_KeyBuff[1] |= 0x04;	/* SW3 */
		}

		usb_gcstd_KeyBuff[2] = usb_gcstd_KeyBuff[3];	/* Backup key code & initialized key code*/
		usb_gcstd_KeyBuff[3] = 0;
		if( usb_gcstd_KeyBuff[1] != 0 )
		{
			/* Now press key */
			if(usb_gcstd_KeyBuff[0] == usb_gcstd_KeyBuff[1])
			{
				usb_gcstd_KeyTime[1]++;	/* To keep pushing the same key */
			}
			else
			{
				usb_gcstd_KeyTime[1]	= 0;	/* Another key was pushed. */
			}
			if( usb_gcstd_KeyTime[1] > 10 )
			{
				usb_gcstd_KeyBuff[3] = usb_gcstd_KeyBuff[0];	/* Encoding of key data */
			}
		}
		break;
	default:
		usb_gcstd_KeyBuff[2] = usb_gcstd_KeyBuff[3];	/* Backup key code & initialized key code*/
		break;
	}
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usbc_cpu_KeepKeyNo
Description     : input key port
Arguments       : none
Return value    : uint8_t              : key_no
******************************************************************************/
uint8_t usbc_cpu_KeepKeyNo(void)
{
	return	usb_gcstd_KeyBuff[3];
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usbc_cpu_SingleKeyNo
Description     : input key port
Arguments       : none
Return value    : uint8_t              : key_no
******************************************************************************/
uint8_t usbc_cpu_SingleKeyNo(void)
{
	if( usb_gcstd_KeyBuff[2] == usb_gcstd_KeyBuff[3] )
	{
		return	(uint8_t)0;
	}
	else
	{
		return	usb_gcstd_KeyBuff[3];
	}
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
SLEEP & STANDBY control function
******************************************************************************/

/******************************************************************************
Function Name   : usbc_cpu_GoLpwrSleep
Description     : Go low power sleep mode
Arguments       : none
Return value    : none
******************************************************************************/
void usbc_cpu_GoLpwrSleep(void)
{
/* Condition compilation by the difference of the operating system */
 #if USBC_FW_PP == USBC_FW_NONOS_PP
/* Condition compilation by the difference of the devices */
  #if (USBC_LPWR_MODE_PP == USBC_LPWR_USE_PP)
	if( usb_gcpu_RemoteProcess == USBC_OFF )
	{
		if( usb_gpstd_RemoteWakeup == USBC_YES )
		{
			/* IRQ8(RX630-RSK_SW1) for Remote wakeup */
			usbc_cpu_IRQ2_Enable();
		}

		/* Standby control register (Software Standby disable)
		b7-b0  Reserved 0
		b12-b8 STS      Standby timer select bit
		b13    Reserved 0
		b14    OPE      Output port enable bit
		b15    SSBY     Software standby bit
		*/
		SYSTEM.SBYCR.BIT.SSBY		= 0;

		/* Goto WAIT */
		usbc_cpu_GoWait();

		if( usb_gpstd_RemoteWakeup == USBC_YES )
		{
			usb_gcpu_RemoteProcess = USBC_ON;
		}
	}
  #endif /* (USBC_LPWR_MODE_PP == USBC_LPWR_USE_PP) */
 #else /* USBC_FW_PP != USBC_FW_NONOS_PP */
	if( usb_gpstd_RemoteWakeup == USBC_YES )
	{
		/* IRQ8(RX630-RSK_SW1) for Remote wakeup */
		usbc_cpu_IRQ2_Enable();
	}

	/* Standby control register (Software Standby disable)
	b7-b0  Reserved 0
	b12-b8 STS      Standby timer select bit
	b13    Reserved 0
	b14    OPE      Output port enable bit
	b15    SSBY     Software standby bit
	*/
	SYSTEM.SBYCR.BIT.SSBY		= 0;

	/* Goto WAIT */
	usbc_cpu_GoWait();
 #endif /* USBC_FW_PP != USBC_FW_NONOS_PP */
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usbc_cpu_GoLpwrDeepStby
Description     : Go Low Power Deep Standby
Arguments       : none
Return value    : none
******************************************************************************/
void usbc_cpu_GoLpwrDeepStby(void)
{
	/* DTC Transfer disable
	b0    DTCST    DTC module start bit
	b7-b1 Reserved 0
	*/
	DTC.DTCST.BIT.DTCST		= 0;

	/* Sleep mode return main clock select register (Main clock OSC select)
	b2-b0   RSTCKSEL Sleep mode return main clock source select bit
	b6-b3   Reserved 0
	b7      RSTCKEN  Sleep mode return main clock source change enable bit
	*/
	SYSTEM.RSTCKCR.BIT.RSTCKSEL		= 2;

	/* Sleep mode return main clock select register (Source change enable)
	b2-b0   RSTCKSEL Sleep mode return main clock source select bit
	b6-b3   Reserved 0
	b7      RSTCKEN  Sleep mode return main clock source change enable bit
	*/
	SYSTEM.RSTCKCR.BIT.RSTCKEN		= 1;

	/* RTC control register3 (Sub clock OSC input disable)
	b0      RTCEN    Sub clock OSC input enable/disable
	b7-b1   Reserved 0
	*/
	RTC.RCR3.BIT.RTCEN				= 0;

	/* Sub clock Oscillator control register (SOSTP=Sub clock enable)
	b0      SOSTP    Sub clock Oscillator stop bit
	b7-b1   Reserved 0
	*/
	SYSTEM.SOSCCR.BIT.SOSTP			= 0;

	/* System clock control register3(CKSEL(10-8)=Sub clock)
	b7-b0   Reserved 0
	b10-b8  CKSEL    Clock source select bit
	b15-b11 Reserved 0
	*/
	SYSTEM.SCKCR3.BIT.CKSEL			= 3;

	/* Go Deep Standby Mode */
	usbc_cpu_GoDeepStbyMode();

	/* Protect register (protect off)
	b0    PRC0     Protect bit0
	b1    PRC1     Protect bit1
	b2    Reserved 0
	b3    PRC3     Protect bit3
	b7-b4 Reserved 0
	b15-b8 PRKEY   PRC Key code bit
	*/
	SYSTEM.PRCR.WORD		= 0xA503;	/* protect off */
	(*(volatile unsigned char  *)0x0008C28E)	= 0;;

	/* Goto WAIT */
	usbc_cpu_GoWait();
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usbc_cpu_GoDeepStbyMode
Description     : Go Deep Standby Mode
Arguments       : none
Return value    : none
******************************************************************************/
void usbc_cpu_GoDeepStbyMode(void)
{
/***************************************/
/*  Can not use DTC in Standby Mode !! */
/***************************************/
	/* Standby control register (Software Standby Shift)
	b7-b0  Reserved 0
	b12-b8 STS      Standby timer select bit
	b13    Reserved 0
	b14    OPE      Output port enable bit
	b15    SSBY     Software standby bit
	*/
	SYSTEM.SBYCR.BIT.SSBY		= 1;

	/* Deep Standby control register (Deep Standby Shift)
	b0-b1 DEEPCUT  RAM off bit
	b5-b2 Reserved 0
	b6    IOKEEP   IO port keep bit
	b7    DPSBY    Deep standby bit
	*/
	SYSTEM.DPSBYCR.BIT.DPSBY	= 1;

	/* Deep Standby control register (Internal RAM0, USB Power supply on)
	b0-b1 DEEPCUT  RAM off bit
	b5-b2 Reserved 0
	b6    IOKEEP   IO port keep bit
	b7    DPSBY    Deep standby bit
	*/
	SYSTEM.DPSBYCR.BIT.DEEPCUT	= 0;

	/* Standby control register (Bus Signal Output status keep)
	b7-b0  Reserved 0
	b12-b8 STS      Standby timer select bit
	b13    Reserved 0
	b14    OPE      Output port enable bit
	b15    SSBY     Software standby bit
	*/
	SYSTEM.SBYCR.BIT.OPE		= 1;

	/* Deep Standby control register (IO status keep by the return)
	b0-b1 DEEPCUT  RAM off bit
	b5-b2 Reserved 0
	b6    IOKEEP   IO port keep bit
	b7    DPSBY    Deep standby bit
	*/
	SYSTEM.DPSBYCR.BIT.IOKEEP	= 1;

	/* Deep standby interrupt edge register (Down Edge Clear)
	b0    DLVD1EG  LVD1 edge select bit
	b1    DLVD2EG  LVD2 edge select bit
	b3-2  Reserved 0
	b4    DNMIEG   NMI edge select bit
	b5    DRIICDEG SDA2-DS edge select bit
	b6    DRIICCEG SCA2-DS edge select bit
	b7    Reserved 0
	*/
	SYSTEM.DPSIEGR2.BYTE			= 0;

	/* LDV,DRT,NMI,RIIC disable & USB enable */
	/* Deep standby interrupt enable register (Cancel by USB enable)
	b0 DLVD1IE  LVD1 deep standby cancel enable bit
	b1 DLVD2IE  LVD2 deep standby cancel enable bit
	b2 DRTCIIE  RTC deep standby cancel enable bit
	b3 DRTCAIE  RTC periodic interrupt deep standby cancel enable bit
	b4 DNMIE    NMI deep standby cancel enable bit
	b5 DRIICDIE SDA2-DS deep standby cancel enable bit
	b6 DRIICCIE SCL2-DS deep standby cancel enable bit
	b7 DUSBIE   USB suspend/resume deep standby cancel enable bit
	*/
	SYSTEM.DPSIER2.BYTE			= 0x80;

	/* IRQ0-7 interrupt clear */
	/* Deep standby interrupt flag register2 (IRQ0-7 Request clear)
	b0 DIRQ0F  IRQ0 deep standby cancel flag
	b1 DIRQ1F  IRQ1 deep standby cancel flag
	b2 DIRQ2F  IRQ2 deep standby cancel flag
	b3 DIRQ3F  IRQ3 deep standby cancel flag
	b4 DIRQ4F  IRQ4 deep standby cancel flag
	b5 DIRQ5F  IRQ5 deep standby cancel flag
	b6 DIRQ6F  IRQ6 deep standby cancel flag
	b7 DIRQ7F  IRQ7 deep standby cancel flag
	*/
	SYSTEM.DPSIFR0.BYTE			= 0;

	/* IRQ8-15 deep standby cancel request clear */
	/* Deep standby interrupt flag register2 (IRQ8-15 Request clear)
	b0 DIRQ8F   IRQ8 deep standby cancel flag
	b1 DIRQ9F   IRQ9 deep standby cancel flag
	b2 DIRQ10F  IRQ10 deep standby cancel flag
	b3 DIRQ11F  IRQ11 deep standby cancel flag
	b4 DIRQ12F  IRQ12 deep standby cancel flag
	b5 DIRQ13F  IRQ13 deep standby cancel flag
	b6 DIRQ14F  IRQ14 deep standby cancel flag
	b7 DIRQ15F  IRQ15 deep standby cancel flag
	*/
	SYSTEM.DPSIFR1.BYTE			= 0;

	/* LDV,DRT,NMI,RIIC & USB interrupt clear */
	/* Deep standby interrupt flag register2 (Cancel by USB Request clear)
	b0 DLVD1IF LVD1 deep standby cancel flag
	b1 DLVD2IF LVD2 deep standby cancel flag
	b2 DRTCIIF RTC Cycle interrupt deep standby cancel flag
	b3 DRTCAIF RTC Alarm interrupt deep standby cancel flag
	b4 DNMIF   NMI deep standby cancel flag
	b5 DRIICDIF  SDA2-DS deep standby cancel flag
	b6 DRIICCIF  SCL2-DS deep standby cancel flag
	b7 DUSBIF  USB suspend/resume deep standby cancel flag
	*/
	SYSTEM.DPSIFR2.BYTE			= 0;

	/* CAN interrupt clear */
	/* Deep standby interrupt flag register3 (CRX1-DS Request clear)
	b0 DCANIF CRX1-DS deep standby cancel flag
	b7-b1     Reserved 0
	*/
	SYSTEM.DPSIFR3.BYTE			= 0;

	/* Oscillator stop detect control register (disable)
	b0     OSTDIE   Oscillator stop detect interrupt enable
	b1-b6  Reserved 0
	b7     OSTDE    Oscillator stop detect enable
	*/
	SYSTEM.OSTDCR.BYTE			= 0x00;

	/* Go Deep Standby Mode 0 */
	usbc_cpu_GoDeepStbyMode0();
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usbc_cpu_GoDeepStbyMode0
Description     : Go Deep Standby Mode 0
Arguments       : none
Return value    : none
******************************************************************************/
void usbc_cpu_GoDeepStbyMode0(void)
{
	/* Deep standby USB monitor register (Single end Receiver Control)
	b0      SRPC0    USB0 single end control
	b3-b1   Reserved 0
	b4      FIXPHY0  USB0 transceiver output fix
	b7-b5   Reserved 0
	b8      SRPC1    USB1 single end control
	b11-b9  Reserved 0
	b12     FIXPHY1  USB1 transceiver output fix
	b15-b13 Reserved 0
	b16     DP0      USB0 DP input
	b17     DM0      USB0 DM input
	b19-b18 Reserved 0
	b20     DOVCA0   USB0 OVRCURA input
	b21     DOVCB0   USB0 OVRCURB input
	b22     Reserved 0
	b23     DVBSTS0  USB1 VBUS input
	b24     DP1      USB1 DP input
	b25     DM1      USB1 DM input
	b27-b26 Reserved 0
	b28     DOVCA1   USB1 OVRCURA input
	b29     DOVCB1   USB1 OVRCURB input
	b30     Reserved 0
	b31     DVBSTS1  USB1 VBUS input
	*/
	USB.DPUSR0R.BIT.SRPC0		= 1;

	/* Deep standby USB monitor register (Transceiver Output fixed)
	b0      SRPC0    USB0 single end control
	b3-b1   Reserved 0
	b4      FIXPHY0  USB0 transceiver output fix
	b7-b5   Reserved 0
	b8      SRPC1    USB1 single end control
	b11-b9  Reserved 0
	b12     FIXPHY1  USB1 transceiver output fix
	b15-b13 Reserved 0
	b16     DP0      USB0 DP input
	b17     DM0      USB0 DM input
	b19-b18 Reserved 0
	b20     DOVCA0   USB0 OVRCURA input
	b21     DOVCB0   USB0 OVRCURB input
	b22     Reserved 0
	b23     DVBSTS0  USB1 VBUS input
	b24     DP1      USB1 DP input
	b25     DM1      USB1 DM input
	b27-b26 Reserved 0
	b28     DOVCA1   USB1 OVRCURA input
	b29     DOVCB1   USB1 OVRCURB input
	b30     Reserved 0
	b31     DVBSTS1  USB1 VBUS input
	*/
	USB.DPUSR0R.BIT.FIXPHY0		= 1;

/* Condition compilation by the difference of USB function */
#if USB_FUNCSEL_PP == USBC_PERI_PP
	/* Deep standby USB interrupt register(VBUS clear)
	b0      DPINTE0   USB0 DP interrupt enable clear bit
	b1      DMINTE0   USB0 DM interrupt enable clear bit
	b3-b2   Reserved  0
	b4      DOVRCRAE0 USB0 OVRCURA interrupt enable clear bit
	b5      DOVRCRBE0 USB0 OVRCURB interrupt enable clear bit
	b6      Reserved  0
	b7      DVBSE0    USB0 VBUS interrupt enable clear bit
	b8      DPINTE1   USB1 DP interrupt enable clear bit
	b9      DMINTE1   USB1 DM interrupt enable clear bit
	b11-b10 Reserved  0
	b12     DOVRCRAE1 USB1 OVRCURA interrupt enable clear bit
	b13     OVRCRBE1  USB1 OVRCURB interrupt enable clear bit
	b14     Reserved  0
	b15     DVBSE1    USB1 VBUS interrupt enable clear bit
	*/
	USB.DPUSR1R.BIT.DVBSE0		= 0;

	/* Deep standby USB interrupt register(VBUS set)
	b0      DPINTE0   USB0 DP interrupt enable clear bit
	b1      DMINTE0   USB0 DM interrupt enable clear bit
	b3-b2   Reserved  0
	b4      DOVRCRAE0 USB0 OVRCURA interrupt enable clear bit
	b5      DOVRCRBE0 USB0 OVRCURB interrupt enable clear bit
	b6      Reserved  0
	b7      DVBSE0    USB0 VBUS interrupt enable clear bit
	b8      DPINTE1   USB1 DP interrupt enable clear bit
	b9      DMINTE1   USB1 DM interrupt enable clear bit
	b11-b10 Reserved  0
	b12     DOVRCRAE1 USB1 OVRCURA interrupt enable clear bit
	b13     OVRCRBE1  USB1 OVRCURB interrupt enable clear bit
	b14     Reserved  0
	b15     DVBSE1    USB1 VBUS interrupt enable clear bit
	*/
	USB.DPUSR1R.BIT.DVBSE0		= 1;
#endif /* USBC_PERI_PP */

	/* Deep standby USB interrupt register(DP clear)
	b0      DPINTE0   USB0 DP interrupt enable clear bit
	b1      DMINTE0   USB0 DM interrupt enable clear bit
	b3-b2   Reserved  0
	b4      DOVRCRAE0 USB0 OVRCURA interrupt enable clear bit
	b5      DOVRCRBE0 USB0 OVRCURB interrupt enable clear bit
	b6      Reserved  0
	b7      DVBSE0    USB0 VBUS interrupt enable clear bit
	b8      DPINTE1   USB1 DP interrupt enable clear bit
	b9      DMINTE1   USB1 DM interrupt enable clear bit
	b11-b10 Reserved  0
	b12     DOVRCRAE1 USB1 OVRCURA interrupt enable clear bit
	b13     OVRCRBE1  USB1 OVRCURB interrupt enable clear bit
	b14     Reserved  0
	b15     DVBSE1    USB1 VBUS interrupt enable clear bit
	*/
	USB.DPUSR1R.BIT.DPINTE0		= 0;

	/* Deep standby USB interrupt register(DP set)
	b0      DPINTE0   USB0 DP interrupt enable clear bit
	b1      DMINTE0   USB0 DM interrupt enable clear bit
	b3-b2   Reserved  0
	b4      DOVRCRAE0 USB0 OVRCURA interrupt enable clear bit
	b5      DOVRCRBE0 USB0 OVRCURB interrupt enable clear bit
	b6      Reserved  0
	b7      DVBSE0    USB0 VBUS interrupt enable clear bit
	b8      DPINTE1   USB1 DP interrupt enable clear bit
	b9      DMINTE1   USB1 DM interrupt enable clear bit
	b11-b10 Reserved  0
	b12     DOVRCRAE1 USB1 OVRCURA interrupt enable clear bit
	b13     OVRCRBE1  USB1 OVRCURB interrupt enable clear bit
	b14     Reserved  0
	b15     DVBSE1    USB1 VBUS interrupt enable clear bit
	*/
	USB.DPUSR1R.BIT.DPINTE0		= 1;
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cpu_RegRecovEnable
Description     : Register Recover Enable
Arguments       : none
Return value    : none
******************************************************************************/
void usb_cpu_RegRecovEnable(void)
{
	/* Device state change register
	b14-b0 Reserved 0
	b15    DVCHG    Device state change bit
	*/
	USB0.DVCHGR.BIT.DVCHG = 1;
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cpu_RegRecovDisable
Description     : Register Recover Disable
Arguments       : none
Return value    : none
******************************************************************************/
void usb_cpu_RegRecovDisable(void)
{
	/* Device state change register
	b14-b0 Reserved 0
	b15    DVCHG    Device state change bit
	*/
	USB0.DVCHGR.BIT.DVCHG = 0;
}
/******************************************************************************
End of function
******************************************************************************/

/******************************************************************************
Function Name   : usbc_cpu_GoWait
Description     : Go Wait
Arguments       : none
Return value    : none
******************************************************************************/
void usbc_cpu_GoWait(void)
{
	int_exception(11);				/* Goto usbc_cpu_GoWait_Int() */
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usbc_cpu_GoWait_Int
Description     : Go Wait Interrupt
Arguments       : none
Return value    : none
******************************************************************************/
void usbc_cpu_GoWait_Int(void)
{
	wait();							/* Wait */
}
/******************************************************************************
End of function
******************************************************************************/

/******************************************************************************
Function Name   : usbc_cpu_GetDPSRSTF
Description     : Get Deep Software Standby Reset Flag
Arguments       : none
Return value    : Deep Software Standby Reset Flag
******************************************************************************/
uint8_t usbc_cpu_GetDPSRSTF(void)
{
	/* Reset status register (Deep software standby reset check)
	b0    POFR     Power on reset Detection flag
	b1    LVD0F    Voltage Watch 0 reset detection flag
	b2    LVD1F    Voltage Watch 1 reset detection flag
	b3    LVD2F    Voltage Watch 2 reset detection flag
	b6-b4 Reserved 0
	b7    DPSRSTF  Deep software standby reset flag
	*/
	return (uint8_t)SYSTEM.RSTSR0.BIT.DPSRSTF;
}
/******************************************************************************
End of function
******************************************************************************/

/******************************************************************************
IRQ function
******************************************************************************/

/******************************************************************************
Function Name   : usbc_cpu_IRQ2_Enable
Description     : IRQ8 Interrupt Enable
Arguments       : none
Return value    : none
******************************************************************************/
void usbc_cpu_IRQ2_Enable(void)
{
/* Condition compilation by the difference of USB function */
#if USB_FUNCSEL_PP == USBC_PERI_PP
	if( usb_gpstd_RemoteWakeup == USBC_YES )
	{
		/* Interrupt enable register (IEN0 disable))
		b0 IEN0 Interrupt enable bit
		b1 IEN1 Interrupt enable bit
		b2 IEN2 Interrupt enable bit
		b3 IEN3 Interrupt enable bit
		b4 IEN4 Interrupt enable bit
		b5 IEN5 Interrupt enable bit
		b6 IEN6 Interrupt enable bit
		b7 IEN7 Interrupt enable bit
		*/
		ICU.IER[8].BIT.IEN2		= 0u;

		/* IRQ control register
		b1-b0 Reserved 0
		b3-b2 IRQMD    IRQ detect bit
		b7-b4 Reserved 0
		*/
		ICU.IRQCR[2].BIT.IRQMD	= 1u;	/* Down Edge Set */

		/* Priority Resume0=B
		b3-b0 IPR      Interrupt priority
		b7-b4 Reserved 0
		*/
		ICU.IPR[66].BYTE		= 0x0B;

		/* Interrupt request register
		b0    IR       Interrupt status flag
		b7-b1 Reserved 0
		*/
		ICU.IR[66].BIT.IR		= 0u;

		/* Interrupt enable register (IEN0 enable)
		b0 IEN0 Interrupt enable bit
		b1 IEN1 Interrupt enable bit
		b2 IEN2 Interrupt enable bit
		b3 IEN3 Interrupt enable bit
		b4 IEN4 Interrupt enable bit
		b5 IEN5 Interrupt enable bit
		b6 IEN6 Interrupt enable bit
		b7 IEN7 Interrupt enable bit
		*/
		ICU.IER[8].BIT.IEN2		= 1u;
	}
#endif /* USB_FUNCSEL_PP == USBC_PERI_PP */
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usbc_cpu_IRQ2_DisEnable
Description     : IRQ8 Interrupt Disable
Arguments       : none
Return value    : none
******************************************************************************/
void usbc_cpu_IRQ2_DisEnable(void)
{
	/* Interrupt request register
	b0    IR       Interrupt status flag
	b7-b1 Reserved 0
	*/
	ICU.IR[66].BIT.IR			= 0u;

	/* Interrupt enable register (IEN0 disable)
	b0 IEN0 Interrupt enable bit
	b1 IEN1 Interrupt enable bit
	b2 IEN2 Interrupt enable bit
	b3 IEN3 Interrupt enable bit
	b4 IEN4 Interrupt enable bit
	b5 IEN5 Interrupt enable bit
	b6 IEN6 Interrupt enable bit
	b7 IEN7 Interrupt enable bit
	*/
	ICU.IER[8].BIT.IEN2			= 0u;

	/* Priority Resume0=0(Disable)
	b3-b0 IPR      Interrupt priority
	b7-b4 Reserved 0
	*/
	ICU.IPR[66].BYTE			= 0x00;
}
/******************************************************************************
End of function
******************************************************************************/

/******************************************************************************
Function Name   : usb_cpu_IRQ8_Enable
Description     : IRQ9 Interrupt Enable
Arguments       : none
Return value    : none
******************************************************************************/
void usb_cpu_IRQ8_Enable(void)
{
	/* Interrupt enable register (IEN4 disable)
	b0 IEN0 Interrupt enable bit
	b1 IEN1 Interrupt enable bit
	b2 IEN2 Interrupt enable bit
	b3 IEN3 Interrupt enable bit
	b4 IEN4 Interrupt enable bit
	b5 IEN5 Interrupt enable bit
	b6 IEN6 Interrupt enable bit
	b7 IEN7 Interrupt enable bit
	*/
	ICU.IER[9].BIT.IEN0		= 0;		/* Interrupt disable */

	/* IRQ control register
	b1-b0 Reserved 0
	b3-b2 IRQMD    IRQ detect bit
	b7-b4 Reserved 0
	*/
	ICU.IRQCR[9].BIT.IRQMD	= 1;		/* Down Edge Set */

	/* Priority Resume0=B
	b3-b0 IPR      Interrupt priority
	b7-b4 Reserved 0
	*/
	ICU.IPR[72].BYTE		= 0x0B;		/* Priority Resume0=B */

	/* Interrupt request register
	b0    IR       Interrupt status flag
	b7-b1 Reserved 0
	*/
	ICU.IR[72].BIT.IR		= 0;

	/* Interrupt enable register (IEN4 enable)
	b0 IEN0 Interrupt enable bit
	b1 IEN1 Interrupt enable bit
	b2 IEN2 Interrupt enable bit
	b3 IEN3 Interrupt enable bit
	b4 IEN4 Interrupt enable bit
	b5 IEN5 Interrupt enable bit
	b6 IEN6 Interrupt enable bit
	b7 IEN7 Interrupt enable bit
	*/
	ICU.IER[9].BIT.IEN0		= 1;
}
/******************************************************************************
End of function usb_cpu_IRQ8_Enable
******************************************************************************/

/******************************************************************************
Function Name   : usbc_cpu_IRQ12_Enable
Description     : IRQ9 Interrupt Enable
Arguments       : none
Return value    : none
******************************************************************************/
void usbc_cpu_IRQ12_Enable(void)
{
	/* Interrupt enable register (IEN4 disable)
	b0 IEN0 Interrupt enable bit
	b1 IEN1 Interrupt enable bit
	b2 IEN2 Interrupt enable bit
	b3 IEN3 Interrupt enable bit
	b4 IEN4 Interrupt enable bit
	b5 IEN5 Interrupt enable bit
	b6 IEN6 Interrupt enable bit
	b7 IEN7 Interrupt enable bit
	*/
	ICU.IER[9].BIT.IEN4		= 0;		/* Interrupt disable */

	/* IRQ control register
	b1-b0 Reserved 0
	b3-b2 IRQMD    IRQ detect bit
	b7-b4 Reserved 0
	*/
	ICU.IRQCR[12].BIT.IRQMD	= 1;		/* Down Edge Set */

	/* Priority Resume0=B
	b3-b0 IPR      Interrupt priority
	b7-b4 Reserved 0
	*/
	ICU.IPR[76].BYTE		= 0x0B;		/* Priority Resume0=B */

	/* Interrupt request register
	b0    IR       Interrupt status flag
	b7-b1 Reserved 0
	*/
	ICU.IR[76].BIT.IR		= 0;

	/* Interrupt enable register (IEN4 enable)
	b0 IEN0 Interrupt enable bit
	b1 IEN1 Interrupt enable bit
	b2 IEN2 Interrupt enable bit
	b3 IEN3 Interrupt enable bit
	b4 IEN4 Interrupt enable bit
	b5 IEN5 Interrupt enable bit
	b6 IEN6 Interrupt enable bit
	b7 IEN7 Interrupt enable bit
	*/
	ICU.IER[9].BIT.IEN4		= 1;
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usbc_cpu_IRQ12_Disable
Description     : IRQ9 Interrupt Disable
Arguments       : none
Return value    : none
******************************************************************************/
void usbc_cpu_IRQ12_Disable(void)
{
	/* Interrupt request register
	b0    IR       Interrupt status flag
	b7-b1 Reserved 0
	*/
	ICU.IR[72].BIT.IR		= 0;

	/* Interrupt enable register
	b0 IEN0 Interrupt enable bit
	b1 IEN1 Interrupt enable bit
	b2 IEN2 Interrupt enable bit
	b3 IEN3 Interrupt enable bit
	b4 IEN4 Interrupt enable bit
	b5 IEN5 Interrupt enable bit
	b6 IEN6 Interrupt enable bit
	b7 IEN7 Interrupt enable bit
	*/
	ICU.IER[9].BIT.IEN0		= 0;

	/* Priority Resume0=0
	b3-b0 IPR      Interrupt priority
	b7-b4 Reserved 0
	*/
	ICU.IPR[72].BYTE		= 0x00;
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usbc_cpu_IRQ2Int
Description     : IRQ2 Interrupt process
Arguments       : none
Return value    : none
******************************************************************************/
void usbc_cpu_IRQ2Int(void)
{
/* Condition compilation by the difference of USB function */
#ifdef USB_FUNCSEL_PP
/* Condition compilation by the difference of the devices */
 #if (USBC_LPWR_MODE_PP == USBC_LPWR_USE_PP)
	R_usb_pstd_PcdChangeDeviceState(USBC_DO_REMOTEWAKEUP,
		(uint16_t)0, (USBC_CB_INFO_t)usb_cstd_DummyFunction);
	usbc_cpu_IRQ2_DisEnable();
 #endif /* (USBC_LPWR_MODE_PP == USBC_LPWR_USE_PP) */
#endif /* USB_FUNCSEL_PP */
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cpu_IRQ8Int
Description     : IRQ8 Interrupt process
Arguments       : none
Return value    : none
******************************************************************************/
void usbc_cpu_IRQ8Int(void)
{
}
/******************************************************************************
End of function usb_cpu_IRQ8Int
******************************************************************************/

/******************************************************************************
Function Name   : usbc_cpu_IRQ12Int
Description     : IRQ9 Interrupt process
Arguments       : none
Return value    : none
******************************************************************************/
void usbc_cpu_IRQ12Int(void)
{
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usbc_cpu_McuInitialize
Description     : MCU Initialize
Arguments       : none
Return value    : none
******************************************************************************/
void usbc_cpu_McuInitialize(void)
{
	uint32_t	i;

	/* Protect register (protect off)
	b0    PRC0     Protect bit0
	b1    PRC1     Protect bit1
	b2    Reserved 0
	b3    PRC3     Protect bit3
	b7-b4 Reserved 0
	b15-b8 PRKEY   PRC Key code bit
	*/
	SYSTEM.PRCR.WORD				= 0xA503;

	/* Reset status register (Deep software standby reset check)
	b0    POFR     Power on reset Detection flag
	b1    LVD0F    Voltage Watch 0 reset detection flag
	b2    LVD1F    Voltage Watch 1 reset detection flag
	b3    LVD2F    Voltage Watch 2 reset detection flag
	b6-b4 Reserved 0
	b7    DPSRSTF  Deep software standby reset flag
	*/
	if( SYSTEM.RSTSR0.BIT.DPSRSTF == 1 )
	{
		/* Deep standby interrupt flag register2
		b0 DLVD1IF LVD1 deep standby cancel flag
		b1 DLVD2IF LVD2 deep standby cancel flag
		b2 DRTCIIF RTC Cycle interrupt deep standby cancel flag
		b3 DRTCAIF RTC Alarm interrupt deep standby cancel flag
		b4 DNMIF   NMI deep standby cancel flag
		b5 DRIICDIF  SDA2-DS deep standby cancel flag
		b6 DRIICCIF  SCL2-DS deep standby cancel flag
		b7 DUSBIF  USB suspend/resume deep standby cancel flag
		*/
		if( SYSTEM.DPSIFR2.BIT.DUSBIF == 1 )
		{
			SYSTEM.DPSIFR2.BIT.DUSBIF	= 0;	/*  Clear USB Request */
		}

		/* Port mode register (Xin-Xout) and start main clock */
		/* Port mode register
		b0 B0 Pn0 pin mode control bit
		b1 B1 Pn1 pin mode control bit
		b2 B2 Pn2 pin mode control bit
		b3 B3 Pn3 pin mode control bit
		b4 B4 Pn4 pin mode control bit
		b5 B5 Pn5 pin mode control bit
		b6 B6 Pn6 pin mode control bit
		b7 B7 Pn7 pin mode control bit
		*/
		PORT3.PMR.BIT.B6		= 1;
		/* Port mode register
		b0 B0 Pn0 pin mode control bit
		b1 B1 Pn1 pin mode control bit
		b2 B2 Pn2 pin mode control bit
		b3 B3 Pn3 pin mode control bit
		b4 B4 Pn4 pin mode control bit
		b5 B5 Pn5 pin mode control bit
		b6 B6 Pn6 pin mode control bit
		b7 B7 Pn7 pin mode control bit
		*/
		PORT3.PMR.BIT.B7		= 1;

		/* Main clock Oscillator control register
		b4-b0   MOSTP     Main clock oscillator stop bit
		b7-b5   Reserved 0
		*/
		SYSTEM.MOSCCR.BYTE		= 0x00;	/* Main clock oscillator is operated */

		/* Main clock Oscillator wait control register
		b4-b0   MSTS     Main clock Oscillator wait time bit
		b7-b5   Reserved 0
		*/
		SYSTEM.MOSCWTCR.BYTE	= 0x0D;	/* 131072 state */

		/* Sub clock Oscillator control register
		b0      SOSTP    Sub clock Oscillator stop bit
		b7-b1   Reserved 0
		*/
		SYSTEM.SOSCCR.BYTE		= 0x01;	/* Sub clock Oscillator is stopped */

		/* Start PLL Controler */
		/* PLL control register
		b1-b0   PLIDIV   PLL input division ratio select bit
		b7-b2   Reserved 0
		b13-b8  STC      frequency synthesizer set bit
		b15-b14 Reserved 0
		*/
		SYSTEM.PLLCR.WORD		= 0x0F00;	/* PLIDIV = 12MHz(/1), STC = 192MHz(*16) */

		/* PLL control register2
		b0      PLLEN    PLL stop control bit
		b7-b1   Reserved 0
		*/
		SYSTEM.PLLCR2.BYTE		= 0x00;	/* PLL enable */

		/* PLL wait control register
		b4-b0   PSTS     PLL wait time set bit
		b7-b5   Reserved 0
		*/
		SYSTEM.PLLWTCR.BYTE		= 0x0F;	/* 4194304cycle(Default) */

		for(i = 0;i< 200;i++)	/* wait over 4ms ? */
		{
		}

		/* System clock control register
		b7-b0   Reserved 0
		b11-b8  PCK      PCLK
		b15-b12 Reserved 0
		b19-b16 BCK      BCLK
		b22-b20 Reserved 0
		b23     PSTOP1   BCLK control bit
		b27-b24 ICK      System clock (ICLK) select bit
		b31-b28 FCK      Flash IC Clock(FCLK) select bit
		*/
		SYSTEM.SCKCR.LONG		= 0x21032222;	/* ICK(96MHz)=PLL/2,BCK(24MHz)=PLL/8,FCK,PCK(48MHz)=PLL/4 */

		/* System clock control register3
		b7-b0   Reserved 0
		b10-b8  CKSEL    Clock source select bit
		b15-b11 Reserved 0
		*/
		SYSTEM.SCKCR3.WORD		= 0x0400;		/* PLL */

		/* System clock control register2
		b3-b0   IEBCK    IEBUS clock(IECLK) select bit
		b7-b4   UCK      USB clock(UCLK) select bit
		b15-b8  Reserved 0
		*/
		SYSTEM.SCKCR2.BIT.UCK	= 3;	/* USB clock : 48MHz */

		/* Module stop control register (Disable ACSE)
		b3-b0   Reserved 0
		b4	    MSTPA4   8bit timer3,2 stop bit
		b5      MSTPA5   8bit timer1,0 stop bit
		b8-b6   Reserved 0
		b9      MSTPA9   Multifunction timer unit0 stop bit
		b10     MSTPA10  Programmable pulse unit1 stop bit
		b11     MSTPA11  Programmable pulse unit0 stop bit
		b12     MSTPA12  16Bit timer pulse unit1 stop bit
		b13     MSTPA13  16Bit timer pulse unit0 stop bit
		b14     MSTPA14  Compare timer unit1 stop bit
		b15     MSTPA15  Compare timer unit0 stop bit
		b16     Reserved 0
		b17     MSTPA17  12bit AD stop bit
		b18     Reserved 0
		b19     MSTPA19  DA stop bit
		b22-b20 Reserved 0
		b23     MSTPA23  10bit AD unit0 stop bit
		b24     MSTPA24  Module stop A24 set bit
		b26-b25 Reserved 0
		b27     MSTPA27  Module stop A27 set bit
		b28     MSTPA28  DMA/DTC stop bit
		b29     MSTPA29  Module stop A29 set bit
		b30     Reserved 0
		b31     ACSE     All clock stop bit
		*/
		SYSTEM.MSTPCRA.LONG		= 0x7FFFFFFF;

// #if USB_FUNCSEL_USBIP0_PP != USB_NOUSE_PP
    	/* Module stop control register
    	b0     MSTPB0   CAN stop bit
    	b14-b1 Reserved 0
    	b15    MSTPB15  Ether DMA stop bit
    	b16    MSTPB16  Serial peri1 stop bit
    	b17    MSTPB17  Serial peri0 stop bit
    	b18    MSTPB18  USB port1 stop bit
    	b19    MSTPB19  USB port0 stop bit
    	b20    MSTPB20  I2C interface1 stop bit
    	b21    MSTPB21  I2C interface0 stop bit
    	b22    Reserved 0
    	b23    MSTPB23  CRC stop bit
    	b24    Reserved 0
    	b25    MSTPB25  Serial interface6 stop bit
    	b26    MSTPB26  Serial interface5 stop bit
    	b27    Reserved 0
    	b28    MSTPB28  Serial interface3 stop bit
    	b29    MSTPB29  Serial interface2 stop bit
    	b30    MSTPB30  Serial interface1 stop bit
    	b31    MSTPB31  Serial interface0 stop bit
    	*/
    	SYSTEM.MSTPCRB.BIT.MSTPB19	= 0u;	/*  Enable USB0 module */
//#endif	/* USB_FUNCSEL_USBIP0_PP != USB_NOUSE_PP */


		/* Port mode register (BCLK) */
		/* System control register
		b0     ROME     Internal ROM Enable bit
		b1     EXBE     External Bus Enable bit
		b7-b2  Reserved 0
		b15-b8 KEY[7:0] SYSCR0 Key code
		*/
		SYSTEM.SYSCR0.WORD		= 0x5A03;	/* External Bus Enable */

		/* Port mode register
		b0 B0 Pn0 pin mode control bit
		b1 B1 Pn1 pin mode control bit
		b2 B2 Pn2 pin mode control bit
		b3 B3 Pn3 pin mode control bit
		b4 B4 Pn4 pin mode control bit
		b5 B5 Pn5 pin mode control bit
		b6 B6 Pn6 pin mode control bit
		b7 B7 Pn7 pin mode control bit
		*/
		PORT5.PMR.BIT.B3			= 1;

		/* External Bus clock control register
		b0     BCLKDIV  BCLK Pin output select bit
		b7-b1  Reserved 0
		*/
		SYSTEM.BCKCR.BIT.BCLKDIV	= 1;	/* BCLK * 1/2 */

		/* System clock control register
		b7-b0   Reserved 0
		b11-b8  PCK      PCLK
		b15-b12 Reserved 0
		b19-b16 BCK      BCLK
		b22-b20 Reserved 0
		b23     PSTOP1   BCLK control bit
		b27-b24 ICK      System clock (ICLK) select bit
		b31-b28 FCK      Flash IC Clock(FCLK) select bit
		*/
		SYSTEM.SCKCR.BIT.PSTOP1		= 0;	/* BCLK Pin output enable */

		/* Deep Standby control register
		b0-b1 DEEPCUT  RAM off bit
		b5-b2 Reserved 0
		b6    IOKEEP   IO port keep bit
		b7    DPSBY    Deep standby bit
		*/
		SYSTEM.DPSBYCR.BIT.IOKEEP	= 0;	/* IO status keep disable */

		/* Reset status register
		b0    POFR     Power on reset Detection flag
		b1    LVD0F    Voltage Watch 0 reset detection flag
		b2    LVD1F    Voltage Watch 1 reset detection flag
		b3    LVD2F    Voltage Watch 2 reset detection flag
		b6-b4 Reserved 0
		b7    DPSRSTF  Deep software standby reset flag
		*/
		SYSTEM.RSTSR0.BIT.DPSRSTF	= 0;	/* Deep software standby reset */
	}
	else
	{
		/* Port mode register (Xin-Xout) and start main clock */
		/* Port mode register
		b0 B0 Pn0 pin mode control bit
		b1 B1 Pn1 pin mode control bit
		b2 B2 Pn2 pin mode control bit
		b3 B3 Pn3 pin mode control bit
		b4 B4 Pn4 pin mode control bit
		b5 B5 Pn5 pin mode control bit
		b6 B6 Pn6 pin mode control bit
		b7 B7 Pn7 pin mode control bit
		*/
		PORT3.PMR.BIT.B6		= 1;

		/* Port mode register
		b0 B0 Pn0 pin mode control bit
		b1 B1 Pn1 pin mode control bit
		b2 B2 Pn2 pin mode control bit
		b3 B3 Pn3 pin mode control bit
		b4 B4 Pn4 pin mode control bit
		b5 B5 Pn5 pin mode control bit
		b6 B6 Pn6 pin mode control bit
		b7 B7 Pn7 pin mode control bit
		*/
		PORT3.PMR.BIT.B7		= 1;

		/* Main clock Oscillator wait control register
		b4-b0   MSTS     Main clock Oscillator wait time bit
		b7-b5   Reserved 0
		*/
		SYSTEM.MOSCWTCR.BYTE	= 0x0D;	/* 131072 state */

		/* Start PLL Controler */
		/* PLL control register ( PLIDIV = 12MHz(/1), STC = 192MHz(*16) )
		b1-b0   PLIDIV   PLL input division ratio select bit
		b7-b2   Reserved 0
		b13-b8  STC      frequency synthesizer set bit
		b15-b14 Reserved 0
		*/
		SYSTEM.PLLCR.WORD		= 0x0F00;

		/* Main clock Oscillator control register
		b4-b0   MOSTP     Main clock Oscillator stop bit
		b7-b5   Reserved 0
		*/
		SYSTEM.MOSCCR.BYTE		= 0x00;	/* EXTAL ON */
		
		/* PLL control register2
		b0      PLLEN    PLL stop control bit
		b7-b1   Reserved 0
		*/
		SYSTEM.PLLCR2.BYTE		= 0x00;	/* PLL ON */

		/* PLL wait control register(Default:4194304cycle)
		b4-b0   PSTS     PLL wait time set bit
		b7-b5   Reserved 0
		*/
		SYSTEM.PLLWTCR.BYTE		= 0x0F;

		/* wait over 11ms */
		for(i = 0; i< 0x14A; i++)
		{
		}

		/* System clock control register(ICK=PLL/2,BCK,FCK,PCK=PLL/4)
		b7-b0   Reserved 0
		b11-b8  PCK      PCLK
		b15-b12 Reserved 0
		b19-b16 BCK      BCLK
		b22-b20 Reserved 0
		b23     PSTOP1   BCLK control bit
		b27-b24 ICK      System clock (ICLK) select bit
		b31-b28 FCK      Flash IC Clock(FCLK) select bit
		*/
		SYSTEM.SCKCR.LONG		= 0x21832222;

		/* System clock control register3(CKSEL(10-8)=PLL)
		b7-b0   Reserved 0
		b10-b8  CKSEL    Clock source select bit
		b15-b11 Reserved 0
		*/
		SYSTEM.SCKCR3.WORD		= 0x0400;

		/* System clock control register2(UCK(7-4)=48MHz(/4))
		b3-b0   IEBCK    IEBUS clock(IECLK) select bit
		b7-b4   UCK      USB clock(UCLK) select bit
		b15-b8  Reserved 0
		*/
		SYSTEM.SCKCR2.BIT.UCK	= 3;

		/* Module stop control register (Disable ACSE)
		b3-b0   Reserved 0
		b4	    MSTPA4   8bit timer3,2 stop bit
		b5      MSTPA5   8bit timer1,0 stop bit
		b8-b6   Reserved 0
		b9      MSTPA9   Multifunction timer unit0 stop bit
		b10     MSTPA10  Programmable pulse unit1 stop bit
		b11     MSTPA11  Programmable pulse unit0 stop bit
		b12     MSTPA12  16Bit timer pulse unit1 stop bit
		b13     MSTPA13  16Bit timer pulse unit0 stop bit
		b14     MSTPA14  Compare timer unit1 stop bit
		b15     MSTPA15  Compare timer unit0 stop bit
		b16     Reserved 0
		b17     MSTPA17  12bit AD stop bit
		b18     Reserved 0
		b19     MSTPA19  DA stop bit
		b22-b20 Reserved 0
		b23     MSTPA23  10bit AD unit0 stop bit
		b24     MSTPA24  Module stop A24 set bit
		b26-b25 Reserved 0
		b27     MSTPA27  Module stop A27 set bit
		b28     MSTPA28  DMA/DTC stop bit
		b29     MSTPA29  Module stop A29 set bit
		b30     Reserved 0
		b31     ACSE     All clock stop bit
		*/
		SYSTEM.MSTPCRA.LONG		= 0x7FFFFFFF;


//#if USB_FUNCSEL_USBIP0_PP != USB_NOUSE_PP
    	/* Module stop control register
    	b0     MSTPB0   CAN stop bit
    	b14-b1 Reserved 0
    	b15    MSTPB15  Ether DMA stop bit
    	b16    MSTPB16  Serial peri1 stop bit
    	b17    MSTPB17  Serial peri0 stop bit
    	b18    MSTPB18  USB port1 stop bit
    	b19    MSTPB19  USB port0 stop bit
    	b20    MSTPB20  I2C interface1 stop bit
    	b21    MSTPB21  I2C interface0 stop bit
    	b22    Reserved 0
    	b23    MSTPB23  CRC stop bit
    	b24    Reserved 0
    	b25    MSTPB25  Serial interface6 stop bit
    	b26    MSTPB26  Serial interface5 stop bit
    	b27    Reserved 0
    	b28    MSTPB28  Serial interface3 stop bit
    	b29    MSTPB29  Serial interface2 stop bit
    	b30    MSTPB30  Serial interface1 stop bit
    	b31    MSTPB31  Serial interface0 stop bit
    	*/
    	SYSTEM.MSTPCRB.BIT.MSTPB19	= 0u;	/*  Enable USB0 module */
//#endif	/* USB_FUNCSEL_USBIP0_PP != USB_NOUSE_PP */

		/* Port mode register (BCLK) */
		/* System control register (External Bus Enable)
		b0     ROME     Internal ROM Enable bit
		b1     EXBE     External Bus Enable bit
		b7-b2  Reserved 0
		b15-b8 KEY[7:0] SYSCR0 Key code
		*/
		SYSTEM.SYSCR0.WORD				= 0x5A03;

		/* Port mode register
		b0 B0 Pn0 pin mode control bit
		b1 B1 Pn1 pin mode control bit
		b2 B2 Pn2 pin mode control bit
		b3 B3 Pn3 pin mode control bit
		b4 B4 Pn4 pin mode control bit
		b5 B5 Pn5 pin mode control bit
		b6 B6 Pn6 pin mode control bit
		b7 B7 Pn7 pin mode control bit
		*/
		PORT5.PMR.BIT.B3			= 1;

		/* External Bus clock control register (BCLK * 1/2)
		b0     BCLKDIV  BCLK Pin output select bit
		b7-b1  Reserved 0
		*/
		SYSTEM.BCKCR.BIT.BCLKDIV	= 1;

		/* System clock control register(BCLK Pin output enable)
		b7-b0   Reserved 0
		b11-b8  PCK      PCLK
		b15-b12 Reserved 0
		b19-b16 BCK      BCLK
		b22-b20 Reserved 0
		b23     PSTOP1   BCLK control bit
		b27-b24 ICK      System clock (ICLK) select bit
		b31-b28 FCK      Flash IC Clock(FCLK) select bit
		*/
		SYSTEM.SCKCR.BIT.PSTOP1		= 0;
	}
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usbc_cpu_LcdInit
Description     : LCD initialize
Arguments       : none
Return value    : none
******************************************************************************/
void usbc_cpu_LcdInit(void)
{
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usbc_cpu_LcdDisp
Description     : LCD display string data
Arguments       : uint8_t pos           : Position
                : uint8_t *str          : Write data (string data)
Return value    : none
******************************************************************************/
void usbc_cpu_LcdDisp(uint8_t pos, uint8_t *str)
{
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usbc_cpu_LcdWrite
Description     : LCD write data        : Write mode (USB_CTRL_WR/USB_DATA_WR)
Arguments       : uint8_t data          : Write data (8bit data)
Return value    : none
******************************************************************************/
void usbc_cpu_LcdWrite(uint8_t mode, uint8_t value)
{
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usbc_cpu_LcdNibbleWrite
Description     : LCD Nibble data write
Arguments       : uint8_t mode          : Write mode (USB_CTRL_WR/USB_DATA_WR)
                : uint8_t value         : Write data (4bit data)
Return value    : none
******************************************************************************/
void usbc_cpu_LcdNibbleWrite(uint8_t mode, uint8_t value)
{
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usbc_cpu_LcdWait
Description     : Wait process
Arguments       : uint32_t units        : Wait time
Return value    : none
******************************************************************************/
void usbc_cpu_LcdWait(uint32_t units)
{
}
/******************************************************************************
End of function
******************************************************************************/

/******************************************************************************
Serial Port function
******************************************************************************/

/******************************************************************************
Function Name   : usb_ComPortSet1
Description     : Line coding set
Arguments       : data_rate   : Data terminal rate in bits per second
                : stop_bit    : Stop bits 0-1Stopbit 1-1.5Stopbit 2-2Stopbit
                : parity      : Parity 0-None 1-Odd 2-Even 3-Mark 4-Spase
                : data_bit    : Data bits(5,6,7,8 or 16)
Return value    : Error code  : Error code
******************************************************************************/
uint16_t usbc_cpu_Sci_Set1(uint8_t *data_rate_top, uint8_t stop_bit
	, uint8_t parity, uint8_t data_bit)
{
	return USBC_OK;
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_ComPortSet2
Description     : RS-232 signal RTS & DTR Set
Arguments       : rts         : 0-Deactivate carrier 1-Activate carrier
                : dtr         : 0-Not Present 1-Present
Return value    : uint16_t    : Error code
******************************************************************************/
uint16_t usbc_cpu_Sci_Set2(uint8_t rts, uint8_t dtr)
{
	return USBC_OK;
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_ComPortSendBreak
Description     : RS-232 style break
Arguments       : dutation    : Duration of Break
Return value    : uint16_t    : Error code
******************************************************************************/
uint16_t usbc_cpu_Sci_SendBreak(uint16_t dutation)
{
	return USBC_OK;
}
/******************************************************************************
End of function
******************************************************************************/

/******************************************************************************
Function Name   : usb_ComPortDataSend
Description     : Communications Port Data Send Process
Arguments       : tranadr     : Transfer Data Address
                : length      : Transfer Data Length
Return value    : uint16_t    : 1-Echo mode data copy request
******************************************************************************/
void usbc_cpu_Sci_DataSend(uint16_t mode, void *tranadr
	,uint16_t length)
{
}
/******************************************************************************
End of function
******************************************************************************/

/******************************************************************************
Function Name   : usb_ComPortDataReceive
Description     : Communications Port Data Receive Process
Arguments       : tranadr     : Transfer Data Address
                : length      : Transfer Data Length
Return value    : uint16_t    : Receive data byte count
******************************************************************************/
uint16_t usbc_cpu_Sci_DataReceive(uint8_t *tranadr,uint16_t receive_length)
{
	return 0;
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usbc_cpu_Sci_ER_Int
Description     : ComPort Receive Error Interrupt
Arguments       : none
                : 
Return value    : none
******************************************************************************/
void usbc_cpu_Sci_ER_Int(void)
{
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usbc_cpu_Sci_RX_Int
Description     : ComPort Receive Interrupt
Arguments       : none
                : 
Return value    : none
******************************************************************************/
void usbc_cpu_Sci_RX_Int(void)
{
}
/******************************************************************************
End of function
******************************************************************************/

/******************************************************************************
Function Name   : usbc_cpu_Sci_TX_Int
Description     : ComPort Transfer Buffer Empty Interrupt
Arguments       : none
                : 
Return value    : none
******************************************************************************/
void usbc_cpu_Sci_TX_Int(void)
{
}
/******************************************************************************
End of function
******************************************************************************/

/******************************************************************************
Function Name   : usbc_cpu_Sci_TE_Int
Description     : ComPort Transfer End Interrupt
Arguments       : none
                : 
Return value    : none
******************************************************************************/
void usbc_cpu_Sci_TE_Int(void)
{
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usbc_cpu_Sci_StxBuffStatus
Description     : Serial port send buffer status check
Arguments       : none
                : 
Return value    : Serial port send buffer status 1:Busy 0:Not Busy
******************************************************************************/
uint16_t usbc_cpu_Sci_StxBuffStatus(void)
{
	return 0;
}
/******************************************************************************
End of function
******************************************************************************/

/******************************************************************************
Function Name   : usbc_cpu_Sci_Buffer_init
Description     : Serial Tx Rx data buffer clear
Argument        : none
Return          : none
******************************************************************************/
void usbc_cpu_Sci_Buffer_init(void)
{
}
/******************************************************************************
End of function
******************************************************************************/

/******************************************************************************
Function Name   : usbc_cpu_Sci_HW_init
Description     : ComPort Hw Initialize
Argument        : none
Return          : none
******************************************************************************/
void usbc_cpu_Sci_HW_init(void)
{
}
/******************************************************************************
End of function
******************************************************************************/

/******************************************************************************
Function Name   : usbc_cpu_Sci_enable
Description     : Serial port enable (SCI0)
Argument        : none
Return          : none
******************************************************************************/
void usbc_cpu_Sci_enable(void)
{
}
/******************************************************************************
End of function
******************************************************************************/

/******************************************************************************
Function Name   : usbc_cpu_Sci_disable
Description     : Serial port disable (SCI0)
Argument        : none
Return          : none
******************************************************************************/
void usbc_cpu_Sci_disable(void)
{
}
/******************************************************************************
End of function
******************************************************************************/

/******************************************************************************
Function Name   : usbc_cpu_Sci_CopyData_for_Echo
Description     : Echo mode data copy Serial TX -> Serial RX
Argument        : none
Return          : Error code
******************************************************************************/
uint16_t usbc_cpu_Sci_CopyData_for_Echo(void)
{
	return 0;
}
/******************************************************************************
End of function
******************************************************************************/

/******************************************************************************
Function Name   : usbc_cpu_Sci_GetSeraalState
Description     : Serial State Get process
Argument        : SeraalState(CDC PSTN Subclass UART State bitmap) store addr
Return          : 1-Serial error detect
******************************************************************************/
uint16_t usbc_cpu_Sci_GetSerialState(uint16_t *serial_state)
{
	return USBC_OK;
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usbc_cpu_Sci_EnableStatus
Description     : Serial Port Enable State Get process
Argument        : none
Return          : 1-Serial port enable 0-Serial port disable
******************************************************************************/
uint16_t usbc_cpu_Sci_EnableStatus(void)
{
	return 0;
}
/******************************************************************************
End of function
******************************************************************************/



/******************************************************************************
Function Name   : usbc_cpu_Sci_SetDataChk
Description     : Line coding set data check
Arguments       : data_rate   : Data terminal rate in bits per second
                : stop_bit    : Stop bits 0-1Stopbit 1-1.5Stopbit 2-2Stopbit
                : parity      : Parity 0-None 1-Odd 2-Even 3-Mark 4-Spase
                : data_bit    : Data bits(5,6,7,8 or 16)
Return value    : Error code  : Error code
******************************************************************************/
uint16_t usbc_cpu_Sci_SetDataChk(uint8_t *data_rate_top, uint8_t stop_bit
	, uint8_t parity, uint8_t data_bit)
{
	return USBC_OK;
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
A/D function
******************************************************************************/


/******************************************************************************
Function Name   : usbc_cpu_AdInit
Description     : 12bit A/D initialize
Arguments       : none
Return value    : none
******************************************************************************/
void usbc_cpu_AdInit(void)
{
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usbc_cpu_AdData
Description     : Read A/D data
Arguments       : none
Return value    : uint32_t              : AD data
******************************************************************************/
uint32_t usbc_cpu_AdData(void)
{
	return (uint32_t)0;
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usbc_cstd_DummyIntFunction
Description     : dummy function
Arguments       : uint16_t data1            : not use
                : uint16_t data2            : not use
Return value    : none
******************************************************************************/
void usbc_cstd_DummyIntFunction(uint16_t data1, uint16_t data2)
{
}
/******************************************************************************
End of function
******************************************************************************/

/******************************************************************************
Function Name   : usbc_cstd_fvect20
Description     : dummy function
Arguments       : none
Return value    : none
******************************************************************************/
void usbc_cstd_fvect20( void )
{
}
/******************************************************************************
End of function
******************************************************************************/

/******************************************************************************
Function Name   : usbc_cstd_fvect23
Description     : dummy function
Arguments       : none
Return value    : none
******************************************************************************/
void usbc_cstd_fvect23( void )
{
	while( 1 );
}
/******************************************************************************
End of function
******************************************************************************/

/******************************************************************************
Function Name   : usbc_cstd_fvect25
Description     : dummy function
Arguments       : none
Return value    : none
******************************************************************************/
void usbc_cstd_fvect25( void )
{
}
/******************************************************************************
End of function
******************************************************************************/

/******************************************************************************
Function Name   : usbc_cstd_fvect30
Description     : dummy function
Arguments       : none
Return value    : none
******************************************************************************/
void usbc_cstd_fvect30( void )
{
}
/******************************************************************************
End of function
******************************************************************************/

/******************************************************************************
End  Of File
******************************************************************************/
