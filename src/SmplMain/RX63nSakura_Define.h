/******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized.
* This software is owned by Renesas Electronics Corporation and is  protected
* under all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES
* REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY,
* INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR  A
* PARTICULAR PURPOSE AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE  EXPRESSLY
* DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE  LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES
* FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS
* AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this
* software and to discontinue the availability of this software.
* By using this software, you agree to the additional terms and
* conditions found by accessing the following link:
* http://www.renesas.com/disclaimer
*******************************************************************************
* Copyright (C) 2012 Medialogic Corpration. All rights reserved.
*******************************************************************************
* File Name    : RX630RSK_Define.h
* Version      : 1.00
* Device(s)    : Renesas RX-Series
* Tool-Chain   : Renesas RX Standard Toolchain
* OS           : none
* H/W Platform : Independent
* Description  : RX630 RSK Extern
******************************************************************************
* History : DD.MM.YYYY Version Description
*         : 07.09.2012 1.00    First Release
******************************************************************************/
#ifndef __RX630RSK_DEFINE_H__
#define __RX630RSK_DEFINE_H__

#include "r_usbc_cTypedef.h"		/* Type define */

/******************************************************************************
	Definition of Product parameters
******************************************************************************/

#define	ENABLE_KEEP_FILE_INFO			0			// 1�ɂ���ƃt�@�C������ێ�����

#define	ENABLE_FAT16					0			// 1�ɂ����FAT16�t�H�[�}�b�g

#define	MEMORY_MAP_TYPE					2			// 1=768KB
													// 2=1MB

#define	ENABLE_PRINTF					0			// 1�ɂ����DBG_printf��L����
													// ���̏ꍇ�́A�R�[�h�̃Z�N�V������FFFE0000h�ɂ���
#if 	ENABLE_PRINTF
#include <stdlib.h>
#include <stdio.h>
#define	DBG_printf(x)					printf x			
#else
#define	DBG_printf(x)
#endif

#define	IDLE_TIME_FOR_NO_USB_ENUM		3000		// USB�o�X���Enumeration���J�n����Ȃ��Ɣ��f���鎞��[ms]
#define	IDLE_TIME_FOR_WRITE_END			2000		// �������ݏI���𔻒f���鎞��[ms]


/******************************************************************************
	Definition of Constant values
******************************************************************************/

#if	(MEMORY_MAP_TYPE==1)
	//
	//	768KB�Ń������}�b�v��`
	//
#define	STORAGE_CAPACITY_KB				352			// �ۑ��\�ȃf�[�^�p�Z�N�^��(352KB)

#define	FATFS_SECTOR_PER_CLUSTER		32			// 16KB�N���X�^

#define	USBC_PMSC_USER_APPLICATION_ADDR	0xFFF40000	// ���[�U�[APL�̈�
#define	USBC_PMSC_USER_APPLICATION_END	0xFFF97FFF
#define	USBC_PMSC_STORAGE_FLASH_ADDR	0xFFF98000	// �X�g���[�W�̈�

#elif (MEMORY_MAP_TYPE==2)
	//
	//	1MB�Ń������}�b�v��`
	//
#define	STORAGE_CAPACITY_KB				448			// 保存可能なデータ用セクタ数(448KB)

#define	FATFS_SECTOR_PER_CLUSTER		32			// 16KBクラスタ

#define	USBC_PMSC_USER_APPLICATION_ADDR	0xFFF00000	// ユーザーAPL領域
#define	USBC_PMSC_USER_APPLICATION_END	0xFFF7FFFF
#define	USBC_PMSC_STORAGE_FLASH_ADDR	0xFFF80000	// ストレージ領域

#if 	ENABLE_PRINTF
#undef	USBC_ATAPI_STORAGE_SECTOR_COUNT
#define	USBC_ATAPI_STORAGE_SECTOR_COUNT	((448*1024/512)-128)
#endif

#else
#error Invalid MEMORY_MAP_TYPE!
#endif

#if	ENABLE_KEEP_FILE_INFO
#define	USBC_PMSC_DATA_FLASH_ADDR		0x00107800	// �X�g���[�W�̈�A
#endif


/* Logical Block Unit Size */
#define USBC_ATAPI_BLOCK_UNIT			512
/* Transfer Unit Size */
#define USBC_ATAPI_TRANSFER_UNIT		(256 * USBC_ATAPI_BLOCK_UNIT)

#if 	ENABLE_PRINTF
#define	USBC_ATAPI_STORAGE_SECTOR_COUNT	((STORAGE_CAPACITY_KB-64)*1024/512)
#else
#define	USBC_ATAPI_STORAGE_SECTOR_COUNT	(STORAGE_CAPACITY_KB*1024/512)
#endif

#define	MBR_SECTOR_COUNT				1		// 1�Z�N�^
#define	FATFS_RESERVED_SECTOR_COUNT		1		// 1�Z�N�^
#define	FATFS_ROOT_DIR_SECTOR_COUNT		2		// 2�Z�N�^
#define	FATFS_FAT_SECTOR_COUNT			1		// 1�Z�N�^

#if 	ENABLE_FAT16
#define	FATFS_FILE_SYSTEM_NAME			'F','A','T','1','6',' ',' ',' '
#else
#define	FATFS_FILE_SYSTEM_NAME			'F','A','T','1','2',' ',' ',' '
#endif

#define	FATFS_MEDIATYPE					0xF0		// �����[�o�u�����f�B�A

#define	FATFS_OEM_NAME					'M','S','D','O','S','5','.','0'

#define	FATFS_CLUSTER_SIZE				(FATFS_SECTOR_PER_CLUSTER * USBC_ATAPI_BLOCK_UNIT)



typedef enum { FALSE, TRUE } BOOL;

/**** Function Return Values ****/
/* Operation was successful */
#define	FLASH_SUCCESS					(0x00)
/* Operation failed */
#define	FLASH_FAILURE					(0x06)


/******************************************************************************
	Structure of Directory Entry
******************************************************************************/
struct tagDirectoryEntry
{
	uint8_t		bFilename[8];		// 00: Filename
	uint8_t		bExtension[3];		// 08: Extension
	uint8_t		bAttributes;		// 0B: Attributes flags
	uint8_t		bFilenameFlags;		// 0C: Filename flags
	uint8_t		nCreationTime_ms;	// 0D: Creation time in 10ms
	uint16_t	wCreationTime;		// 0E: Creation time
	uint16_t	wCreationDate;		// 10: Creation date
	uint16_t	wLastAccessDate;	// 12: Last access date
	uint16_t	wClusNoEx;			// 14: (High word of cluster number for FAT32)
	uint16_t	wLastWriteTime;		// 16: Last modified time
	uint16_t	wLastWriteDate;		// 18: Last modified date
	uint16_t	nClusNo;			// 1A: Cluster number
	uint32_t	nFileSize;			// 1C: File size
};	
typedef	struct tagDirectoryEntry	DIRENT, *PDIRENT;



#endif	/* __RX630RSK_DEFINE_H__ */
/******************************************************************************
End  Of File
******************************************************************************/
