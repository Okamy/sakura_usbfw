/******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized.
* This software is owned by Renesas Electronics Corporation and is  protected
* under all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES
* REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY,
* INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR  A
* PARTICULAR PURPOSE AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE  EXPRESSLY
* DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE  LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES
* FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS
* AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this
* software and to discontinue the availability of this software.
* By using this software, you agree to the additional terms and
* conditions found by accessing the following link:
* http://www.renesas.com/disclaimer
*******************************************************************************
* Copyright (C) 2010(2011) Renesas Electronics Corpration
* and Renesas Solutions Corp. All rights reserved.
*******************************************************************************
* File Name    : RX630RSK_Extern.h
* Version      : 1.10
* Device(s)    : Renesas SH-Series, RX-Series
* Tool-Chain   : Renesas SuperH RISC engine Standard Toolchain
*              : Renesas RX Standard Toolchain
* OS           : Common to None and uITRON 4.0 Spec
* H/W Platform : Independent
* Description  : RX630 RSK Extern
******************************************************************************
* History : DD.MM.YYYY Version Description
*         : 17.03.2010 0.80    First Release
*         : 30.07.2010 0.90    Updated comments
*         : 02.08.2010 0.91    Updated comments
*         : 29.10.2010 1.00    Mass Production Release
*         : 01.06.2011 1.10    Version 1.10 Release
******************************************************************************/
#ifndef __RX630RSK_EXTERN_H__
#define __RX630RSK_EXTERN_H__


/******************************************************************************
Includes   <System Includes> , "Project Includes"
******************************************************************************/
#include "r_usbc_cTypedef.h"		/* Type define */


/******************************************************************************
External variables and functions
******************************************************************************/
/* Timer functions */
extern	uint32_t	timer_GetTimeCounter( void );
extern	uint32_t	timer_GetPastTime( const uint32_t timeLast );
#pragma interrupt	timer_IntHandler

/* SW input driver functions */
#if 1	// DEMO_PRG
extern uint16_t		usbc_cpu_KeyScan(void);
#endif
extern uint16_t		usbc_cpu_GetKeyNo(void);
extern void			usbc_cpu_AcquisitionKeyNo(void);
extern uint8_t		usbc_cpu_KeepKeyNo(void);
extern uint8_t		usbc_cpu_SingleKeyNo(void);

/* AD driver functions */
extern void			usbc_cpu_AdInit(void);
extern uint32_t		usbc_cpu_AdData(void);

/* LCD driver functions */
extern void			usbc_cpu_LedInit(void);
extern void			usbc_cpu_LedSetBit(uint8_t bit, uint8_t data);

/* Serial Port driver functions */
extern uint16_t	usbc_cpu_Sci_Set1(uint8_t *data_rate, uint8_t stop_bit
	, uint8_t parity, uint8_t data_bit);
extern uint16_t	usbc_cpu_Sci_Set2(uint8_t rts, uint8_t dtr);
extern uint16_t	usbc_cpu_Sci_SendBreak(uint16_t dutation);
extern void		usbc_cpu_Sci_DataSend(uint16_t mode, void *tranadr
	,uint16_t length);
extern uint16_t	usbc_cpu_Sci_DataReceive(uint8_t *tranadr
	,uint16_t receive_length);
extern uint16_t	usbc_cpu_Sci_StxBuffStatus(void);
extern void		usbc_cpu_Sci_Buffer_init(void);
extern void		usbc_cpu_Sci_HW_init(void);
extern void		usbc_cpu_Sci_enable(void);
extern void		usbc_cpu_Sci_disable(void);
extern uint16_t	usbc_cpu_Sci_CopyData_for_Echo(void);
extern uint16_t	usbc_cpu_Sci_GetSerialState(uint16_t *serial_state);
extern uint16_t	usbc_cpu_Sci_EnableStatus(void);
extern uint16_t	usbc_cpu_Sci_SetDataChk(uint8_t *data_rate, uint8_t stop_bit
	, uint8_t parity, uint8_t data_bit);

/* Condition compilation by the difference of the operating system */
#if USBC_FW_PP == USBC_FW_NONOS_PP
/* Interrupt handler for nonOS */
#pragma interrupt usb_cstd_UsbIntHand			/* USB int */
#pragma interrupt usb2_cstd_UsbIntHand
#pragma interrupt usb_cstd_D0fifoIntHand		/* D0FIFO int */
#pragma	interrupt usb2_cstd_D0fifoIntHand
#pragma interrupt usb_cstd_D1fifoIntHand		/* D1FIFO int */
#pragma	interrupt usb2_cstd_D1fifo1IntHand
#pragma interrupt usb_cstd_UsbIntHandR			/* RESUME int */
#pragma interrupt usb2_cstd_UsbIntHandR
#pragma interrupt usbc_cstd_DummyIntFunction	/* Dummy */
#pragma interrupt usbc_cstd_fvect20
#pragma interrupt usbc_cstd_fvect23
#pragma interrupt usbc_cstd_fvect25
#pragma interrupt usbc_cstd_fvect30
#pragma interrupt usbc_cpu_GoWait_Int			/* int for wait() */
#pragma interrupt usbc_cpu_IRQ2Int				/* IRQ2 (RSK-SW1) */
#pragma interrupt usbc_cpu_IRQ12Int				/* IRQ12 (RSK-SW2) */
#pragma	interrupt usbc_cpu_IRQ15Int				/* IRQ15 (RSK-SW3) */
#pragma	interrupt usbc_cpu_IRQ8Int				/* IRQ8  () */
/* Communication Port Error Interrupt */
#pragma interrupt usbc_cpu_Sci_ER_Int
/* Communication Port Data Receive Interrupt */
#pragma interrupt usbc_cpu_Sci_RX_Int
/* Communication Port Transfer Buffer Empty Interrupt */
#pragma	interrupt usbc_cpu_Sci_TX_Int
/* Communication Port Transfer End Interrupt */
#pragma	interrupt usbc_cpu_Sci_TE_Int
#endif	/* USBC_FW_PP == USBC_FW_NONOS_PP */



/* Initialize erase/programming Flash ROM functions */
extern	uint8_t		flash_Initialize( void );

/* Erase block */
extern uint8_t		flash_coderom_EraseBlock( const uint32_t addr );

/* Program rom */
extern uint8_t		flash_coderom_WriteData( const uint32_t addr, void* pData, const uint16_t nDataSize );


/* Search file information of user application. */
extern	PDIRENT		fatfs_FindUserApplicationFileInfo( void );

/*Get next cluster No. */
extern	uint16_t	fatfs_GetNextClusterNo( const uint16_t nCurClusNo );

/* Get cluster data */
extern	void*		fatfs_GetClusterData( const uint16_t nClusNo );



#endif	/* __RX630RSK_EXTERN_H__ */
/******************************************************************************
End  Of File
******************************************************************************/
