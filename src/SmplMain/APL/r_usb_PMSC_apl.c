/******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized.
* This software is owned by Renesas Electronics Corporation and is  protected
* under all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES
* REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY,
* INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR  A
* PARTICULAR PURPOSE AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE  EXPRESSLY
* DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE  LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES
* FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS
* AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this
* software and to discontinue the availability of this software.
* By using this software, you agree to the additional terms and
* conditions found by accessing the following link:
* http://www.renesas.com/disclaimer
*******************************************************************************
* Copyright (C) 2010 Renesas Electronics Corporation. All rights reserved.
*******************************************************************************
* File Name    : r_usb_PMSC_apl.c
* Version      : 1.00
* Device(s)    : Renesas SH-Series, RX-Series
* Tool-Chain   : Renesas SuperH RISC engine Standard Toolchain
*              : Renesas RX Standard Toolchain
* OS           : Common to None and uITRON 4.0 Spec
* H/W Platform : Independent
* Description  : USB Peripheral Mass-Storage Sample Code
*******************************************************************************
* History : DD.MM.YYYY Version Description
*         : 17.03.2010 0.80    First Release
*         : 30.07.2010 0.90    Updated comments
*         : 02.08.2010 0.91    Updated comments
*         : 29.10.2010 1.00    Mass Production Release
******************************************************************************/

/******************************************************************************
Includes   <System Includes> , "Project Includes"
******************************************************************************/
#include "r_usbc_cDefUsr.h"		/* System definition */
#include "r_usbc_cTypedef.h"			/* Type define */
#include "r_usb_cDefUsr.h"			/* USB-H/W register set (user define) */
#include "r_usbc_cDefUSBIP.h"		/* USB-FW Library Header */
#include "r_usbc_cMacSystemcall.h"	/* uITRON system call macro */
#include "r_usbc_cMacPrint.h"		/* Standard IO macro */
#include "r_usb_cExtern.h"			/* USB-FW grobal define */
#include "r_usbc_cKernelId.h"		/* Kernel ID definition */

#include "RX63nSAKURA_Define.h"		/* RX63nSAKURA Define Header */
#include "RX63nSAKURA_Extern.h"		/* RX63nSAKURA Extern Header */



/******************************************************************************
External variables and functions
******************************************************************************/
void			usb_pmsc_MainLoop(void);
void			usb_pmsc_Open(void);
void			usb_pmsc_Close(void);
/* Condition compilation by the difference of the operating system */
#if USBC_FW_PP == USBC_FW_NONOS_PP
void			usb_pmsc_MainTask(void);
void			usb_pmsc_SetTaskPri(void);
#endif // USBC_FW_PP == USBC_FW_NONOS_PP
void			usb_pmsc_Disconnect( const BOOL fDetachDriver );


#if 1	// DEMO_PRG
#pragma stacksize su=0x400
#pragma stacksize si=0x400

/* Start user application */
extern	void	userapl_StartExecution( void );

/* Jump to User Application */
static	void	userapl_jump_to_user_application( void );
static	void	userapl_reset_to_user_application( void );

/* Error occured for User application execution */
extern	void	userapl_ErrorEnd( void );

#endif

extern	uint32_t	usb_gpmsc_Atapi_timeLastWrite;

/******************************************************************************
Renesas Peripheral Mass-Storage Sample Code functions
******************************************************************************/

/******************************************************************************
Function Name   : usb_pstd_PrAplTitle
Description     : Output Title
Argument        : none
Return          : none
******************************************************************************/
void usb_pmsc_PrAplTitle(void)
{
	usbc_cpu_TargetLcdClear();
	USBC_PRINTF0("\n");
	USBC_PRINTF0("++++++++++++++++++++++++++++++++++++++\n");
	USBC_PRINTF0("+   PERI MASS STORAGE CLASS SAMPLE   +\n");
	USBC_PRINTF0("+                                    +\n");
	USBC_PRINTF0("+     RENESAS ELECTRONICS CORP.      +\n");
	USBC_PRINTF0("+     RENESAS SOLUTIONS  CORP.       +\n");
	USBC_PRINTF0("++++++++++++++++++++++++++++++++++++++\n");
	USBC_PRINTF0("\n");
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pmsc_MainInit
Description     : Initialize process
Argument        : none
Return          : none
******************************************************************************/
void usb_pmsc_MainInit(void)
{
	/* Target System Initialize */
	usb_cstd_TargetInit();

	usbc_cpu_LedSet( 0x00 );

	//
	//	ソフトウェアリセットからの復帰ならユーザーアプリケーションへジャンプ
	//
	if( SYSTEM.RSTSR2.BIT.SWRF == 1 )
	{
		userapl_jump_to_user_application();
	}

	//
	//	Power On Resetなら（PORF=1なら）ユーザーアプリケーションへジャンプ
	//
	if( SYSTEM.RSTSR0.BIT.PORF == 1 )
	{
		userapl_jump_to_user_application();
	}


	usb_pmsc_PrAplTitle();

	usb_cstd_WaitUsbip();				/* Wait USB-H/W access enable */
	usb_cstd_AsspConfig();				/* Set ASSP pin_config */
	usb_cstd_SwReset();
	usb_cstd_InitialClock();			/* Start clock */
	
	//
	//	USB Vbus=Loならユーザーアプリケーションへジャンプ
	//
	if( usb_pstd_ChkVbsts() == USBC_DETACH )
	{
		usb_pmsc_Disconnect( FALSE );	// USB接続を停止する
		userapl_reset_to_user_application();
	}
	

	usb_cstd_Pinconfig();				/* Set pin_config */
	R_usb_pstd_PcdOpen();				/* PCD Open */
	R_usb_pmsc_StrgTaskOpen();			/* Peripheral Mass Storage Class 
												Task Open */
/* Condition compilation by the difference of the operating system */
#if USBC_FW_PP == USBC_FW_NONOS_PP
	usbc_cstd_ScheInit();				/* Host scheduler init */
	usb_pmsc_SetTaskPri();				/* Set Task Priority */
#endif	/* USBC_FW_PP == USBC_FW_NONOS_PP */

	usb_cstd_SetHwFunction(USBC_PERI);	/* Set H/W function */

	/* FCU initialisiation */
	flash_Initialize();
	
	DBG_printf(("\n\n\n------ start GR-SAKURA board ------\n"));
}
/******************************************************************************
End of function
******************************************************************************/

/******************************************************************************
Function Name   : usbc_cstd_MainTaskStart
Description     : Reset Start program
Arguments       : none
Return value    : none
******************************************************************************/
void usbc_cstd_MainTaskStart(void)
{
/* Condition compilation by the difference of the operating system */
#if USBC_FW_PP == USBC_FW_OS_PP

/* Condition compilation by the difference of the device's operating system */
 #if USBC_OS_CRE_MODE_PP == USBC_OS_CRE_USE_PP
	USBC_ER_t	err;
	USBC_TSK_t	tskinfo;
	USBC_MBX_t	mbxinfo;
	USBC_MPL_t	mplinfo;
	USBC_SEM_t	seminfo;
 #endif /* USBC_OS_CRE_MODE_PP == USBC_OS_CRE_USE_PP */

	vsta_knl();

/* Condition compilation by the difference of the device's operating system */
 #if USBC_OS_CRE_MODE_PP == USBC_OS_CRE_USE_PP
	/* Create application task */
	/* Attribute */
	tskinfo.tskatr	= (USBC_ATR_t)(USBC_TA_HLNG);
	/* Start address */
	tskinfo.task	= (USBC_FP_t)&usb_cstd_MainTask;
	/* Priority */
	tskinfo.itskpri	= (USBC_PRI_t)(USB_MAIN_PRI);
	/* Stack size */
	tskinfo.stksz	= (USBC_SIZ_t)(USB_TSK_STK);
	/* Stack address */
	tskinfo.stk		= (USBC_VP_t)(USBC_NULL);
	err = USBC_CRE_TSK(USB_MAIN_TSK, &tskinfo);
	if( err != USBC_E_OK )
	{
		USBC_PRINTF1("CRE_TSK USB_MAIN_TSK Error (%ld)\n", err);
		while( 1 )
		{
			/* do nothing */
		}
	}

	/* Create host class driver mail box */
	/* Attribute */
	mbxinfo.mbxatr	= (USBC_ATR_t)((USBC_TA_TFIFO) | (USBC_TA_MFIFO));
	/* Priority */
	mbxinfo.maxmpri	= (USBC_PRI_t)(USB_MBX_PRI);
	/* Header address */
	mbxinfo.mprihd	= (USBC_VP_t)(USBC_NULL);
	err = USBC_CRE_MBX(USB_CLS_MBX, &mbxinfo);
	if( err != USBC_E_OK )
	{
		USBC_PRINTF1("CRE_MBX USB_CLS_MBX Error (%ld)\n", err);
		while( 1 )
		{
			/* do nothing */
		}
	}

	/* Create host class driver memory pool */
	/* Attribute */
	mplinfo.mpfatr	= (USBC_ATR_t)(USBC_TA_TFIFO);
	/* Block count */
	mplinfo.blkcnt	= (USBC_UINT_t)(USB_BLK_CNT);
	/* Block size */
	mplinfo.blksz	= (USBC_UINT_t)(USB_BLK_SIZ);
	/* Start address */
	mplinfo.mpf		= (USBC_VP_t)(USBC_NULL);
	err = USBC_CRE_MPL(USB_CLS_MPL, &mplinfo);
	if( err != USBC_E_OK )
	{
		USBC_PRINTF1("CRE_MPL USB_CLS_MPL Error (%ld)\n", err);
		while( 1 )
		{
			/* do nothing */
		}
	}

	/* Start task */
	err = USBC_STA_TSK(USB_MAIN_TSK, 0L);
	if( err != USBC_E_OK )
	{
		USBC_PRINTF1("STA_TSK USB_MAIN_TSK Error (%ld)\n", err);
		while( 1 )
		{
			/* do nothing */
		}
	}
 #endif /* USBC_OS_CRE_MODE_PP == USBC_OS_CRE_USE_PP */

#endif /* USBC_FW_PP == USBC_FW_OS_PP */
}
/******************************************************************************
End of function
******************************************************************************/

/******************************************************************************
Function Name   : usb_cstd_MainTask
Description     : main task process
Arguments       : USB_VP_INT stacd      : task start code(not use)
Return value    : none
******************************************************************************/
void usbc_cstd_MainTask(USBC_VP_INT stacd)
{
	/* Sample application initial process */
	usb_pmsc_MainInit();

#if 0	// DEMO_PRG
	/* Idle Task Start process */
	usbc_cstd_IdleTaskStart();
#endif

	/* Sample application main loop process */
	while( 1 )
	{
#if 1	// DEMO_PRG
		static BOOL		fStartUserApplication = FALSE;
#endif

		//
		//	メインループ実行
		//
		usb_pmsc_MainLoop();

		//
		//	IDLE_TIME_FOR_NO_USB_ENUM以内に、USBリクエストが無いようであれば
		//	ユーザーアプリケーションへジャンプ
		//
		if( (usb_gpstd_ReqType == (uint16_t)-1) &&
			(timer_GetPastTime( 0 ) > IDLE_TIME_FOR_NO_USB_ENUM) )
		{
			usb_pmsc_Disconnect( TRUE );	// USB接続を切断する
			userapl_reset_to_user_application();
		}
	
#if 0	// DEMO_PRG
/* Condition compilation by the difference of the operating system */
#if USBC_FW_PP == USBC_FW_NONOS_PP
		/* Idle Task (sleep sample) */
		usbc_cstd_IdleTask(0);
#endif /* USB_FW_PP == USB_FW_NONOS_PP */
#endif

#if 1	// DEMO_PRG

		//
		//	青スイッチが押されたら、ユーザーアプリケーションの実行を開始する
		//
		/*if( usbc_cpu_GetKeyNo() == USBC_ON )
		{
			fStartUserApplication = TRUE;
		}*/
		
		//
		//	最後に書き込みがあってから一定時間が経過して、○○○.binが見つかれば、
		//	ユーザーアプリケーションの実行を開始する
		//
		if( (usb_gpmsc_Atapi_timeLastWrite != 0) &&
			(timer_GetPastTime( usb_gpmsc_Atapi_timeLastWrite ) > IDLE_TIME_FOR_WRITE_END) )
		{
			usb_gpmsc_Atapi_timeLastWrite = 0;
			
			if( fatfs_FindUserApplicationFileInfo() != NULL )
			{
				fStartUserApplication = TRUE;
			}				
		}

		//
		//	ユーザーアプリケーションを実行開始する
		//	もう帰ってこない
		//
		if( fStartUserApplication )
		{
			uint8_t	nCount;

			//
			// まだ通信中の可能性があるので、通信が終わるまで待つ
			//
			for( nCount = 0; nCount <= 5; nCount ++ )
			{
				if( R_usbc_cstd_CheckSchedule() == USBC_FLGSET )
				{
					/* USB Peripheral control driver Task */
					usb_pstd_PcdTask((USBC_VP_INT_t)0);
					/* Execute ATAPI commands Process */
					usb_pmsc_SmpAtapiTask((USBC_VP_INT_t)0);
					/* Peripheral Mass Storage Class Task */
					usb_pmsc_Task();
					
					nCount = 0;
				}
			
				/* Run Schecduler */
				R_usbc_cstd_Scheduler();
			}
			
			// USB接続を切断する
			usb_pmsc_Disconnect( TRUE );
			
			//
			//	ユーザーアプリケーションの実行開始
			//	（もう帰ってこない）
			//
			userapl_StartExecution();
		}

#endif
	}
}
/******************************************************************************
End of function
******************************************************************************/

/* Condition compilation by the difference of the operating system */
#if USBC_FW_PP == USBC_FW_OS_PP
/******************************************************************************
Function Name   : usb_pmsc_MainLoop
Description     : Delay Task
Argument        : none
Return          : none
******************************************************************************/
void usb_pmsc_MainLoop(void)
{
	USBC_DLY_TSK(100);
}
/******************************************************************************
End of function
******************************************************************************/
#endif	/* USBC_FW_PP == USBC_FW_OS_PP */

/* Condition compilation by the difference of the operating system */
#if USBC_FW_PP == USBC_FW_NONOS_PP
/******************************************************************************
Function Name   : usb_pmsc_MainLoop
Description     : Main Loop
Argument        : none
Return          : none
******************************************************************************/
void usb_pmsc_MainLoop(void)
{
	if( R_usbc_cstd_CheckSchedule() == USBC_FLGSET )
	{
		/* USB Peripheral control driver Task */
		usb_pstd_PcdTask((USBC_VP_INT_t)0);
		/* Execute ATAPI commands Process */
		usb_pmsc_SmpAtapiTask((USBC_VP_INT_t)0);
		/* Peripheral Mass Storage Class Task */
		usb_pmsc_Task();
	}
	/* Run Schecduler */
	R_usbc_cstd_Scheduler();
}
/******************************************************************************
End of function
******************************************************************************/

/******************************************************************************
Function Name   : usb_pmsc_SetTaskPri
Description     : Set Task Priority
Argument        : none
Return          : none
******************************************************************************/
void usb_pmsc_SetTaskPri(void)
{
	uint16_t	i;
	uint8_t	taskpri[USBC_PRIMAX * 4] =	/* Scheduler priority table */
	{
		USB_PCD_TSK,	USB_PCD_PRI,
		USB_PMSC_TSK,	USB_PMSC_PRI,
		USB_PFLSH_TSK,	USB_PFLSH_PRI,
		USBC_IDL_TSK,	USBC_IDL_PRI,
	};

	for( i = 0; i <= (USBC_PRIMAX * 3); i = i + 2 )
	{
		/* Set Task priority */
		R_usbc_cstd_SetTaskPri(taskpri[i], taskpri[i + 1]);
	}
}
/******************************************************************************
End of function
******************************************************************************/
#endif	/* USBC_FW_PP == USBC_FW_NONOS_PP */

/******************************************************************************
Function Name   : usb_pmsc_Open
Description     : Sample Open
Argument        : none
Return          : none
******************************************************************************/
void usb_pmsc_Open(void)
{
	USBC_ER_t	err;
/* Condition compilation by the difference of the device's operating system */
#if USBC_OS_CRE_MODE_PP == USBC_OS_CRE_USE_PP
	USBC_TSK_t	tskinfo;
	USBC_MBX_t	mbxinfo;
	USBC_MPL_t	mplinfo;
	USBC_RTST_t	refinfo;

	err = USBC_REF_TST(USB_PMSC_TSK, &refinfo);	/* Check task status */
	if( err == USBC_E_OK )		/* Is HCD task opened ? */
	{
		/* USBC_PRINTF1("Already started PMSC_TSK (%ld)\n", err); */
		return;
	}

	USBC_PRINTF0("*** Open PMSC application ***\n");

	/* Create peri FW test driver task */
	tskinfo.tskatr	= (USB_ATR_t)(TA_HLNG);				/* Attribute */
	tskinfo.task	= &usb_pmsc_MainTask;				/* Start address */
	tskinfo.itskpri	= (USB_PRI_t)(USB_PMSC_PRI);		/* Priority */
	tskinfo.stksz	= (USB_SIZ_t)(USB_TSK_STK);			/* Stack size */
	tskinfo.stk		= (USB_VP_t)(NULL);					/* Stack address */
	err = USBC_CRE_TSK(USB_PMSC_TSK, &tskinfo);
	if( err != USBC_E_OK )
	{
		USBC_PRINTF1("CRE_TSK USB_PMSC_TSK Error (%ld)\n", err);
		while( 1 )
		{
			/* Do nothing */;
		}
	}

	/* Create peri FW test driver mailbox */
	mbxinfo.mbxatr	= (USB_ATR_t)((TA_TFIFO) | (TA_MFIFO));	/* Attribute */
	mbxinfo.maxmpri	= (USB_PRI_t)(USB_MBX_PRI);				/* Priority */
	mbxinfo.mprihd	= (USB_VP_t)(NULL);					/* Header address */
	err = USBC_CRE_MBX(USB_PMSC_MBX, &mbxinfo);
	if( err != USBC_E_OK ) 
	{
		USBC_PRINTF1("CRE_MBX USB_PMSC_MBX Error (%ld)\n", err);
		while( 1 )
		{
			/* Do nothing */;
		}
	}

	/* Create peri FW test driver memorypool */
	mplinfo.mpfatr	= (USB_ATR_t)(TA_TFIFO);		/* Attribute */
	mplinfo.blkcnt	= (USB_UINT_t)(USB_BLK_CNT);	/* Block count */
	mplinfo.blksz	= (USB_UINT_t)(USB_BLK_SIZ);	/* Block size */
	mplinfo.mpf		= (USB_VP_t)(NULL);				/* Start address */
	err = USBC_CRE_MPL(USB_PMSC_MPL, &mplinfo);
	if( err != USBC_E_OK )
	{
		USBC_PRINTF1("CRE_MPL USB_PMSC_MPL Error (%ld)\n", err);
		while( 1 )
		{
			/* Do nothing */;
		}
	}

	/* Start  peri FW test driver task */
	err = USBC_STA_TSK(USB_PMSC_TSK, 0);
	if( err != USBC_E_OK )
	{
		USBC_PRINTF1("STA_TSK USB_PMSC_TSK Error (%ld)\n", err);
		while( 1 )
		{
			/* Do nothing */;
		}
	}
#endif	/* USBC_OS_CRE_MODE_PP == USBC_OS_CRE_USE_PP */

/* Condition compilation by the difference of the device's operating system */
#if USBC_OS_CRE_MODE_PP == USBC_OS_CRE_NOTUSE_PP
	err = USBC_ACT_TSK(USB_PMSC_TSK);
	if( err != USBC_E_OK )
	{
		USBC_PRINTF1("ACT_TSK USB_PMSC_TSK Error (%ld)\n", err);
		while( 1 )
		{
			/* Do nothing */;
		}
	}
#endif	/* USBC_OS_CRE_MODE_PP == USBC_OS_CRE_NOTUSE_PP */
}
/******************************************************************************
End of function
******************************************************************************/

/******************************************************************************
Function Name   : usb_pmsc_Close
Description     : Sample Close
Argument        : none
Return          : none
******************************************************************************/
void usb_pmsc_Close(void)
{
/* Condition compilation by the difference of the operating system */
#if USBC_FW_PP == USBC_FW_NONOS_PP
	/* Nothing */
#else	/* USBC_FW_PP == USBC_FW_NONOS_PP */
	USBC_ER_t	err;
/* Condition compilation by the difference of the device's operating system */
#if USBC_OS_CRE_MODE_PP == USBC_OS_CRE_USE_PP
	USB_RTST_t	refinfo;

	err = USBC_REF_TST(USB_PMSC_TSK, &refinfo);		/* Check task status */
	if( err == USBC_E_OK )					/* Is PMSC task opened ? */
	{
		USBC_PRINTF0("*** Release PMSC driver ***\n");

		/* Stop peri FW test driver task */
		err = USBC_TER_TSK(USB_PMSC_TSK);

		/* Delete peri FW test driver memorypool */
		err = USBC_DEL_MPL(USB_PMSC_MPL);

		/* Delete peri FW test driver mailbox */
		err = USBC_DEL_MBX(USB_PMSC_MBX);

		/* Delete peri FW test driver task */
		err = USBC_DEL_TSK(USB_PMSC_TSK);
	}
#else	/* USBC_OS_CRE_MODE_PP == USBC_OS_CRE_USE_PP */
		err = USBC_TER_TSK(USB_PMSC_TSK);
		if( err != USBC_E_OK ) {
			USBC_PRINTF1("TER_TSK USB_PMSC_TSK Error (%ld)\n", err);
			while( 1 ) {
				/* Do nothing */;
			}
		}
#endif	/* USBC_OS_CRE_MODE_PP == USBC_OS_CRE_USE_PP */
#endif	/* USBC_FW_PP == USBC_FW_NONOS_PP */
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pmsc_Disconnect
Description     : Disconnect from USB and Disable USB module
Argument        : Detach driver(TRUE) / Disable USB only(FALSE)
Return          : none
******************************************************************************/
void usb_pmsc_Disconnect( const BOOL fDetachDriver )
{
	/* Disable USB module */
	extern void		usb_cstd_UsbClear( void );
	
	usbc_cpu_LedSet( 0x00 );		// LED消灯
	usbc_cpu_DelayXms( 500 );		// 念のためWAIT
	if( fDetachDriver )
	{
		usb_pstd_DetachProcess();	// USBバスからの切断処理、USBドライバの停止処理
	}
	usb_cstd_UsbClear();			// USB動作を停止
	usbc_cpu_DelayXms( 500 );		// 念のためWAIT
}


/******************************************************************************
* Function Name	:	userapl_jump_to_user_application()
* Description	:	Jump to User Application
* Argument		:	none
* Return		:	none (not return)
******************************************************************************/
static void userapl_jump_to_user_application( void )
{
	#define	JUMP(_addr_)		((void(*)())_addr_)()
	
	JUMP( USBC_PMSC_USER_APPLICATION_ADDR );
}
/******************************************************************************
End of function
******************************************************************************/

/******************************************************************************
* Function Name	:	userapl_reset_to_user_application()
* Description	:	Reset to User Application
* Argument		:	none
* Return		:	none (not return)
******************************************************************************/
static void userapl_reset_to_user_application( void )
{
	SYSTEM.PRCR.WORD	= 0xA503;	// レジスタプロテクション解除
	SYSTEM.SWRR			= 0xA501;	// ソフトウェアリセット
}
/******************************************************************************
End of function
******************************************************************************/

/******************************************************************************
* Function Name	:	userapl_DispProgress()
* Description	:	Display progress
* Argument		:	none
* Return		:	none (not return)
******************************************************************************/
static void userapl_DispProgress( void )
{
	static uint8_t	bmLed = 0x01;

	usbc_cpu_LedSet( bmLed );
	
	bmLed <<= 1;
	if( bmLed >= 0x10 )
	{
		bmLed = 0x01;
	}
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
* Function Name	:	userapl_StartExecution()
* Description	:	Start user application
* Argument		:	none
* Return		:	none (not return)
******************************************************************************/
void userapl_StartExecution( void )
{
	PDIRENT			pdeFileInfo;
	uint16_t		nClusNo;
	uint32_t		nLeftSize, addrWrite;
	const uint32_t	timeStart = timer_GetTimeCount();

	extern uint8_t	usb_gpmsc_AtapiDataBuffer[];
	uint8_t*		pbBuffer = usb_gpmsc_AtapiDataBuffer;	// 間借りします
	
	// LEDを消灯
	usbc_cpu_LedSet( 0x00 );

	//
	//	ファイル情報から○○○.binファイルを検索
	//
	pdeFileInfo = fatfs_FindUserApplicationFileInfo();
	if( pdeFileInfo == NULL )
	{
		userapl_ErrorEnd();
	}
	if( pdeFileInfo->nFileSize == 0 )
	{
		userapl_ErrorEnd();
	}
	
	//
	//	ユーザーAPL領域を全て消去する
	//
	for( addrWrite = USBC_PMSC_USER_APPLICATION_ADDR;
		 addrWrite <= USBC_PMSC_USER_APPLICATION_END; )
	{
		userapl_DispProgress();
			
		if( flash_coderom_EraseBlock( addrWrite ) != FLASH_SUCCESS )
		{
			userapl_ErrorEnd();
		}
		
		//
		//	次のブロックアドレス
		//
		if( addrWrite >= 0xFFFF8000 )
		{
			addrWrite += 4*1024;
		}
		else if( addrWrite >= 0xFFF80000 )
		{
			addrWrite += 16*1024;
		}
		else if( addrWrite >= 0xFFF00000 )
		{
			addrWrite += 32*1024;
		}
		else if( addrWrite >= 0xFFE00000 )
		{
			addrWrite += 64*1024;
		}
		else
		{
			userapl_ErrorEnd();	// アリエナイ！
		}
	}

	//
	//	○○○.binのデータをユーザーAPL領域へコピーする
	//
	nClusNo		= pdeFileInfo->nClusNo;
	nLeftSize	= pdeFileInfo->nFileSize;
	addrWrite	= USBC_PMSC_USER_APPLICATION_ADDR;

	while( (nLeftSize > 0) && (nClusNo != (uint16_t)-1) )
	{
		void*	pStorageData;

		//
		//	書き込みデータの準備
		//
		userapl_DispProgress();

		pStorageData = fatfs_GetClusterData( nClusNo );
		if( nLeftSize >= FATFS_CLUSTER_SIZE )
		{
			memcpy( pbBuffer, pStorageData, FATFS_CLUSTER_SIZE );
			nLeftSize -= FATFS_CLUSTER_SIZE;
		}
		else
		{
			memcpy( pbBuffer, pStorageData, nLeftSize );
			memset( pbBuffer + nLeftSize, 0, (FATFS_CLUSTER_SIZE-nLeftSize) );	// Wipe invalid area
			nLeftSize = 0;
		}
		
		//
		//	データを書き込み
		//
		userapl_DispProgress();
			
		if( flash_coderom_WriteData( addrWrite, pbBuffer, FATFS_CLUSTER_SIZE ) != FLASH_SUCCESS )
		{
			userapl_ErrorEnd();
		}
		
		//
		//	次のクラスタ番号を取得する
		//
		nClusNo = fatfs_GetNextClusterNo( nClusNo );
		
		addrWrite += FATFS_CLUSTER_SIZE;

	} //end of while( (nLeftSize > 0) && (nClusNo != (uint16_t)-1) )

	//
	//	LEDを消灯（ただし、最低１秒はLEDを点灯させる）
	//
	while( timer_GetPastTime( timeStart ) < 1000 )
	{
		usbc_cpu_DelayXms( 100 );
		userapl_DispProgress();
	}
	usbc_cpu_LedSet( 0x00 );
	usbc_cpu_DelayXms( 500 );

	//
	//	ユーザーAPL領域へジャンプする
	//
	userapl_reset_to_user_application();
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
* Function Name	:	userapl_ErrorEnd()
* Description	:	Error occured!
* Argument		:	none
* Return		:	none (not return)
******************************************************************************/
void userapl_ErrorEnd( void )
{
	while( 1 )
	{
		uint32_t	i;
		usbc_cpu_LedSet( 0xFF );
		for( i = 0; i < 2000000; i ++ );
		usbc_cpu_LedSet( 0x00 );
		for( i = 0; i < 2000000; i ++ );
	}
}
/******************************************************************************
End of function
******************************************************************************/


#pragma	section	_UserApplicationArea

/******************************************************************************
* Function Name	:	userapl_DummyCode()
* Description	:	Dummy code on user application area.
* Argument		:	none
* Return		:	none (not return)
******************************************************************************/
void userapl_DummyCode( void )
{
	userapl_ErrorEnd();
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
End  Of File
******************************************************************************/
