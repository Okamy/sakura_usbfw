/******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized.
* This software is owned by Renesas Electronics Corporation and is  protected
* under all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES
* REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY,
* INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR  A
* PARTICULAR PURPOSE AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE  EXPRESSLY
* DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE  LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES
* FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS
* AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this
* software and to discontinue the availability of this software.
* By using this software, you agree to the additional terms and
* conditions found by accessing the following link:
* http://www.renesas.com/disclaimer
******************************************************************************
* Copyright (C) 2010(2011) Renesas Electronics Corpration
* and Renesas Solutions Corp. All rights reserved.
******************************************************************************
* File Name    : r_usbc_cDefUSBIP.h
* Version      : 1.10
* Device(s)    : Renesas SH-Series, RX-Series
* Tool-Chain   : Renesas SuperH RISC engine Standard Toolchain
*              : Renesas RX Standard Toolchain
* OS           : Common to None and uITRON 4.0 Spec
* H/W Platform : Independent
* Description  : USB definition for IP
******************************************************************************
* History : DD.MM.YYYY Version Description
*         : 17.03.2010 0.80    First Release
*         : 30.07.2010 0.90    Updated comments
*         : 02.08.2010 0.91    Updated comments
*         : 29.10.2010 1.00    Mass Production Release
*         : 01.06.2011 1.10    Version 1.10 Release
******************************************************************************/
#ifndef __R_USBC_CDEFUSBIP_H__
#define __R_USBC_CDEFUSBIP_H__


/*****************************************************************************
Macro definitions
******************************************************************************/

/******************************************************************************
USB specification define
******************************************************************************/

/* Descriptor type	Define */
/* Configuration Descriptor */
#define	USBC_DT_DEVICE					0x01u
/* Configuration Descriptor */
#define	USBC_DT_CONFIGURATION			0x02u
/* Configuration Descriptor */
#define	USBC_DT_STRING					0x03u
/* Interface Descriptor */
#define	USBC_DT_INTERFACE				0x04u
/* Endpoint Descriptor */
#define	USBC_DT_ENDPOINT				0x05u
/* Device Qualifier Descriptor */
#define	USBC_DT_DEVICE_QUALIFIER		0x06u
/* Other Speed Configuration Descriptor */
#define	USBC_DT_OTHER_SPEED_CONF		0x07u
/* Interface Power Descriptor */
#define	USBC_DT_INTERFACE_POWER			0x08u
/* OTG Descriptor */
#define	USBC_DT_OTGDESCRIPTOR			0x09u
/* HUB descriptor */
#define	USBC_DT_HUBDESCRIPTOR			0x29u

/* Device class Define	*/
/* Class information at interface */
#define	USBC_DEVCLS_INTF				0x00u
/* Communication Device */
#define	USBC_DEVCLS_COMM				0x02u
/* HUB Device */
#define	USBC_DEVCLS_HUB					0x90u
/* Diagnostic Device */
#define	USBC_DEVCLS_DIAG				0xDCu
/* Wireless Controller */
#define	USBC_DEVCLS_WIRE				0xE0u
/* Application-Specific */
#define	USBC_DEVCLS_APL					0xFEu
/* Vendor-Specific */
#define	USBC_DEVCLS_VEN					0xFFu

/* Interface class Define */
/* Un corresponding Class */
#define	USBC_IFCLS_NOT					0x00u
/* Audio Class */
#define	USBC_IFCLS_AUD					0x01u
/* CDC-Control Class */
#define	USBC_IFCLS_CDCC					0x02u
/* HID Class */
#define	USBC_IFCLS_HID					0x03u
/* Physical Class */
#define	USBC_IFCLS_PHY					0x05u
/* Image Class */
#define	USBC_IFCLS_IMG					0x06u
/* Printer Class */
#define	USBC_IFCLS_PRN					0x07u
/* Mass Storage Class */
#define	USBC_IFCLS_MAS					0x08u
/* HUB Class */
#define	USBC_IFCLS_HUB					0x09u
/* CDC-Data Class */
#define	USBC_IFCLS_CDCD					0x0Au
/* Chip/Smart Card Class */
#define	USBC_IFCLS_CHIP					0x0Bu
/* Content-Security Class */
#define	USBC_IFCLS_CNT					0x0Cu
/* Video Class */
#define	USBC_IFCLS_VID					0x0Du
/* Diagnostic Device */
#define	USBC_IFCLS_DIAG					0xDCu
/* Wireless Controller */
#define	USBC_IFCLS_WIRE					0xE0u
/* Application-Specific */
#define	USBC_IFCLS_APL					0xFEu
/* Vendor-Specific Class */
#define	USBC_IFCLS_VEN					0xFFu

/* Endpoint Descriptor	Define */
/* Endpoint direction mask [2] */
#define	USBC_EP_DIRMASK					0x80u
/* In  Endpoint */
#define	USBC_EP_IN						0x80u
/* Out Endpoint */
#define	USBC_EP_OUT						0x00u
/* Endpoint number mask [2] */
#define	USBC_EP_NUMMASK					0x0Fu
/* Usage type mask [2] */
#define	USBC_EP_USGMASK					0x30u
/* Synchronization type mask [2] */
#define	USBC_EP_SYNCMASK				0x0Cu
/* Transfer type mask [2] */
#define	USBC_EP_TRNSMASK				0x03u
/* Control	   Transfer */
#define	USBC_EP_CNTRL					0x00u
/* Isochronous Transfer */
#define	USBC_EP_ISO						0x01u
/* Bulk		   Transfer */
#define	USBC_EP_BULK					0x02u
/* Interrupt   Transfer */
#define	USBC_EP_INT						0x03u

/* Configuration descriptor bit define */
/* Reserved(set to 1) */
#define	USBC_CF_RESERVED				0x80u
/* Self Powered */
#define	USBC_CF_SELF					0x40u
/* Remote Wakeup */
#define	USBC_CF_RWUP					0x20u

/* OTG descriptor bit define */
/* HNP support */
#define	USBC_OTG_HNP					0x02u
/* SRP support */
#define	USBC_OTG_SRP					0x01u

/* USB Standard request */
/* USBC_BREQUEST				0xFF00u(b15-8) */
#define	USBC_GET_STATUS					0x0000u
#define	USBC_CLEAR_FEATURE				0x0100u
#define	USBC_REQRESERVED				0x0200u
#define	USBC_SET_FEATURE				0x0300u
#define	USBC_REQRESERVED1				0x0400u
#define	USBC_SET_ADDRESS				0x0500u
#define	USBC_GET_DESCRIPTOR				0x0600u
#define	USBC_SET_DESCRIPTOR				0x0700u
#define	USBC_GET_CONFIGURATION			0x0800u
#define	USBC_SET_CONFIGURATION			0x0900u
#define	USBC_GET_INTERFACE				0x0A00u
#define	USBC_SET_INTERFACE				0x0B00u
#define	USBC_SYNCH_FRAME				0x0C00u

/* USBC_BMREQUESTTYPEDIR		0x0080u(b7) */
#define	USBC_HOST_TO_DEV				0x0000u
#define	USBC_DEV_TO_HOST				0x0080u

/* USBC_BMREQUESTTYPETYPE	0x0060u(b6-5) */
#define	USBC_STANDARD					0x0000u
#define	USBC_CLASS						0x0020u
#define	USBC_VENDOR						0x0040u

/* USBC_BMREQUESTTYPERECIP	0x001Fu(b4-0) */
#define	USBC_DEVICE						0x0000u
#define	USBC_INTERFACE					0x0001u
#define	USBC_ENDPOINT					0x0002u
#define	USBC_OTHER						0x0003u

/* GET_STATUS request information */
/* Standard Device status */
#define	USBC_GS_BUSPOWERD				0x0000u
#define	USBC_GS_SELFPOWERD				0x0001u
#define	USBC_GS_REMOTEWAKEUP			0x0002u

/* Endpoint status */
#define	USBC_GS_NOTHALT					0x0000u
#define	USBC_GS_HALT					0x0001u

/* CLEAR_FEATURE/GET_FEATURE/SET_FEATURE request information */
/* Standard Feature Selector */
#define	USBC_ENDPOINT_HALT				0x0000u
#define	USBC_DEV_REMOTE_WAKEUP			0x0001u
#define	USBC_TEST_MODE					0x0002u

/* GET_DESCRIPTOR/SET_DESCRIPTOR request information */
/* Standard Descriptor type */
#define	USBC_HUB_DESCRIPTOR				0x0000u
#define	USBC_DEV_DESCRIPTOR				0x0100u
#define	USBC_CONF_DESCRIPTOR			0x0200u
#define	USBC_STRING_DESCRIPTOR			0x0300u
#define	USBC_INTERFACE_DESCRIPTOR		0x0400u
#define	USBC_ENDPOINT_DESCRIPTOR		0x0500u
#define	USBC_DEV_QUALIFIER_DESCRIPTOR	0x0600u
#define	USBC_OTHER_SPEED_CONF_DESCRIPTOR	0x0700u
#define	USBC_INTERFACE_POWER_DESCRIPTOR	0x0800u

/* HUB CLASS REQUEST */
#define	USBC_HUB_CLEAR_TT_BUFFER		0x0800u
#define	USBC_HUB_RESET_TT				0x0900u
#define	USBC_HUB_GET_TT_STATE			0x0A00u
#define	USBC_HUB_STOP_TT				0x0B00u

/* HUB CLASS FEATURE SELECTER */
#define	USBC_HUB_C_HUB_LOCAL_POWER		0x0000u
#define	USBC_HUB_C_HUB_OVER_CURRENT		0x0001u
#define	USBC_HUB_PORT_CONNECTION		0x0000u
#define	USBC_HUB_PORT_ENABLE			0x0001u
#define	USBC_HUB_PORT_SUSPEND			0x0002u
#define	USBC_HUB_PORT_OVER_CURRENT		0x0003u
#define	USBC_HUB_PORT_RESET				0x0004u
#define	USBC_HUB_PORT_POWER				0x0008u
#define	USBC_HUB_PORT_LOW_SPEED			0x0009u
#define	USBC_HUB_PORT_HIGH_SPEED		0x000Au
#define	USBC_HUB_C_PORT_CONNECTION		0x0010u
#define	USBC_HUB_C_PORT_ENABLE			0x0011u
#define	USBC_HUB_C_PORT_SUSPEND			0x0012u
#define	USBC_HUB_C_PORT_OVER_CURRENT	0x0013u
#define	USBC_HUB_C_PORT_RESET			0x0014u
#define	USBC_HUB_PORT_TEST				0x0015u
#define	USBC_HUB_PORT_INDICATOR			0x0016u

/* HUB PORT STAUS */
#define	USBC_HUB_STS_PORT_CONNECT		0x0001u
#define	USBC_HUB_STS_PORT_ENABLE		0x0002u
#define	USBC_HUB_STS_PORT_SUSPEND		0x0004u
#define	USBC_HUB_STS_PORT_OVRCURRNET	0x0008u
#define	USBC_HUB_STS_PORT_RESET			0x0010u
#define	USBC_HUB_STS_PORT_POWER			0x0100u
#define	USBC_HUB_STS_PORT_LOWSPEED		0x0200u
#define	USBC_HUB_STS_PORT_FULLSPEED		0x0000u
#define	USBC_HUB_STS_PORT_HIGHSPEED		0x0400u
#define	USBC_HUB_STS_PORT_TEST			0x0800u
#define	USBC_HUB_STS_PORT_INDICATOR		0x1000u

/* HUB PORT CHANGE */
#define	USBC_HUB_CHG_PORT_CONNECT		0x0001u
#define	USBC_HUB_CHG_PORT_ENABLE		0x0002u
#define	USBC_HUB_CHG_PORT_SUSPEND		0x0004u
#define	USBC_HUB_CHG_PORT_OVRCURRNET	0x0008u
#define	USBC_HUB_CHG_PORT_RESET			0x0010u


/******************************************************************************
USB-H/W register define
******************************************************************************/

/* Root port */
#define	USBC_PORT0			0u
/* Condition compilation by the difference of IP */
#if USBC_IPSEL_PP == USBC_597IP_PP
#define	USBC_PORT1			1u
#endif	/* USBC_597IP_PP */

/* Device connect information */
#define	USBC_ATTACH			0x0040u
#define	USBC_ATTACHL		0x0041u
#define	USBC_ATTACHF		0x0042u
#define	USBC_DETACH			0x0043u

/* Reset Handshake result */
#define	USBC_NOCONNECT		0x0000u	/* Speed undecidedness */
#define	USBC_HSCONNECT		0x00C0u	/* Hi-Speed connect */
#define	USBC_FSCONNECT		0x0080u	/* Full-Speed connect */
#define	USBC_LSCONNECT		0x0040u	/* Low-Speed connect */

/* Pipe define */
#define	USBC_USEPIPE		0x00FEu
#define	USBC_PERIPIPE		0x00FDu
#define	USBC_PIPE0			0x0000u	/* PIPE 0 */
#define	USBC_PIPE1			0x0001u	/* PIPE 1 */
#define	USBC_PIPE2			0x0002u	/* PIPE 2 */
#define	USBC_PIPE3			0x0003u	/* PIPE 3 */
#define	USBC_PIPE4			0x0004u	/* PIPE 4 */
#define	USBC_PIPE5			0x0005u	/* PIPE 5 */
#define	USBC_PIPE6			0x0006u	/* PIPE 6 */
#define	USBC_PIPE7			0x0007u	/* PIPE 7 */
/* Condition compilation by the difference of IP */
#if USBC_IPSEL_PP == USBC_597IP_PP
#define	USBC_PIPE8			0x0008u	/* PIPE 8 */
#define	USBC_PIPE9			0x0009u	/* PIPE 9 */
#define	USBC_MAX_PIPE_NO	9u		/* PIPE0 ... PIPE9 */
#endif	/* USBC_597IP_PP */
/* Condition compilation by the difference of IP */
#if USBC_IPSEL_PP != USBC_597IP_PP
#define	USBC_MAX_PIPE_NO	7u		/* PIPE0 ... PIPE7 */
#endif	/* != USBC_597IP_PP */

/* Pipe configuration table define */
#define	USBC_EPL			6u		/* Pipe configuration table length */
#define	USBC_TYPFIELD		0xC000u	/* Transfer type */
#define	  USBC_PERIODIC		0x8000u	/* Periodic pipe */
#define	  USBC_ISO			0xC000u	/* Isochronous */
#define	  USBC_INT			0x8000u	/* Interrupt */
#define	  USBC_BULK			0x4000u	/* Bulk */
#define	  USBC_NOUSE		0x0000u	/* Not configuration */
#define	USBC_BFREFIELD		0x0400u	/* Buffer ready interrupt mode select */
#define	  USBC_BFREON		0x0400u
#define	  USBC_BFREOFF		0x0000u
#define	USBC_DBLBFIELD		0x0200u	/* Double buffer mode select */
#define	  USBC_DBLBON		0x0200u
#define	  USBC_DBLBOFF		0x0000u
#define	USBC_CNTMDFIELD		0x0100u	/* Continuous transfer mode select */
#define	  USBC_CNTMDON		0x0100u
#define	  USBC_CNTMDOFF		0x0000u
#define	USBC_SHTNAKFIELD	0x0080u	/* Transfer end NAK */
#define	  USBC_SHTNAKON		0x0080u
#define	  USBC_SHTNAKOFF	0x0000u
#define	USBC_DIRFIELD		0x0010u	/* Transfer direction select */
#define	  USBC_DIR_H_OUT	0x0010u	/* HOST OUT */
#define	  USBC_DIR_P_IN		0x0010u	/* PERI IN */
#define	  USBC_DIR_H_IN		0x0000u	/* HOST IN */
#define	  USBC_DIR_P_OUT	0x0000u	/* PERI OUT */
#define	  USBC_BUF2FIFO		0x0010u	/* Buffer --> FIFO */
#define	  USBC_FIFO2BUF		0x0000u	/* FIFO --> buffer */
#define	USBC_EPNUMFIELD		0x000Fu	/* Endpoint number select */
#define	USBC_MAX_EP_NO		15u		/* EP0 EP1 ... EP15 */
#define	  USBC_EP0			0x0000u
#define	  USBC_EP1			0x0001u
#define	  USBC_EP2			0x0002u
#define	  USBC_EP3			0x0003u
#define	  USBC_EP4			0x0004u
#define	  USBC_EP5			0x0005u
#define	  USBC_EP6			0x0006u
#define	  USBC_EP7			0x0007u
#define	  USBC_EP8			0x0008u
#define	  USBC_EP9			0x0009u
#define	  USBC_EP10			0x000Au
#define	  USBC_EP11			0x000Bu
#define	  USBC_EP12			0x000Cu
#define	  USBC_EP13			0x000Du
#define	  USBC_EP14			0x000Eu
#define	  USBC_EP15			0x000Fu

#define	USBC_BUF_SIZE(x)	((uint16_t)(((x) / 64u) - 1u) << 10u)
#define	USBC_BUF_NUMB(x)	(x)

#define	USBC_IFISFIELD		0x1000u	/* Isochronous in-buf flash mode */
#define	  USBC_IFISON		0x1000u
#define	  USBC_IFISOFF		0x0000u
#define	USBC_IITVFIELD		0x0007u	/* Isochronous interval */
#define	USBC_IITV_TIME(x)	(x)

/* FIFO port & access define */
#define	USBC_CUSE			0u		/* CFIFO CPU trans */
#define	USBC_D0USE			1u		/* D0FIFO CPU trans */
#define	USBC_D0DMA			2u		/* D0FIFO DMA trans */
#define	USBC_D1USE			3u		/* D1FIFO CPU trans */
#define	USBC_D1DMA			4u		/* D1FIFO DMA trans */
#define	USBC_CUSE2			5u		/* CFIFO CPU trans (not tran count) */


/******************************************************************************
Another define
******************************************************************************/
/* FIFO read / write result */
#define	USBC_FIFOERROR		USBC_ERROR	/* FIFO not ready */
/* End of write (but packet may not be outputting) */
#define	USBC_WRITEEND		0x0000u
/* End of write (send short packet) */
#define	USBC_WRITESHRT		0x0001u
/* Write continues */
#define	USBC_WRITING		0x0002u
/* End of read */
#define	USBC_READEND		0x0000u
/* Insufficient (receive short packet) */
#define	USBC_READSHRT		0x0001u
/* Read continues */
#define	USBC_READING		0x0002u
/* Buffer size over */
#define	USBC_READOVER		0x0003u

/* Pipe define table end code */
#define	USBC_PDTBLEND		0xFFFFu	/* End of table */

/* Transfer status Type */
#define	USBC_CTRL_END		0u
#define	USBC_DATA_NONE		1u
#define	USBC_DATA_WAIT		2u
#define	USBC_DATA_OK		3u
#define	USBC_DATA_SHT		4u
#define	USBC_DATA_OVR		5u
#define	USBC_DATA_STALL		6u
#define	USBC_DATA_ERR		7u
#define	USBC_DATA_STOP		8u
#define	USBC_DATA_TMO		9u
#define	USBC_CTRL_READING	17u
#define	USBC_CTRL_WRITING	18u
#define	USBC_DATA_READING	19u
#define	USBC_DATA_WRITING	20u

/* Utr member (segment) */
#define	USBC_TRAN_CONT		0x00u
#define	USBC_TRAN_END		0x80u

/* Callback argument */
#define	USBC_NO_ARG			0u

/* USB interrupt type (common)*/
#define	USBC_INT_UNKNOWN	0x0000u
#define	USBC_INT_BRDY		0x0001u
#define	USBC_INT_BEMP		0x0002u
#define	USBC_INT_NRDY		0x0003u
/* USB interrupt type (PERI)*/
#define	USBC_INT_VBINT		0x0011u
#define	USBC_INT_RESM		0x0012u
#define	USBC_INT_SOFR		0x0013u
#define	USBC_INT_DVST		0x0014u
#define	USBC_INT_CTRT		0x0015u
#define	USBC_INT_ATTACH		0x0016u
#define	USBC_INT_DETACH		0x0017u
/* USB interrupt type (HOST)*/
#define	USBC_INT_OVRCR0		0x0041u
#define	USBC_INT_BCHG0		0x0042u
#define	USBC_INT_DTCH0		0x0043u
#define	USBC_INT_ATTCH0		0x0044u
#define	USBC_INT_EOFERR0	0x0045u
#define	USBC_INT_OVRCR1		0x0051u
#define	USBC_INT_BCHG1		0x0052u
#define	USBC_INT_ATTCH1		0x0053u
#define	USBC_INT_DTCH1		0x0054u
#define	USBC_INT_EOFERR1	0x0055u
#define	USBC_INT_SACK		0x0061u
#define	USBC_INT_SIGN		0x0062u


/******************************************************************************
HCD driver specification define
******************************************************************************/

/******************************************************************************
HCD driver common define
******************************************************************************/
/* Global macro Define	*/
#define	USBC_UACTON			(1u)
#define	USBC_UACTOFF		(0u)
#define	USBC_VBON			(1u)
#define	USBC_VBOFF			(0u)


/******************************************************************************
USB specification define
******************************************************************************/
/* Device class Define	*/
#define	USBC_NOVENDOR		0xFFFFu	/* Vendor ID nocheck */
#define	USBC_NOPRODUCT		0xFFFFu	/* Product ID nocheck */

/* Interface class Define */
#define	USBC_INTFCLSHET		0xAAu	/* Host electrical test class */


/******************************************************************************
USB-H/W register define
******************************************************************************/
/* Root port */
#define	USBC_NOPORT			0xFFFFu	/* Not connect */

/* Max device */
/* Condition compilation by the difference of IP */
/* Condition compilation by the difference of IP */
#if USBC_IPSEL_PP == USBC_597IP_PP
 /* Condition compilation by the difference of IP */
 #if USBC_IP_DEVADD_PP == USBC_IP_DEVADD_A_PP
	#define USBC_MAXDEVADDR  10u
 #else /* USB_IP_DEVADD_PP = USB_IP_DEVADD_A_PP */
	#define USBC_MAXDEVADDR  5u
 #endif /* USB_IP_DEVADD_PP = USB_IP_DEVADD_A_PP */
#define	  USBC_DEVICE_0		0x0000u	/* Device address 0 */
#define	  USBC_DEVICE_1		0x1000u	/* Device address 1 */
#define	  USBC_DEVICE_2		0x2000u	/* Device address 2 */
#define	  USBC_DEVICE_3		0x3000u	/* Device address 3 */
#define	  USBC_DEVICE_4		0x4000u	/* Device address 4 */
#define	  USBC_DEVICE_5		0x5000u	/* Device address 5 */
#define	  USBC_DEVICE_6		0x6000u	/* Device address 6 */
#define	  USBC_DEVICE_7		0x7000u	/* Device address 7 */
#define	  USBC_DEVICE_8		0x8000u	/* Device address 8 */
#define	  USBC_DEVICE_9		0x9000u	/* Device address 9 */
#define	  USBC_DEVICE_A		0xA000u	/* Device address A */
#define	  USBC_NODEVICE		0xF000u	/* No device */
#define	  USBC_DEVADDRBIT	12u
#endif	/* USBC_597IP_PP */
/* Condition compilation by the difference of IP */
#if USBC_IPSEL_PP == USBC_596IP_PP
#define	USBC_MAXDEVADDR		3u
#define	  USBC_DEVICE_0		0x0000u	/* Device address 0 */
#define	  USBC_DEVICE_1		0x4000u	/* Device address 1 */
#define	  USBC_DEVICE_2		0x8000u	/* Device address 2 */
#define	  USBC_DEVICE_3		0xC000u	/* Device address 3 */
#define	  USBC_NODEVICE		0xF000u	/* No device */
#define	  USBC_DEVADDRBIT	14u
#endif	/* USBC_596IP_PP */

/* DCP Max packetsize */
#define	USBC_MAXPFIELD		0x007Fu	/* Maxpacket size of DCP */


/******************************************************************************
Another define
******************************************************************************/
/* ControlPipe Max Packet size */
#define	USBC_DEFPACKET		0x0040u	/* Default DCP Max packet size */

/* Device state define */
#define	USBC_NONDEVICE			0u
#define	USBC_DEVICEERR			1u
#define	USBC_ATTACHDEVICE		2u
#define	USBC_DEVICEENUMERATION	3u
#define	USBC_DEVICEADDRESSED	4u
#define	USBC_DEVICECONFIGURED	5u
#define	USBC_COMPLETEPIPESET	10u
#define	USBC_DEVICESUSPENDED	20u
#define	USBC_ELECTRICALTEST		30u

/* Control Transfer Stage */
#define	USBC_IDLEST				0u	/* Idle */
#define	USBC_SETUPNDC			1u	/* Setup Stage No Data Control */
#define	USBC_SETUPWR			2u	/* Setup Stage Control Write */
#define	USBC_SETUPRD			3u	/* Setup Stage Control Read */
#define	USBC_DATAWR				4u	/* Data Stage Control Write */
#define	USBC_DATARD				5u	/* Data Stage Control Read */
#define	USBC_STATUSRD			6u	/* Status stage */
#define	USBC_STATUSWR			7u	/* Status stage */
#define	USBC_SETUPWRCNT			17u	/* Setup Stage Control Write */
#define	USBC_SETUPRDCNT			18u	/* Setup Stage Control Read */
#define	USBC_DATAWRCNT			19u	/* Data Stage Control Write */
#define	USBC_DATARDCNT			20u	/* Data Stage Control Read */

/******************************************************************************
HUB define
******************************************************************************/
/* HUB spec */
#define	USBC_FSHUB				1u
#define	USBC_HSHUBS				2u
#define	USBC_HSHUBM				3u

/* Interface number */
#define	USBC_HUB_INTNUMFS		1u
#define	USBC_HUB_INTNUMHSS		1u
#define	USBC_HUB_INTNUMHSM		1u


/******************************************************************************
OTG define
******************************************************************************/
#define	USBC_ADEVICE			0x0000u
#define	USBC_BDEVICE			0x0004u

#define	USBC_UNDER05			0x0000u
#define	USBC_MID05TO14			0x4000u
#define	USBC_MID14TO45			0x8000u
#define	USBC_OVER45				0xC000u


/*	USB Manager mode	*/
#define	USBC_PORTOFF			0u		/* Disconnect(VBUSoff) */
#define	USBC_DETACHED			10u		/* Disconnect(VBUSon) */
#define	USBC_ATTACHED			20u		/* Disconnect(HUBdiconfigured) */
#define	USBC_POWERED			30u		/* Start reset handshake */
#define	USBC_DEFAULT			40u		/* Set device address */
#define	USBC_ADDRESS			50u		/* Enumeration start */
#define	USBC_ENUMERATION		60u		/* Wait device enumeration */
#define	USBC_CONFIGURED			70u		/* Detach detected */
#define	USBC_SUSPENDED			80u		/* Device suspended */
#define	USBC_DETACH_PROCESS		101u	/* Wait device detach */
#define	USBC_SUSPENDED_PROCESS	102u	/* Wait device suspend */
#define	USBC_RESUME_PROCESS		103u	/* Wait device resume */
/* Condition compilation by the difference of IP */
#if USBC_IPSEL_PP == USBC_596IP_PP
#define	USBC_DISABLEDET			90u		/* Disable detach detected */
#endif	/* USBC_596IP_PP */


/******************************************************************************
Task request message type
******************************************************************************/
/* HCD common task message command */
#define	USBC_MSG_HCD_ATTACH			0x0101u
#define	USBC_MSG_HCD_DETACH			0x0102u
#define	USBC_MSG_HCD_USBRESET		0x0103u
#define	USBC_MSG_HCD_SUSPEND		0x0104u
#define	USBC_MSG_HCD_RESUME			0x0105u
#define	USBC_MSG_HCD_REMOTE			0x0106u
#define	USBC_MSG_HCD_VBON			0x0107u
#define	USBC_MSG_HCD_VBOFF			0x0108u
#define	USBC_MSG_HCD_CLR_STALL		0x0109u
#define	USBC_MSG_HCD_DETACH_MGR		0x010Au
#define	USBC_MSG_HCD_ATTACH_MGR		0x010Bu

#define	USBC_MSG_HCD_CLR_STALL_RESULT	0x010Cu
#define	USBC_MSG_HCD_CLR_STALLBIT	0x010Du
#define	USBC_MSG_HCD_CTRL_END		0x010Eu
#define	USBC_MSG_HCD_SQTGLBIT		0x010Fu

/* HCD task message command */
#define	USBC_MSG_HCD_SETDEVICEINFO	0x0111u
#define	USBC_MSG_HCD_SUBMITUTR		0x0112u
#define	USBC_MSG_HCD_TRANSEND1		0x0113u
#define	USBC_MSG_HCD_TRANSEND2		0x0114u
#define	USBC_MSG_HCD_CLRSEQBIT		0x0115u
#define	USBC_MSG_HCD_SETSEQBIT		0x0116u
#define	USBC_MSG_HCD_INT			0x0117u
#define	USBC_MSG_HCD_PCUTINT		0x0118u
#define	USBC_MSG_HCD_DMAINT			0x0119u

#if USBC_TARGET_CHIP_PP == USBC_RX600_PP || USBC_TARGET_CHIP_PP == USBC_ASSP_PP
#define	USBC_MSG_HCD_D0FIFO_INT			0x0141u
#define	USBC_MSG_HCD_D1FIFO_INT			0x0142u
#define	USBC_MSG_HCD_RESM_INT			0x0143u
#define	USBC_MSG_PCD_D0FIFO_INT			0x0144u
#define	USBC_MSG_PCD_D1FIFO_INT			0x0145u
#define	USBC_MSG_PCD_RESM_INT			0x0146u
#endif
/* USBC_TARGET_CHIP_PP == USBC_RX600_PP || USBC_TARGET_CHIP_PP == USBC_ASSP_PP */

/* USB Manager task message command */
#define	USBC_MSG_MGR_AORDETACH		0x0121u
#define	USBC_MSG_MGR_OVERCURRENT	0x0122u
#define	USBC_MSG_MGR_STATUSRESULT	0x0123u
#define	USBC_MSG_MGR_SUBMITRESULT	0x0124u
#define	USBC_MSG_MGR_TRANSENDRESULT	0x0125u

/* Condition compilation by the difference of IP */
#if USBC_IPSEL_PP == USBC_596IP_PP
#define	USBC_MSG_MGR_DETACHCONTROL	0x0126u
#define	USBC_MSG_MGR_DETACHDETECT	0x0127u
#endif	/* USBC_596IP_PP */

/* Condition compilation by the difference of IP */
#if USBC_IPSEL_PP == USBC_597IP_PP
/* USB HUB task message command */
#define	USBC_MSG_HUB_HUB2HUBSTART	0x0131u
#define	USBC_MSG_HUB_START			0x0132u
#define	USBC_MSG_HUB_STOP			0x0133u
#define	USBC_MSG_HUB_SUBMITRESULT	0x0134u
#define	USBC_MSG_HUB_IVENT			0x0135u	/* nonOS */
#define	USBC_MSG_HUB_ATTACH			0x0136u	/* nonOS */
#define	USBC_MSG_HUB_RESET			0x0137u	/* nonOS */

/* CLS task message command */
#define	USBC_MSG_CLS_CHECKREQUEST	0x0201u	/* nonOS */
#define	USBC_MSG_CLS_INIT			0x0202u	/* nonOS */
#define	USBC_MSG_CLS_TASK			0x0203u	/* nonOS */
#define	USBC_MSG_CLS_WAIT			0x0204u	/* nonOS */
#define	USBC_MSG_CLS_PROCESSRESULT	0x0205u	/* nonOS */
#endif	/* USBC_IPSEL_PP == USBC_597IP_PP */

/* HET task message command */
#define	USBC_MSG_HET_UACTOFF		0x0171u
#define	USBC_MSG_HET_UACTON			0x0172u
#define	USBC_MSG_HET_VBUSOFF		0x0173u
#define	USBC_MSG_HET_VBUSON			0x0174u
#define	USBC_MSG_HET_RESET			0x0175u
#define	USBC_MSG_HET_SUSPEND		0x0176u
#define	USBC_MSG_HET_RESUME			0x0177u
#define	USBC_MSG_HET_ENUMERATION	0x0178u
#define	USBC_MSG_HET_TESTNONE		0x0181u
#define	USBC_MSG_HET_TESTPACKET		0x0182u
#define	USBC_MSG_HET_TESTJ			0x0183u
#define	USBC_MSG_HET_TESTK			0x0184u
#define	USBC_MSG_HET_TESTSE0		0x0185u
#define	USBC_MSG_HET_TESTSIGSTOP	0x0186u
#define	USBC_MSG_HET_SINGLESETUP	0x0187u
#define	USBC_MSG_HET_SINGLEDATA		0x0188u


/******************************************************************************
USB driver specification define
******************************************************************************/

/******************************************************************************
Another define
******************************************************************************/
/* Descriptor index */
#define	USBC_DEV_MAX_PKT_SIZE		7u	/* Index of bMAXPacketSize */
#define	USBC_DEV_NUM_CONFIG			17u	/* Index of bNumConfigurations */
#define	USBC_ALT_NO					255u
#define	USBC_SOFT_CHANGE			0u


/******************************************************************************
Task request message type
******************************************************************************/
/* USB Peripheral task message command */
#define	USBC_MSG_PCD_INT				0x0151u
#define	USBC_MSG_PCD_SUBMITUTR			0x0152u
#define	USBC_MSG_PCD_TRANSEND1			0x0153u
#define	USBC_MSG_PCD_TRANSEND2			0x0154u
#define	USBC_MSG_PCD_REMOTEWAKEUP		0x0155u
#define	USBC_MSG_PCD_DETACH				0x0161u
#define	USBC_MSG_PCD_ATTACH				0x0162u
#define	USBC_MSG_PCD_CLRSEQBIT			0x0163u
#define	USBC_MSG_PCD_SETSTALL			0x0164u
#define	USBC_MSG_PCD_PCUTINT			0x0156u

#define	USBC_MSG_PCD_DP_ENABLE			0x0157u
#define	USBC_MSG_PCD_DP_DISABLE			0x0158u
#define	USBC_MSG_PCD_DM_ENABLE			0x0159u
#define	USBC_MSG_PCD_DM_DISABLE			0x015Au

#define	USBC_MSG_PCD_DMAINT				0x015bu

#define	USBC_DO_REMOTEWAKEUP			USBC_MSG_PCD_REMOTEWAKEUP
#define	USBC_DP_ENABLE					USBC_MSG_PCD_DP_ENABLE
#define	USBC_DP_DISABLE					USBC_MSG_PCD_DP_DISABLE
#define	USBC_DM_ENABLE					USBC_MSG_PCD_DM_ENABLE
#define	USBC_DM_DISABLE					USBC_MSG_PCD_DM_DISABLE

#define	USBC_DO_STALL					USBC_MSG_PCD_SETSTALL

#define	USBC_GO_POWEREDSTATE			0x0201u	/* USBC_MSG_HCD_DETACH */
#define	USBC_DO_RESET_AND_ENUMERATION	0x0202u	/* USBC_MSG_HCD_ATTACH */
#define	USBC_PORT_ENABLE				0x0203u	/* USBC_MSG_HCD_VBON */
#define	USBC_PORT_DISABLE				0x0204u	/* USBC_MSG_HCD_VBOFF */
#define	USBC_DO_GLOBAL_SUSPEND			0x0205u	/* USBC_MSG_HCD_SUSPEND */
#define	USBC_DO_SELECTIVE_SUSPEND		0x0206u	/* USBC_MSG_HCD_SUSPEND */
#define	USBC_DO_GLOBAL_RESUME			0x0207u	/* USBC_MSG_HCD_RESUME */
#define	USBC_DO_SELECTIVE_RESUME		0x0208u	/* USBC_MSG_HCD_RESUME */

#endif	/* __R_USBC_CDEFUSBIP_H__ */
/******************************************************************************
End  Of File
******************************************************************************/
