/******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized.
* This software is owned by Renesas Electronics Corporation and is  protected
* under all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES
* REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY,
* INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR  A
* PARTICULAR PURPOSE AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE  EXPRESSLY
* DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE  LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES
* FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS
* AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this
* software and to discontinue the availability of this software.
* By using this software, you agree to the additional terms and
* conditions found by accessing the following link:
* http://www.renesas.com/disclaimer
******************************************************************************
* Copyright (C) 2010(2011) Renesas Electronics Corpration
* and Renesas Solutions Corp. All rights reserved.
******************************************************************************
* File Name    : r_usbc_cDef592IP.h
* Version      : 1.10
* Device(s)    : Renesas SH-Series, RX-Series
* Tool-Chain   : Renesas SuperH RISC engine Standard Toolchain
*              : Renesas RX Standard Toolchain
* OS           : Common to None and uITRON 4.0 Spec
* H/W Platform : Independent
* Description  : USB definition for 592IP
******************************************************************************
* History : DD.MM.YYYY Version Description
*         : 17.03.2010 0.80    First Release
*         : 30.07.2010 0.90    Updated comments
*         : 02.08.2010 0.91    Updated comments
*         : 29.10.2010 1.00    Mass Production Release
*         : 01.06.2011 1.10    Version 1.10 Release
******************************************************************************/

#ifndef __R_USBC_CDEF592IP_H__
#define __R_USBC_CDEF592IP_H__


/*****************************************************************************
Structure Types
******************************************************************************/

/* USBC_592IP Register definition */
/* Register and Bit name are united with USBC_597IP_PP */
/* , because the uITRON-FW is compatible. */ 

struct	USBC_REGISTER
{
	volatile uint16_t		SYSCFG0;		/* 00h */
	volatile uint16_t		SYSSTS0;		/* 02h */
	volatile uint16_t		DVSTCTR0;		/* 04h */
	volatile uint16_t		TESTMODE;		/* 06h */
	volatile uint16_t		REGISTx08;		/* 08h */
	volatile uint16_t		PINCFG;			/* 0Ah */
	volatile uint16_t		DMA0CFG;		/* 0Ch */
	volatile uint16_t		DMA1CFG;		/* 0Eh */
	union
	{
		volatile uint16_t	CFIFO16;
		volatile uint8_t	CFIFO08;
	} CFIFOREG;								/* 10h */
	volatile uint16_t		REGISTx12;		/* 12h */
	union
	{
		volatile uint16_t	D0FIFO16;
		volatile uint8_t	D0FIFO08;
	} D0FIFOREG;							/* 14h */
	volatile uint16_t		REGISTx16;		/* 16h */
	union
	{
		volatile uint16_t	D1FIFO16;
		volatile uint8_t	D1FIFO08;
	} D1FIFOREG;							/* 18h */
	volatile uint16_t		REGISTx1A;		/* 1Ah */
	volatile uint16_t		REGISTx1C;		/* 1Ch */
	volatile uint16_t		CFIFOSEL;		/* 1Eh */
	volatile uint16_t		CFIFOCTR;		/* 20h */
	volatile uint16_t		CFIFOSIE;		/* 22h */
	volatile uint16_t		D0FIFOSEL;		/* 24h */
	volatile uint16_t		D0FIFOCTR;		/* 26h */
	volatile uint16_t		D0FIFOTRN;		/* 28h */
	volatile uint16_t		D1FIFOSEL;		/* 2Ah */
	volatile uint16_t		D1FIFOCTR;		/* 2Ch */
	volatile uint16_t		D1FIFOTRN;		/* 2Eh */
	volatile uint16_t		INTENB0;		/* 30h */
	volatile uint16_t		INTENB1;		/* 32h */
	volatile uint16_t		REGISTx34;		/* 34h */
	volatile uint16_t		BRDYENB;		/* 36h */
	volatile uint16_t		NRDYENB;		/* 38h */
	volatile uint16_t		BEMPENB;		/* 3Ah */
	volatile uint16_t		SOFCFG;			/* 3Ch */
	volatile uint16_t		REGISTx3E;		/* 3Eh */
	volatile uint16_t		INTSTS0;		/* 40h */
	volatile uint16_t		REGISTx42;		/* 42h */
	volatile uint16_t		REGISTx44;		/* 44h */
	volatile uint16_t		BRDYSTS;		/* 46h */
	volatile uint16_t		NRDYSTS;		/* 48h */
	volatile uint16_t		BEMPSTS;		/* 4Ah */
	volatile uint16_t		FRMNUM;			/* 4Ch */
	volatile uint16_t		UFRMNUM;		/* 4Eh */
	volatile uint16_t		USBADDR;		/* 50h(RECOVER) */
	volatile uint16_t		REGISTx52;		/* 52h */
	volatile uint16_t		USBREQ;			/* 54h */
	volatile uint16_t		USBVAL;			/* 56h */
	volatile uint16_t		USBINDX;		/* 58h */
	volatile uint16_t		USBLENG;		/* 5Ah */
	volatile uint16_t		DCPCFG;			/* 5Ch */
	volatile uint16_t		DCPMAXP;		/* 5Eh */
	volatile uint16_t		DCPCTR;			/* 60h */
	volatile uint16_t		REGISTx62;		/* 62h */
	volatile uint16_t		PIPESEL;		/* 64h */
	volatile uint16_t		PIPECFG;		/* 66h */
	volatile uint16_t		PIPEBUF;		/* 68h */
	volatile uint16_t		PIPEMAXP;		/* 6Ah */
	volatile uint16_t		PIPEPERI;		/* 6Ch */
	volatile uint16_t		REGISTx6E;		/* 6Eh */
	volatile uint16_t		PIPE1CTR;		/* 70h */
	volatile uint16_t		PIPE2CTR;		/* 72h */
	volatile uint16_t		PIPE3CTR;		/* 74h */
	volatile uint16_t		PIPE4CTR;		/* 76h */
	volatile uint16_t		PIPE5CTR;		/* 78h */
	volatile uint16_t		PIPE6CTR;		/* 7Ah */
	volatile uint16_t		PIPE7CTR;		/* 7Ch */
	volatile uint16_t		INVALID_REG;	/* 7Eh */
};

#endif	/* __R_USBC_CDEF592IP_H__ */
/******************************************************************************
End  Of File
******************************************************************************/
