/******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized.
* This software is owned by Renesas Electronics Corporation and is  protected
* under all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES
* REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY,
* INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR  A
* PARTICULAR PURPOSE AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE  EXPRESSLY
* DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE  LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES
* FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS
* AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this
* software and to discontinue the availability of this software.
* By using this software, you agree to the additional terms and
* conditions found by accessing the following link:
* http://www.renesas.com/disclaimer
*******************************************************************************
* Copyright (C) 2010(2011) Renesas Electronics Corpration
* and Renesas Solutions Corp. All rights reserved.
*******************************************************************************
* File Name    : r_usbc_cMacSystemcall.h
* Version      : 1.00
* Device(s)    : Renesas SH-Series, RX-Series
* Tool-Chain   : Renesas SuperH RISC engine Standard Toolchain
*              : Renesas RX Standard Toolchain
* OS           : Common to None and uITRON 4.0 Spec
* H/W Platform : Independent
* Description  : uITRON System Call Definition Header File
******************************************************************************
* History : DD.MM.YYYY Version Description
*         : 17.03.2010 0.80    First Release
*         : 30.07.2010 0.90    Updated comments
*         : 02.08.2010 0.91    Updated comments
*         : 29.10.2010 1.00    Mass Production Release
******************************************************************************/
#ifndef __R_USBC_CMACSYSTEMCALL_H__
#define __R_USBC_CMACSYSTEMCALL_H__


/*****************************************************************************
Macro definitions
******************************************************************************/
/* uITRON4.0 system call */
#ifndef USBC_NO_SYSTEM_PP
	#define	USBC_CRE_TSK(ID,INFO)		\
		cre_tsk( (USBC_ID_t)ID, (USBC_TSK_t*)INFO )
	#define	USBC_DEL_TSK(ID)			\
		del_tsk( (USBC_ID_t)ID )
	#define	USBC_STA_TSK(ID,CODE)		\
		sta_tsk( (USBC_ID_t)ID, (USBC_VI_t)CODE )
	#define	USBC_ACT_TSK(ID)			\
		act_tsk( (USBC_ID_t)ID )
	#define	USBC_TER_TSK(ID)			\
		ter_tsk( (USBC_ID_t)ID )
	#define	USBC_EXT_TSK()				\
		ext_tsk( )
	#define	USBC_REF_TST(ID, STS)		\
		ref_tst( (USBC_ID_t)ID, (USBC_RTST_t*)STS )

	#define	USBC_DLY_TSK(TIME)			\
		dly_tsk( (USBC_RT_t)TIME )

	#define	USBC_CRE_MBX(ID, INFO)		\
		cre_mbx( (USBC_ID_t)ID, (USBC_MBX_t*)INFO )
	#define	USBC_DEL_MBX(ID)			\
		del_mbx( (USBC_ID_t)ID )
	#define	USBC_SND_MSG(ID, MESS)		\
		snd_mbx( (USBC_ID_t)ID, (USBC_MSG_t*)MESS )
	#define	USBC_ISND_MSG(ID, MESS)		\
		isnd_mbx( (USBC_ID_t)ID, (USBC_MSG_t*)MESS )
	#define	USBC_RCV_MSG(ID, MESS)		\
		rcv_mbx( (USBC_ID_t)ID, (USBC_MSG_t**)MESS )
	#define	USBC_PRCV_MSG(ID, MESS)		\
		prcv_mbx( (USBC_ID_t)ID, (USBC_MSG_t**)MESS )
	#define	USBC_TRCV_MSG(ID, MESS, TM)	\
		trcv_mbx( (USBC_ID_t)ID, (USBC_MSG_t**)MESS, (USBC_TM_t)TM )

	#define	USBC_CRE_MPL(ID, INFO)		\
		cre_mpf( (USBC_ID_t)ID, (USBC_MPL_t*)INFO )
	#define	USBC_DEL_MPL(ID)			\
		del_mpf( (USBC_ID_t)ID )
	#define	USBC_PGET_BLK(ID, BLK)		\
		pget_mpf( (USBC_ID_t)ID, (USBC_MH_t*)BLK )
	#define	USBC_IPGET_BLK(ID, BLK)		\
		ipget_mpf( (USBC_ID_t)ID, (USBC_MH_t*)BLK )
	#define	USBC_REL_BLK(ID, BLK)		\
		rel_mpf( (USBC_ID_t)ID, (USBC_MH_t)BLK )

	#define	USBC_CRE_SEM(ID, INFO)		\
		cre_sem( (USBC_ID_t)ID, (USBC_SEM_t*)INFO )
	#define	USBC_WAI_SEM(ID)			\
		wai_sem( (USBC_ID_t)ID )
	#define	USBC_POL_SEM(ID)			\
		pol_sem( (USBC_ID_t)ID )
	#define	USBC_SIG_SEM(ID)			\
		sig_sem( (USBC_ID_t)ID )

	#define	USBC_CRE_ALM(ID, INFO)		\
		cre_alm( (USBC_ID_t)ID, (USBC_ALM_t*)INFO )
	#define	USBC_STA_ALM(ID, TIME)		\
		sta_alm( (USBC_ID_t)ID, (USBC_RT_t)TIME )
	#define	USBC_STP_ALM(ID)			\
		stp_alm( (USBC_ID_t)ID )
	#define	USBC_DEL_ALM(ID)			\
		del_alm( (USBC_ID_t)ID )
#else	/* USBC_NO_SYSTEM_PP */
	#define	USBC_CRE_TSK(ID,INFO)		USBC_NG
	#define	USBC_DEL_TSK(ID)			USBC_NG
	#define	USBC_STA_TSK(ID,CODE)		USBC_NG
	#define	USBC_ACT_TSK(ID)			USBC_NG
	#define	USBC_TER_TSK(ID)			USBC_NG
	#define	USBC_EXT_TSK()				USBC_NG
	#define	USBC_REF_TST(ID, STS)		USBC_NG

	#define	USBC_DLY_TSK(TIME)

	#define	USBC_CRE_MBX(ID, INFO)		USBC_NG
	#define	USBC_DEL_MBX(ID)			USBC_NG
	/* nonOS */
	#define	USBC_SND_MSG(ID, MESS)		\
		usbc_cstd_SndMsg( (uint8_t)ID, (USBC_MSG_t*)MESS )
	/* nonOS */
	#define	USBC_ISND_MSG(ID, MESS)		\
		usbc_cstd_iSndMsg( (uint8_t)ID, (USBC_MSG_t*)MESS )
	#define USBC_WAI_MSG(ID, MESS, TM)	\
		usbc_cstd_WaiMsg( (uint8_t)ID, (USBC_MSG_t*)MESS, (uint16_t)TM )
	#define	USBC_RCV_MSG(ID, MESS)		\
		usbc_cstd_RecMsg( (uint8_t)ID, (USBC_MSG_t**)MESS, (USBC_TM_t)0u )
	#define	USBC_PRCV_MSG(ID, MESS)		USBC_NG
	/* nonOS */
	#define	USBC_TRCV_MSG(ID, MESS, TM)	\
		usbc_cstd_RecMsg( (uint8_t)ID, (USBC_MSG_t**)MESS, (USBC_TM_t)TM )
	
	#define	USBC_CRE_MPL(ID, INFO)		USBC_NG
	#define	USBC_DEL_MPL(ID)			USBC_NG
	/* nonOS */
	#define	USBC_PGET_BLK(ID, BLK)		\
		usbc_cstd_PgetBlk( (uint8_t)ID, (USBC_UTR_t**)BLK )
	#define	USBC_IPGET_BLK(ID, BLK)		USBC_NG
	/* nonOS */
	#define	USBC_REL_BLK(ID, BLK)		\
		usbc_cstd_RelBlk( (uint8_t)ID,  (USBC_UTR_t*)BLK )

	#define	USBC_CRE_SEM(ID, INFO)		USBC_NG
	#define	USBC_WAI_SEM(ID)			USBC_NG
	#define	USBC_POL_SEM(ID)			USBC_NG
	#define	USBC_SIG_SEM(ID)			USBC_NG

	#define	USBC_CRE_ALM(ID, INFO)		USBC_NG
	#define	USBC_STA_ALM(ID, TIME)		USBC_NG
	#define	USBC_STP_ALM(ID)			USBC_NG
	#define	USBC_DEL_ALM(ID)			USBC_NG
#endif	/* USBC_NO_SYSTEM_PP */


#endif	/* __R_USBC_CMACSYSTEMCALL_H__ */

/******************************************************************************
End  Of File
******************************************************************************/
