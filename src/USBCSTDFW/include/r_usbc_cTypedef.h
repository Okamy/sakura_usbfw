/******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized.
* This software is owned by Renesas Electronics Corporation and is  protected
* under all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES
* REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY,
* INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR  A
* PARTICULAR PURPOSE AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE  EXPRESSLY
* DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE  LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES
* FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS
* AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this
* software and to discontinue the availability of this software.
* By using this software, you agree to the additional terms and
* conditions found by accessing the following link:
* http://www.renesas.com/disclaimer
*******************************************************************************
* Copyright (C) 2010(2011) Renesas Electronics Corpration
* and Renesas Solutions Corp. All rights reserved.
*******************************************************************************
* File Name    : r_usbc_cTypedef.h
* Version      : 1.10
* Device(s)    : Renesas SH-Series, RX-Series
* Tool-Chain   : Renesas SuperH RISC engine Standard Toolchain
*              : Renesas RX Standard Toolchain
* OS           : Common to None and uITRON 4.0 Spec
* H/W Platform : Independent
* Description  : Type Definition Header File
*******************************************************************************
* History : DD.MM.YYYY Version Description
*         : 17.03.2010 0.80    First Release
*         : 30.07.2010 0.90    Updated comments
*         : 02.08.2010 0.91    Updated comments
*         : 29.10.2010 1.00    Mass Production Release
*         : 01.06.2011 1.10    Version 1.10 Release
******************************************************************************/
#ifndef __R_USBC_CTYPEDEF_H__
#define __R_USBC_CTYPEDEF_H__


/******************************************************************************
Typedef definitions
******************************************************************************/
typedef	unsigned char		uint8_t;	/*	8bit */
typedef	unsigned short		uint16_t;	/* 16bit */
typedef	unsigned long		uint32_t;	/* 32bit */
typedef	signed char			int8_t;
typedef	signed short		int16_t;
typedef	signed long			int32_t;


/******************************************************************************
Includes   <System Includes> , "Project Includes"
******************************************************************************/
#include "r_usbc_cDefUsr.h"
#include "r_usbc_cItron.h"


/******************************************************************************
Typedef definitions
******************************************************************************/
typedef struct USBC_SUTR	USBC_UTR_t;
typedef struct st_usb		USBC_STNBYINT_t;

typedef struct
{
	uint16_t		ReqType;			/* Request type */
	uint16_t		ReqTypeType;		/* Request type TYPE */
	uint16_t		ReqTypeRecip;		/* Request type RECIPIENT */
	uint16_t		ReqRequest;			/* Request */
	uint16_t		ReqValue;			/* Value */
	uint16_t		ReqIndex;			/* Index */
	uint16_t		ReqLength;			/* Length */
} USBC_REQUEST_t;


typedef	void (*USBC_CB_INFO_t)(uint16_t, uint16_t);
typedef	void (*USBC_CB_CHECK_t)(uint16_t**);
typedef	void (*USBC_CB_t)(USBC_UTR_t*);
typedef	void (*USBC_CB_MSG_t)(uint16_t, uint8_t*);
typedef	void (*USBC_CB_REQ_t)(void *);
typedef	void (*USBC_CB_TRN_t)(USBC_REQUEST_t*, uint16_t);

typedef struct
{
	uint16_t		rootport;		/* Root port */
	uint16_t		devaddr;		/* Device address */
	uint16_t		devstate;		/* Device state */
	uint16_t		ifclass;		/* Interface Class */
	uint16_t		*tpl;			/* Target peripheral list 
										(Vendor ID, Product ID) */
	uint16_t		*pipetbl;		/* Pipe Define Table address */
	USBC_CB_INFO_t	classinit;		/* Driver init */
	USBC_CB_CHECK_t	classcheck;		/* Driver check */
	USBC_CB_INFO_t	devconfig;		/* Device configured */
	USBC_CB_INFO_t	devdetach;		/* Device detach */
	USBC_CB_INFO_t	devsuspend;		/* Device suspend */
	USBC_CB_INFO_t	devresume;		/* Device resume */
	USBC_CB_INFO_t	overcurrent;	/* Device over current */
} USBC_HCDREG_t;

typedef struct
{
	uint16_t		**pipetbl;		/* Pipe Define Table address */
	uint8_t			*devicetbl;		/* Device descriptor Table address */
	uint8_t			*qualitbl;		/* Qualifier descriptor Table address */
	uint8_t			**configtbl;	/* Configuration descriptor
										Table address */
	uint8_t			**othertbl;		/* Other configuration descriptor
										Table address */
	uint8_t			**stringtbl;	/* String descriptor Table address */
	USBC_CB_INFO_t	classinit;		/* Driver init */
	USBC_CB_INFO_t	devdefault;		/* Device default */
	USBC_CB_INFO_t	devconfig;		/* Device configured */
	USBC_CB_INFO_t	devdetach;		/* Device detach */
	USBC_CB_INFO_t	devsuspend;		/* Device suspend */
	USBC_CB_INFO_t	devresume;		/* Device resume */
	USBC_CB_INFO_t	interface;		/* Interface changed */
	USBC_CB_TRN_t	ctrltrans;		/* Control Transfer */
} USBC_PCDREG_t;


typedef struct
{
	USBC_MH_t		msghead;		/* Message header (for SH-solution) */
	uint16_t		msginfo;		/* Message Info for F/W */
	uint16_t		keyword;		/* Rootport / Device address
											/ Pipe number */
	void			*tranadr;		/* Transfer data Start address */
	USBC_CB_INFO_t	complete;		/* Call Back Function Info */
} USBC_HCDINFO_t;


typedef struct
{
	USBC_MH_t		msghead;		/* Message header (for SH-solution) */
	uint16_t		msginfo;		/* Message Info for F/W */
	uint16_t		keyword;		/* Rootport / Device address 
										/ Pipe number */
	uint16_t		result;			/* Result */
} USBC_MGRINFO_t;


typedef struct
{
	USBC_MH_t		msghead;		/* Message header (for SH-solution) */
	uint16_t		msginfo;		/* Message Info for F/W */
	uint16_t		keyword;		/* Pipe number */
	USBC_CB_INFO_t	complete;		/* Call Back Function Info */
} USBC_PCDINFO_t;

// nonOS
typedef struct
{
	USBC_MH_t		msghead;		/* Message header (for SH-solution) */
	uint16_t		msginfo;		/* Message Info for F/W */
	uint16_t		keyword;		/* Rootport / Device address 
										/ Pipe number */
	uint16_t		result;			/* Result */
} USBC_CLSINFO_t;

/*****************************************************************************
Struct definitions
******************************************************************************/
struct USBC_SUTR
{
	USBC_MH_t		msghead;		/* Message header (for SH-solution) */
	uint16_t		msginfo;		/* Message Info for F/W */
	uint16_t		keyword;		/* Rootport / Device address 
										/ Pipe number */
	void			*tranadr;		/* Transfer data Start address */
	uint32_t		tranlen;		/* Transfer data length */
	uint16_t		*setup;			/* Setup packet(for control only) */
	uint16_t		status;			/* Status */
	uint16_t		pipectr;		/* Pipe control register */
	USBC_CB_t		complete;		/* Call Back Function Info */
	uint8_t			errcnt;			/* Error count */
	uint8_t			segment;		/* Last flag */
};

/*****************************************************************************
Macro definitions
******************************************************************************/
#define	USBC_NONE			(uint16_t)(0)
#define	USBC_YES			(uint16_t)(1)
#define	USBC_NO				(uint16_t)(0)
#define	USBC_DONE			(uint16_t)(0)
#define	USBC_ERROR			(uint16_t)(0xFFFF)
#define	USBC_OK				(uint16_t)(0)
#define	USBC_NG				(uint16_t)(0xFFFF)
#define	USBC_ON				(uint16_t)(1)
#define	USBC_OFF			(uint16_t)(0)
#define	USBC_OTG_DONE		(uint16_t)(2)

#endif	/* __R_USBC_CTYPEDEF_H__ */
/******************************************************************************
End  Of File
******************************************************************************/
