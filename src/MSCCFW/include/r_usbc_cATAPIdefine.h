/******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized.
* This software is owned by Renesas Electronics Corporation and is  protected
* under all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES
* REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY,
* INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR  A
* PARTICULAR PURPOSE AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE  EXPRESSLY
* DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE  LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES
* FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS
* AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this
* software and to discontinue the availability of this software.
* By using this software, you agree to the additional terms and
* conditions found by accessing the following link:
* http://www.renesas.com/disclaimer
******************************************************************************
* Copyright (C) 2010 Renesas Electronics Corporation. All rights reserved.
******************************************************************************
* File Name    : r_usbc_cATAPIdefine.h
* Version      : 1.00
* Device(s)    : Renesas SH-Series, RX-Series
* Tool-Chain   : Renesas SuperH RISC engine Standard Toolchain
*              : Renesas RX Standard Toolchain
* OS           : Common to None and uITRON 4.0 Spec
* H/W Platform : Independent
* Description  : USB common extern header
******************************************************************************
* History : DD.MM.YYYY Version Description
*         : 17.03.2010 0.80    First Release
*         : 30.07.2010 0.90    Updated comments
*         : 02.08.2010 0.91    Updated comments
*         : 29.10.2010 1.00    Mass Production Release
******************************************************************************/
#ifndef __R_USBC_CATAPIDEFINE_H__
#define __R_USBC_CATAPIDEFINE_H__

/*****************************************************************************
Enum definitions
******************************************************************************/
enum
{
	/*--- SFF-8070i command define ---*/
	USBC_ATAPI_TEST_UNIT_READY			= 0x00u,
	USBC_ATAPI_REQUEST_SENSE			= 0x03u,
	USBC_ATAPI_FORMAT_UNIT				= 0x04u,
	USBC_ATAPI_INQUIRY					= 0x12u,
	USBC_ATAPI_MODE_SELECT6				= 0x15u,
	USBC_ATAPI_MODE_SENSE6				= 0x1Au,
	USBC_ATAPI_START_STOP_UNIT			= 0x1Bu,
	USBC_ATAPI_PREVENT_ALLOW			= 0x1Eu,
	USBC_ATAPI_READ_FORMAT_CAPACITY		= 0x23u,
	USBC_ATAPI_READ_CAPACITY			= 0x25u,
	USBC_ATAPI_READ10					= 0x28u,
	USBC_ATAPI_WRITE10					= 0x2Au,
	USBC_ATAPI_SEEK						= 0x2Bu,
	USBC_ATAPI_WRITE_AND_VERIFY			= 0x2Eu,
	USBC_ATAPI_VERIFY10					= 0x2Fu,
	USBC_ATAPI_MODE_SELECT10			= 0x55u,
	USBC_ATAPI_MODE_SENSE10				= 0x5Au,
};


/******************************************************************************
Constant macro definitions
******************************************************************************/
/* prevent allow key */
#define	USBC_MEDIA_UNLOCK	0u			/* Media unlock */
#define	USBC_MEDIA_LOCK		1u			/* Media Lock */

/* Peripheral Device Type (InquiryRequest) */
#define	USBC_PDT_DIRECT		0x00u
#define	USBC_PDT_SEQUENTIAL	0x01u
#define	USBC_PDT_WRITEONCE	0x04u
#define	USBC_PDT_CDROM		0x05u
#define	USBC_PDT_OPTICAL	0x07u
#define	USBC_PDT_UNKNOWN	0x1Fu

/* max Partiton */
#define	USBC_MAXUNITNUM		4u
#define	USBC_BOOTPARTNUM	4u

/* Partision check */
#define	USBC_PBR_ADDR		0x01u
#define	USBC_MBR_ADDR		0x02u
#define	USBC_EMBR_ADDR		0x03u
#define	USBC_BOOT_ERROR		0x05u
#define	USBC_BOOTRECORD_SIG	0xAA55u
#define	USBC_STARTDISK		0x80u
#define	USBC_NOTSTARTDISK	0x00u
#define	USBC_NOPCODE		0x90u
#define	USBC_JMPCODE1		0xEBu
#define	USBC_JMPCODE2		0xE9u

/* Partition type */
#define	USBC_PT_NONE		0x00u
#define	USBC_PT_FAT12A		0x01u
#define	USBC_PT_FAT16A		0x04u
#define	USBC_PT_EPRTA		0x05u
#define	USBC_PT_FAT16B		0x06u
#define	USBC_PT_FAT32A		0x0Bu
#define	USBC_PT_FAT32X		0x0Cu
#define	USBC_PT_FAT16X		0x0Eu
#define	USBC_PT_EPRTB		0x0Fu
#define	USBC_PT_EPRTC		0x85u

#define	USBC_PT_FAT12		0x01u
#define	USBC_PT_FAT16		0x04u
#define	USBC_PT_FAT32		0x0Bu
#define	USBC_PT_EPRT		0x05u

/* FAT TYPE */
#define	USBC_PMSC_FATTYPE_12_PP		1
#define	USBC_PMSC_FATTYPE_16_PP		2

#define USBC_PMSC_FATTYPE_PP	USBC_PMSC_FATTYPE_12_PP
//#define USBC_PMSC_FATTYPE_PP	USBC_PMSC_FATTYPE_16_PP

/*****************************************************************************
Typedef definitions
******************************************************************************/
/* MBR */
typedef struct
{
	uint8_t	JMPcode;
	uint8_t	JMPaddr;
	uint8_t	NOPcode;
	uint8_t	BSRcode[443];
	uint8_t	PartitionTable[64];
	uint8_t	Signature[2];
} USBC_MBR_t;

/* PTBL */
typedef struct
{
	uint8_t	ActiveFlag;
	uint8_t	StartHead;
	uint8_t	StartCS[2];
	uint8_t	PartitionType;
	uint8_t	StopHead;
	uint8_t	StopCS[2];
	uint8_t	StartSectorNum[4];
	uint8_t	PartitionSect[4];
} USBC_PTBL_t;

/* PBR */
typedef struct
{
	uint8_t	JMPcode;
	uint8_t	JMPaddr;
	uint8_t	NOPcode;
	uint8_t	Name[8];
	uint8_t	SectorSize[2];
	uint8_t	ClusterSize;
	uint8_t	ReservedSector[2];
	uint8_t	FatCount;
	uint8_t	RootDirTop[2];
	uint8_t	TotalSector0[2];
	uint8_t	DfsMediaType;
	uint8_t	FATSector[2];
	uint8_t	TrackSector[2];
	uint8_t	CylinderSector[2];
	uint8_t	OffsetSector[4];
	uint8_t	TotalSector1[4];
	uint8_t	FATSigData[474];
	uint8_t	Signature[2];
} USBC_PBR_t;

/* FAT12 */
typedef struct
{
	uint8_t	DriveNum;
	uint8_t	Reserve;
	uint8_t	BootSig;
	uint8_t	VolSirial[4];
	uint8_t	VolLabel[11];
	uint8_t	FileSystemType[8];
} USBC_FAT1216_t;

/* FAT32 */
typedef struct
{
	uint8_t	FATSector[4];
	uint8_t	ExtendedFlag[2];
	uint8_t	FileSystemVer[2];
	uint8_t	RootDirCluster[4];
	uint8_t	FSinfoSector[2];
	uint8_t	BackupBootSector[2];
	uint8_t	Reserve12[12];
	uint8_t	DriveNum;
	uint8_t	Reserve;
	uint8_t	BootSig;
	uint8_t	VolSirial[4];
	uint8_t	VolLabel[11];
	uint8_t	FileSystemType[8];
} USBC_FAT32_t;


/* Callback Message format define. */
typedef struct
{
	uint32_t	ar_rst;
	uint32_t	ul_size;
}
USBC_PMSC_CBM_t;

/******************************************************************************
Bit Order Definition "LEFT"
******************************************************************************/
#pragma bit_order left

/* Command Descriptor Block format define. */
typedef union
{
	struct
	{
		uint8_t	uc_OpCode;
		struct
		{
			uint8_t b_LUN:3;
			uint8_t b_reserved:5;
		}
		s_LUN;
		uint8_t	uc_data;
	}
	s_usb_ptn0;
	struct
	{
		uint8_t	uc_OpCode;
		struct
		{
			uint8_t b_LUN:3;
			uint8_t b_reserved4:4;
			uint8_t b_immed:1;
		}
		s_LUN;
		uint8_t	uc_rsv2[2];
		uint8_t	uc_Allocation;
		uint8_t	uc_rsv1[1];
		uint8_t	uc_rsv6[6];
	}
	s_usb_ptn12;
	struct
	{
		uint8_t	uc_OpCode;
		struct
		{
			uint8_t b_LUN:3;
			uint8_t b_FmtData:1;
			uint8_t b_CmpList:1;
			uint8_t b_Defect:3;
		}
		s_LUN;
		uint8_t	ul_LBA0;
		uint8_t	ul_LBA1;
		uint8_t	ul_LBA2;
		uint8_t	ul_LBA3;
		uint8_t	uc_rsv6[6];
	}
	s_usb_ptn378;
	struct
	{
		uint8_t	uc_OpCode;
		struct
		{
			uint8_t b_LUN:3;
			uint8_t b_1:1;
			uint8_t b_reserved2:2;
			uint8_t b_ByteChk:1;
			uint8_t b_SP:1;
		}
		s_LUN;
		/* Logical block */
		uint8_t	ul_LogicalBlock0;
		uint8_t	ul_LogicalBlock1;
		uint8_t	ul_LogicalBlock2;
		uint8_t	ul_LogicalBlock3;
		uint8_t	uc_rsv1[1];
		uint8_t	us_Length_Hi;
		uint8_t	us_Length_Lo;
		uint8_t	uc_rsv3[3];
	}
	s_usb_ptn4569;
}
USBC_PMSC_CDB_t;

/******************************************************************************
Bit Order Definition default
******************************************************************************/
#pragma bit_order


/*****************************************************************************
Enum definitions
******************************************************************************/
enum usbc_gpmsc_AtapiResult
{
	USBC_ATAPI_SUCCESS							= 0x11,
	/* Command receive events */
	USBC_ATAPI_NO_DATA							= 0x21,
	USBC_ATAPI_A_SND_DATA						= 0x22,
	USBC_ATAPI_A_RCV_DATA						= 0x23,
	USBC_ATAPI_SND_DATAS						= 0x24,
	USBC_ATAPI_RCV_DATAS						= 0x25,
	USBC_ATAPI_COMMAND_ANALYZE_ERROR			= 0x26,
	/* Complete events */
	USBC_ATAPI_CMD_CONTINUE						= 0x31,
	USBC_ATAPI_CMD_COMPLETE						= 0x32,
	USBC_ATAPI_CMD_FAILED						= 0x33,
	/* ATAPI Start events */
	USBC_ATAPI_READY							= 0x41,
	// respond error
	USBC_ATAPI_ERROR							= 0x51,
	/*** ERR CODE ***/
	USBC_ATAPI_ERR_CODE_SEPARATER				= 0x100,
	USBC_ATAPI_ERR_INVAL						= 0x61
};


#endif	/* __R_USBC_CATAPIDEFINE_H__ */
/******************************************************************************
End  Of File
******************************************************************************/
